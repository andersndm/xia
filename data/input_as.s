	.intel_syntax noprefix
	.text
	.global 	_start
	.p2align	4, 0x90
_start:
	mov rbp, rsp
	call main
	mov rbx, rax
	jmp _exit

	.global 	exit
	.p2align 	4, 0x90
exit:
	mov rdi, rax
_exit:
	mov rax, 60
	syscall

	.global main
	.p2align	4, 0x90
main:	
	push rbp
	mov rbp, rsp
	sub rsp, 16

	mov rcx, 1
	mov dword ptr [rbp - 0x4], 0x00000A21
	mov dword ptr [rbp - 0x8], 0x646C726F
	mov dword ptr [rbp - 0xC], 0x77202C6F
	mov dword ptr [rbp - 0x10], 0x6C6C6548

	mov rax, 1
	mov rdi, 1
	lea rsi, [rip + string]
	mov rdx, 14
	syscall
	cmp rax, 0
	jge .main_exit
	add rsp, 16

	pop rbp
	mov rax, 0
	ret

.main_exit:
	mov rbx, 1
	call exit

	.section .data
global_int:
	.int 0xDEADBEEF

	.section .rodata
string:	
	.asciz "Hello, world!\n"

	.section .bss
gint:	
	.long 0
