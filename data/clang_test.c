#include "clang_include_test.c"

#include <string.h>
#include <unistd.h>

unsigned long global_int = 0;
const unsigned long global_const = 5;

typedef struct data
{
	int a;
	unsigned long b;
} data;

void test_fn(data *d, const unsigned long q)
{
	if (q > d->b)
	{
		d->a = 10;
	}
}

int main(void)
{
	for (int i = 0; i < 10; ++i)
	{
		global_int += i;
	}

	data d = { 0 };
	test_fn(&d, global_int);

	const char *str = get_str();
	write(1, str, strlen(str));
	test_fn(&d, global_const);
	return 0;
}
