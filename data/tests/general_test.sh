#!/bin/bash

python gen_test.py

printf "\x1b[32mtest8\x1b[0m\n"
../xia -o xia_test8.bin test8.s
nasm -f bin test8.s -o nasm_test8.bin

radiff2 nasm_test8.bin xia_test8.bin

printf "\x1b[32mtest16\x1b[0m\n"
../xia -o xia_test16.bin test16.s
nasm -f bin test16.s -o nasm_test16.bin

radiff2 nasm_test16.bin xia_test16.bin

printf "\x1b[32mtest32\x1b[0m\n"
../xia -o xia_test32.bin test32.s
nasm -f bin test32.s -o nasm_test32.bin

radiff2 nasm_test32.bin xia_test32.bin

printf "\x1b[32mtest64\x1b[0m\n"
../xia -o xia_test64.bin test64.s
nasm -f bin test64.s -o nasm_test64.bin

radiff2 nasm_test64.bin xia_test64.bin
