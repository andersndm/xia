#      0      1      2      3      4      5      6      7      8      9      10      11      12      13      14      15
r8 =  ["al",  "cl",  "dl",  "bl",  "ah",  "ch",  "dh",  "bh",  "r8b", "r9b", "r10b", "r11b", "r12b", "r13b", "r14b", "r15b"]
r16 = ["ax",  "cx",  "dx",  "bx",  "sp",  "bp",  "si",  "di",  "r8w", "r9w", "r10w", "r11w", "r12w", "r13w", "r14w", "r15w"]
r32 = ["eax", "ecx", "edx", "ebx", "esp", "ebp", "esi", "edi", "r8d", "r9d", "r10d", "r11d", "r12d", "r13d", "r14d", "r15d"]
r64 = ["rax", "rcx", "rdx", "rbx", "rsp", "rbp", "rsi", "rdi", "r8",  "r9",  "r10",  "r11",  "r12",  "r13",  "r14",  "r15"]
log2_scale = ["1", "2", "4", "8"]
log2_scale = ["4"]

jcc = ["jo", "jno", "jb", "jnb", "je", "jne", "jna", "ja", "js", "jns", "jp", "jnp", "jl", "jnl", "jng", "jg", "jnae", "jc", "jae", "jnc", "jz", "jnz", "jbe", "jnbe", "jpe", "jpo", "jnge", "jge", "jle", "jnle"]
cmovcc = ["cmovo", "cmovno", "cmovb", "cmovnb", "cmove", "cmovne", "cmovna", "cmova", "cmovs", "cmovns", "cmovp", "cmovnp", "cmovl", "cmovnl", "cmovng", "cmovg", "cmovnae", "cmovc", "cmovae", "cmovnc", "cmovz", "cmovnz", "cmovbe", "cmovnbe", "cmovpe", "cmovpo", "cmovnge", "cmovge", "cmovle", "cmovnle"]
setcc = ["seto", "setno", "setb", "setnb", "sete", "setne", "setna", "seta", "sets", "setns", "setp", "setnp", "setl", "setnl", "setng", "setg", "setnae", "setc", "setae", "setnc", "setz", "setnz", "setbe", "setnbe", "setpe", "setpo", "setnge", "setge", "setle", "setnle"]

def valid8_r_r(dest, src):
    # dest in ["ah", "ch", "dh", "bh"] and src in ["r8" -> "r15"]
    if dest >= 4 and dest < 8 and src >= 8:
        return False
    if src >= 4 and src < 8 and dest >= 8:
        return False
    return True

def valid8_r_sib(dest, base, index, scale):
    if index == 4:
        return False
    elif index == 4 and scale != "1":
        return False
    elif dest >= 4 and dest < 8 and index >= 8:
        return False
    elif dest >= 4 and dest < 8 and base >= 8:
        return False
    else:
        return True

def validX_r_sib(dest, base, index, scale):
    if index == 4:
        return False
    elif index == 4 and scale != "1":
        return False
    else:
        return True

def gen8_r_r(filehandle, op):
    result = ""
    for dest in range(len(r8)):
        for src in range(len(r8)):
            if valid8_r_r(dest, src):
                result += "\t" + op + " " + r8[dest] + ", " + r8[src] + "\n"
    filehandle.write(result)

def gen16_r_r(filehandle, op):
    result = ""
    for dest in range(len(r16)):
        for src in range(len(r16)):
            result += "\t" + op + " " + r16[dest] + ", " + r16[src] + "\n"
    filehandle.write(result)

def gen32_r_r(filehandle, op):
    result = ""
    for dest in range(len(r32)):
        for src in range(len(r32)):
            result += "\t" + op + " " + r32[dest] + ", " + r32[src] + "\n"
    filehandle.write(result)

def gen64_r_r(filehandle, op):
    result = ""
    for dest in range(len(r64)):
        for src in range(len(r64)):
            result += "\t" + op + " " + r64[dest] + ", " + r64[src] + "\n"
    filehandle.write(result)

def gen_r_r(filehandle, op):
    gen8_r_r(filehandle, op)
    gen16_r_r(filehandle, op)
    gen32_r_r(filehandle, op)
    gen64_r_r(filehandle, op)

def gen8_r_ib(filehandle, op):
    result = ""
    for src in range(len(r8)):
        result += "\t" + op + " " + r8[src] + ", 0x12\n"
    filehandle.write(result)

def gen8_r_ix(filehandle, op):
    gen8_r_ib(filehandle, op)

def gen16_r_ib(filehandle, op):
    result = ""
    for src in range(len(r16)):
        result += "\t" + op + " " + r16[src] + ", 0x12\n"
    filehandle.write(result)

def gen16_r_iw(filehandle, op):
    result = ""
    for src in range(len(r16)):
        result += "\t" + op + " " + r16[src] + ", 0x1234\n"
    filehandle.write(result)

def gen16_r_ix(filehandle, op):
    gen16_r_ib(filehandle, op)
    gen16_r_iw(filehandle, op)

def gen32_r_ib(filehandle, op):
    result = ""
    for src in range(len(r32)):
        result += "\t" + op + " " + r32[src] + ", 0x12\n"
    filehandle.write(result)

def gen32_r_id(filehandle, op):
    result = ""
    for src in range(len(r32)):
        result += "\t" + op + " " + r32[src] + ", 0x12345678\n"
    filehandle.write(result)

def gen32_r_ix(filehandle, op):
    gen32_r_ib(filehandle, op)
    gen32_r_id(filehandle, op)

def gen64_r_ib(filehandle, op):
    result = ""
    for src in range(len(r64)):
        result += "\t" + op + " " + r64[src] + ", 0x12\n"
    filehandle.write(result)

def gen64_r_id(filehandle, op):
    result = ""
    for src in range(len(r64)):
        result += "\t" + op + " " + r64[src] + ", 0x12345678\n"
    filehandle.write(result)

def gen64_r_iq(filehandle, op):
    result = ""
    for src in range(len(r64)):
        result += "\t" + op + " " + r64[src] + ", 0x123456789ABCDEF0\n"
    filehandle.write(result)

def gen64_r_ix(filehandle, op):
    gen64_r_ib(filehandle, op)
    gen64_r_id(filehandle, op)

def gen_r_ix(filehandle, op):
    gen8_r_ix(filehandle, op)
    gen16_r_ix(filehandle, op)
    gen32_r_ix(filehandle, op)

def gen8_r_m(filehandle, op):
    result = ""
    for dest in range(len(r8)):
        for src in range(len(r8)):
            if valid8_r_r(dest, src):
                result += "\t" + op + " " + r8[dest] + ", byte [" + r32[src] + "]\n"
                result += "\t" + op + " " + r8[dest] + ", byte [" + r64[src] + "]\n"
    filehandle.write(result)

def gen16_r_m(filehandle, op):
    result = ""
    for dest in range(len(r16)):
        for src in range(len(r16)):
            result += "\t" + op + " " + r16[dest] + ", word [" + r32[src] + "]\n"
            result += "\t" + op + " " + r16[dest] + ", word [" + r64[src] + "]\n"
    filehandle.write(result)

def gen32_r_m(filehandle, op):
    result = ""
    for dest in range(len(r32)):
        for src in range(len(r32)):
            result += "\t" + op + " " + r32[dest] + ", dword [" + r32[src] + "]\n"
            result += "\t" + op + " " + r32[dest] + ", dword [" + r64[src] + "]\n"
    filehandle.write(result)

def gen64_r_m(filehandle, op):
    result = ""
    for dest in range(len(r64)):
        for src in range(len(r64)):
            result += "\t" + op + " " + r64[dest] + ", qword [" + r32[src] + "]\n"
            result += "\t" + op + " " + r64[dest] + ", qword [" + r64[src] + "]\n"
    filehandle.write(result)

def gen_r_m(filehandle, op):
    gen8_r_m(filehandle, op)
    gen16_r_m(filehandle, op)
    gen32_r_m(filehandle, op)
    gen64_r_m(filehandle, op)

def gen8_r_mdb(filehandle, op):
    result = ""
    for dest in range(len(r8)):
        for src in range(len(r8)):
            if valid8_r_r(dest, src):
                result += "\t" + op + " " + r8[dest] + ", byte [" + r32[src] + " + 0x12" + "]\n"
                result += "\t" + op + " " + r8[dest] + ", byte [" + r64[src] + " + 0x12" + "]\n"
    filehandle.write(result)

def gen16_r_mdb(filehandle, op):
    result = ""
    for dest in range(len(r16)):
        for src in range(len(r16)):
            result += "\t" + op + " " + r16[dest] + ", word [" + r32[src] + " + 0x12" + "]\n"
            result += "\t" + op + " " + r16[dest] + ", word [" + r64[src] + " + 0x12" + "]\n"
    filehandle.write(result)
    
def gen32_r_mdb(filehandle, op):
    result = ""
    for dest in range(len(r32)):
        for src in range(len(r32)):
            result += "\t" + op + " " + r32[dest] + ", dword [" + r32[src] + " + 0x12" + "]\n"
            result += "\t" + op + " " + r32[dest] + ", dword [" + r64[src] + " + 0x12" + "]\n"
    filehandle.write(result)

def gen64_r_mdb(filehandle, op):
    result = ""
    for dest in range(len(r64)):
        for src in range(len(r64)):
            result += "\t" + op + " " + r64[dest] + ", qword [" + r32[src] + " + 0x12" + "]\n"
            result += "\t" + op + " " + r64[dest] + ", qword [" + r64[src] + " + 0x12" + "]\n"
    filehandle.write(result)

def gen_r_mdb(filehandle, op):
    gen8_r_mdb(filehandle, op)
    gen16_r_mdb(filehandle, op)
    gen32_r_mdb(filehandle, op)
    gen64_r_mdb(filehandle, op)

def gen8_r_mdd(filehandle, op):
    result = ""
    for dest in range(len(r8)):
        for src in range(len(r8)):
            if valid8_r_r(dest, src):
                result += "\t" + op + " " + r8[dest] + ", byte [" + r32[src] + " + 0x1234" + "]\n"
                result += "\t" + op + " " + r8[dest] + ", byte [" + r64[src] + " + 0x1234" + "]\n"
    filehandle.write(result)

def gen16_r_mdd(filehandle, op):
    result = ""
    for dest in range(len(r16)):
        for src in range(len(r16)):
            result += "\t" + op + " " + r16[dest] + ", word [" + r32[src] + " + 0x1234" + "]\n"
            result += "\t" + op + " " + r16[dest] + ", word [" + r64[src] + " + 0x1234" + "]\n"
    filehandle.write(result)
    
def gen32_r_mdd(filehandle, op):
    result = ""
    for dest in range(len(r32)):
        for src in range(len(r32)):
            result += "\t" + op + " " + r32[dest] + ", dword [" + r32[src] + " + 0x1234" + "]\n"
            result += "\t" + op + " " + r32[dest] + ", dword [" + r64[src] + " + 0x1234" + "]\n"
    filehandle.write(result)

def gen64_r_mdd(filehandle, op):
    result = ""
    for dest in range(len(r64)):
        for src in range(len(r64)):
            result += "\t" + op + " " + r64[dest] + ", qword [" + r32[src] + " + 0x1234" + "]\n"
            result += "\t" + op + " " + r64[dest] + ", qword [" + r64[src] + " + 0x1234" + "]\n"
    filehandle.write(result)

def gen_r_mdd(filehandle, op):
    gen8_r_mdd(filehandle, op)
    gen16_r_mdd(filehandle, op)
    gen32_r_mdd(filehandle, op)
    gen64_r_mdd(filehandle, op)

def gen8_r_sib(filehandle, op):
    result = ""
    for dest in range(len(r8)):
        for base in range(len(r8)):
            for index in range(len(r8)):
                for scale in log2_scale:
                    if valid8_r_sib(dest, base, index, scale):
                        result += "\t" + op + " " + r8[dest] + ", byte [" + r32[base] + " + " + scale + " * " + r32[index] + "]\n"
                        result += "\t" + op + " " + r8[dest] + ", byte [" + r64[base] + " + " + scale + " * " + r64[index] + "]\n"
    filehandle.write(result)

def gen16_r_sib(filehandle, op):
    result = ""
    for dest in range(len(r16)):
        for base in range(len(r16)):
            for index in range(len(r16)):
                for scale in log2_scale:
                    if validX_r_sib(dest, base, index, scale):
                        result += "\t" + op + " " + r16[dest] + ", word [" + r32[base] + " + " + scale + " * " + r32[index] + "]\n"
                        result += "\t" + op + " " + r16[dest] + ", word [" + r64[base] + " + " + scale + " * " + r64[index] + "]\n"
    filehandle.write(result)
    
def gen32_r_sib(filehandle, op):
    result = ""
    for dest in range(len(r32)):
        for base in range(len(r32)):
            for index in range(len(r32)):
                for scale in log2_scale:
                    if validX_r_sib(dest, base, index, scale):
                        result += "\t" + op + " " + r32[dest] + ", dword [" + r32[base] + " + " + scale + " * " + r32[index] + "]\n"
                        result += "\t" + op + " " + r32[dest] + ", dword [" + r64[base] + " + " + scale + " * " + r64[index] + "]\n"
    filehandle.write(result)

def gen64_r_sib(filehandle, op):
    result = ""
    for dest in range(len(r64)):
        for base in range(len(r64)):
            for index in range(len(r64)):
                for scale in log2_scale:
                    if validX_r_sib(dest, base, index, scale):
                        result += "\t" + op + " " + r64[dest] + ", qword [" + r32[base] + " + " + scale + " * " + r32[index] + "]\n"
                        result += "\t" + op + " " + r64[dest] + ", qword [" + r64[base] + " + " + scale + " * " + r64[index] + "]\n"
    filehandle.write(result)

def gen_r_sib(filehandle, op):
    gen8_r_sib(filehandle, op)
    gen16_r_sib(filehandle, op)
    gen32_r_sib(filehandle, op)
    gen64_r_sib(filehandle, op)

def gen8_r_sibdb(filehandle, op):
    result = ""
    for dest in range(len(r8)):
        for base in range(len(r8)):
            for index in range(len(r8)):
                for scale in log2_scale:
                    if valid8_r_sib(dest, base, index, scale):
                        result += "\t" + op + " " + r8[dest] + ", byte [" + r32[base] + " + " + scale + " * " + r32[index] + " + 0x12" + "]\n"
                        result += "\t" + op + " " + r8[dest] + ", byte [" + r64[base] + " + " + scale + " * " + r64[index] + " + 0x12" + "]\n"
    filehandle.write(result)

def gen16_r_sibdb(filehandle, op):
    result = ""
    for dest in range(len(r16)):
        for base in range(len(r16)):
            for index in range(len(r16)):
                for scale in log2_scale:
                    if validX_r_sib(dest, base, index, scale):
                        result += "\t" + op + " " + r16[dest] + ", word [" + r32[base] + " + " + scale + " * " + r32[index] + " + 0x12" + "]\n"
                        result += "\t" + op + " " + r16[dest] + ", word [" + r64[base] + " + " + scale + " * " + r64[index] + " + 0x12" + "]\n"
    filehandle.write(result)
    
def gen32_r_sibdb(filehandle, op):
    result = ""
    for dest in range(len(r32)):
        for base in range(len(r32)):
            for index in range(len(r32)):
                for scale in log2_scale:
                    if validX_r_sib(dest, base, index, scale):
                        result += "\t" + op + " " + r32[dest] + ", dword [" + r32[base] + " + " + scale + " * " + r32[index] + " + 0x12" + "]\n"
                        result += "\t" + op + " " + r32[dest] + ", dword [" + r64[base] + " + " + scale + " * " + r64[index] + " + 0x12" + "]\n"
    filehandle.write(result)

def gen64_r_sibdb(filehandle, op):
    result = ""
    for dest in range(len(r64)):
        for base in range(len(r64)):
            for index in range(len(r64)):
                for scale in log2_scale:
                    if validX_r_sib(dest, base, index, scale):
                        result += "\t" + op + " " + r64[dest] + ", qword [" + r32[base] + " + " + scale + " * " + r32[index] + " + 0x12" + "]\n"
                        result += "\t" + op + " " + r64[dest] + ", qword [" + r64[base] + " + " + scale + " * " + r64[index] + " + 0x12" + "]\n"
    filehandle.write(result)

def gen_r_sibdb(filehandle, op):
    gen8_r_sibdb(filehandle, op)
    gen16_r_sibdb(filehandle, op)
    gen32_r_sibdb(filehandle, op)
    gen64_r_sibdb(filehandle, op)

def gen8_r_sibdd(filehandle, op):
    result = ""
    for dest in range(len(r8)):
        for base in range(len(r8)):
            for index in range(len(r8)):
                for scale in log2_scale:
                    if valid8_r_sib(dest, base, index, scale):
                        result += "\t" + op + " " + r8[dest] + ", byte [" + r32[base] + " + " + scale + " * " + r32[index] + " + 0x12345678" + "]\n"
                        result += "\t" + op + " " + r8[dest] + ", byte [" + r64[base] + " + " + scale + " * " + r64[index] + " + 0x12345678" + "]\n"
    filehandle.write(result)

def gen16_r_sibdd(filehandle, op):
    result = ""
    for dest in range(len(r16)):
        for base in range(len(r16)):
            for index in range(len(r16)):
                for scale in log2_scale:
                    if validX_r_sib(dest, base, index, scale):
                        result += "\t" + op + " " + r16[dest] + ", word [" + r32[base] + " + " + scale + " * " + r32[index] + " + 0x12345678" + "]\n"
                        result += "\t" + op + " " + r16[dest] + ", word [" + r64[base] + " + " + scale + " * " + r64[index] + " + 0x12345678" + "]\n"
    filehandle.write(result)
    
def gen32_r_sibdd(filehandle, op):
    result = ""
    for dest in range(len(r32)):
        for base in range(len(r32)):
            for index in range(len(r32)):
                for scale in log2_scale:
                    if validX_r_sib(dest, base, index, scale):
                        result += "\t" + op + " " + r32[dest] + ", dword [" + r32[base] + " + " + scale + " * " + r32[index] + " + 0x12345678" + "]\n"
                        result += "\t" + op + " " + r32[dest] + ", dword [" + r64[base] + " + " + scale + " * " + r64[index] + " + 0x12345678" + "]\n"
    filehandle.write(result)

def gen64_r_sibdd(filehandle, op):
    result = ""
    for dest in range(len(r64)):
        for base in range(len(r64)):
            for index in range(len(r64)):
                for scale in log2_scale:
                    if validX_r_sib(dest, base, index, scale):
                        result += "\t" + op + " " + r64[dest] + ", qword [" + r32[base] + " + " + scale + " * " + r32[index] + " + 0x12345678" + "]\n"
                        result += "\t" + op + " " + r64[dest] + ", qword [" + r64[base] + " + " + scale + " * " + r64[index] + " + 0x12345678" + "]\n"
    filehandle.write(result)

def gen_r_sibdd(filehandle, op):
    gen8_r_sibdd(filehandle, op)
    gen16_r_sibdd(filehandle, op)
    gen32_r_sibdd(filehandle, op)
    gen64_r_sibdd(filehandle, op)

def gen8_r_disp(filehandle, op):
    result = ""
    for dest in range(len(r8)):
        result += "\t" + op + " " + r8[dest] + ", byte [0x12345678]\n"
    filehandle.write(result)

def gen16_r_disp(filehandle, op):
    result = ""
    for dest in range(len(r8)):
        result += "\t" + op + " " + r16[dest] + ", word [0x12345678]\n"
    filehandle.write(result)
    
def gen32_r_disp(filehandle, op):
    result = ""
    for dest in range(len(r8)):
        result += "\t" + op + " " + r32[dest] + ", dword [0x12345678]\n"
    filehandle.write(result)
    
def gen64_r_disp(filehandle, op):
    result = ""
    for dest in range(len(r8)):
        result += "\t" + op + " " + r64[dest] + ", qword [0x12345678]\n"
    filehandle.write(result)

def gen_r_disp(filehandle, op):
    gen8_r_disp(filehandle, op)
    gen16_r_disp(filehandle, op)
    gen32_r_disp(filehandle, op)
    gen64_r_disp(filehandle, op)

def gen8_r_rel(filehandle, op):
    result = ""
    for dest in range(len(r8)):
        result += "\t" + op + " " + r8[dest] + ", byte [rel $ + 0x12345678]\n"
    filehandle.write(result)

def gen16_r_rel(filehandle, op):
    result = ""
    for dest in range(len(r16)):
        result += "\t" + op + " " + r16[dest] + ", word [rel $ + 0x12345678]\n"
    filehandle.write(result)

def gen32_r_rel(filehandle, op):
    result = ""
    for dest in range(len(r32)):
        result += "\t" + op + " " + r32[dest] + ", dword [rel $ + 0x12345678]\n"
    filehandle.write(result)

def gen64_r_rel(filehandle, op):
    result = ""
    for dest in range(len(r64)):
        result += "\t" + op + " " + r64[dest] + ", qword [rel $ + 0x12345678]\n"
    filehandle.write(result)

def gen_r_rel(filehandle, op):
    gen8_r_rel(filehandle, op)
    gen16_r_rel(filehandle, op)
    gen32_r_rel(filehandle, op)
    gen64_r_rel(filehandle, op)

def gen8_m_r(filehandle, op):
    result = ""
    for dest in range(len(r8)):
        for src in range(len(r8)):
            if valid8_r_r(dest, src):
                result += "\t" + op + " byte [" + r32[dest] + "], " + r8[src] + "\n"
                result += "\t" + op + " byte [" + r64[dest] + "], " + r8[src] + "\n"
    filehandle.write(result)

def gen16_m_r(filehandle, op):
    result = ""
    for dest in range(len(r16)):
        for src in range(len(r16)):
            result += "\t" + op + " word [" + r32[dest] + "], " + r16[src] + "\n"
            result += "\t" + op + " word [" + r64[dest] + "], " + r16[src] + "\n"
    filehandle.write(result)

def gen32_m_r(filehandle, op):
    result = ""
    for dest in range(len(r32)):
        for src in range(len(r32)):
            result += "\t" + op + " dword [" + r32[dest] + "], " + r32[src] + "\n"
            result += "\t" + op + " dword [" + r64[dest] + "], " + r32[src] + "\n"
    filehandle.write(result)

def gen64_m_r(filehandle, op):
    result = ""
    for dest in range(len(r64)):
        for src in range(len(r64)):
            result += "\t" + op + " qword [" + r32[dest] + "], " + r64[src] + "\n"
            result += "\t" + op + " qword [" + r64[dest] + "], " + r64[src] + "\n"
    filehandle.write(result)

def gen_m_r(filehandle, op):
    gen8_m_r(filehandle, op)
    gen16_m_r(filehandle, op)
    gen32_m_r(filehandle, op)
    gen64_m_r(filehandle, op)

def gen8_mdb_r(filehandle, op):
    result = ""
    for dest in range(len(r8)):
        for src in range(len(r8)):
            if valid8_r_r(dest, src):
                result += "\t" + op + " byte [" + r32[dest] + " + 0x12], " + r8[src] + "\n"
                result += "\t" + op + " byte [" + r64[dest] + " + 0x12], " + r8[src] + "\n"
    filehandle.write(result)

def gen16_mdb_r(filehandle, op):
    result = ""
    for dest in range(len(r16)):
        for src in range(len(r16)):
            result += "\t" + op + " word [" + r32[dest] + " + 0x12], " + r16[src] + "\n"
            result += "\t" + op + " word [" + r64[dest] + " + 0x12], " + r16[src] + "\n"
    filehandle.write(result)

def gen32_mdb_r(filehandle, op):
    result = ""
    for dest in range(len(r32)):
        for src in range(len(r32)):
            result += "\t" + op + " dword [" + r32[dest] + " + 0x12], " + r32[src] + "\n"
            result += "\t" + op + " dword [" + r64[dest] + " + 0x12], " + r32[src] + "\n"
    filehandle.write(result)

def gen64_mdb_r(filehandle, op):
    result = ""
    for dest in range(len(r64)):
        for src in range(len(r64)):
            result += "\t" + op + " qword [" + r32[dest] + " + 0x12], " + r64[src] + "\n"
            result += "\t" + op + " qword [" + r64[dest] + " + 0x12], " + r64[src] + "\n"
    filehandle.write(result)

def gen_mdb_r(filehandle, op):
    gen8_mdb_r(filehandle, op)
    gen16_mdb_r(filehandle, op)
    gen32_mdb_r(filehandle, op)
    gen64_mdb_r(filehandle, op)

def gen8_mdd_r(filehandle, op):
    result = ""
    for dest in range(len(r8)):
        for src in range(len(r8)):
            if valid8_r_r(dest, src):
                result += "\t" + op + " byte [" + r32[dest] + " + 0x12345678], " + r8[src] + "\n"
                result += "\t" + op + " byte [" + r64[dest] + " + 0x12345678], " + r8[src] + "\n"
    filehandle.write(result)

def gen16_mdd_r(filehandle, op):
    result = ""
    for dest in range(len(r16)):
        for src in range(len(r16)):
            result += "\t" + op + " word [" + r32[dest] + " + 0x12345678], " + r16[src] + "\n"
            result += "\t" + op + " word [" + r64[dest] + " + 0x12345678], " + r16[src] + "\n"
    filehandle.write(result)

def gen32_mdd_r(filehandle, op):
    result = ""
    for dest in range(len(r32)):
        for src in range(len(r32)):
            result += "\t" + op + " dword [" + r32[dest] + " + 0x12345678], " + r32[src] + "\n"
            result += "\t" + op + " dword [" + r64[dest] + " + 0x12345678], " + r32[src] + "\n"
    filehandle.write(result)

def gen64_mdd_r(filehandle, op):
    result = ""
    for dest in range(len(r64)):
        for src in range(len(r64)):
            result += "\t" + op + " qword [" + r32[dest] + " + 0x12345678], " + r64[src] + "\n"
            result += "\t" + op + " qword [" + r64[dest] + " + 0x12345678], " + r64[src] + "\n"
    filehandle.write(result)

def gen_mdd_r(filehandle, op):
    gen8_mdd_r(filehandle, op)
    gen16_mdd_r(filehandle, op)
    gen32_mdd_r(filehandle, op)
    gen64_mdd_r(filehandle, op)

def gen8_sib_r(filehandle, op):
    result = ""
    for src in range(len(r8)):
        for base in range(len(r8)):
            for index in range(len(r8)):
                for scale in log2_scale:
                    if valid8_r_sib(src, base, index, scale):
                        result += "\t" + op + " byte [" + r32[base] + " + " + scale + "*" + r32[index] + "], " + r8[src] + "\n"
                        result += "\t" + op + " byte [" + r64[base] + " + " + scale + "*" + r64[index] + "], " + r8[src] + "\n"
    filehandle.write(result)

def gen16_sib_r(filehandle, op):
    result = ""
    for src in range(len(r16)):
        for base in range(len(r16)):
            for index in range(len(r16)):
                for scale in log2_scale:
                    if validX_r_sib(src, base, index, scale):
                        result += "\t" + op + " word [" + r32[base] + " + " + scale + "*" + r32[index] + "], " + r16[src] + "\n"
                        result += "\t" + op + " word [" + r64[base] + " + " + scale + "*" + r64[index] + "], " + r16[src] + "\n"
    filehandle.write(result)

def gen32_sib_r(filehandle, op):
    result = ""
    for src in range(len(r32)):
        for base in range(len(r32)):
            for index in range(len(r32)):
                for scale in log2_scale:
                    if validX_r_sib(src, base, index, scale):
                        result += "\t" + op + " dword [" + r32[base] + " + " + scale + "*" + r32[index] + "], " + r32[src] + "\n"
                        result += "\t" + op + " dword [" + r64[base] + " + " + scale + "*" + r64[index] + "], " + r32[src] + "\n"
    filehandle.write(result)

def gen64_sib_r(filehandle, op):
    result = ""
    for src in range(len(r64)):
        for base in range(len(r64)):
            for index in range(len(r64)):
                for scale in log2_scale:
                    if validX_r_sib(src, base, index, scale):
                        result += "\t" + op + " qword [" + r32[base] + " + " + scale + "*" + r32[index] + "], " + r64[src] + "\n"
                        result += "\t" + op + " qword [" + r64[base] + " + " + scale + "*" + r64[index] + "], " + r64[src] + "\n"
    filehandle.write(result)

def gen_sib_r(filehandle, op):
    gen8_sib_r(filehandle, op)
    gen16_sib_r(filehandle, op)
    gen32_sib_r(filehandle, op)
    gen64_sib_r(filehandle, op)

def gen8_sibdb_r(filehandle, op):
    result = ""
    for src in range(len(r8)):
        for base in range(len(r8)):
            for index in range(len(r8)):
                for scale in log2_scale:
                    if valid8_r_sib(src, base, index, scale):
                        result += "\t" + op + " byte [" + r32[base] + " + " + scale + "*" + r32[index] + "+ 0x12], " + r8[src] + "\n"
                        result += "\t" + op + " byte [" + r64[base] + " + " + scale + "*" + r64[index] + "+ 0x12], " + r8[src] + "\n"
    filehandle.write(result)

def gen16_sibdb_r(filehandle, op):
    result = ""
    for src in range(len(r16)):
        for base in range(len(r16)):
            for index in range(len(r16)):
                for scale in log2_scale:
                    if validX_r_sib(src, base, index, scale):
                        result += "\t" + op + " word [" + r32[base] + " + " + scale + "*" + r32[index] + "+ 0x12], " + r16[src] + "\n"
                        result += "\t" + op + " word [" + r64[base] + " + " + scale + "*" + r64[index] + "+ 0x12], " + r16[src] + "\n"
    filehandle.write(result)

def gen32_sibdb_r(filehandle, op):
    result = ""
    for src in range(len(r32)):
        for base in range(len(r32)):
            for index in range(len(r32)):
                for scale in log2_scale:
                    if validX_r_sib(src, base, index, scale):
                        result += "\t" + op + " dword [" + r32[base] + " + " + scale + "*" + r32[index] + "+ 0x12], " + r32[src] + "\n"
                        result += "\t" + op + " dword [" + r64[base] + " + " + scale + "*" + r64[index] + "+ 0x12], " + r32[src] + "\n"
    filehandle.write(result)

def gen64_sibdb_r(filehandle, op):
    result = ""
    for src in range(len(r64)):
        for base in range(len(r64)):
            for index in range(len(r64)):
                for scale in log2_scale:
                    if validX_r_sib(src, base, index, scale):
                        result += "\t" + op + " qword [" + r32[base] + " + " + scale + "*" + r32[index] + "+ 0x12], " + r64[src] + "\n"
                        result += "\t" + op + " qword [" + r64[base] + " + " + scale + "*" + r64[index] + "+ 0x12], " + r64[src] + "\n"
    filehandle.write(result)

def gen_sibdb_r(filehandle, op):
    gen8_sibdb_r(filehandle, op)
    gen16_sibdb_r(filehandle, op)
    gen32_sibdb_r(filehandle, op)
    gen64_sibdb_r(filehandle, op)

def gen8_sibdd_r(filehandle, op):
    result = ""
    for src in range(len(r8)):
        for base in range(len(r8)):
            for index in range(len(r8)):
                for scale in log2_scale:
                    if valid8_r_sib(src, base, index, scale):
                        result += "\t" + op + " byte [" + r32[base] + " + " + scale + "*" + r32[index] + "+ 0x12345678], " + r8[src] + "\n"
                        result += "\t" + op + " byte [" + r64[base] + " + " + scale + "*" + r64[index] + "+ 0x12345678], " + r8[src] + "\n"
    filehandle.write(result)

def gen16_sibdd_r(filehandle, op):
    result = ""
    for src in range(len(r16)):
        for base in range(len(r16)):
            for index in range(len(r16)):
                for scale in log2_scale:
                    if validX_r_sib(src, base, index, scale):
                        result += "\t" + op + " word [" + r32[base] + " + " + scale + "*" + r32[index] + "+ 0x12345678], " + r16[src] + "\n"
                        result += "\t" + op + " word [" + r64[base] + " + " + scale + "*" + r64[index] + "+ 0x12345678], " + r16[src] + "\n"
    filehandle.write(result)

def gen32_sibdd_r(filehandle, op):
    result = ""
    for src in range(len(r32)):
        for base in range(len(r32)):
            for index in range(len(r32)):
                for scale in log2_scale:
                    if validX_r_sib(src, base, index, scale):
                        result += "\t" + op + " dword [" + r32[base] + " + " + scale + "*" + r32[index] + "+ 0x12345678], " + r32[src] + "\n"
                        result += "\t" + op + " dword [" + r64[base] + " + " + scale + "*" + r64[index] + "+ 0x12345678], " + r32[src] + "\n"
    filehandle.write(result)

def gen64_sibdd_r(filehandle, op):
    result = ""
    for src in range(len(r64)):
        for base in range(len(r64)):
            for index in range(len(r64)):
                for scale in log2_scale:
                    if validX_r_sib(src, base, index, scale):
                        result += "\t" + op + " qword [" + r32[base] + " + " + scale + "*" + r32[index] + "+ 0x12345678], " + r64[src] + "\n"
                        result += "\t" + op + " qword [" + r64[base] + " + " + scale + "*" + r64[index] + "+ 0x12345678], " + r64[src] + "\n"
    filehandle.write(result)

def gen_sibdd_r(filehandle, op):
    gen8_sibdd_r(filehandle, op)
    gen16_sibdd_r(filehandle, op)
    gen32_sibdd_r(filehandle, op)
    gen64_sibdd_r(filehandle, op)

def gen8_disp_r(filehandle, op):
    result = ""
    for src in range(len(r8)):
        result += "\t" + op + " byte [0x12345678], " + r8[src] + "\n"
    filehandle.write(result)

def gen16_disp_r(filehandle, op):
    result = ""
    for src in range(len(r8)):
        result += "\t" + op + "  word [0x12345678], " + r16[src] + "\n"
    filehandle.write(result)

def gen32_disp_r(filehandle, op):
    result = ""
    for src in range(len(r8)):
        result += "\t" + op + "  dword [0x12345678], " + r32[src] + "\n"
    filehandle.write(result)

def gen64_disp_r(filehandle, op):
    result = ""
    for src in range(len(r8)):
        result += "\t" + op + "  qword [0x12345678], " + r64[src] + "\n"
    filehandle.write(result)

def gen_disp_r(filehandle, op):
    gen8_disp_r(filehandle, op)
    gen16_disp_r(filehandle, op)
    gen32_disp_r(filehandle, op)
    gen64_disp_r(filehandle, op)

def gen8_rel_r(filehandle, op):
    result = ""
    for src in range(len(r8)):
        result += "\t" + op + " byte [rel $ - 0x12345678], " + r8[src] + "\n"
    filehandle.write(result)

def gen16_rel_r(filehandle, op):
    result = ""
    for src in range(len(r8)):
        result += "\t" + op + "  word [rel $ - 0x12345678], " + r16[src] + "\n"
    filehandle.write(result)

def gen32_rel_r(filehandle, op):
    result = ""
    for src in range(len(r8)):
        result += "\t" + op + "  dword [rel $ - 0x12345678], " + r32[src] + "\n"
    filehandle.write(result)

def gen64_rel_r(filehandle, op):
    result = ""
    for src in range(len(r8)):
        result += "\t" + op + "  qword [rel $ - 0x12345678], " + r64[src] + "\n"
    filehandle.write(result)

def gen_rel_r(filehandle, op):
    gen8_rel_r(filehandle, op)
    gen16_rel_r(filehandle, op)
    gen32_rel_r(filehandle, op)
    gen64_rel_r(filehandle, op)

def gen8_r(filehandle, op):
    result = ""
    for src in range(len(r8)):
        result += "\t" + op + " " + r8[src] + "\n"
    filehandle.write(result)

def gen16_r(filehandle, op):
    result = ""
    for src in range(len(r16)):
        result += "\t" + op + " " + r16[src] + "\n"
    filehandle.write(result)

def gen32_r(filehandle, op):
    result = ""
    for src in range(len(r32)):
        result += "\t" + op + " " + r32[src] + "\n"
    filehandle.write(result)

def gen64_r(filehandle, op):
    result = ""
    for src in range(len(r64)):
        result += "\t" + op + " " + r64[src] + "\n"
    filehandle.write(result)

def gen_r(filehandle, op):
    gen8_r(filehandle, op)
    gen16_r(filehandle, op)
    gen32_r(filehandle, op)
    gen64_r(filehandle, op)

def gen8_m(filehandle, op):
    result = ""
    for base in range(len(r8)):
        result += "\t" + op + " byte [" + r32[base] + "]\n"
        result += "\t" + op + " byte [" + r64[base] + "]\n"
    filehandle.write(result)

def gen16_m(filehandle, op):
    result = ""
    for base in range(len(r16)):
        result += "\t" + op + " word [" + r32[base] + "]\n"
        result += "\t" + op + " word [" + r64[base] + "]\n"
    filehandle.write(result)

def gen32_m(filehandle, op):
    result = ""
    for base in range(len(r32)):
        result += "\t" + op + " dword [" + r32[base] + "]\n"
        result += "\t" + op + " dword [" + r64[base] + "]\n"
    filehandle.write(result)

def gen64_m(filehandle, op):
    result = ""
    for base in range(len(r64)):
        result += "\t" + op + " qword [" + r32[base] + "]\n"
        result += "\t" + op + " qword [" + r64[base] + "]\n"
    filehandle.write(result)

def gen_m(filehandle, op):
    gen8_m(filehandle, op)
    gen16_m(filehandle, op)
    gen32_m(filehandle, op)
    gen64_m(filehandle, op)

def gen8_mdb(filehandle, op):
    result = ""
    for base in range(len(r8)):
        result += "\t" + op + " byte [" + r32[base] + " + 0x12]\n"
        result += "\t" + op + " byte [" + r64[base] + " + 0x12]\n"
    filehandle.write(result)

def gen16_mdb(filehandle, op):
    result = ""
    for base in range(len(r16)):
        result += "\t" + op + " word [" + r32[base] + " + 0x12]\n"
        result += "\t" + op + " word [" + r64[base] + " + 0x12]\n"
    filehandle.write(result)

def gen32_mdb(filehandle, op):
    result = ""
    for base in range(len(r32)):
        result += "\t" + op + " dword [" + r32[base] + " + 0x12]\n"
        result += "\t" + op + " dword [" + r64[base] + " + 0x12]\n"
    filehandle.write(result)

def gen64_mdb(filehandle, op):
    result = ""
    for base in range(len(r64)):
        result += "\t" + op + " qword [" + r32[base] + " + 0x12]\n"
        result += "\t" + op + " qword [" + r64[base] + " + 0x12]\n"
    filehandle.write(result)

def gen_mdb(filehandle, op):
    gen8_mdb(filehandle, op)
    gen16_mdb(filehandle, op)
    gen32_mdb(filehandle, op)
    gen64_mdb(filehandle, op)

def gen8_mdd(filehandle, op):
    result = ""
    for base in range(len(r8)):
        result += "\t" + op + " byte [" + r32[base] + " + 0x12]\n"
        result += "\t" + op + " byte [" + r64[base] + " + 0x12]\n"
    filehandle.write(result)

def gen16_mdd(filehandle, op):
    result = ""
    for base in range(len(r16)):
        result += "\t" + op + " word [" + r32[base] + " + 0x12]\n"
        result += "\t" + op + " word [" + r64[base] + " + 0x12]\n"
    filehandle.write(result)

def gen32_mdd(filehandle, op):
    result = ""
    for base in range(len(r32)):
        result += "\t" + op + " dword [" + r32[base] + " + 0x12]\n"
        result += "\t" + op + " dword [" + r64[base] + " + 0x12]\n"
    filehandle.write(result)

def gen64_mdd(filehandle, op):
    result = ""
    for base in range(len(r64)):
        result += "\t" + op + " qword [" + r32[base] + " + 0x12]\n"
        result += "\t" + op + " qword [" + r64[base] + " + 0x12]\n"
    filehandle.write(result)

def gen_mdd(filehandle, op):
    gen8_mdd(filehandle, op)
    gen16_mdd(filehandle, op)
    gen32_mdd(filehandle, op)
    gen64_mdd(filehandle, op)

def gen8_sib(filehandle, op):
    result = ""
    for base in range(len(r8)):
        for index in range(len(r8)):
            for scale in log2_scale:
                if valid8_r_sib(0, base, index, scale):
                    result += "\t" + op + " byte [" + r32[base] + " + " + scale + " * " + r32[index] + "]\n"
                    result += "\t" + op + " byte [" + r64[base] + " + " + scale + " * " + r64[index] + "]\n"
    filehandle.write(result)

def gen16_sib(filehandle, op):
    result = ""
    for base in range(len(r16)):
        for index in range(len(r16)):
            for scale in log2_scale:
                if valid8_r_sib(0, base, index, scale):
                    result += "\t" + op + " word [" + r32[base] + " + " + scale + " * " + r32[index] + "]\n"
                    result += "\t" + op + " word [" + r64[base] + " + " + scale + " * " + r64[index] + "]\n"
    filehandle.write(result)

def gen32_sib(filehandle, op):
    result = ""
    for base in range(len(r32)):
        for index in range(len(r32)):
            for scale in log2_scale:
                if valid8_r_sib(0, base, index, scale):
                    result += "\t" + op + " dword [" + r32[base] + " + " + scale + " * " + r32[index] + "]\n"
                    result += "\t" + op + " dword [" + r64[base] + " + " + scale + " * " + r64[index] + "]\n"
    filehandle.write(result)

def gen64_sib(filehandle, op):
    result = ""
    for base in range(len(r64)):
        for index in range(len(r64)):
            for scale in log2_scale:
                if valid8_r_sib(0, base, index, scale):
                    result += "\t" + op + " qword [" + r32[base] + " + " + scale + " * " + r32[index] + "]\n"
                    result += "\t" + op + " qword [" + r64[base] + " + " + scale + " * " + r64[index] + "]\n"
    filehandle.write(result)

def gen_sib(filehandle, op):
    gen8_sib(filehandle, op)
    gen16_sib(filehandle, op)
    gen32_sib(filehandle, op)
    gen64_sib(filehandle, op)

def gen8_sibdb(filehandle, op):
    result = ""
    for base in range(len(r8)):
        for index in range(len(r8)):
            for scale in log2_scale:
                if valid8_r_sib(0, base, index, scale):
                    result += "\t" + op + " byte [" + r32[base] + " + " + scale + " * " + r32[index] + " + 0x12]\n"
                    result += "\t" + op + " byte [" + r64[base] + " + " + scale + " * " + r64[index] + " + 0x12]\n"
    filehandle.write(result)

def gen16_sibdb(filehandle, op):
    result = ""
    for base in range(len(r16)):
        for index in range(len(r16)):
            for scale in log2_scale:
                if valid8_r_sib(0, base, index, scale):
                    result += "\t" + op + " word [" + r32[base] + " + " + scale + " * " + r32[index] + " + 0x12]\n"
                    result += "\t" + op + " word [" + r64[base] + " + " + scale + " * " + r64[index] + " + 0x12]\n"
    filehandle.write(result)

def gen32_sibdb(filehandle, op):
    result = ""
    for base in range(len(r32)):
        for index in range(len(r32)):
            for scale in log2_scale:
                if valid8_r_sib(0, base, index, scale):
                    result += "\t" + op + " dword [" + r32[base] + " + " + scale + " * " + r32[index] + " + 0x12]\n"
                    result += "\t" + op + " dword [" + r64[base] + " + " + scale + " * " + r64[index] + " + 0x12]\n"
    filehandle.write(result)

def gen64_sibdb(filehandle, op):
    result = ""
    for base in range(len(r64)):
        for index in range(len(r64)):
            for scale in log2_scale:
                if valid8_r_sib(0, base, index, scale):
                    result += "\t" + op + " qword [" + r32[base] + " + " + scale + " * " + r32[index] + " + 0x12]\n"
                    result += "\t" + op + " qword [" + r64[base] + " + " + scale + " * " + r64[index] + " + 0x12]\n"
    filehandle.write(result)

def gen_sibdb(filehandle, op):
    gen8_sibdb(filehandle, op)
    gen16_sibdb(filehandle, op)
    gen32_sibdb(filehandle, op)
    gen64_sibdb(filehandle, op)

def gen8_sibdd(filehandle, op):
    result = ""
    for base in range(len(r8)):
        for index in range(len(r8)):
            for scale in log2_scale:
                if valid8_r_sib(0, base, index, scale):
                    result += "\t" + op + " byte [" + r32[base] + " + " + scale + " * " + r32[index] + " + 0x12345678]\n"
                    result += "\t" + op + " byte [" + r64[base] + " + " + scale + " * " + r64[index] + " + 0x12345678]\n"
    filehandle.write(result)

def gen16_sibdd(filehandle, op):
    result = ""
    for base in range(len(r16)):
        for index in range(len(r16)):
            for scale in log2_scale:
                if valid8_r_sib(0, base, index, scale):
                    result += "\t" + op + " word [" + r32[base] + " + " + scale + " * " + r32[index] + " + 0x12345678]\n"
                    result += "\t" + op + " word [" + r64[base] + " + " + scale + " * " + r64[index] + " + 0x12345678]\n"
    filehandle.write(result)

def gen32_sibdd(filehandle, op):
    result = ""
    for base in range(len(r32)):
        for index in range(len(r32)):
            for scale in log2_scale:
                if valid8_r_sib(0, base, index, scale):
                    result += "\t" + op + " dword [" + r32[base] + " + " + scale + " * " + r32[index] + " + 0x12345678]\n"
                    result += "\t" + op + " dword [" + r64[base] + " + " + scale + " * " + r64[index] + " + 0x12345678]\n"
    filehandle.write(result)

def gen64_sibdd(filehandle, op):
    result = ""
    for base in range(len(r64)):
        for index in range(len(r64)):
            for scale in log2_scale:
                if valid8_r_sib(0, base, index, scale):
                    result += "\t" + op + " qword [" + r32[base] + " + " + scale + " * " + r32[index] + " + 0x12345678]\n"
                    result += "\t" + op + " qword [" + r64[base] + " + " + scale + " * " + r64[index] + " + 0x12345678]\n"
    filehandle.write(result)

def gen_sibdd(filehandle, op):
    gen8_sibdd(filehandle, op)
    gen16_sibdd(filehandle, op)
    gen32_sibdd(filehandle, op)
    gen64_sibdd(filehandle, op)

def gen8_m_ib(filehandle, op):
    result = ""
    for dest in range(len(r8)):
        for src in range(len(r8)):
            if valid8_r_r(dest, src):
                result += "\t" + op + " byte [" + r32[dest] + "], 0x12\n"
                result += "\t" + op + " byte [" + r64[dest] + "], 0x12\n"
    filehandle.write(result)

def gen8_mdb_ib(filehandle, op):
    result = ""
    for dest in range(len(r8)):
        for src in range(len(r8)):
            if valid8_r_r(dest, src):
                result += "\t" + op + " byte [" + r32[dest] + " + 0x12], 0x12\n"
                result += "\t" + op + " byte [" + r64[dest] + " + 0x12], 0x12\n"
    filehandle.write(result)

def gen8_mdd_ib(filehandle, op):
    result = ""
    for dest in range(len(r8)):
        for src in range(len(r8)):
            if valid8_r_r(dest, src):
                result += "\t" + op + " byte [" + r32[dest] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " byte [" + r64[dest] + " + 0x12345678], 0x12\n"
    filehandle.write(result)

def gen8_sib_ib(filehandle, op):
    result = ""
    for base in range(len(r8)):
        for index in range(len(r8)):
            for scale in log2_scale:
                if valid8_r_sib(base, base, index, scale):
                    result += "\t" + op + " byte [" + r32[base] + " + " + scale + "*" + r32[index] + "], 0x12\n"
                    result += "\t" + op + " byte [" + r64[base] + " + " + scale + "*" + r64[index] + "], 0x12\n"
    filehandle.write(result)

def gen8_sibdb_ib(filehandle, op):
    result = ""
    for base in range(len(r8)):
        for index in range(len(r8)):
            for scale in log2_scale:
                if valid8_r_sib(base, base, index, scale):
                    result += "\t" + op + " byte [" + r32[base] + " + " + scale + "*" + r32[index] + "], 0x12\n"
                    result += "\t" + op + " byte [" + r64[base] + " + " + scale + "*" + r64[index] + "], 0x12\n"
    filehandle.write(result)

def gen8_sibdd_ib(filehandle, op):
    pass

def gen8_disp_ib(filehandle, op):
    pass

def gen8_rel_ib(filehandle, op):
    pass

def gen16_m_iw(filehandle, op):
    pass

def gen16_mdb_iw(filehandle, op):
    pass

def gen16_mdd_iw(filehandle, op):
    pass

def gen16_sib_iw(filehandle, op):
    pass

def gen16_sibdb_iw(filehandle, op):
    pass

def gen16_sibdd_iw(filehandle, op):
    pass

def gen16_disp_iw(filehandle, op):
    pass

def gen16_rel_iw(filehandle, op):
    pass

def gen32_m_id(filehandle, op):
    pass

def gen32_mdb_id(filehandle, op):
    pass

def gen32_mdd_id(filehandle, op):
    pass

def gen32_sib_id(filehandle, op):
    pass

def gen32_sibdb_id(filehandle, op):
    pass

def gen32_sibdd_id(filehandle, op):
    pass

def gen32_disp_id(filehandle, op):
    pass

def gen32_rel_id(filehandle, op):
    pass

def gen64_m_id(filehandle, op):
    pass

def gen64_mdb_id(filehandle, op):
    pass

def gen64_mdd_id(filehandle, op):
    pass

def gen64_sib_id(filehandle, op):
    pass

def gen64_sibdb_id(filehandle, op):
    pass

def gen64_sibdd_id(filehandle, op):
    pass

def gen64_disp_id(filehandle, op):
    pass

def gen64_rel_id(filehandle, op):
    pass


class general_2op_instruction():
    def __init__(self, op):
        self.op = op

    def test(self, filehandle):
        gen_r_r(filehandle, self.op)
        gen_r_ix(filehandle, self.op)
        gen_r_m(filehandle, self.op)
        gen_r_mdb(filehandle, self.op)
        gen_r_mdd(filehandle, self.op)
        gen_r_sib(filehandle, self.op)
        gen_r_sibdb(filehandle, self.op)
        gen_r_sibdd(filehandle, self.op)
        gen_r_disp(filehandle, self.op)
        gen_r_rel(filehandle, self.op)

        gen_m_r(filehandle, self.op)
        gen_mdb_r(filehandle, self.op)
        gen_mdd_r(filehandle, self.op)
        gen_sib_r(filehandle, self.op)
        gen_sibdb_r(filehandle, self.op)
        gen_sibdd_r(filehandle, self.op)
        gen_disp_r(filehandle, self.op)
        gen_rel_r(filehandle, self.op)

class general_1op_instruction():
    def __init__(self, op):
        self.op = op

    def test(self, filehandle):
        result = "\t" + self.op + " qword [0x12345678]\n"
        result += "\t" + self.op + " qword [rel $ + 0x12345678]\n"
        filehandle.write(result)

        gen_r(filehandle, self.op)
        gen_m(filehandle, self.op)
        gen_mdb(filehandle, self.op)
        gen_mdd(filehandle, self.op)
        gen_sib(filehandle, self.op)
        gen_sibdb(filehandle, self.op)
        gen_sibdd(filehandle, self.op)

class jmp_call_instruction():
    def __init__(self, op):
        self.op = op

    def test(self, filehandle):
        result = "\t" + self.op + " 0x12345678\n"
        result += "\t" + self.op + " qword [0x12345678]\n"
        result += "\t" + self.op + " qword [rel $ + 0x12345678]\n"
        for base in range(len(r64)):
            result += "\t" + self.op + " " + r64[base] + "\n"
            result += "\t" + self.op + " qword [" + r32[base] + "]\n"
            result += "\t" + self.op + " qword [" + r64[base] + " + 0x12]\n"
            result += "\t" + self.op + " qword [" + r32[base] + " + 0x12345678]\n"
            result += "\t" + self.op + " qword [" + r64[base] + "]\n"
            result += "\t" + self.op + " qword [" + r32[base] + " + 0x12]\n"
            result += "\t" + self.op + " qword [" + r64[base] + " + 0x12345678]\n"
            for index in range(len(r64)):
                if validX_r_sib(0, base, index, "1"):
                    result += "\t" + self.op + " qword [" + r32[base] + " + " + r32[index] + "]\n"
                    result += "\t" + self.op + " qword [" + r64[base] + " + " + r64[index] + "]\n"
                    result += "\t" + self.op + " qword [" + r32[base] + " + " + r32[index] + " + 0x12]\n"
                    result += "\t" + self.op + " qword [" + r64[base] + " + " + r64[index] + " + 0x12]\n"
                    result += "\t" + self.op + " qword [" + r32[base] + " + " + r32[index] + " + 0x12345678]\n"
                    result += "\t" + self.op + " qword [" + r64[base] + " + " + r64[index] + " + 0x12345678]\n"
        filehandle.write(result)

class jcc_instruction():
    def test(self, filehandle):
        result = ""
        for op in jcc:
            result += "\t" + op + " 0x12\n"
            result += "\t" + op + " 0x1234\n"
            result += "\t" + op + " 0x12345678\n"
        filehandle.write(result)

class lea_instruction():
    def test(self, filehandle):
        op = "lea"
        gen16_r_disp(filehandle, op)
        gen16_r_rel(filehandle, op)
        gen16_r_m(filehandle, op)
        gen16_r_mdb(filehandle, op)
        gen16_r_mdd(filehandle, op)
        gen16_r_sib(filehandle, op)
        gen16_r_sibdb(filehandle, op)
        gen16_r_sibdd(filehandle, op)

        gen32_r_disp(filehandle, op)
        gen32_r_rel(filehandle, op)
        gen32_r_m(filehandle, op)
        gen32_r_mdb(filehandle, op)
        gen32_r_mdd(filehandle, op)
        gen32_r_sib(filehandle, op)
        gen32_r_sibdb(filehandle, op)
        gen32_r_sibdd(filehandle, op)

        gen64_r_disp(filehandle, op)
        gen64_r_rel(filehandle, op)
        gen64_r_m(filehandle, op)
        gen64_r_mdb(filehandle, op)
        gen64_r_mdd(filehandle, op)
        gen64_r_sib(filehandle, op)
        gen64_r_sibdb(filehandle, op)
        gen64_r_sibdd(filehandle, op)

class mov_instruction():
    def test(self, filehandle):
        op = "mov"
        # ADD_LAYOUT(OPTYPE_RM8,  OPTYPE_R8,  OPTYPE_NULL)
        gen8_m_r(filehandle, op);
        gen8_mdb_r(filehandle, op);
        gen8_mdd_r(filehandle, op);
        gen8_sib_r(filehandle, op);
        gen8_sibdb_r(filehandle, op);
        gen8_sibdd_r(filehandle, op);
        gen8_disp_r(filehandle, op);
        gen8_rel_r(filehandle, op);
        # ADD_LAYOUT(OPTYPE_RM16, OPTYPE_R16, OPTYPE_NULL)
        gen16_m_r(filehandle, op);
        gen16_mdb_r(filehandle, op);
        gen16_mdd_r(filehandle, op);
        gen16_sib_r(filehandle, op);
        gen16_sibdb_r(filehandle, op);
        gen16_sibdd_r(filehandle, op);
        gen16_disp_r(filehandle, op);
        gen16_rel_r(filehandle, op);
        # ADD_LAYOUT(OPTYPE_RM32, OPTYPE_R32, OPTYPE_NULL)
        gen32_m_r(filehandle, op);
        gen32_mdb_r(filehandle, op);
        gen32_mdd_r(filehandle, op);
        gen32_sib_r(filehandle, op);
        gen32_sibdb_r(filehandle, op);
        gen32_sibdd_r(filehandle, op);
        gen32_disp_r(filehandle, op);
        gen32_rel_r(filehandle, op);
        # ADD_LAYOUT(OPTYPE_RM64, OPTYPE_R64, OPTYPE_NULL)
        gen64_m_r(filehandle, op);
        gen64_mdb_r(filehandle, op);
        gen64_mdd_r(filehandle, op);
        gen64_sib_r(filehandle, op);
        gen64_sibdb_r(filehandle, op);
        gen64_sibdd_r(filehandle, op);
        gen64_disp_r(filehandle, op);
        gen64_rel_r(filehandle, op);

        # ADD_LAYOUT(OPTYPE_R8,  OPTYPE_RM8,  OPTYPE_NULL)
        gen8_r_m(filehandle, op);
        gen8_r_mdb(filehandle, op);
        gen8_r_mdd(filehandle, op);
        gen8_r_sib(filehandle, op);
        gen8_r_sibdb(filehandle, op);
        gen8_r_sibdd(filehandle, op);
        gen8_r_disp(filehandle, op);
        gen8_r_rel(filehandle, op);
        # ADD_LAYOUT(OPTYPE_R16, OPTYPE_RM16, OPTYPE_NULL)
        gen16_r_m(filehandle, op);
        gen16_r_mdb(filehandle, op);
        gen16_r_mdd(filehandle, op);
        gen16_r_sib(filehandle, op);
        gen16_r_sibdb(filehandle, op);
        gen16_r_sibdd(filehandle, op);
        gen16_r_disp(filehandle, op);
        gen16_r_rel(filehandle, op);
        # ADD_LAYOUT(OPTYPE_R32, OPTYPE_RM32, OPTYPE_NULL)
        gen32_r_m(filehandle, op);
        gen32_r_mdb(filehandle, op);
        gen32_r_mdd(filehandle, op);
        gen32_r_sib(filehandle, op);
        gen32_r_sibdb(filehandle, op);
        gen32_r_sibdd(filehandle, op);
        gen32_r_disp(filehandle, op);
        gen32_r_rel(filehandle, op);
        # ADD_LAYOUT(OPTYPE_R64, OPTYPE_RM64, OPTYPE_NULL)
        gen64_r_m(filehandle, op);
        gen64_r_mdb(filehandle, op);
        gen64_r_mdd(filehandle, op);
        gen64_r_sib(filehandle, op);
        gen64_r_sibdb(filehandle, op);
        gen64_r_sibdd(filehandle, op);
        gen64_r_disp(filehandle, op);
        gen64_r_rel(filehandle, op);

        result = ""
        # ADD_LAYOUT(OPTYPE_R8,  OPTYPE_IMM8,  OPTYPE_NULL)
        for dest in r8:
            result += "\tmov " + dest + ", 0x12\n"
        # ADD_LAYOUT(OPTYPE_R16, OPTYPE_IMM16, OPTYPE_NULL)
        for dest in r16:
            result += "\tmov " + dest + ", 0x1234\n"
        # ADD_LAYOUT(OPTYPE_R32, OPTYPE_IMM32, OPTYPE_NULL)
        for dest in r32:
            result += "\tmov " + dest + ", 0x12345678\n"
        # ADD_LAYOUT(OPTYPE_R64, OPTYPE_IMM64, OPTYPE_NULL)
        for dest in r64:
            result += "\tmov " + dest + ", 0x12345678\n"
            result += "\tmov " + dest + ", 0x123456789ABCDEF0\n"

        # ADD_LAYOUT(OPTYPE_RM8,  OPTYPE_IMM8,  OPTYPE_NULL)
        # todo add immediate types
        for base in range(len(r8)):
            result += "\tmov byte [" + r32[base] + "], 0x12\n"
            result += "\tmov byte [" + r64[base] + "], 0x12\n"
            result += "\tmov byte [" + r32[base] + " + 0x12], 0x12\n"
            result += "\tmov byte [" + r64[base] + " + 0x12], 0x12\n"
            result += "\tmov byte [" + r32[base] + " + 0x12345678], 0x12\n"
            result += "\tmov byte [" + r64[base] + " + 0x12345678], 0x12\n"
        for base in range(len(r8)):
            for index in range(len(r8)):
                if valid8_r_sib(0, base, index, "1"):
                    result += "\tmov byte [" + r32[base] + " + " + r32[index] + "], 0x12\n"
                    result += "\tmov byte [" + r64[base] + " + " + r64[index] + "], 0x12\n"
                    result += "\tmov byte [" + r32[base] + " + " + r32[index] + " + 0x12], 0x12\n"
                    result += "\tmov byte [" + r64[base] + " + " + r64[index] + " + 0x12], 0x12\n"
                    result += "\tmov byte [" + r32[base] + " + " + r32[index] + " + 0x12345678], 0x12\n"
                    result += "\tmov byte [" + r64[base] + " + " + r64[index] + " + 0x12345678], 0x12\n"
        # ADD_LAYOUT(OPTYPE_RM16, OPTYPE_IMM16, OPTYPE_NULL)
        for base in range(len(r16)):
            result += "\tmov word [" + r32[base] + "], 0x1234\n"
            result += "\tmov word [" + r64[base] + "], 0x1234\n"
            result += "\tmov word [" + r32[base] + " + 0x12], 0x1234\n"
            result += "\tmov word [" + r64[base] + " + 0x12], 0x1234\n"
            result += "\tmov word [" + r32[base] + " + 0x12345678], 0x1234\n"
            result += "\tmov word [" + r64[base] + " + 0x12345678], 0x1234\n"
        for base in range(len(r16)):
            for index in range(len(r16)):
                if validX_r_sib(0, base, index, "1"):
                    result += "\tmov word [" + r32[base] + " + " + r32[index] + "], 0x1234\n"
                    result += "\tmov word [" + r64[base] + " + " + r64[index] + "], 0x1234\n"
                    result += "\tmov word [" + r32[base] + " + " + r32[index] + " + 0x12], 0x1234\n"
                    result += "\tmov word [" + r64[base] + " + " + r64[index] + " + 0x12], 0x1234\n"
                    result += "\tmov word [" + r32[base] + " + " + r32[index] + " + 0x12345678], 0x1234\n"
                    result += "\tmov word [" + r64[base] + " + " + r64[index] + " + 0x12345678], 0x1234\n"
        # ADD_LAYOUT(OPTYPE_RM32, OPTYPE_IMM32, OPTYPE_NULL)
        for base in range(len(r32)):
            result += "\tmov dword [" + r32[base] + "], 0x12345678\n"
            result += "\tmov dword [" + r64[base] + "], 0x12345678\n"
            result += "\tmov dword [" + r32[base] + " + 0x12], 0x12345678\n"
            result += "\tmov dword [" + r64[base] + " + 0x12], 0x12345678\n"
            result += "\tmov dword [" + r32[base] + " + 0x12345678], 0x12345678\n"
            result += "\tmov dword [" + r64[base] + " + 0x12345678], 0x12345678\n"
        for base in range(len(r32)):
            for index in range(len(r32)):
                if validX_r_sib(0, base, index, "1"):
                    result += "\tmov dword [" + r32[base] + " + " + r32[index] + "], 0x12345678\n"
                    result += "\tmov dword [" + r64[base] + " + " + r64[index] + "], 0x12345678\n"
                    result += "\tmov dword [" + r32[base] + " + " + r32[index] + " + 0x12], 0x12345678\n"
                    result += "\tmov dword [" + r64[base] + " + " + r64[index] + " + 0x12], 0x12345678\n"
                    result += "\tmov dword [" + r32[base] + " + " + r32[index] + " + 0x12345678], 0x12345678\n"
                    result += "\tmov dword [" + r64[base] + " + " + r64[index] + " + 0x12345678], 0x12345678\n"
        # ADD_LAYOUT(OPTYPE_RM64, OPTYPE_IMM32, OPTYPE_NULL)
        for base in range(len(r64)):
            result += "\tmov qword [" + r32[base] + "], 0x12345678\n"
            result += "\tmov qword [" + r64[base] + "], 0x12345678\n"
            result += "\tmov qword [" + r32[base] + " + 0x12], 0x12345678\n"
            result += "\tmov qword [" + r64[base] + " + 0x12], 0x12345678\n"
            result += "\tmov qword [" + r32[base] + " + 0x12345678], 0x12345678\n"
            result += "\tmov qword [" + r64[base] + " + 0x12345678], 0x12345678\n"
        for base in range(len(r64)):
            for index in range(len(r64)):
                if validX_r_sib(0, base, index, "1"):
                    result += "\tmov qword [" + r32[base] + " + " + r32[index] + "], 0x12345678\n"
                    result += "\tmov qword [" + r64[base] + " + " + r64[index] + "], 0x12345678\n"
                    result += "\tmov qword [" + r32[base] + " + " + r32[index] + " + 0x12], 0x12345678\n"
                    result += "\tmov qword [" + r64[base] + " + " + r64[index] + " + 0x12], 0x12345678\n"
                    result += "\tmov qword [" + r32[base] + " + " + r32[index] + " + 0x12345678], 0x12345678\n"
                    result += "\tmov qword [" + r64[base] + " + " + r64[index] + " + 0x12345678], 0x12345678\n"
        result += "\tmov byte [0x12345678], 0x12\n"
        result += "\tmov byte [rel $ + 0x12], 0x12\n"
        result += "\tmov word [0x12345678], 0x1234\n"
        result += "\tmov word [rel $ + 0x12], 0x1234\n"
        result += "\tmov dword [0x12345678], 0x12345678\n"
        result += "\tmov dword [rel $ + 0x12], 0x12345678\n"
        result += "\tmov qword [0x12345678], 0x12345678\n"
        result += "\tmov qword [rel $ + 0x12], 0x12345678\n"

        filehandle.write(result)

class cmov_instruction():
    def test(self, filehandle):
        for op in cmovcc:
            gen16_r_r(filehandle, op)
            gen16_r_m(filehandle, op)
            gen16_r_mdb(filehandle, op)
            gen16_r_mdd(filehandle, op)
            gen16_r_sib(filehandle, op)
            gen16_r_sibdb(filehandle, op)
            gen16_r_sibdd(filehandle, op)

            gen32_r_r(filehandle, op)
            gen32_r_m(filehandle, op)
            gen32_r_mdb(filehandle, op)
            gen32_r_mdd(filehandle, op)
            gen32_r_sib(filehandle, op)
            gen32_r_sibdb(filehandle, op)
            gen32_r_sibdd(filehandle, op)

            gen64_r_r(filehandle, op)
            gen64_r_m(filehandle, op)
            gen64_r_mdb(filehandle, op)
            gen64_r_mdd(filehandle, op)
            gen64_r_sib(filehandle, op)
            gen64_r_sibdb(filehandle, op)
            gen64_r_sibdd(filehandle, op)

            result = ""
            for dest in range(len(r16)):
                result += "\t" + op + " " + r16[dest] + ", word [0x12345678]\n"
                result += "\t" + op + " " + r32[dest] + ", dword [0x12345678]\n"
                result += "\t" + op + " " + r64[dest] + ", qword [0x12345678]\n"
                result += "\t" + op + " " + r16[dest] + ", word [rel $ + 0x12345678]\n"
                result += "\t" + op + " " + r32[dest] + ", dword [rel $ + 0x12345678]\n"
                result += "\t" + op + " " + r64[dest] + ", qword [rel $ + 0x12345678]\n"
            filehandle.write(result)

class movsx_instruction():
    def test(self, filehandle):
        result = ""
        for dest in range(len(r16)):
            for src in range(len(r8)):
                if valid8_r_r(dest, src):
                    result += "\tmovsx " + r16[dest] + ", " + r8[src] + "\n"
            for base in range(len(r8)):
                result += "\tmovsx " + r16[dest] + ", byte [" + r32[base] + "]\n"
                result += "\tmovsx " + r16[dest] + ", byte [" + r64[base] + "]\n"
                result += "\tmovsx " + r16[dest] + ", byte [" + r32[base] + " + 0x12]\n"
                result += "\tmovsx " + r16[dest] + ", byte [" + r64[base] + " + 0x12]\n"
                result += "\tmovsx " + r16[dest] + ", byte [" + r32[base] + " + 0x12345678]\n"
                result += "\tmovsx " + r16[dest] + ", byte [" + r64[base] + " + 0x12345678]\n"
            for base in range(len(r8)):
                for index in range(len(r8)):
                    if valid8_r_sib(dest, base, index, "1"):
                        result += "\tmovsx " + r16[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + "]\n"
                        result += "\tmovsx " + r16[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + "]\n"
                        result += "\tmovsx " + r16[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12]\n"
                        result += "\tmovsx " + r16[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + " + 0x12]\n"
                        result += "\tmovsx " + r16[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678]\n"
                        result += "\tmovsx " + r16[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + " + 0x12345678]\n"

        for dest in range(len(r32)):
            for src in range(len(r8)):
                if valid8_r_r(dest, src):
                    result += "\tmovsx " + r32[dest] + ", " + r8[src] + "\n"
                result += "\tmovsx " + r32[dest] + ", " + r16[src] + "\n"
            for base in range(len(r8)):
                result += "\tmovsx " + r32[dest] + ", byte [" + r32[base] + "]\n"
                result += "\tmovsx " + r32[dest] + ", byte [" + r64[base] + "]\n"
                result += "\tmovsx " + r32[dest] + ", byte [" + r32[base] + " + 0x12]\n"
                result += "\tmovsx " + r32[dest] + ", byte [" + r64[base] + " + 0x12]\n"
                result += "\tmovsx " + r32[dest] + ", byte [" + r32[base] + " + 0x12345678]\n"
                result += "\tmovsx " + r32[dest] + ", byte [" + r64[base] + " + 0x12345678]\n"
                result += "\tmovsx " + r32[dest] + ", word [" + r32[base] + "]\n"
                result += "\tmovsx " + r32[dest] + ", word [" + r64[base] + "]\n"
                result += "\tmovsx " + r32[dest] + ", word [" + r32[base] + " + 0x12]\n"
                result += "\tmovsx " + r32[dest] + ", word [" + r64[base] + " + 0x12]\n"
                result += "\tmovsx " + r32[dest] + ", word [" + r32[base] + " + 0x12345678]\n"
                result += "\tmovsx " + r32[dest] + ", word [" + r64[base] + " + 0x12345678]\n"
            for base in range(len(r8)):
                for index in range(len(r8)):
                    if valid8_r_sib(dest, base, index, "1"):
                        result += "\tmovsx " + r32[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + "]\n"
                        result += "\tmovsx " + r32[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + "]\n"
                        result += "\tmovsx " + r32[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12]\n"
                        result += "\tmovsx " + r32[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + " + 0x12]\n"
                        result += "\tmovsx " + r32[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678]\n"
                        result += "\tmovsx " + r32[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + " + 0x12345678]\n"
                    if validX_r_sib(dest, base, index, "1"):
                        result += "\tmovsx " + r32[dest] + ", word [" + r32[base] + " + 1 * " + r32[index] + "]\n"
                        result += "\tmovsx " + r32[dest] + ", word [" + r64[base] + " + 1 * " + r64[index] + "]\n"
                        result += "\tmovsx " + r32[dest] + ", word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12]\n"
                        result += "\tmovsx " + r32[dest] + ", word [" + r64[base] + " + 1 * " + r64[index] + " + 0x12]\n"
                        result += "\tmovsx " + r32[dest] + ", word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678]\n"
                        result += "\tmovsx " + r32[dest] + ", word [" + r64[base] + " + 1 * " + r64[index] + " + 0x12345678]\n"

        for dest in range(len(r64)):
            for src in range(len(r8)):
                    if valid8_r_r(8, src):
                        result += "\tmovsx " + r64[dest] + ", " + r8[src] + "\n"
                    result += "\tmovsx " + r64[dest] + ", " + r16[src] + "\n"
                    result += "\tmovsx " + r64[dest] + ", " + r32[src] + "\n"
            for base in range(len(r8)):
                result += "\tmovsx " + r64[dest] + ", byte [" + r32[base] + "]\n"
                result += "\tmovsx " + r64[dest] + ", byte [" + r64[base] + "]\n"
                result += "\tmovsx " + r64[dest] + ", byte [" + r32[base] + " + 0x12]\n"
                result += "\tmovsx " + r64[dest] + ", byte [" + r64[base] + " + 0x12]\n"
                result += "\tmovsx " + r64[dest] + ", byte [" + r32[base] + " + 0x12345678]\n"
                result += "\tmovsx " + r64[dest] + ", byte [" + r64[base] + " + 0x12345678]\n"
                result += "\tmovsx " + r64[dest] + ", word [" + r32[base] + "]\n"
                result += "\tmovsx " + r64[dest] + ", word [" + r64[base] + "]\n"
                result += "\tmovsx " + r64[dest] + ", word [" + r32[base] + " + 0x12]\n"
                result += "\tmovsx " + r64[dest] + ", word [" + r64[base] + " + 0x12]\n"
                result += "\tmovsx " + r64[dest] + ", word [" + r32[base] + " + 0x12345678]\n"
                result += "\tmovsx " + r64[dest] + ", word [" + r64[base] + " + 0x12345678]\n"
                result += "\tmovsx " + r64[dest] + ", dword [" + r32[base] + "]\n"
                result += "\tmovsx " + r64[dest] + ", dword [" + r64[base] + "]\n"
                result += "\tmovsx " + r64[dest] + ", dword [" + r32[base] + " + 0x12]\n"
                result += "\tmovsx " + r64[dest] + ", dword [" + r64[base] + " + 0x12]\n"
                result += "\tmovsx " + r64[dest] + ", dword [" + r32[base] + " + 0x12345678]\n"
                result += "\tmovsx " + r64[dest] + ", dword [" + r64[base] + " + 0x12345678]\n"
            for base in range(len(r8)):
                for index in range(len(r8)):
                    if valid8_r_sib(dest, base, index, "1"):
                        result += "\tmovsx " + r64[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + "]\n"
                        result += "\tmovsx " + r64[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + "]\n"
                        result += "\tmovsx " + r64[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12]\n"
                        result += "\tmovsx " + r64[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + " + 0x12]\n"
                        result += "\tmovsx " + r64[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678]\n"
                        result += "\tmovsx " + r64[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + " + 0x12345678]\n"
                    if validX_r_sib(dest, base, index, "1"):
                        result += "\tmovsx " + r64[dest] + ", word [" + r32[base] + " + 1 * " + r32[index] + "]\n"
                        result += "\tmovsx " + r64[dest] + ", word [" + r64[base] + " + 1 * " + r64[index] + "]\n"
                        result += "\tmovsx " + r64[dest] + ", word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12]\n"
                        result += "\tmovsx " + r64[dest] + ", word [" + r64[base] + " + 1 * " + r64[index] + " + 0x12]\n"
                        result += "\tmovsx " + r64[dest] + ", word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678]\n"
                        result += "\tmovsx " + r64[dest] + ", word [" + r64[base] + " + 1 * " + r64[index] + " + 0x12345678]\n"
                        result += "\tmovsx " + r64[dest] + ", dword [" + r32[base] + " + 1 * " + r32[index] + "]\n"
                        result += "\tmovsx " + r64[dest] + ", dword [" + r64[base] + " + 1 * " + r64[index] + "]\n"
                        result += "\tmovsx " + r64[dest] + ", dword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12]\n"
                        result += "\tmovsx " + r64[dest] + ", dword [" + r64[base] + " + 1 * " + r64[index] + " + 0x12]\n"
                        result += "\tmovsx " + r64[dest] + ", dword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678]\n"
                        result += "\tmovsx " + r64[dest] + ", dword [" + r64[base] + " + 1 * " + r64[index] + " + 0x12345678]\n"
        filehandle.write(result)

class movzx_instruction():
    def test(self, filehandle):
        result = ""
        for dest in range(len(r16)):
            for src in range(len(r8)):
                if valid8_r_r(dest, src):
                    result += "\tmovzx " + r16[dest] + ", " + r8[src] + "\n"
            for base in range(len(r8)):
                result += "\tmovzx " + r16[dest] + ", byte [" + r32[base] + "]\n"
                result += "\tmovzx " + r16[dest] + ", byte [" + r64[base] + "]\n"
                result += "\tmovzx " + r16[dest] + ", byte [" + r32[base] + " + 0x12]\n"
                result += "\tmovzx " + r16[dest] + ", byte [" + r64[base] + " + 0x12]\n"
                result += "\tmovzx " + r16[dest] + ", byte [" + r32[base] + " + 0x12345678]\n"
                result += "\tmovzx " + r16[dest] + ", byte [" + r64[base] + " + 0x12345678]\n"
            for base in range(len(r8)):
                for index in range(len(r8)):
                    if valid8_r_sib(dest, base, index, "1"):
                        result += "\tmovzx " + r16[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + "]\n"
                        result += "\tmovzx " + r16[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + "]\n"
                        result += "\tmovzx " + r16[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12]\n"
                        result += "\tmovzx " + r16[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + " + 0x12]\n"
                        result += "\tmovzx " + r16[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678]\n"
                        result += "\tmovzx " + r16[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + " + 0x12345678]\n"

        for dest in range(len(r32)):
            for src in range(len(r8)):
                if valid8_r_r(dest, src):
                    result += "\tmovzx " + r32[dest] + ", " + r8[src] + "\n"
                result += "\tmovzx " + r32[dest] + ", " + r16[src] + "\n"
            for base in range(len(r8)):
                result += "\tmovzx " + r32[dest] + ", byte [" + r32[base] + "]\n"
                result += "\tmovzx " + r32[dest] + ", byte [" + r64[base] + "]\n"
                result += "\tmovzx " + r32[dest] + ", byte [" + r32[base] + " + 0x12]\n"
                result += "\tmovzx " + r32[dest] + ", byte [" + r64[base] + " + 0x12]\n"
                result += "\tmovzx " + r32[dest] + ", byte [" + r32[base] + " + 0x12345678]\n"
                result += "\tmovzx " + r32[dest] + ", byte [" + r64[base] + " + 0x12345678]\n"
                result += "\tmovzx " + r32[dest] + ", word [" + r32[base] + "]\n"
                result += "\tmovzx " + r32[dest] + ", word [" + r64[base] + "]\n"
                result += "\tmovzx " + r32[dest] + ", word [" + r32[base] + " + 0x12]\n"
                result += "\tmovzx " + r32[dest] + ", word [" + r64[base] + " + 0x12]\n"
                result += "\tmovzx " + r32[dest] + ", word [" + r32[base] + " + 0x12345678]\n"
                result += "\tmovzx " + r32[dest] + ", word [" + r64[base] + " + 0x12345678]\n"
            for base in range(len(r8)):
                for index in range(len(r8)):
                    if valid8_r_sib(dest, base, index, "1"):
                        result += "\tmovzx " + r32[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + "]\n"
                        result += "\tmovzx " + r32[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + "]\n"
                        result += "\tmovzx " + r32[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12]\n"
                        result += "\tmovzx " + r32[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + " + 0x12]\n"
                        result += "\tmovzx " + r32[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678]\n"
                        result += "\tmovzx " + r32[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + " + 0x12345678]\n"
                    if validX_r_sib(dest, base, index, "1"):
                        result += "\tmovzx " + r32[dest] + ", word [" + r32[base] + " + 1 * " + r32[index] + "]\n"
                        result += "\tmovzx " + r32[dest] + ", word [" + r64[base] + " + 1 * " + r64[index] + "]\n"
                        result += "\tmovzx " + r32[dest] + ", word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12]\n"
                        result += "\tmovzx " + r32[dest] + ", word [" + r64[base] + " + 1 * " + r64[index] + " + 0x12]\n"
                        result += "\tmovzx " + r32[dest] + ", word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678]\n"
                        result += "\tmovzx " + r32[dest] + ", word [" + r64[base] + " + 1 * " + r64[index] + " + 0x12345678]\n"

        for dest in range(len(r64)):
            for src in range(len(r8)):
                    if valid8_r_r(8, src):
                        result += "\tmovzx " + r64[dest] + ", " + r8[src] + "\n"
                    result += "\tmovzx " + r64[dest] + ", " + r16[src] + "\n"
            for base in range(len(r8)):
                result += "\tmovzx " + r64[dest] + ", byte [" + r32[base] + "]\n"
                result += "\tmovzx " + r64[dest] + ", byte [" + r64[base] + "]\n"
                result += "\tmovzx " + r64[dest] + ", byte [" + r32[base] + " + 0x12]\n"
                result += "\tmovzx " + r64[dest] + ", byte [" + r64[base] + " + 0x12]\n"
                result += "\tmovzx " + r64[dest] + ", byte [" + r32[base] + " + 0x12345678]\n"
                result += "\tmovzx " + r64[dest] + ", byte [" + r64[base] + " + 0x12345678]\n"
                result += "\tmovzx " + r64[dest] + ", word [" + r32[base] + "]\n"
                result += "\tmovzx " + r64[dest] + ", word [" + r64[base] + "]\n"
                result += "\tmovzx " + r64[dest] + ", word [" + r32[base] + " + 0x12]\n"
                result += "\tmovzx " + r64[dest] + ", word [" + r64[base] + " + 0x12]\n"
                result += "\tmovzx " + r64[dest] + ", word [" + r32[base] + " + 0x12345678]\n"
                result += "\tmovzx " + r64[dest] + ", word [" + r64[base] + " + 0x12345678]\n"
            for base in range(len(r8)):
                for index in range(len(r8)):
                    if valid8_r_sib(dest, base, index, "1"):
                        result += "\tmovzx " + r64[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + "]\n"
                        result += "\tmovzx " + r64[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + "]\n"
                        result += "\tmovzx " + r64[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12]\n"
                        result += "\tmovzx " + r64[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + " + 0x12]\n"
                        result += "\tmovzx " + r64[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678]\n"
                        result += "\tmovzx " + r64[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + " + 0x12345678]\n"
                    if validX_r_sib(dest, base, index, "1"):
                        result += "\tmovzx " + r64[dest] + ", word [" + r32[base] + " + 1 * " + r32[index] + "]\n"
                        result += "\tmovzx " + r64[dest] + ", word [" + r64[base] + " + 1 * " + r64[index] + "]\n"
                        result += "\tmovzx " + r64[dest] + ", word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12]\n"
                        result += "\tmovzx " + r64[dest] + ", word [" + r64[base] + " + 1 * " + r64[index] + " + 0x12]\n"
                        result += "\tmovzx " + r64[dest] + ", word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678]\n"
                        result += "\tmovzx " + r64[dest] + ", word [" + r64[base] + " + 1 * " + r64[index] + " + 0x12345678]\n"
        filehandle.write(result)

class imm8_instruction():
    def __init__(self, op):
        self.op = op

    def test(self, filehandle):
        result = ""
        for i in range(256):
            result += "\t" + self.op + " " + str(hex(i)) + "\n"
        filehandle.write(result)

class no_operand_instruction():
    def __init__(self, op):
        self.op = op

    def test(self, filehandle):
        result = self.op + "\n"
        filehandle.write(result)

class pop_instruction():
    def test(self, filehandle):
        op = "pop"
        result = "\t" + op + " word [0x12345678]\n"
        result += "\t" + op + " word [rel $ + 0x12345678]\n"
        result += "\t" + op + " qword [0x12345678]\n"
        result += "\t" + op + " qword [rel $ + 0x12345678]\n"
        filehandle.write(result)

        # todo popping into fs and gs not handled

        gen16_r(filehandle, op)
        gen64_r(filehandle, op)

        gen16_m(filehandle, op)
        gen64_m(filehandle, op)

        gen16_mdb(filehandle, op)
        gen64_mdb(filehandle, op)

        gen16_mdd(filehandle, op)
        gen64_mdd(filehandle, op)

        gen16_sib(filehandle, op)
        gen64_sib(filehandle, op)

        gen16_sibdb(filehandle, op)
        gen64_sibdb(filehandle, op)

        gen16_sibdd(filehandle, op)
        gen64_sibdd(filehandle, op)

class push_instruction():
    def test(self, filehandle):
        op = "push"

        result = "\t" + op + " 0x55\n"
        #result += "\t" + op + " 0x1234\n"
        result += "\t" + op + " 0x12345678\n"
        result += "\t" + op + " word [0x12345678]\n"
        result += "\t" + op + " word [rel $ + 0x12345678]\n"
        result += "\t" + op + " qword [0x12345678]\n"
        result += "\t" + op + " qword [rel $ + 0x12345678]\n"
        filehandle.write(result)

        gen16_r(filehandle, op)
        gen64_r(filehandle, op)

        gen16_m(filehandle, op)
        gen64_m(filehandle, op)

        gen16_mdb(filehandle, op)
        gen64_mdb(filehandle, op)

        gen16_mdd(filehandle, op)
        gen64_mdd(filehandle, op)

        gen16_sib(filehandle, op)
        gen64_sib(filehandle, op)

        gen16_sibdb(filehandle, op)
        gen64_sibdb(filehandle, op)

        gen16_sibdd(filehandle, op)
        gen64_sibdd(filehandle, op)

class ret_instruction():
    def test(self, filehandle):
        result = "\tret\n"
        result += "\t ret 0x1234\n"
        filehandle.write(result)

class shift_instruction():
    def __init__(self, op):
        self.op = op
    
    def test(self, filehandle):
        result = ""
        shift_amount = ["1", "cl", "0x12"]
        for amount in shift_amount:
            result += "\t" + self.op + " byte [0x12345678], " + amount + "\n"
            result += "\t" + self.op + " word [0x12345678], " + amount + "\n"
            result += "\t" + self.op + " dword [0x12345678], " + amount + "\n"
            result += "\t" + self.op + " qword [0x12345678], " + amount + "\n"
            result += "\t" + self.op + " byte [rel $ + 0x12345678], " + amount + "\n"
            result += "\t" + self.op + " word [rel $ + 0x12345678], " + amount + "\n"
            result += "\t" + self.op + " dword [rel $ + 0x12345678], " + amount + "\n"
            result += "\t" + self.op + " qword [rel $ + 0x12345678], " + amount + "\n"

            for dest in range(len(r8)):
                result += "\t" + self.op + " " + r8[dest] + ", " + amount + "\n"
                result += "\t" + self.op + " byte [" + r32[dest] + "], " + amount + "\n"
                result += "\t" + self.op + " byte [" + r64[dest] + "], " + amount + "\n"
                result += "\t" + self.op + " byte [" + r32[dest] + " + 0x12], " + amount + "\n"
                result += "\t" + self.op + " byte [" + r64[dest] + " + 0x12], " + amount + "\n"
                result += "\t" + self.op + " byte [" + r32[dest] + " + 0x12345678], " + amount + "\n"
                result += "\t" + self.op + " byte [" + r64[dest] + " + 0x12345678], " + amount + "\n"
                for index in range(len(r8)):
                    if validX_r_sib(0, dest, index, "2"):
                        result += "\t" + self.op + " byte [" + r32[dest] + " + 2 * " + r32[index] + "], " + amount + "\n"
                        result += "\t" + self.op + " byte [" + r64[dest] + " + 2 * " + r64[index] + "], " + amount + "\n"
                        result += "\t" + self.op + " byte [" + r32[dest] + " + 2 * " + r32[index] + " + 0x12], " + amount + "\n"
                        result += "\t" + self.op + " byte [" + r64[dest] + " + 2 * " + r64[index] + " + 0x12], " + amount + "\n"
                        result += "\t" + self.op + " byte [" + r32[dest] + " + 2 * " + r32[index] + " + 0x12345678], " + amount + "\n"
                        result += "\t" + self.op + " byte [" + r64[dest] + " + 2 * " + r64[index] + " + 0x12345678], " + amount + "\n"

            for dest in range(len(r16)):
                result += "\t" + self.op + " " + r16[dest] + ", " + amount + "\n"
                result += "\t" + self.op + " word [" + r32[dest] + "], " + amount + "\n"
                result += "\t" + self.op + " word [" + r64[dest] + "], " + amount + "\n"
                result += "\t" + self.op + " word [" + r32[dest] + " + 0x12], " + amount + "\n"
                result += "\t" + self.op + " word [" + r64[dest] + " + 0x12], " + amount + "\n"
                result += "\t" + self.op + " word [" + r32[dest] + " + 0x12345678], " + amount + "\n"
                result += "\t" + self.op + " word [" + r64[dest] + " + 0x12345678], " + amount + "\n"
                for index in range(len(r16)):
                    if validX_r_sib(0, dest, index, "2"):
                        result += "\t" + self.op + " word [" + r32[dest] + " + 2 * " + r32[index] + "], " + amount + "\n"
                        result += "\t" + self.op + " word [" + r64[dest] + " + 2 * " + r64[index] + "], " + amount + "\n"
                        result += "\t" + self.op + " word [" + r32[dest] + " + 2 * " + r32[index] + " + 0x12], " + amount + "\n"
                        result += "\t" + self.op + " word [" + r64[dest] + " + 2 * " + r64[index] + " + 0x12], " + amount + "\n"
                        result += "\t" + self.op + " word [" + r32[dest] + " + 2 * " + r32[index] + " + 0x12345678], " + amount + "\n"
                        result += "\t" + self.op + " word [" + r64[dest] + " + 2 * " + r64[index] + " + 0x12345678], " + amount + "\n"

            for dest in range(len(r32)):
                result += "\t" + self.op + " " + r32[dest] + ", " + amount + "\n"
                result += "\t" + self.op + " dword [" + r32[dest] + "], " + amount + "\n"
                result += "\t" + self.op + " dword [" + r64[dest] + "], " + amount + "\n"
                result += "\t" + self.op + " dword [" + r32[dest] + " + 0x12], " + amount + "\n"
                result += "\t" + self.op + " dword [" + r64[dest] + " + 0x12], " + amount + "\n"
                result += "\t" + self.op + " dword [" + r32[dest] + " + 0x12345678], " + amount + "\n"
                result += "\t" + self.op + " dword [" + r64[dest] + " + 0x12345678], " + amount + "\n"
                for index in range(len(r32)):
                    if validX_r_sib(0, dest, index, "2"):
                        result += "\t" + self.op + " dword [" + r32[dest] + " + 2 * " + r32[index] + "], " + amount + "\n"
                        result += "\t" + self.op + " dword [" + r64[dest] + " + 2 * " + r64[index] + "], " + amount + "\n"
                        result += "\t" + self.op + " dword [" + r32[dest] + " + 2 * " + r32[index] + " + 0x12], " + amount + "\n"
                        result += "\t" + self.op + " dword [" + r64[dest] + " + 2 * " + r64[index] + " + 0x12], " + amount + "\n"
                        result += "\t" + self.op + " dword [" + r32[dest] + " + 2 * " + r32[index] + " + 0x12345678], " + amount + "\n"
                        result += "\t" + self.op + " dword [" + r64[dest] + " + 2 * " + r64[index] + " + 0x12345678], " + amount + "\n"

            for dest in range(len(r64)):
                result += "\t" + self.op + " " + r64[dest] + ", " + amount + "\n"
                result += "\t" + self.op + " qword [" + r32[dest] + "], " + amount + "\n"
                result += "\t" + self.op + " qword [" + r64[dest] + "], " + amount + "\n"
                result += "\t" + self.op + " qword [" + r32[dest] + " + 0x12], " + amount + "\n"
                result += "\t" + self.op + " qword [" + r64[dest] + " + 0x12], " + amount + "\n"
                result += "\t" + self.op + " qword [" + r32[dest] + " + 0x12345678], " + amount + "\n"
                result += "\t" + self.op + " qword [" + r64[dest] + " + 0x12345678], " + amount + "\n"
                for index in range(len(r64)):
                    if validX_r_sib(0, dest, index, "2"):
                        result += "\t" + self.op + " qword [" + r32[dest] + " + 2 * " + r32[index] + "], " + amount + "\n"
                        result += "\t" + self.op + " qword [" + r64[dest] + " + 2 * " + r64[index] + "], " + amount + "\n"
                        result += "\t" + self.op + " qword [" + r32[dest] + " + 2 * " + r32[index] + " + 0x12], " + amount + "\n"
                        result += "\t" + self.op + " qword [" + r64[dest] + " + 2 * " + r64[index] + " + 0x12], " + amount + "\n"
                        result += "\t" + self.op + " qword [" + r32[dest] + " + 2 * " + r32[index] + " + 0x12345678], " + amount + "\n"
                        result += "\t" + self.op + " qword [" + r64[dest] + " + 2 * " + r64[index] + " + 0x12345678], " + amount + "\n"

        filehandle.write(result)

class test_instruction():
    def test(self, filehandle):
        op = "test"
        result = ""

        result += "\t" + op + " byte [0x12345678], 0x12\n"
        result += "\t" + op + " word [0x12345678], 0x1234\n"
        result += "\t" + op + " dword [0x12345678], 0x12345678\n"
        result += "\t" + op + " qword [0x12345678], 0x12345678\n"
        result += "\t" + op + " byte [rel $ + 0x12345678], 0x12\n"
        result += "\t" + op + " word [rel $ + 0x12345678], 0x1234\n"
        result += "\t" + op + " dword [rel $ + 0x12345678], 0x12345678\n"
        result += "\t" + op + " qword [rel $ + 0x12345678], 0x12345678\n"
        
        for base in range(len(r8)):
            result += "\t" + op + " " + r8[base] + ", 0x12\n"
            result += "\t" + op + " byte [" + r32[base] + "], 0x12\n"
            result += "\t" + op + " byte [" + r64[base] + "], 0x12\n"
            result += "\t" + op + " byte [" + r32[base] + " + 0x12], 0x12\n"
            result += "\t" + op + " byte [" + r64[base] + " + 0x12], 0x12\n"
            result += "\t" + op + " byte [" + r32[base] + " + 0x12345678], 0x12\n"
            result += "\t" + op + " byte [" + r64[base] + " + 0x12345678], 0x12\n"
        for base in range(len(r8)):
            for index in range(len(r8)):
                if valid8_r_sib(0, base, index, "1"):
                    result += "\t" + op + " byte [" + r32[base] + " + 1 * " + r32[index] + "], 0x12\n"
                    result += "\t" + op + " byte [" + r64[base] + " + 1 * " + r64[index] + "], 0x12\n"
                    result += "\t" + op + " byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], 0x12\n"
                    result += "\t" + op + " byte [" + r64[base] + " + 1 * " + r64[index] + " + 0x12], 0x12\n"
                    result += "\t" + op + " byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], 0x12\n"
                    result += "\t" + op + " byte [" + r64[base] + " + 1 * " + r64[index] + " + 0x12345678], 0x12\n"

        for base in range(len(r16)):
            result += "\t" + op + " " + r16[base] + ", 0x1234\n"
            result += "\t" + op + " word [" + r32[base] + "], 0x1234\n"
            result += "\t" + op + " word [" + r64[base] + "], 0x1234\n"
            result += "\t" + op + " word [" + r32[base] + " + 0x12], 0x1234\n"
            result += "\t" + op + " word [" + r64[base] + " + 0x12], 0x1234\n"
            result += "\t" + op + " word [" + r32[base] + " + 0x12345678], 0x1234\n"
            result += "\t" + op + " word [" + r64[base] + " + 0x12345678], 0x1234\n"
        for base in range(len(r16)):
            for index in range(len(r16)):
                if validX_r_sib(0, base, index, "1"):
                    result += "\t" + op + " word [" + r32[base] + " + 1 * " + r32[index] + "], 0x1234\n"
                    result += "\t" + op + " word [" + r64[base] + " + 1 * " + r64[index] + "], 0x1234\n"
                    result += "\t" + op + " word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], 0x1234\n"
                    result += "\t" + op + " word [" + r64[base] + " + 1 * " + r64[index] + " + 0x12], 0x1234\n"
                    result += "\t" + op + " word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], 0x1234\n"
                    result += "\t" + op + " word [" + r64[base] + " + 1 * " + r64[index] + " + 0x12345678], 0x1234\n"

        for base in range(len(r32)):
            result += "\t" + op + " " + r32[base] + ", 0x12345678\n"
            result += "\t" + op + " dword [" + r32[base] + "], 0x12345678\n"
            result += "\t" + op + " dword [" + r64[base] + "], 0x12345678\n"
            result += "\t" + op + " dword [" + r32[base] + " + 0x12], 0x12345678\n"
            result += "\t" + op + " dword [" + r64[base] + " + 0x12], 0x12345678\n"
            result += "\t" + op + " dword [" + r32[base] + " + 0x12345678], 0x12345678\n"
            result += "\t" + op + " dword [" + r64[base] + " + 0x12345678], 0x12345678\n"
        for base in range(len(r32)):
            for index in range(len(r32)):
                if validX_r_sib(0, base, index, "1"):
                    result += "\t" + op + " dword [" + r32[base] + " + 1 * " + r32[index] + "], 0x12345678\n"
                    result += "\t" + op + " dword [" + r64[base] + " + 1 * " + r64[index] + "], 0x12345678\n"
                    result += "\t" + op + " dword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], 0x12345678\n"
                    result += "\t" + op + " dword [" + r64[base] + " + 1 * " + r64[index] + " + 0x12], 0x12345678\n"
                    result += "\t" + op + " dword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], 0x12345678\n"
                    result += "\t" + op + " dword [" + r64[base] + " + 1 * " + r64[index] + " + 0x12345678], 0x12345678\n"

        for base in range(len(r64)):
            result += "\t" + op + " " + r64[base] + ", 0x12345678\n"
            result += "\t" + op + " qword [" + r32[base] + "], 0x12345678\n"
            result += "\t" + op + " qword [" + r64[base] + "], 0x12345678\n"
            result += "\t" + op + " qword [" + r32[base] + " + 0x12], 0x12345678\n"
            result += "\t" + op + " qword [" + r64[base] + " + 0x12], 0x12345678\n"
            result += "\t" + op + " qword [" + r32[base] + " + 0x12345678], 0x12345678\n"
            result += "\t" + op + " qword [" + r64[base] + " + 0x12345678], 0x12345678\n"
        for base in range(len(r64)):
            for index in range(len(r64)):
                if validX_r_sib(0, base, index, "1"):
                    result += "\t" + op + " qword [" + r32[base] + " + 1 * " + r32[index] + "], 0x12345678\n"
                    result += "\t" + op + " qword [" + r64[base] + " + 1 * " + r64[index] + "], 0x12345678\n"
                    result += "\t" + op + " qword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], 0x12345678\n"
                    result += "\t" + op + " qword [" + r64[base] + " + 1 * " + r64[index] + " + 0x12], 0x12345678\n"
                    result += "\t" + op + " qword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], 0x12345678\n"
                    result += "\t" + op + " qword [" + r64[base] + " + 1 * " + r64[index] + " + 0x12345678], 0x12345678\n"

        filehandle.write(result)

        gen_r_r(filehandle, op)
        gen_m_r(filehandle, op)
        gen_mdb_r(filehandle, op)
        gen_mdd_r(filehandle, op)
        gen_sib_r(filehandle, op)
        gen_sibdb_r(filehandle, op)
        gen_sibdd_r(filehandle, op)
        gen_disp_r(filehandle, op)
        gen_rel_r(filehandle, op)

class xchg_instruction():
    def test(self, filehandle):
        op = "xchg"

        # note causes diff with nasm because nasm flips them
        gen_r_r(filehandle, op)
        gen_r_m(filehandle, op)
        gen_r_mdb(filehandle, op)
        gen_r_mdd(filehandle, op)
        gen_r_sib(filehandle, op)
        gen_r_sibdb(filehandle, op)
        gen_r_sibdd(filehandle, op)
        gen_r_disp(filehandle, op)
        gen_r_rel(filehandle, op)
        gen_m_r(filehandle, op)
        gen_mdb_r(filehandle, op)
        gen_mdd_r(filehandle, op)
        gen_sib_r(filehandle, op)
        gen_sibdb_r(filehandle, op)
        gen_sibdd_r(filehandle, op)
        gen_disp_r(filehandle, op)
        gen_rel_r(filehandle, op)

class setcc_instruction():
    def test(self, filehandle):
        result = ""
        for op in setcc:
            result += "\t" + op + " byte [0x12345678]\n"
            result += "\t" + op + " byte [rel $ + 0x12345678]\n"
            for dest in r8:
                result += "\t" + op + " " + dest + "\n"
            for base in range(len(r8)):
                result += "\t" + op + " byte [" + r32[base] + "]\n"
                result += "\t" + op + " byte [" + r64[base] + "]\n"
                result += "\t" + op + " byte [" + r32[base] + " + 0x12]\n"
                result += "\t" + op + " byte [" + r64[base] + " + 0x12]\n"
                result += "\t" + op + " byte [" + r32[base] + " + 0x12345678]\n"
                result += "\t" + op + " byte [" + r64[base] + " + 0x12345678]\n"
                for index in range(len(r8)):
                    if valid8_r_sib(0, base, index, "1"):
                        result += "\t" + op + " byte [" + r32[base] + " + 1 * " + r32[index] + "]\n"
                        result += "\t" + op + " byte [" + r64[base] + " + 1 * " + r64[index] + "]\n"
                        result += "\t" + op + " byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12]\n"
                        result += "\t" + op + " byte [" + r64[base] + " + 1 * " + r64[index] + " + 0x12]\n"
                        result += "\t" + op + " byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678]\n"
                        result += "\t" + op + " byte [" + r64[base] + " + 1 * " + r64[index] + " + 0x12345678]\n"
        filehandle.write(result)

def main():
    test_file = open("test.s", "w")
    test_file.write("\tbits 64\n")

    adc = general_2op_instruction("adc")
    add = general_2op_instruction("add")
    and_ = general_2op_instruction("and")
    call = jmp_call_instruction("call")
    cmp_ = general_2op_instruction("cmp")
    dec = general_1op_instruction("dec")
    div = general_1op_instruction("div")
    idiv = general_1op_instruction("idiv")
    imul = general_1op_instruction("imul")
    inc = general_1op_instruction("inc")
    int_ = imm8_instruction("int")
    jmp = jmp_call_instruction("jmp")
    jcc = jcc_instruction()
    lea = lea_instruction()
    mov = mov_instruction()
    cmov = cmov_instruction()
    movsx = movsx_instruction()
    movzx = movzx_instruction()
    mul = general_1op_instruction("mul")
    neg = general_1op_instruction("neg")
    nop = no_operand_instruction("nop")
    not_ = general_1op_instruction("not")
    or_ = general_2op_instruction("or")
    pop = pop_instruction()
    push = push_instruction()
    ret = ret_instruction()
    sal = shift_instruction("sal")
    shl = shift_instruction("shl")
    sar = shift_instruction("sar")
    shr = shift_instruction("shr")
    sbb = general_2op_instruction("sbb")
    sub = general_2op_instruction("sub")
    syscall = no_operand_instruction("syscall")
    test = test_instruction()
    xchg = xchg_instruction()
    xor = general_2op_instruction("xor")
    setcc = setcc_instruction()

    #adc.test(test_file)
    #add.test(test_file)
    #and_.test(test_file)
    #call.test(test_file)
    #cmp_.test(test_file)
    #dec.test(test_file)
    #div.test(test_file)
    #idiv.test(test_file)
    #imul.test(test_file)
    #inc.test(test_file)
    #int_.test(test_file) #note known deviation for int3
    #jmp.test(test_file)
    #jcc.test(test_file)
    #lea.test(test_file)
    #mov.test(test_file) #note known deviation for mov iq
    #cmov.test(test_file)
    #movsx.test(test_file)
    #movzx.test(test_file)
    #mul.test(test_file)
    #neg.test(test_file)
    #nop.test(test_file)
    #not_.test(test_file)
    #or_.test(test_file)
    #pop.test(test_file)
    #push.test(test_file)
    #ret.test(test_file)
    #sal.test(test_file)
    #shl.test(test_file)
    #sar.test(test_file)
    #shr.test(test_file)
    #sbb.test(test_file)
    #sub.test(test_file)
    #syscall.test(test_file)
    #test.test(test_file)
    #xchg.test(test_file) #note known deviation for r_r, nasm flips order
    #xor.test(test_file)
    #setcc.test(test_file)

    test_file.close()

if __name__ == "__main__":
    main()
