#      0      1      2      3      4      5      6      7      8      9      10      11      12      13      14      15
r8 =  ["al",  "cl",  "dl",  "bl",  "ah",  "ch",  "dh",  "bh",  "r8b", "r9b", "r10b", "r11b", "r12b", "r13b", "r14b", "r15b"]
r16 = ["ax",  "cx",  "dx",  "bx",  "sp",  "bp",  "si",  "di",  "r8w", "r9w", "r10w", "r11w", "r12w", "r13w", "r14w", "r15w"]
r32 = ["eax", "ecx", "edx", "ebx", "esp", "ebp", "esi", "edi", "r8d", "r9d", "r10d", "r11d", "r12d", "r13d", "r14d", "r15d"]
r64 = ["rax", "rcx", "rdx", "rbx", "rsp", "rbp", "rsi", "rdi", "r8",  "r9",  "r10",  "r11",  "r12",  "r13",  "r14",  "r15"]

jcc = ["jo", "jno", "jb", "jc", "jnae", "jae", "jnb", "jnc", "je", "jz", "jne", "jnz", "jne", "jnz", "jbe", "jna", \
    "ja", "jnbe", "js", "jns", "jp", "jpe", "jpo", "jnp", "jl", "jnge", "jge", "jnl", "jle", "jng", "jg", "jnle" ]

# section gen_r_x
# {{{

# section gen_r
# {{{

def gen8_r(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r8)):
        #if valid8_r(dest, src):
            result += "\t" + op + " " + r8[src] + "\n"
    file_handle.write(result)

def gen16_r(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r16)):
        result += "\t" + op + " " + r16[src] + "\n"
    file_handle.write(result)

def gen32_r(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r32)):
        result += "\t" + op + " " + r32[src] + "\n"
    file_handle.write(result)

def gen64_r(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r64)):
        result += "\t" + op + " " + r64[src] + "\n"
    file_handle.write(result)

# }}}

# section gen_r_ix
# {{{

def gen8_r_ix(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r8)):
#        if valid8_r(dest, src):
        result += "\t" + op + " " + r8[src] + ", 0x12\n"
    file_handle.write(result)

def gen16_r_ix(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r16)):
        result += "\t" + op + " " + r16[src] + ", 0x12\n"
        result += "\t" + op + " " + r16[src] + ", 0x1234\n"
    file_handle.write(result)

def gen32_r_ix(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r32)):
        result += "\t" + op + " " + r32[src] + ", 0x12\n"
        result += "\t" + op + " " + r32[src] + ", 0x12345678\n"
    file_handle.write(result)

def gen64_r_ix(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r64)):
        result += "\t" + op + " " + r64[src] + ", 0x12\n"
        result += "\t" + op + " " + r64[src] + ", 0x12345678\n"
    file_handle.write(result)

# }}}

def valid8_r_r(dest, src):
    # dest in ["ah", "ch", "dh", "bh"] and src in ["r8" -> "r15"]
    if dest >= 4 and dest < 8 and src >= 8:
        return False
    if src >= 4 and src < 8 and dest >= 8:
        return False

    return True

# section gen_r_r
# {{{

def gen8_r_r(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r8)):
        for src in range(len(r8)):
            if valid8_r_r(dest, src):
                result += "\t" + op + " " + r8[dest] + ", " + r8[src] + "\n"
    file_handle.write(result)

def gen16_r_r(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r16)):
        for src in range(len(r16)):
            result += "\t" + op + " " + r16[dest] + ", " + r16[src] + "\n"
    file_handle.write(result)

def gen32_r_r(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r32)):
        for src in range(len(r32)):
            result += "\t" + op + " " + r32[dest] + ", " + r32[src] + "\n"
    file_handle.write(result)

def gen64_r_r(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r64)):
        for src in range(len(r64)):
            result += "\t" + op + " " + r64[dest] + ", " + r64[src] + "\n"
    file_handle.write(result)

# }}}

# section gen_r_m
# {{{

def gen8_r_m(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r8)):
        for src in range(len(r8)):
            if valid8_r_r(dest, src):
                result += "\t" + op + " " + r8[dest] + ", byte [" + r32[src] + "]\n"
                result += "\t" + op + " " + r8[dest] + ", byte [" + r64[src] + "]\n"
    file_handle.write(result)

def gen16_r_m(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r16)):
        for src in range(len(r16)):
            result += "\t" + op + " " + r16[dest] + ", word [" + r32[src] + "]\n"
            result += "\t" + op + " " + r16[dest] + ", word [" + r64[src] + "]\n"
    file_handle.write(result)

def gen32_r_m(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r32)):
        for src in range(len(r32)):
            result += "\t" + op + " " + r32[dest] + ", dword [" + r32[src] + "]\n"
            result += "\t" + op + " " + r32[dest] + ", dword [" + r64[src] + "]\n"
    file_handle.write(result)

def gen64_r_m(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r64)):
        for src in range(len(r64)):
            result += "\t" + op + " " + r64[dest] + ", qword [" + r32[src] + "]\n"
            result += "\t" + op + " " + r64[dest] + ", qword [" + r64[src] + "]\n"
    file_handle.write(result)

# }}}

# section gen_r_mdb
# {{{

def gen8_r_mdb(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r8)):
        for src in range(len(r8)):
            if valid8_r_r(dest, src):
                result += "\t" + op + " " + r8[dest] + ", byte [" + r32[src] + " + 0x12" + "]\n"
                result += "\t" + op + " " + r8[dest] + ", byte [" + r64[src] + " + 0x12" + "]\n"
    file_handle.write(result)

def gen16_r_mdb(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r16)):
        for src in range(len(r16)):
            result += "\t" + op + " " + r16[dest] + ", word [" + r32[src] + " + 0x12" + "]\n"
            result += "\t" + op + " " + r16[dest] + ", word [" + r64[src] + " + 0x12" + "]\n"
    file_handle.write(result)
    
def gen32_r_mdb(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r32)):
        for src in range(len(r32)):
            result += "\t" + op + " " + r32[dest] + ", dword [" + r32[src] + " + 0x12" + "]\n"
            result += "\t" + op + " " + r32[dest] + ", dword [" + r64[src] + " + 0x12" + "]\n"
    file_handle.write(result)

def gen64_r_mdb(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r64)):
        for src in range(len(r64)):
            result += "\t" + op + " " + r64[dest] + ", qword [" + r32[src] + " + 0x12" + "]\n"
            result += "\t" + op + " " + r64[dest] + ", qword [" + r64[src] + " + 0x12" + "]\n"
    file_handle.write(result)

# }}}

# section gen_r_mdd
# {{{

def gen8_r_mdd(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r8)):
        for src in range(len(r8)):
            if valid8_r_r(dest, src):
                result += "\t" + op + " " + r8[dest] + ", byte [" + r32[src] + " + 0x1234" + "]\n"
                result += "\t" + op + " " + r8[dest] + ", byte [" + r64[src] + " + 0x1234" + "]\n"
    file_handle.write(result)

def gen16_r_mdd(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r16)):
        for src in range(len(r16)):
            result += "\t" + op + " " + r16[dest] + ", word [" + r32[src] + " + 0x1234" + "]\n"
            result += "\t" + op + " " + r16[dest] + ", word [" + r64[src] + " + 0x1234" + "]\n"
    file_handle.write(result)
    
def gen32_r_mdd(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r32)):
        for src in range(len(r32)):
            result += "\t" + op + " " + r32[dest] + ", dword [" + r32[src] + " + 0x1234" + "]\n"
            result += "\t" + op + " " + r32[dest] + ", dword [" + r64[src] + " + 0x1234" + "]\n"
    file_handle.write(result)

def gen64_r_mdd(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r64)):
        for src in range(len(r64)):
            result += "\t" + op + " " + r64[dest] + ", qword [" + r32[src] + " + 0x1234" + "]\n"
            result += "\t" + op + " " + r64[dest] + ", qword [" + r64[src] + " + 0x1234" + "]\n"
    file_handle.write(result)

# }}}

def valid8_r_sib(dest, base, index):
    # if base == index and base == 4: # cannot use rsp as base and index
    #     return False
    if index == 4:
        return False
    elif dest >= 4 and dest < 8 and index >= 8:
        return False
    elif dest >= 4 and dest < 8 and base >= 8:
        return False
    else:
        return True

def validX_r_sib(dest, base, index):
    if index == 4:
        return False
    else:
        return True

# section gen_r_sib
# {{{

def gen8_r_sib(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r8)):
        for base in range(len(r8)):
            for index in range(len(r8)):
                if valid8_r_sib(dest, base, index):
                    result += "\t" + op + " " + r8[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + "]\n"
                    result += "\t" + op + " " + r8[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + "]\n"
    file_handle.write(result)

def gen16_r_sib(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r16)):
        for base in range(len(r16)):
            for index in range(len(r16)):
                if validX_r_sib(dest, base, index):
                    result += "\t" + op + " " + r16[dest] + ", word [" + r32[base] + " + 2 * " + r32[index] + "]\n"
                    result += "\t" + op + " " + r16[dest] + ", word [" + r64[base] + " + 2 * " + r64[index] + "]\n"
    file_handle.write(result)
    
def gen32_r_sib(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r32)):
        for base in range(len(r32)):
            for index in range(len(r32)):
                if validX_r_sib(dest, base, index):
                    result += "\t" + op + " " + r32[dest] + ", dword [" + r32[base] + " + 4 * " + r32[index] + "]\n"
                    result += "\t" + op + " " + r32[dest] + ", dword [" + r64[base] + " + 4 * " + r64[index] + "]\n"
    file_handle.write(result)

def gen64_r_sib(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r64)):
        for base in range(len(r64)):
            for index in range(len(r64)):
                if validX_r_sib(dest, base, index):
                    result += "\t" + op + " " + r64[dest] + ", qword [" + r32[base] + " + 8 * " + r32[index] + "]\n"
                    result += "\t" + op + " " + r64[dest] + ", qword [" + r64[base] + " + 8 * " + r64[index] + "]\n"
    file_handle.write(result)

# }}}

# section gen_r_sibdb
# {{{

def gen8_r_sibdb(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r8)):
        for base in range(len(r8)):
            for index in range(len(r8)):
                if valid8_r_sib(dest, base, index):
                    result += "\t" + op + " " + r8[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12" + "]\n"
                    result += "\t" + op + " " + r8[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + " + 0x12" + "]\n"
    file_handle.write(result)

def gen16_r_sibdb(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r16)):
        for base in range(len(r16)):
            for index in range(len(r16)):
                if validX_r_sib(dest, base, index):
                    result += "\t" + op + " " + r16[dest] + ", word [" + r32[base] + " + 2 * " + r32[index] + " + 0x12" + "]\n"
                    result += "\t" + op + " " + r16[dest] + ", word [" + r64[base] + " + 2 * " + r64[index] + " + 0x12" + "]\n"
    file_handle.write(result)
    
def gen32_r_sibdb(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r32)):
        for base in range(len(r32)):
            for index in range(len(r32)):
                if validX_r_sib(dest, base, index):
                    result += "\t" + op + " " + r32[dest] + ", dword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12" + "]\n"
                    result += "\t" + op + " " + r32[dest] + ", dword [" + r64[base] + " + 4 * " + r64[index] + " + 0x12" + "]\n"
    file_handle.write(result)

def gen64_r_sibdb(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r64)):
        for base in range(len(r64)):
            for index in range(len(r64)):
                if validX_r_sib(dest, base, index):
                    result += "\t" + op + " " + r64[dest] + ", qword [" + r32[base] + " + 8 * " + r32[index] + " + 0x12" + "]\n"
                    result += "\t" + op + " " + r64[dest] + ", qword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12" + "]\n"
    file_handle.write(result)

# }}}

# section gen_r_sibdd
# {{{

def gen8_r_sibdd(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r8)):
        for base in range(len(r8)):jcc = ["jo", "jno", "jb", "jc", "jnae", "jae", "jnb", "jnc", "je", "jz", "jne", "jnz", "jne", "jnz", "jbe", "jna", \
    "ja", "jnbe", "js", "jns", "jp", "jpe", "jpo", "jnp", "jl", "jnge", "jge", "jnl", "jle", "jng", "jg", "jnle" ]
                if valid8_r_sib(dest, base, index):
                    result += "\t" + op + " " + r8[dest] + ", byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678" + "]\n"
                    result += "\t" + op + " " + r8[dest] + ", byte [" + r64[base] + " + 1 * " + r64[index] + " + 0x12345678" + "]\n"
    file_handle.write(result)

def gen16_r_sibdd(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r16)):
        for base in range(len(r16)):
            for index in range(len(r16)):
                if validX_r_sib(dest, base, index):
                    result += "\t" + op + " " + r16[dest] + ", word [" + r32[base] + " + 2 * " + r32[index] + " + 0x12345678" + "]\n"
                    result += "\t" + op + " " + r16[dest] + ", word [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678" + "]\n"
    file_handle.write(result)
    
def gen32_r_sibdd(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r32)):
        for base in range(len(r32)):
            for index in range(len(r32)):
                if validX_r_sib(dest, base, index):
                    result += "\t" + op + " " + r32[dest] + ", dword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678" + "]\n"
                    result += "\t" + op + " " + r32[dest] + ", dword [" + r64[base] + " + 4 * " + r64[index] + " + 0x12345678" + "]\n"
    file_handle.write(result)

def gen64_r_sibdd(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r64)):
        for base in range(len(r64)):
            for index in range(len(r64)):
                if validX_r_sib(dest, base, index):
                    result += "\t" + op + " " + r64[dest] + ", qword [" + r32[base] + " + 8 * " + r32[index] + " + 0x12345678" + "]\n"
                    result += "\t" + op + " " + r64[dest] + ", qword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678" + "]\n"
    file_handle.write(result)

# }}}

# section gen_r_disp
# {{{

def gen8_r_disp(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r8)):
        result += "\t" + op + " " + r8[dest] + ", byte [0x12345678]\n"
    file_handle.write(result)

def gen16_r_disp(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r16)):
        result += "\t" + op + " " + r16[dest] + ", word [0x12345678]\n"
    file_handle.write(result)

def gen32_r_disp(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r32)):
        result += "\t" + op + " " + r32[dest] + ", dword [0x12345678]\n"
    file_handle.write(result)

def gen64_r_disp(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r64)):
        result += "\t" + op + " " + r64[dest] + ", qword [0x12345678]\n"
    file_handle.write(result)

# }}}

# section gen_r_rip_disp
# {{{

# #note in ia assembler, currently rip relative will only be accessible internally for PiC

# def gen8_r_rip_disp(file_handle, op):
#     result = "\tbits 64\n"
#     for dest in range(len(r8)):
#         result += "\t" + op + " " + r8[dest] + ", byte [0x12345678]\n"
#     file_handle.write(result)

# def gen16_r_rip_disp(file_handle, op):
#     result = "\tbits 64\n"
#     for dest in range(len(r16)):
#         result += "\t" + op + " " + r16[dest] + ", word [0x12345678]\n"
#     file_handle.write(result)

# def gen32_r_rip_disp(file_handle, op):
#     result = "\tbits 64\n"
#     for dest in range(len(r32)):
#         result += "\t" + op + " " + r32[dest] + ", dword [0x12345678]\n"
#     file_handle.write(result)

# def gen64_r_rip_disp(file_handle, op):
#     result = "\tbits 64\n"
#     for dest in range(len(r64)):
#         result += "\t" + op + " " + r64[dest] + ", qword [0x12345678]\n"
#     file_handle.write(result)

# }}}
#}}}

# sertion gen_m_x
# {{{
# section gen_m
# {{{

def gen8_m(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r8)):
#        if valid8_r(dest, src):
        result += "\t" + op + " byte [" + r32[src] + "]\n"
        result += "\t" + op + " byte [" + r64[src] + "]\n"
    file_handle.write(result)

def gen16_m(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r16)):
        result += "\t" + op + " word [" + r32[src] + "]\n"
        result += "\t" + op + " word [" + r64[src] + "]\n"
    file_handle.write(result)

def gen32_m(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r32)):
        result += "\t" + op + " dword [" + r32[src] + "]\n"
        result += "\t" + op + " dword [" + r64[src] + "]\n"
    file_handle.write(result)

def gen64_m(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r64)):
        result += "\t" + op + " qword [" + r32[src] + "]\n"
        result += "\t" + op + " qword [" + r64[src] + "]\n"
    file_handle.write(result)

# }}}
# section gen_m_r
# {{{

def gen8_m_r(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r8)):
        for src in range(len(r8)):
            if valid8_r_r(dest, src):
                result += "\t" + op + " byte [" + r32[dest] + "], " + r8[src] + "\n"
                result += "\t" + op + " byte [" + r64[dest] + "], " + r8[src] + "\n"
    file_handle.write(result)

def gen16_m_r(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r16)):
        for src in range(len(r16)):
            result += "\t" + op + " word [" + r32[dest] + "], " + r16[src] + "\n"
            result += "\t" + op + " word [" + r64[dest] + "], " + r16[src] + "\n"
    file_handle.write(result)

def gen32_m_r(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r32)):
        for src in range(len(r32)):
            result += "\t" + op + " dword [" + r32[dest] + "], " + r32[src] + "\n"
            result += "\t" + op + " dword [" + r64[dest] + "], " + r32[src] + "\n"
    file_handle.write(result)

def gen64_m_r(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r64)):
        for src in range(len(r64)):
            result += "\t" + op + " qword [" + r32[dest] + "], " + r64[src] + "\n"
            result += "\t" + op + " qword [" + r64[dest] + "], " + r64[src] + "\n"
    file_handle.write(result)

# }}}
# section gen_m_r
# {{{

def gen8_m_ix(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r8)):
        result += "\t" + op + " byte [" + r32[dest] + "], 0x12\n"
        result += "\t" + op + " byte [" + r64[dest] + "], 0x12\n"
    file_handle.write(result)

def gen16_m_ix(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r16)):
        result += "\t" + op + " word [" + r32[dest] + "], 0x12\n"
        result += "\t" + op + " word [" + r64[dest] + "], 0x12\n"
        result += "\t" + op + " word [" + r32[dest] + "], 0x1234\n"
        result += "\t" + op + " word [" + r64[dest] + "], 0x1234\n"
    file_handle.write(result)

def gen32_m_ix(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r32)):
        result += "\t" + op + " dword [" + r32[dest] + "], 0x12\n"
        result += "\t" + op + " dword [" + r64[dest] + "], 0x12\n"
        result += "\t" + op + " dword [" + r32[dest] + "], 0x12345678\n"
        result += "\t" + op + " dword [" + r64[dest] + "], 0x12345678\n"
    file_handle.write(result)

def gen64_m_ix(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r64)):
        result += "\t" + op + " qword [" + r32[dest] + "], 0x12\n"
        result += "\t" + op + " qword [" + r64[dest] + "], 0x12\n"
        result += "\t" + op + " qword [" + r32[dest] + "], 0x12345678\n"
        result += "\t" + op + " qword [" + r64[dest] + "], 0x12345678\n"
    file_handle.write(result)

# }}}

# }}}

# sertion gen_mdx_x
# {{{
# section gen_mdx
# {{{

def gen8_mdx(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r8)):
#        if valid8_r(dest, src):
        result += "\t" + op + " byte [" + r32[src] + " + 0x12]\n"
        result += "\t" + op + " byte [" + r64[src] + " + 0x12]\n"
        result += "\t" + op + " byte [" + r32[src] + " + 0x12345678]\n"
        result += "\t" + op + " byte [" + r64[src] + " + 0x12345678]\n"
    file_handle.write(result)

def gen16_mdx(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r16)):
        result += "\t" + op + " word [" + r32[src] + " + 0x12]\n"
        result += "\t" + op + " word [" + r64[src] + " + 0x12]\n"
        result += "\t" + op + " word [" + r32[src] + " + 0x12345678]\n"
        result += "\t" + op + " word [" + r64[src] + " + 0x12345678]\n"
    file_handle.write(result)

def gen32_mdx(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r32)):
        result += "\t" + op + " dword [" + r32[src] + " + 0x12]\n"
        result += "\t" + op + " dword [" + r64[src] + " + 0x12]\n"
        result += "\t" + op + " dword [" + r32[src] + " + 0x12345678]\n"
        result += "\t" + op + " dword [" + r64[src] + " + 0x12345678]\n"
    file_handle.write(result)

def gen64_mdx(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r64)):
        result += "\t" + op + " qword [" + r32[src] + " + 0x12]\n"
        result += "\t" + op + " qword [" + r64[src] + " + 0x12]\n"
        result += "\t" + op + " qword [" + r32[src] + " + 0x12345678]\n"
        result += "\t" + op + " qword [" + r64[src] + " + 0x12345678]\n"
    file_handle.write(result)

# }}}

# section gen_mdx
# {{{

def gen8_mdx_r(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r8)):
        for src in range(len(r8)):
            if valid8_r_r(dest, src):
                result += "\t" + op + " byte [" + r32[dest] + " + 0x12], " + r8[src] + "\n"
                result += "\t" + op + " byte [" + r64[dest] + " + 0x12], " + r8[src] + "\n"
                result += "\t" + op + " byte [" + r32[dest] + " + 0x12345678], " + r8[src] + "\n"
                result += "\t" + op + " byte [" + r64[dest] + " + 0x12345678], " + r8[src] + "\n"
    file_handle.write(result)

def gen16_mdx_r(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r16)):
        for src in range(len(r16)):
            result += "\t" + op + " word [" + r32[dest] + " + 0x12], " + r16[src] + "\n"
            result += "\t" + op + " word [" + r64[dest] + " + 0x12], " + r16[src] + "\n"
            result += "\t" + op + " word [" + r32[dest] + " + 0x12345678], " + r16[src] + "\n"
            result += "\t" + op + " word [" + r64[dest] + " + 0x12345678], " + r16[src] + "\n"
    file_handle.write(result)

def gen32_mdx_r(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r32)):
        for src in range(len(r32)):
            result += "\t" + op + " dword [" + r32[dest] + " + 0x12], " + r32[src] + "\n"
            result += "\t" + op + " dword [" + r64[dest] + " + 0x12], " + r32[src] + "\n"
            result += "\t" + op + " dword [" + r32[dest] + " + 0x12345678], " + r32[src] + "\n"
            result += "\t" + op + " dword [" + r64[dest] + " + 0x12345678], " + r32[src] + "\n"
    file_handle.write(result)

def gen64_mdx_r(file_handle, op):
    result = "\tbits 64\n"
    for dest in range(len(r64)):
        for src in range(len(r64)):
            result += "\t" + op + " qword [" + r32[dest] + " + 0x12], " + r64[src] + "\n"
            result += "\t" + op + " qword [" + r64[dest] + " + 0x12], " + r64[src] + "\n"
            result += "\t" + op + " qword [" + r32[dest] + " + 0x12345678], " + r64[src] + "\n"
            result += "\t" + op + " qword [" + r64[dest] + " + 0x12345678], " + r64[src] + "\n"
    file_handle.write(result)

# }}}

# section gen_mdx
# {{{

def gen8_mdx_ix(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r8)):
#        if valid8_r(dest, src):
        result += "\t" + op + " byte [" + r32[src] + " + 0x12], 0x12\n"
        result += "\t" + op + " byte [" + r64[src] + " + 0x12], 0x12\n"
        result += "\t" + op + " byte [" + r32[src] + " + 0x12345678], 0x12\n"
        result += "\t" + op + " byte [" + r64[src] + " + 0x12345678], 0x12\n"
    file_handle.write(result)

def gen16_mdx_ix(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r16)):
        result += "\t" + op + " word [" + r32[src] + " + 0x12], 0x12\n"
        result += "\t" + op + " word [" + r64[src] + " + 0x12], 0x12\n"
        result += "\t" + op + " word [" + r32[src] + " + 0x12345678], 0x12\n"
        result += "\t" + op + " word [" + r64[src] + " + 0x12345678], 0x12\n"
        result += "\t" + op + " word [" + r32[src] + " + 0x12], 0x1234\n"
        result += "\t" + op + " word [" + r64[src] + " + 0x12], 0x1234\n"
        result += "\t" + op + " word [" + r32[src] + " + 0x12345678], 0x1234\n"
        result += "\t" + op + " word [" + r64[src] + " + 0x12345678], 0x1234\n"
    file_handle.write(result)

def gen32_mdx_ix(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r32)):
        result += "\t" + op + " dword [" + r32[src] + " + 0x12], 0x12\n"
        result += "\t" + op + " dword [" + r64[src] + " + 0x12], 0x12\n"
        result += "\t" + op + " dword [" + r32[src] + " + 0x12345678], 0x12\n"
        result += "\t" + op + " dword [" + r64[src] + " + 0x12345678], 0x12\n"
        result += "\t" + op + " dword [" + r32[src] + " + 0x12], 0x12345678\n"
        result += "\t" + op + " dword [" + r64[src] + " + 0x12], 0x12345678\n"
        result += "\t" + op + " dword [" + r32[src] + " + 0x12345678], 0x12345678\n"
        result += "\t" + op + " dword [" + r64[src] + " + 0x12345678], 0x12345678\n"
    file_handle.write(result)

def gen64_mdx_ix(file_handle, op):
    result = "\tbits 64\n"
    for src in range(len(r64)):
        result += "\t" + op + " qword [" + r32[src] + " + 0x12], 0x12\n"
        result += "\t" + op + " qword [" + r64[src] + " + 0x12], 0x12\n"
        result += "\t" + op + " qword [" + r32[src] + " + 0x12345678], 0x12\n"
        result += "\t" + op + " qword [" + r64[src] + " + 0x12345678], 0x12\n"
        result += "\t" + op + " qword [" + r32[src] + " + 0x12], 0x12345678\n"
        result += "\t" + op + " qword [" + r64[src] + " + 0x12], 0x12345678\n"
        result += "\t" + op + " qword [" + r32[src] + " + 0x12345678], 0x12345678\n"
        result += "\t" + op + " qword [" + r64[src] + " + 0x12345678], 0x12345678\n"
    file_handle.write(result)

# }}}

# }}}

# sertion gen_sib_x
# {{{
# section gen_sib
# {{{

def gen8_sib(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r8)):
        for index in range(len(r8)):
#        if valid8_r(dest, src):
            if index != 4 and index != 12: # no RSP or R12 as index
                result += "\t" + op + " byte [" + r32[base] + " + 1 * " + r32[index] + "]\n"
                result += "\t" + op + " byte [" + r64[base] + " + 2 * " + r64[index] + "]\n"
                result += "\t" + op + " byte [" + r32[base] + " + 4 * " + r32[index] + "]\n"
                result += "\t" + op + " byte [" + r64[base] + " + 8 * " + r64[index] + "]\n"
    file_handle.write(result)

def gen16_sib(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r16)):
        for index in range(len(r16)):
            if index != 4 and index != 12: # no RSP or R12 as index
                result += "\t" + op + " word [" + r32[base] + " + 1 * " + r32[index] + "]\n"
                result += "\t" + op + " word [" + r64[base] + " + 2 * " + r64[index] + "]\n"
                result += "\t" + op + " word [" + r32[base] + " + 4 * " + r32[index] + "]\n"
                result += "\t" + op + " word [" + r64[base] + " + 8 * " + r64[index] + "]\n"
    file_handle.write(result)

def gen32_sib(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r32)):
        for index in range(len(r32)):
            if index != 4 and index != 12: # no RSP or R12 as index
                result += "\t" + op + " dword [" + r32[base] + " + 1 * " + r32[index] + "]\n"
                result += "\t" + op + " dword [" + r64[base] + " + 2 * " + r64[index] + "]\n"
                result += "\t" + op + " dword [" + r32[base] + " + 4 * " + r32[index] + "]\n"
                result += "\t" + op + " dword [" + r64[base] + " + 8 * " + r64[index] + "]\n"
    file_handle.write(result)

def gen64_sib(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r64)):
        for index in range(len(r64)):
            if index != 4 and index != 12: # no RSP or R12 as index
                result += "\t" + op + " qword [" + r32[base] + " + 1 * " + r32[index] + "]\n"
                result += "\t" + op + " qword [" + r64[base] + " + 2 * " + r64[index] + "]\n"
                result += "\t" + op + " qword [" + r32[base] + " + 4 * " + r32[index] + "]\n"
                result += "\t" + op + " qword [" + r64[base] + " + 8 * " + r64[index] + "]\n"
    file_handle.write(result)

# }}}

# section gen_sib_r
# {{{

def gen8_sib_r(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r8)):
        for index in range(len(r8)):
            for src in range(len(r8)):
                if r8[src] not in ["ah", "ch", "dh", "bh"]:
                    if index != 4 and index != 12: # no RSP or R12 as index
                        result += "\t" + op + " byte [" + r32[base] + " + 1 * " + r32[index] + "], " +  r8[src] + "\n"
                        result += "\t" + op + " byte [" + r64[base] + " + 2 * " + r64[index] + "], " +  r8[src] + "\n"
                        result += "\t" + op + " byte [" + r32[base] + " + 4 * " + r32[index] + "], " +  r8[src] + "\n"
                        result += "\t" + op + " byte [" + r64[base] + " + 8 * " + r64[index] + "], " +  r8[src] + "\n"
    file_handle.write(result)

def gen16_sib_r(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r16)):
        for index in range(len(r16)):
            for src in range(len(r16)):
                if index != 4 and index != 12: # no RSP or R12 as index
                    result += "\t" + op + " word [" + r32[base] + " + 1 * " + r32[index] + "], " +  r16[src] + "\n"
                    result += "\t" + op + " word [" + r64[base] + " + 2 * " + r64[index] + "], " +  r16[src] + "\n"
                    result += "\t" + op + " word [" + r32[base] + " + 4 * " + r32[index] + "], " +  r16[src] + "\n"
                    result += "\t" + op + " word [" + r64[base] + " + 8 * " + r64[index] + "], " +  r16[src] + "\n"
    file_handle.write(result)

def gen32_sib_r(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r32)):
        for index in range(len(r32)):
            for src in range(len(r32)):
                if index != 4 and index != 12: # no RSP or R12 as index
                    result += "\t" + op + " dword [" + r32[base] + " + 1 * " + r32[index] + "], " +  r32[src] + "\n"
                    result += "\t" + op + " dword [" + r64[base] + " + 2 * " + r64[index] + "], " +  r32[src] + "\n"
                    result += "\t" + op + " dword [" + r32[base] + " + 4 * " + r32[index] + "], " +  r32[src] + "\n"
                    result += "\t" + op + " dword [" + r64[base] + " + 8 * " + r64[index] + "], " +  r32[src] + "\n"
    file_handle.write(result)

def gen64_sib_r(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r64)):
        for index in range(len(r64)):
            for src in range(len(r64)):
                if index != 4 and index != 12: # no RSP or R12 as index
                    result += "\t" + op + " qword [" + r32[base] + " + 1 * " + r32[index] + "], " +  r64[src] + "\n"
                    result += "\t" + op + " qword [" + r64[base] + " + 2 * " + r64[index] + "], " +  r64[src] + "\n"
                    result += "\t" + op + " qword [" + r32[base] + " + 4 * " + r32[index] + "], " +  r64[src] + "\n"
                    result += "\t" + op + " qword [" + r64[base] + " + 8 * " + r64[index] + "], " +  r64[src] + "\n"
    file_handle.write(result)

# }}}
# section gen_sib_ix
# {{{

def gen8_sib_ix(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r8)):
        for index in range(len(r8)):
            if index != 4 and index != 12: # no RSP or R12 as index
                result += "\t" + op + " byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " byte [" + r64[base] + " + 2 * " + r64[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " byte [" + r32[base] + " + 4 * " + r32[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " byte [" + r64[base] + " + 8 * " + r64[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " byte [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " byte [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " byte [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678], 0x12\n"
    file_handle.write(result)

def gen16_sib_ix(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r16)):
        for index in range(len(r16)):
            if index != 4 and index != 12: # no RSP or R12 as index
                result += "\t" + op + " word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " word [" + r64[base] + " + 2 * " + r64[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " word [" + r32[base] + " + 4 * " + r32[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " word [" + r64[base] + " + 8 * " + r64[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], 0x1234\n"
                result += "\t" + op + " word [" + r64[base] + " + 2 * " + r64[index] + " + 0x12], 0x1234\n"
                result += "\t" + op + " word [" + r32[base] + " + 4 * " + r32[index] + " + 0x12], 0x1234\n"
                result += "\t" + op + " word [" + r64[base] + " + 8 * " + r64[index] + " + 0x12], 0x1234\n"
                result += "\t" + op + " word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " word [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " word [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " word [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], 0x1234\n"
                result += "\t" + op + " word [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678], 0x1234\n"
                result += "\t" + op + " word [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678], 0x1234\n"
                result += "\t" + op + " word [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678], 0x1234\n"
    file_handle.write(result)

def gen32_sib_ix(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r32)):
        for index in range(len(r32)):
            if index != 4 and index != 12: # no RSP or R12 as index
                result += "\t" + op + " dword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " dword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " dword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " dword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " dword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], 0x12345678\n"
                result += "\t" + op + " dword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12], 0x12345678\n"
                result += "\t" + op + " dword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12], 0x12345678\n"
                result += "\t" + op + " dword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12], 0x12345678\n"
                result += "\t" + op + " dword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " dword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " dword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " dword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " dword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], 0x12345678\n"
                result += "\t" + op + " dword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678], 0x12345678\n"
                result += "\t" + op + " dword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678], 0x12345678\n"
                result += "\t" + op + " dword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678], 0x12345678\n"
    file_handle.write(result)

def gen64_sib_ix(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r64)):
        for index in range(len(r64)):
            if index != 4 and index != 12: # no RSP or R12 as index
                result += "\t" + op + " qword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " qword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " qword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " qword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " qword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], 0x12345678\n"
                result += "\t" + op + " qword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12], 0x12345678\n"
                result += "\t" + op + " qword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12], 0x12345678\n"
                result += "\t" + op + " qword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12], 0x12345678\n"
                result += "\t" + op + " qword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " qword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " qword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " qword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " qword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], 0x12345678\n"
                result += "\t" + op + " qword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678], 0x12345678\n"
                result += "\t" + op + " qword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678], 0x12345678\n"
                result += "\t" + op + " qword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678], 0x12345678\n"
    file_handle.write(result)

# }}}
# }}}

# sertion gen_sibdx_x
# {{{
# section gen_sibdx
# {{{

def gen8_sibdx(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r8)):
        for index in range(len(r8)):
#        if valid8_r(dest, src):
            if index != 4 and index != 12: # no RSP or R12 as index
                result += "\t" + op + " byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12]\n"
                result += "\t" + op + " byte [" + r64[base] + " + 2 * " + r64[index] + " + 0x12]\n"
                result += "\t" + op + " byte [" + r32[base] + " + 4 * " + r32[index] + " + 0x12]\n"
                result += "\t" + op + " byte [" + r64[base] + " + 8 * " + r64[index] + " + 0x12]\n"
                result += "\t" + op + " byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678]\n"
                result += "\t" + op + " byte [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678]\n"
                result += "\t" + op + " byte [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678]\n"
                result += "\t" + op + " byte [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678]\n"
    file_handle.write(result)

def gen16_sibdx(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r16)):
        for index in range(len(r16)):
            if index != 4 and index != 12: # no RSP or R12 as index
                result += "\t" + op + " word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12]\n"
                result += "\t" + op + " word [" + r64[base] + " + 2 * " + r64[index] + " + 0x12]\n"
                result += "\t" + op + " word [" + r32[base] + " + 4 * " + r32[index] + " + 0x12]\n"
                result += "\t" + op + " word [" + r64[base] + " + 8 * " + r64[index] + " + 0x12]\n"
                result += "\t" + op + " word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678]\n"
                result += "\t" + op + " word [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678]\n"
                result += "\t" + op + " word [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678]\n"
                result += "\t" + op + " word [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678]\n"
    file_handle.write(result)

def gen32_sibdx(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r32)):
        for index in range(len(r32)):
            if index != 4 and index != 12: # no RSP or R12 as index
                result += "\t" + op + " dword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12]\n"
                result += "\t" + op + " dword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12]\n"
                result += "\t" + op + " dword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12]\n"
                result += "\t" + op + " dword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12]\n"
                result += "\t" + op + " dword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678]\n"
                result += "\t" + op + " dword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678]\n"
                result += "\t" + op + " dword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678]\n"
                result += "\t" + op + " dword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678]\n"
    file_handle.write(result)

def gen64_sibdx(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r64)):
        for index in range(len(r64)):
            if index != 4 and index != 12: # no RSP or R12 as index
                result += "\t" + op + " qword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12]\n"
                result += "\t" + op + " qword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12]\n"
                result += "\t" + op + " qword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12]\n"
                result += "\t" + op + " qword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12]\n"
                result += "\t" + op + " qword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678]\n"
                result += "\t" + op + " qword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678]\n"
                result += "\t" + op + " qword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678]\n"
                result += "\t" + op + " qword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678]\n"
    file_handle.write(result)

# }}}

# section gen_sibdx_r
# {{{

def gen8_sibdx_r(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r8)):
        for index in range(len(r8)):
            for src in range(len(r8)):
                if r8[src] not in ["ah", "ch", "dh", "bh"]:
                    if index != 4 and index != 12: # no RSP or R12 as index
                        result += "\t" + op + " byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], " +  r8[src] + "\n"
                        result += "\t" + op + " byte [" + r64[base] + " + 2 * " + r64[index] + " + 0x12], " +  r8[src] + "\n"
                        result += "\t" + op + " byte [" + r32[base] + " + 4 * " + r32[index] + " + 0x12], " +  r8[src] + "\n"
                        result += "\t" + op + " byte [" + r64[base] + " + 8 * " + r64[index] + " + 0x12], " +  r8[src] + "\n"
                        result += "\t" + op + " byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], " +  r8[src] + "\n"
                        result += "\t" + op + " byte [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678], " +  r8[src] + "\n"
                        result += "\t" + op + " byte [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678], " +  r8[src] + "\n"
                        result += "\t" + op + " byte [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678], " +  r8[src] + "\n"
    file_handle.write(result)

def gen16_sibdx_r(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r16)):
        for index in range(len(r16)):
            for src in range(len(r16)):
                if index != 4 and index != 12: # no RSP or R12 as index
                    result += "\t" + op + " word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], " +  r16[src] + "\n"
                    result += "\t" + op + " word [" + r64[base] + " + 2 * " + r64[index] + " + 0x12], " +  r16[src] + "\n"
                    result += "\t" + op + " word [" + r32[base] + " + 4 * " + r32[index] + " + 0x12], " +  r16[src] + "\n"
                    result += "\t" + op + " word [" + r64[base] + " + 8 * " + r64[index] + " + 0x12], " +  r16[src] + "\n"
                    result += "\t" + op + " word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], " +  r16[src] + "\n"
                    result += "\t" + op + " word [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678], " +  r16[src] + "\n"
                    result += "\t" + op + " word [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678], " +  r16[src] + "\n"
                    result += "\t" + op + " word [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678], " +  r16[src] + "\n"
    file_handle.write(result)

def gen32_sibdx_r(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r32)):
        for index in range(len(r32)):
            for src in range(len(r32)):
                if index != 4 and index != 12: # no RSP or R12 as index
                    result += "\t" + op + " dword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], " +  r32[src] + "\n"
                    result += "\t" + op + " dword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12], " +  r32[src] + "\n"
                    result += "\t" + op + " dword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12], " +  r32[src] + "\n"
                    result += "\t" + op + " dword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12], " +  r32[src] + "\n"
                    result += "\t" + op + " dword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], " +  r32[src] + "\n"
                    result += "\t" + op + " dword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678], " +  r32[src] + "\n"
                    result += "\t" + op + " dword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678], " +  r32[src] + "\n"
                    result += "\t" + op + " dword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678], " +  r32[src] + "\n"
    file_handle.write(result)

def gen64_sibdx_r(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r64)):
        for index in range(len(r64)):
            for src in range(len(r64)):
                if index != 4 and index != 12: # no RSP or R12 as index
                    result += "\t" + op + " qword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], " +  r64[src] + "\n"
                    result += "\t" + op + " qword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12], " +  r64[src] + "\n"
                    result += "\t" + op + " qword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12], " +  r64[src] + "\n"
                    result += "\t" + op + " qword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12], " +  r64[src] + "\n"
                    result += "\t" + op + " qword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], " +  r64[src] + "\n"
                    result += "\t" + op + " qword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678], " +  r64[src] + "\n"
                    result += "\t" + op + " qword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678], " +  r64[src] + "\n"
                    result += "\t" + op + " qword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678], " +  r64[src] + "\n"
    file_handle.write(result)

# }}}
# section gen_sibdx_ix
# {{{

def gen8_sibdx_ix(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r8)):
        for index in range(len(r8)):
            if index != 4 and index != 12: # no RSP or R12 as index
                result += "\t" + op + " byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " byte [" + r64[base] + " + 2 * " + r64[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " byte [" + r32[base] + " + 4 * " + r32[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " byte [" + r64[base] + " + 8 * " + r64[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " byte [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " byte [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " byte [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " byte [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678], 0x12\n"
    file_handle.write(result)

def gen16_sibdx_ix(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r16)):
        for index in range(len(r16)):
            if index != 4 and index != 12: # no RSP or R12 as index
                result += "\t" + op + " word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " word [" + r64[base] + " + 2 * " + r64[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " word [" + r32[base] + " + 4 * " + r32[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " word [" + r64[base] + " + 8 * " + r64[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], 0x1234\n"
                result += "\t" + op + " word [" + r64[base] + " + 2 * " + r64[index] + " + 0x12], 0x1234\n"
                result += "\t" + op + " word [" + r32[base] + " + 4 * " + r32[index] + " + 0x12], 0x1234\n"
                result += "\t" + op + " word [" + r64[base] + " + 8 * " + r64[index] + " + 0x12], 0x1234\n"
                result += "\t" + op + " word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " word [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " word [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " word [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " word [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], 0x1234\n"
                result += "\t" + op + " word [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678], 0x1234\n"
                result += "\t" + op + " word [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678], 0x1234\n"
                result += "\t" + op + " word [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678], 0x1234\n"
    file_handle.write(result)

def gen32_sibdx_ix(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r32)):
        for index in range(len(r32)):
            if index != 4 and index != 12: # no RSP or R12 as index
                result += "\t" + op + " dword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " dword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " dword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " dword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " dword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], 0x12345678\n"
                result += "\t" + op + " dword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12], 0x12345678\n"
                result += "\t" + op + " dword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12], 0x12345678\n"
                result += "\t" + op + " dword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12], 0x12345678\n"
                result += "\t" + op + " dword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " dword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " dword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " dword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " dword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], 0x12345678\n"
                result += "\t" + op + " dword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678], 0x12345678\n"
                result += "\t" + op + " dword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678], 0x12345678\n"
                result += "\t" + op + " dword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678], 0x12345678\n"
    file_handle.write(result)

def gen64_sibdx_ix(file_handle, op):
    result = "\tbits 64\n"
    for base in range(len(r64)):
        for index in range(len(r64)):
            if index != 4 and index != 12: # no RSP or R12 as index
                result += "\t" + op + " qword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " qword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " qword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " qword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12], 0x12\n"
                result += "\t" + op + " qword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12], 0x12345678\n"
                result += "\t" + op + " qword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12], 0x12345678\n"
                result += "\t" + op + " qword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12], 0x12345678\n"
                result += "\t" + op + " qword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12], 0x12345678\n"
                result += "\t" + op + " qword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " qword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " qword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " qword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678], 0x12\n"
                result += "\t" + op + " qword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678], 0x12345678\n"
                result += "\t" + op + " qword [" + r64[base] + " + 2 * " + r64[index] + " + 0x12345678], 0x12345678\n"
                result += "\t" + op + " qword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678], 0x12345678\n"
                result += "\t" + op + " qword [" + r64[base] + " + 8 * " + r64[index] + " + 0x12345678], 0x12345678\n"
    file_handle.write(result)

# }}}
# }}}

# sertion gen_special
# {{{
# section gen_jmp
# {{{
def gen_jmp(file_handle):
    result = "\tbits 64\n"
    result += "\tjmp 0x12\n"
    result += "\tjmp 0x1234\n"
    result += "\tjmp 0x12345678\n"
    for src in range(len(r64)):
        result += "\tjmp " + r64[src] + "\n"
    for base in range(len(r64)):
        result += "\tjmp qword [" + r32[base] + "]\n"
        result += "\tjmp qword [" + r64[base] + "]\n"
        result += "\tjmp qword [" + r32[base] + " + 0x12]\n"
        result += "\tjmp qword [" + r64[base] + " + 0x12]\n"
        result += "\tjmp qword [" + r32[base] + " + 0x12345678]\n"
        result += "\tjmp qword [" + r64[base] + " + 0x12345678]\n"
        
    for base in range(len(r64)):
        for index in range(len(r64)):
            if index != 4 and index != 12: # rsp and r12 not allowed
                result += "\tjmp qword [" + r32[base] + " + 1 * " + r32[index] + "]\n"
                result += "\tjmp qword [" + r32[base] + " + 2 * " + r32[index] + "]\n"
                result += "\tjmp qword [" + r32[base] + " + 4 * " + r32[index] + "]\n"
                result += "\tjmp qword [" + r32[base] + " + 8 * " + r32[index] + "]\n"
                result += "\tjmp qword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12]\n"
                result += "\tjmp qword [" + r32[base] + " + 2 * " + r32[index] + " + 0x12]\n"
                result += "\tjmp qword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12]\n"
                result += "\tjmp qword [" + r32[base] + " + 8 * " + r32[index] + " + 0x12]\n"
                result += "\tjmp qword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678]\n"
                result += "\tjmp qword [" + r32[base] + " + 2 * " + r32[index] + " + 0x12345678]\n"
                result += "\tjmp qword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678]\n"
                result += "\tjmp qword [" + r32[base] + " + 8 * " + r32[index] + " + 0x12345678]\n"
    file_handle.write(result)
# }}}

# section gen_jcc
# {{{
def gen_jcc(file_handle):
    result = "\tbits 64\n"
    for op in jcc:
        result += "\t" + op + " 0x12\n"
        result += "\t" + op + " 0x1234\n"
        result += "\t" + op + " 0x12345678\n"
    
    file_handle.write(result)
# }}}

# section gen_call
# {{{
def gen_call(file_handle):
    result = "\tbits 64\n"
    result += "\tcall 0x12\n"
    result += "\tcall 0x1234\n"
    result += "\tcall 0x12345678\n"
    for src in range(len(r64)):
        result += "\tcall " + r64[src] + "\n"
    for base in range(len(r64)):
        result += "\tcall qword [" + r32[base] + "]\n"
        result += "\tcall qword [" + r64[base] + "]\n"
        result += "\tcall qword [" + r32[base] + " + 0x12]\n"
        result += "\tcall qword [" + r64[base] + " + 0x12]\n"
        result += "\tcall qword [" + r32[base] + " + 0x12345678]\n"
        result += "\tcall qword [" + r64[base] + " + 0x12345678]\n"
        
    for base in range(len(r64)):
        for index in range(len(r64)):
            if index != 4 and index != 12: # rsp and r12 not allowed
                result += "\tcall qword [" + r32[base] + " + 1 * " + r32[index] + "]\n"
                result += "\tcall qword [" + r32[base] + " + 2 * " + r32[index] + "]\n"
                result += "\tcall qword [" + r32[base] + " + 4 * " + r32[index] + "]\n"
                result += "\tcall qword [" + r32[base] + " + 8 * " + r32[index] + "]\n"
                result += "\tcall qword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12]\n"
                result += "\tcall qword [" + r32[base] + " + 2 * " + r32[index] + " + 0x12]\n"
                result += "\tcall qword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12]\n"
                result += "\tcall qword [" + r32[base] + " + 8 * " + r32[index] + " + 0x12]\n"
                result += "\tcall qword [" + r32[base] + " + 1 * " + r32[index] + " + 0x12345678]\n"
                result += "\tcall qword [" + r32[base] + " + 2 * " + r32[index] + " + 0x12345678]\n"
                result += "\tcall qword [" + r32[base] + " + 4 * " + r32[index] + " + 0x12345678]\n"
                result += "\tcall qword [" + r32[base] + " + 8 * " + r32[index] + " + 0x12345678]\n"
    file_handle.write(result)
# }}}

# section gen_int
# {{{
def gen_int(file_handle):
    result = "\tbits 64\n"
    for i in range(256):
        result += "\tint " + hex(i) + "\n"
    
    file_handle.write(result)
# }}}

# }}}

def main():

    special_instructions = True

    if special_instructions:
        file_handle = open("test.s", "w")
        jmp = 0
        jcc = 1
        call = 2
        int_ = 3

        current_test = int_
        
        if current_test == jmp:
            gen_jmp(file_handle)
        elif current_test == jcc:
            gen_jcc(file_handle)
        elif current_test == call:
            gen_call(file_handle)
        elif current_test == int_:
            gen_int(file_handle)
        else:
            print("no test selected")

        file_handle.close()


    else:
        op = "and"
        file8 = open("test8.s", "w")
        file16 = open("test16.s", "w")
        file32 = open("test32.s", "w")
        file64 = open("test64.s", "w")

        r = 0
        r_ix = 1
        r_r = 2
        r_m = 3
        r_mdb = 4
        r_mdd = 5
        r_sib = 6
        r_sibdb = 7
        r_sibdd = 8
        r_disp = 9
        r_rip_disp = 10

        m = 11
        m_r = 12
        m_ix = 13

        mdx = 14
        mdx_r = 15
        mdx_ix = 16

        sib = 17
        sib_r = 18
        sib_ix = 19

        sibdx = 20
        sibdx_r = 21
        sibdx_ix = 22

        current_test = sibdx_ix

        if current_test == r:
            gen8_r(file8, op)
            gen16_r(file16, op)
            gen32_r(file32, op)
            gen64_r(file64, op)
        elif current_test == r_ix:
            gen8_r_ix(file8, op)
            gen16_r_ix(file16, op)
            gen32_r_ix(file32, op)
            gen64_r_ix(file64, op)
        elif current_test == r_r:
            gen8_r_r(file8, op)
            gen16_r_r(file16, op)
            gen32_r_r(file32, op)
            gen64_r_r(file64, op)
        elif current_test == r_m:
            gen8_r_m(file8, op)
            gen16_r_m(file16, op)
            gen32_r_m(file32, op)
            gen64_r_m(file64, op)
        elif current_test == r_mdb:
            gen8_r_mdb(file8, op)
            gen16_r_mdb(file16, op)
            gen32_r_mdb(file32, op)
            gen64_r_mdb(file64, op)
        elif current_test == r_mdd:
            gen8_r_mdd(file8, op)
            gen16_r_mdd(file16, op)
            gen32_r_mdd(file32, op)
            gen64_r_mdd(file64, op)
        elif current_test == r_sib:
            gen8_r_sib(file8, op)
            gen16_r_sib(file16, op)
            gen32_r_sib(file32, op)
            gen64_r_sib(file64, op)
        elif current_test == r_sibdb:
            gen8_r_sibdb(file8, op)
            gen16_r_sibdb(file16, op)
            gen32_r_sibdb(file32, op)
            gen64_r_sibdb(file64, op)
        elif current_test == r_sibdd:
            gen8_r_sibdd(file8, op)
            gen16_r_sibdd(file16, op)
            gen32_r_sibdd(file32, op)
            gen64_r_sibdd(file64, op)
        elif current_test == r_disp:
            gen8_r_disp(file8, op)
            gen16_r_disp(file16, op)
            gen32_r_disp(file32, op)
            gen64_r_disp(file64, op)
        elif current_test == r_rip_disp:
            gen8_r_rip_disp(file8, op)
            gen16_r_rip_disp(file16, op)
            gen32_r_rip_disp(file32, op)
            gen64_r_rip_disp(file64, op)
        elif current_test == m:
            gen8_m(file8, op)
            gen16_m(file16, op)
            gen32_m(file32, op)
            gen64_m(file64, op)
        elif current_test == m_r:
            gen8_m_r(file8, op)
            gen16_m_r(file16, op)
            gen32_m_r(file32, op)
            gen64_m_r(file64, op)
        elif current_test == m_ix:
            gen8_m_ix(file8, op)
            gen16_m_ix(file16, op)
            gen32_m_ix(file32, op)
            gen64_m_ix(file64, op)
        elif current_test == mdx:
            gen8_mdx(file8, op)
            gen16_mdx(file16, op)
            gen32_mdx(file32, op)
            gen64_mdx(file64, op)
        elif current_test == mdx_r:
            gen8_mdx_r(file8, op)
            gen16_mdx_r(file16, op)
            gen32_mdx_r(file32, op)
            gen64_mdx_r(file64, op)
        elif current_test == mdx_ix:
            gen8_mdx_ix(file8, op)
            gen16_mdx_ix(file16, op)
            gen32_mdx_ix(file32, op)
            gen64_mdx_ix(file64, op)
        elif current_test == sib:
            gen8_sib(file8, op)
            gen16_sib(file16, op)
            gen32_sib(file32, op)
            gen64_sib(file64, op)
        elif current_test == sib_r:
            gen8_sib_r(file8, op)
            gen16_sib_r(file16, op)
            gen32_sib_r(file32, op)
            gen64_sib_r(file64, op)
        elif current_test == sib_ix:
            gen8_sib_ix(file8, op)
            gen16_sib_ix(file16, op)
            gen32_sib_ix(file32, op)
            gen64_sib_ix(file64, op)
        elif current_test == sibdx:
            gen8_sibdx(file8, op)
            gen16_sibdx(file16, op)
            gen32_sibdx(file32, op)
            gen64_sibdx(file64, op)
        elif current_test == sibdx_r:
            gen8_sibdx_r(file8, op)
            gen16_sibdx_r(file16, op)
            gen32_sibdx_r(file32, op)
            gen64_sibdx_r(file64, op)
        elif current_test == sibdx_ix:
            gen8_sibdx_ix(file8, op)
            gen16_sibdx_ix(file16, op)
            gen32_sibdx_ix(file32, op)
            gen64_sibdx_ix(file64, op)
        else:
            print("no test selected")

        file8.close()
        file16.close()
        file32.close()
        file64.close()

if __name__ == "__main__":
    main()
