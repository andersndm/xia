#!/bin/bash

python gen_op_test.py

printf "\x1b[32mtest\x1b[0m\n"
../xia -o xia_test.bin test.s
nasm -f bin test.s -o nasm_test.bin

radiff2 nasm_test.bin xia_test.bin >> diff.txt
du -h diff.txt
rm diff.txt

objdump -D -M intel -b binary -m i386:x86-64 xia_test.bin > out_xia.txt
objdump -D -M intel -b binary -m i386:x86-64 nasm_test.bin > out_nasm.txt
