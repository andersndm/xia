	.text
	.intel_syntax noprefix
	.file	"clang_test.c"
	.globl	get_str                 # -- Begin function get_str
	.p2align	4, 0x90
	.type	get_str,@function
get_str:                                # @get_str
	.cfi_startproc
# %bb.0:                                # %entry
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset rbp, -16
	mov	rbp, rsp
	.cfi_def_cfa_register rbp
	lea	rax, [rip + .L.str]
	pop	rbp
	.cfi_def_cfa rsp, 8
	ret
.Lfunc_end0:
	.size	get_str, .Lfunc_end0-get_str
	.cfi_endproc
                                        # -- End function
	.globl	test_fn                 # -- Begin function test_fn
	.p2align	4, 0x90
	.type	test_fn,@function
test_fn:                                # @test_fn
	.cfi_startproc
# %bb.0:                                # %entry
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset rbp, -16
	mov	rbp, rsp
	.cfi_def_cfa_register rbp
	mov	qword ptr [rbp - 8], rdi
	mov	qword ptr [rbp - 16], rsi
	mov	rax, qword ptr [rbp - 16]
	mov	rcx, qword ptr [rbp - 8]
	cmp	rax, qword ptr [rcx + 8]
	jbe	.LBB1_2
# %bb.1:                                # %if.then
	mov	rax, qword ptr [rbp - 8]
	mov	dword ptr [rax], 10
.LBB1_2:                                # %if.end
	pop	rbp
	.cfi_def_cfa rsp, 8
	ret
.Lfunc_end1:
	.size	test_fn, .Lfunc_end1-test_fn
	.cfi_endproc
                                        # -- End function
	.globl	main                    # -- Begin function main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# %bb.0:                                # %entry
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset rbp, -16
	mov	rbp, rsp
	.cfi_def_cfa_register rbp
	sub	rsp, 80
	mov	rax, qword ptr fs:[40]
	mov	qword ptr [rbp - 8], rax
	mov	dword ptr [rbp - 36], 0
	mov	dword ptr [rbp - 40], 0
.LBB2_1:                                # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	cmp	dword ptr [rbp - 40], 10
	jge	.LBB2_4
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB2_1 Depth=1
	movsxd	rax, dword ptr [rbp - 40]
	add	rax, qword ptr [rip + global_int]
	mov	qword ptr [rip + global_int], rax
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB2_1 Depth=1
	mov	eax, dword ptr [rbp - 40]
	add	eax, 1
	mov	dword ptr [rbp - 40], eax
	jmp	.LBB2_1
.LBB2_4:                                # %for.end
	xorps	xmm0, xmm0
	movaps	xmmword ptr [rbp - 32], xmm0
	mov	rsi, qword ptr [rip + global_int]
	lea	rax, [rbp - 32]
	mov	rdi, rax
	mov	qword ptr [rbp - 56], rax # 8-byte Spill
	call	test_fn
	call	get_str
	mov	qword ptr [rbp - 48], rax
	mov	rax, qword ptr [rbp - 48]
	mov	rdi, rax
	mov	qword ptr [rbp - 64], rax # 8-byte Spill
	call	strlen@PLT
	mov	edi, 1
	mov	rsi, qword ptr [rbp - 64] # 8-byte Reload
	mov	rdx, rax
	call	write@PLT
	mov	esi, 5
	mov	rdi, qword ptr [rbp - 56] # 8-byte Reload
	mov	qword ptr [rbp - 72], rax # 8-byte Spill
	call	test_fn
	mov	rax, qword ptr fs:[40]
	mov	rcx, qword ptr [rbp - 8]
	cmp	rax, rcx
	jne	.LBB2_6
# %bb.5:                                # %SP_return
	xor	eax, eax
	add	rsp, 80
	pop	rbp
	.cfi_def_cfa rsp, 8
	ret
.LBB2_6:                                # %CallStackCheckFailBlk
	.cfi_def_cfa rbp, 16
	call	__stack_chk_fail@PLT
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc
                                        # -- End function
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Hello, world!\n"
	.size	.L.str, 15

	.type	global_int,@object      # @global_int
	.bss
	.globl	global_int
	.p2align	3
global_int:
	.quad	0                       # 0x0
	.size	global_int, 8

	.type	global_const,@object    # @global_const
	.section	.rodata,"a",@progbits
	.globl	global_const
	.p2align	3
global_const:
	.quad	5                       # 0x5
	.size	global_const, 8

	.ident	"clang version 10.0.1 "
	.section	".note.GNU-stack","",@progbits
