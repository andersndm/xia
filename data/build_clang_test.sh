#!/bin/bash

clang -S -fno-discard-value-names -emit-llvm clang_test.c -o clang_test.ll
clang -fno-discard-value-names -no-integrated-as -fno-discard-value-names -S -mllvm --x86-asm-syntax=intel clang_test.c
as clang_test.s -o clang_test.o
ld -o clang_test -dynamic-linker /lib/ld-linux-x86-64.so.2 /usr/lib/crt1.o /usr/lib/crti.o -lc clang_test.o /usr/lib/crtn.o
