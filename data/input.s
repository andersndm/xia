	.section	.text
	.global 	_start
	.align		4, 0x90
_start:
	mov rbp, rsp
	call main
	mov rdi, rax
	jmp _exit

	.global 	exit
	.align 		4, 0x90
exit: ;; exit(rax)
	mov rdi, rax
_exit:							; expects the return value to be in rbx when called
	mov rax, 60 ;; exit(rbx);
	;; int 0x80
	syscall

	.global		main
	.align		4, 0x90
main:	
	;; preamble
	push rbp
	mov rbp, rsp
	sub rsp, 16


	mov rax, 1					; sys_write call
	mov rdi, 1					; write to stdout, (fd = 1)
	;; mov rsi, rsp				; use the char on stack
	;; "Hello, world!\n"
	lea rsi, qword [string]
	mov rdx, 14					; num chars to write
	;;       rdi     rsi              rdx
	;; write(int fd, const void *buf, size_t count)
	syscall

	cmp rax, 0
	jl _main_exit

	add rsp, 16

	;; postamble
	pop rbp
	mov rax, 0
	ret

_main_exit:
	mov rax, 1
	call exit

	.section .data
global_int:
	.i32 0xDEADBEEF

	.section .rodata
	.i8 0b1100_0011
string:	
	.ascii "Hello, world!\n"
	.i64 0x1234_5678

	.section .bss
gint:	
	.i32 0
bint:
	.i8 0

	;; calling convention:
	;; rdi, rsi, rdx, r10, r8, r9
