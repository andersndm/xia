#include "keywords.h"

const char *keyword_names[NUM_KEYWORDS] =
{
	/* directives not keywords
	[KEYWORD_GLOBAL] = "global",

	[KEYWORD_ALIGN] = "align",
	[KEYWORD_RESB] = "resb",

	[KEYWORD_DB] = "db",
	[KEYWORD_DW] = "dw",
	[KEYWORD_DD] = "dd",
	[KEYWORD_QD] = "qd",
	*/
	[KEYWORD_BYTE] = "byte",
	[KEYWORD_WORD] = "word",
	[KEYWORD_DWORD] = "dword",
	[KEYWORD_QWORD] = "qword",

	[KEYWORD_REL] = "rel",
};
