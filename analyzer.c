#include "analyzer.h"

#include "common/buf.h"
#include "common/macros.h"
#include "common/warnings.h"

#include "x64/instruction.h"

internal void analyzer_msgv(analyzer *self, msg_kind msg, printing_pos pp, char const *fmt, va_list args)
{
	if (self->onmsg)
	{
		va_list args_copy;
		va_copy(args_copy, args);
		usize size = vsnprintf(0, 0, fmt, args) + 1;
		// #todo custom allocator
		char *str = xmalloc(size);
		vsnprintf(str, size, fmt, args_copy);
		self->onmsg(0, pp, msg, str);
		xfree(str);
		print_pos(self->srcbase, pp);
	}
}

WRNG_IGNORE_PUSH(WRNG_UNUSED_FUNCTION)
internal void analyzer_log(analyzer *self, printing_pos pp, char const *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	analyzer_msgv(self, MSG_INFO, pp, fmt, args);
	va_end(args);
}

internal void analyzer_warn(analyzer *self, printing_pos pp, char const *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	analyzer_msgv(self, MSG_WARNING, pp, fmt, args);
	va_end(args);
}
WRNG_IGNORE_POP

internal void analyzer_error(analyzer *self, printing_pos pp, char const *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	analyzer_msgv(self, MSG_ERROR, pp, fmt, args);
	va_end(args);
	++self->errors;
}

void internal analyzer_onmsg(void *userdata, printing_pos pp, msg_kind msg, const char *str)
{
	if (msg == MSG_ERROR)
	{
		PC_RED_BOLD("error: ");
	}
	else if (msg == MSG_WARNING)
	{
		PC_MAGENTA_BOLD("warning: ");
	}
	else
	{
		PC_WHITE_BOLD("note: ");
	}
	printf("%s\n", str);
}

void analyzer_init(analyzer *self, source_base *srcbase)
{
	self->srcbase = srcbase;
	self->errors = 0;
	self->onmsg = analyzer_onmsg;
}

typedef enum oper_type
{
	OPTYPE_NULL,

	OPTYPE_IMM8,
	OPTYPE_IMM16,
	OPTYPE_IMM32,
	OPTYPE_IMM64,

	OPTYPE_R8,
	OPTYPE_R16,
	OPTYPE_R32,
	OPTYPE_R64,

	OPTYPE_RM8,
	OPTYPE_RM16,
	OPTYPE_RM32,
	OPTYPE_RM64,

	OPTYPE_M8,
	OPTYPE_M16,
	OPTYPE_M32,
	OPTYPE_M64,

	OPTYPE_CC,
	OPTYPE_CL,
	OPTYPE_LABEL,

	OPTYPE_END
} oper_type;

internal const char *oper_type_repr(oper_type ot)
{
	switch (ot)
	{
		case OPTYPE_NULL:
		{
			// #todo should this return a valid string?
			INVALID_CODE_PATH;
			return "";
		} break;
		case OPTYPE_IMM8:
		{
			return "imm8";
		} break;
		case OPTYPE_IMM16:
		{
			return "imm16";
		} break;
		case OPTYPE_IMM32:
		{
			return "imm32";
		} break;
		case OPTYPE_IMM64:
		{
			return "imm64";
		} break;
		case OPTYPE_R8:
		{
			return "r8";
		} break;
		case OPTYPE_R16:
		{
			return "r16";
		} break;
		case OPTYPE_R32:
		{
			return "r32";
		} break;
		case OPTYPE_R64:
		{
			return "r64";
		} break;
		case OPTYPE_RM8:
		{
			return "r/m8";
		} break;
		case OPTYPE_RM16:
		{
			return "r/m16";
		} break;
		case OPTYPE_RM32:
		{
			return "r/m32";
		} break;
		case OPTYPE_RM64:
		{
			return "r/m64";
		} break;
		case OPTYPE_M8:
		{
			return "m8";
		} break;
		case OPTYPE_M16:
		{
			return "m16";
		} break;
		case OPTYPE_M32:
		{
			return "m32";
		} break;
		case OPTYPE_M64:
		{
			return "m64";
		} break;
		case OPTYPE_CC:
		{
			return "cc";
		} break;
		case OPTYPE_CL:
		{
			return "cl";
		} break;
		case OPTYPE_LABEL:
		{
			return "label";
		} break;

		case OPTYPE_END:
		{
			INVALID_CODE_PATH;
			return "";
		} break;
	}
}

typedef struct instruction_layout
{
	oper_type op1;
	oper_type op2;
	oper_type op3;
} instruction_layout;

#define ADD_LAYOUT(arg1, arg2, arg3) (instruction_layout){ .op1 = arg1, .op2 = arg2, .op3 = arg3 },
#define END_LAYOUT (instruction_layout){ .op1 = OPTYPE_END, .op2 = OPTYPE_END, .op3 = OPTYPE_END }

const instruction_layout general_2op_instr_check[] =
{
	ADD_LAYOUT(OPTYPE_RM8,  OPTYPE_IMM8,  OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM16, OPTYPE_IMM16, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM32, OPTYPE_IMM32, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM64, OPTYPE_IMM32, OPTYPE_NULL)

	ADD_LAYOUT(OPTYPE_RM16, OPTYPE_IMM8, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM32, OPTYPE_IMM8, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM64, OPTYPE_IMM8, OPTYPE_NULL)

	ADD_LAYOUT(OPTYPE_R8,  OPTYPE_RM8,  OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R16, OPTYPE_RM16, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R32, OPTYPE_RM32, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R64, OPTYPE_RM64, OPTYPE_NULL)

	ADD_LAYOUT(OPTYPE_RM8,  OPTYPE_R8,  OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM16, OPTYPE_R16, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM32, OPTYPE_R32, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM64, OPTYPE_R64, OPTYPE_NULL)

	END_LAYOUT
};

const instruction_layout call_instr_check[] =
{
	ADD_LAYOUT(OPTYPE_IMM32, OPTYPE_NULL, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM64, OPTYPE_NULL, OPTYPE_NULL)

	END_LAYOUT
};

const instruction_layout jmp_instr_check[] =
{
	ADD_LAYOUT(OPTYPE_CC, OPTYPE_IMM8,   OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_CC, OPTYPE_IMM16,  OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_CC, OPTYPE_IMM32,  OPTYPE_NULL)

	ADD_LAYOUT(OPTYPE_IMM32, OPTYPE_NULL, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM64, OPTYPE_NULL, OPTYPE_NULL)

	END_LAYOUT
};

const instruction_layout general_1op_instr_check[] =
{
	ADD_LAYOUT(OPTYPE_RM8,  OPTYPE_NULL, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM16, OPTYPE_NULL, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM32, OPTYPE_NULL, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM64, OPTYPE_NULL, OPTYPE_NULL)

	END_LAYOUT
};

const instruction_layout lea_instr_check[] =
{
	// #note the bit-width of the OPTYPE_M just means that the registers are either 32-bit or 64-bit,
	// any width specifiers are ignored
	// #todo allow the width specifier to be omitted, because it is not actually a pointer
	// #todo do not allow width specifiers?
	ADD_LAYOUT(OPTYPE_R16, OPTYPE_M16, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R16, OPTYPE_M32, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R16, OPTYPE_M64, OPTYPE_NULL)

	ADD_LAYOUT(OPTYPE_R32, OPTYPE_M32, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R32, OPTYPE_M64, OPTYPE_NULL)

	ADD_LAYOUT(OPTYPE_R64, OPTYPE_M32, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R64, OPTYPE_M64, OPTYPE_NULL)
	
	END_LAYOUT
};

const instruction_layout mov_instr_check[] =
{
	ADD_LAYOUT(OPTYPE_RM8,  OPTYPE_R8,  OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM16, OPTYPE_R16, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM32, OPTYPE_R32, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM64, OPTYPE_R64, OPTYPE_NULL)

	ADD_LAYOUT(OPTYPE_R8,  OPTYPE_RM8,  OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R16, OPTYPE_RM16, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R32, OPTYPE_RM32, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R64, OPTYPE_RM64, OPTYPE_NULL)

	// #todo sreg ignored, is it needed?

	// #todo moffs ignored, is it needed?

	ADD_LAYOUT(OPTYPE_R8,  OPTYPE_IMM8,  OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R16, OPTYPE_IMM16, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R32, OPTYPE_IMM32, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R64, OPTYPE_IMM64, OPTYPE_NULL)

	ADD_LAYOUT(OPTYPE_RM8,  OPTYPE_IMM8,  OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM16, OPTYPE_IMM16, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM32, OPTYPE_IMM32, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM64, OPTYPE_IMM32, OPTYPE_NULL)

	// #note CMOV instructions
	ADD_LAYOUT(OPTYPE_CC, OPTYPE_R16, OPTYPE_RM16)
	ADD_LAYOUT(OPTYPE_CC, OPTYPE_R32, OPTYPE_RM32)
	ADD_LAYOUT(OPTYPE_CC, OPTYPE_R64, OPTYPE_RM64)

	END_LAYOUT
};

const instruction_layout movsx_instr_check[] =
{
	ADD_LAYOUT(OPTYPE_R16, OPTYPE_RM8, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R32, OPTYPE_RM8, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R64, OPTYPE_RM8, OPTYPE_NULL)

	ADD_LAYOUT(OPTYPE_R32, OPTYPE_RM16, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R64, OPTYPE_RM16, OPTYPE_NULL)

	ADD_LAYOUT(OPTYPE_R64, OPTYPE_RM32, OPTYPE_NULL)

	END_LAYOUT
};

const instruction_layout movzx_instr_check[] =
{
	ADD_LAYOUT(OPTYPE_R16, OPTYPE_RM8, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R32, OPTYPE_RM8, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R64, OPTYPE_RM8, OPTYPE_NULL)

	ADD_LAYOUT(OPTYPE_R32, OPTYPE_RM16, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R64, OPTYPE_RM16, OPTYPE_NULL)

	END_LAYOUT
};

const instruction_layout imm8_op_instr_check[] =
{
	ADD_LAYOUT(OPTYPE_IMM8, OPTYPE_NULL, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_END, OPTYPE_END, OPTYPE_END)

	END_LAYOUT
};

const instruction_layout no_operand_instr_check[] =
{
	ADD_LAYOUT(OPTYPE_NULL, OPTYPE_NULL, OPTYPE_NULL)

	END_LAYOUT
};

const instruction_layout pop_instr_check[] =
{
	ADD_LAYOUT(OPTYPE_RM16, OPTYPE_NULL, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM64, OPTYPE_NULL, OPTYPE_NULL)

	ADD_LAYOUT(OPTYPE_R16, OPTYPE_NULL, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R64, OPTYPE_NULL, OPTYPE_NULL)

	// #todo pop fs, pop gs

	END_LAYOUT
};

const instruction_layout push_instr_check[] =
{
	ADD_LAYOUT(OPTYPE_RM16, OPTYPE_NULL, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM64, OPTYPE_NULL, OPTYPE_NULL)

	ADD_LAYOUT(OPTYPE_R16, OPTYPE_NULL, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R64, OPTYPE_NULL, OPTYPE_NULL)

	ADD_LAYOUT(OPTYPE_IMM8,  OPTYPE_NULL, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_IMM16, OPTYPE_NULL, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_IMM32, OPTYPE_NULL, OPTYPE_NULL)

	END_LAYOUT

	// #todo push fs, push gs
};

const instruction_layout ret_instr_check[] =
{
	ADD_LAYOUT(OPTYPE_IMM16, OPTYPE_NULL, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_NULL, OPTYPE_NULL, OPTYPE_NULL)

	END_LAYOUT
};

const instruction_layout shift_instr_check[] =
{
	ADD_LAYOUT(OPTYPE_RM8,  OPTYPE_IMM8, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM8,  OPTYPE_CL,   OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM16, OPTYPE_IMM8, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM16, OPTYPE_CL,   OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM32, OPTYPE_IMM8, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM32, OPTYPE_CL,   OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM64, OPTYPE_IMM8, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM64, OPTYPE_CL,   OPTYPE_NULL)

	END_LAYOUT
};

const instruction_layout set_instr_check[] =
{
	ADD_LAYOUT(OPTYPE_CC, OPTYPE_RM8, OPTYPE_NULL)

	END_LAYOUT
};

const instruction_layout test_instr_check[] =
{
	ADD_LAYOUT(OPTYPE_RM8,  OPTYPE_IMM8,  OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM16, OPTYPE_IMM16, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM32, OPTYPE_IMM32, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM64, OPTYPE_IMM32, OPTYPE_NULL)

	ADD_LAYOUT(OPTYPE_RM8,  OPTYPE_R8,  OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM16, OPTYPE_R16, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM32, OPTYPE_R32, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM64, OPTYPE_R64, OPTYPE_NULL)

	END_LAYOUT
};

const instruction_layout xchg_instr_check[] =
{
	ADD_LAYOUT(OPTYPE_R8,  OPTYPE_RM8,  OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R16, OPTYPE_RM16, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R32, OPTYPE_RM32, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_R64, OPTYPE_RM64, OPTYPE_NULL)

	ADD_LAYOUT(OPTYPE_RM8,  OPTYPE_R8,  OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM16, OPTYPE_R16, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM32, OPTYPE_R32, OPTYPE_NULL)
	ADD_LAYOUT(OPTYPE_RM64, OPTYPE_R64, OPTYPE_NULL)

	END_LAYOUT
};

#undef ADD_LAYOUT
#undef END_LAYOUT

const instruction_layout *const instr_check[NUM_OPCODES] =
{
	[ADC]     = general_2op_instr_check,
	[ADD]     = general_2op_instr_check,
	[AND]     = general_2op_instr_check,
	[CALL]    = call_instr_check,
	[CMP]     = general_2op_instr_check,
	[DEC]     = general_1op_instr_check,
	[DIV]     = general_1op_instr_check,
	[IDIV]    = general_1op_instr_check,
	[IMUL]    = general_1op_instr_check,
	[INC]     = general_1op_instr_check,
	[INT]     = imm8_op_instr_check,
	[JMP]     = jmp_instr_check,
	[LEA]     = lea_instr_check,
	[MOV]     = mov_instr_check,
	[MOVSX]   = movsx_instr_check, 
	[MOVZX]   = movzx_instr_check,
	[MUL]     = general_1op_instr_check,
	[NEG]     = general_1op_instr_check,
	[NOP]     = no_operand_instr_check,
	[NOT]     = general_1op_instr_check,
	[OR]      = general_2op_instr_check,
	[POP]     = pop_instr_check,
	[PUSH]    = push_instr_check,
	[RET]     = ret_instr_check,
	[SAL]     = shift_instr_check,
	[SAR]     = shift_instr_check,
	[SET]     = set_instr_check,
	[SHL]     = shift_instr_check,
	[SHR]     = shift_instr_check,
	[SBB]     = general_2op_instr_check,
	[SUB]     = general_2op_instr_check,
	[SYSCALL] = no_operand_instr_check,
	[TEST]    = test_instr_check,
	[XCHG]    = xchg_instr_check,
	[XOR]     = general_2op_instr_check
};

internal oper_type operand_to_type(operand oper)
{
	switch (oper.kind)
	{
		case OPER_NONE:
		{
			return OPTYPE_NULL;
		} break;
		case OPER_IMMEDIATE:
		{
			switch (oper.imm_op.kind)
			{
				case IB:
				{
					return OPTYPE_IMM8;
				} break;
				case IW:
				{
					return OPTYPE_IMM16;
				} break;
				case ID:
				{
					return OPTYPE_IMM32;
				} break;
				case IQ:
				{
					return OPTYPE_IMM64;
				} break;
			}
		} break;
		case OPER_DISPLACEMENT:
		case OPER_RIP_DISPLACEMENT:
		case OPER_MEM_LABEL:
		case OPER_MEM_REGISTER:
		case OPER_MEM_REGISTER_OFFSET:
		case OPER_MEM_REGISTER_INDEXED:
		case OPER_MEM_REGISTER_INDEXED_OFFSET:
		{
			switch (oper.width)
			{
				case BNONE:
				{
					INVALID_CODE_PATH;
					return OPTYPE_NULL;
				} break;
				case B8:
				{
					return OPTYPE_M8;
				} break;
				case B16:
				{
					return OPTYPE_M16;
				} break;
				case B32:
				{
					return OPTYPE_M32;
				} break;
				case B64:
				{
					return OPTYPE_M64;
				} break;
			}
		} break;
		case OPER_CC:
		{
			return OPTYPE_CC;
		} break;
		case OPER_REGISTER:
		{
			switch (oper.reg.width)
			{
				case BNONE:
				{
					INVALID_CODE_PATH;
					return OPTYPE_NULL;
				} break;
				case B8:
				{
					if (oper.reg.reg == CL)
					{
						return OPTYPE_CL;
					}
					else
					{
						return OPTYPE_R8;
					}
				} break;
				case B16:
				{
					return OPTYPE_R16;
				} break;
				case B32:
				{
					return OPTYPE_R32;
				} break;
				case B64:
				{
					return OPTYPE_R64;
				} break;
			}
		} break;
		case OPER_LABEL:
		{
			// #todo would this always be m64?
			return OPTYPE_LABEL;
		} break;
	}
}

#define WARNINGS 0

internal bool check_operand(analyzer *self, operand oper)
{
	switch (oper.kind)
	{
		case OPER_NONE:
		case OPER_IMMEDIATE:
		case OPER_DISPLACEMENT:
		case OPER_CC:
		case OPER_REGISTER:
		case OPER_LABEL:
		case OPER_MEM_LABEL:
		case OPER_RIP_DISPLACEMENT:
		{
			return true;
		} break;
		case OPER_MEM_REGISTER:
		{
			if (oper.reg.width != B32 && oper.reg.width != B64)
			{
				analyzer_error(self, ppos_range(oper.reg.range),
							   "only 32-bit and 64-bit registers can be used ");
				return false;
			}
			return true;
		} break;
		case OPER_MEM_REGISTER_OFFSET:
		{
			if (oper.mem_offset.base.width != B32 && oper.mem_offset.base.width != B64)
			{
				analyzer_error(self, ppos_range(oper.mem_offset.offset.range),
							   "only 32-bit and 64-bit registers can be used ");
				return false;
			}
			else if (oper.mem_offset.offset.kind == IQ)
			{
				analyzer_error(self, ppos_range(oper.mem_offset.offset.range),
							   "64-bit offsets are not permitted");
				return false;
			}
			else if (oper.mem_offset.offset.kind == IW)
			{
				#if WARNINGS
				analyzer_warn(self, ppos_range(oper.mem_offset.offset.range),
								 "16-bit immediate promoted to 32-bit immediate");
				#endif
				return true;
			}
			return true;
		} break;
		case OPER_MEM_REGISTER_INDEXED:
		{
			
			if (oper.mem_indexed.base.width != B32 && oper.mem_indexed.base.width != B64)
			{
				analyzer_error(self, ppos_range(oper.reg.range),
					"only 32-bit and 64-bit registers can be used ");
				return false;
			}
			else if (oper.mem_indexed.base.width != oper.mem_indexed.index.width)
			{
				analyzer_error(self, ppos_range2(oper.mem_indexed.base.range, oper.mem_indexed.index.range),
							   "base register and index register must have the same bit-width");
				return false;
			}
			else if (oper.mem_indexed.index.reg == RSP)
			{
				registers reg = oper.mem_indexed.index;				
				analyzer_error(self, ppos_range(reg.range),
							   "cannot use '%s' as an index register",
							   reg.width == B32 ? register_x32_names[reg.reg] :
							   register_x64_names[reg.reg]);
				return false;
			}
			else if (oper.mem_indexed.index.reg == RSP && oper.mem_indexed.scale != X1)
			{
				registers reg = oper.mem_indexed.index;	
				analyzer_error(self, ppos_range(oper.mem_indexed.index.range),
								"can only use a scale of 1 when using '%s' as an index",
								reg.width == B32 ? register_x32_names[reg.reg] :
								register_x64_names[reg.reg]);
			}
			return true;
		} break;
		case OPER_MEM_REGISTER_INDEXED_OFFSET:
		{
			if (oper.mem_indexed_offset.base.width != B32 && oper.mem_indexed_offset.base.width != B64)
			{
				analyzer_error(self, ppos_range(oper.reg.range),
					"only 32-bit and 64-bit registers can be used ");
				return false;
			}
			else if (oper.mem_indexed_offset.base.width != oper.mem_indexed_offset.index.width)
			{
				analyzer_error(self, ppos_range2(oper.mem_indexed.base.range, oper.mem_indexed.index.range),
							   "base register and index register must have the same bit-width");
				return false;
			}
			else if (oper.mem_indexed.index.reg == RSP)
			{
				registers reg = oper.mem_indexed.index;				
				analyzer_error(self, ppos_range(reg.range),
							   "cannot use '%s' as an index register",
							   reg.width == B32 ? register_x32_names[reg.reg] :
							   register_x64_names[reg.reg]);
				return false;
			}
			else if (oper.mem_indexed.index.reg == RSP && oper.mem_indexed.scale != X1)
			{
				registers reg = oper.mem_indexed.index;	
				analyzer_error(self, ppos_range(oper.mem_indexed.index.range),
								"can only use a scale of 1 when using '%s' as an index",
								reg.width == B32 ? register_x32_names[reg.reg] :
								register_x64_names[reg.reg]);
			}
			else if (oper.mem_indexed_offset.offset.kind == IQ)
			{
				analyzer_error(self, ppos_range(oper.imm_op.range),
							   "64-bit offsets are not permitted");
				return false;
			}
			else if (oper.mem_indexed_offset.offset.kind == IW)
			{
				#if WARNINGS
				analyzer_warn(self, ppos_range(oper.imm_op.range),
								 "16-bit immediate promoted to 32-bit immediate");
				#endif
				return true;
			}
			return true;
		} break;
	}
}

internal bool valid8_regs(register_x64 r1, register_x64 r2)
{
	if (r1 >= RSP && r1 < R8 && r2 >= R8)
	{
		return false;
	}
	else if (r2 >= RSP && r2 < R8 && r1 >= R8)
	{
		return false;
	}
	return true;
}

internal bool check_x8_regs(analyzer *self, registers r1, registers r2)
{
	bool result = false;
	if (r1.width != B8)
	{
		result = valid8_regs(RAX, r2.reg);
	}
	else if (r2.width != B8)
	{
		result = valid8_regs(r1.reg, RAX);
	}
	else
	{
		result = valid8_regs(r1.reg, r2.reg);
	}
	if (!result)
	{
		registers reg = r1;
		if (r1.reg >= R8)
		{
			reg = r2;
		}
		// #todo obtuse explanation?
		analyzer_error(self, ppos_range(reg.range),
						"cannot access '%s' register with a rex instruction",
						register_x8_names[reg.reg]);
	}
	return result;
}

internal bool check_x8_sib(analyzer *self, registers r1, registers base, registers index)
{
	if (r1.reg >= RSP && r1.reg < R8 && (index.reg >= R8 || base.reg >= R8))
	{
		registers reg = r1;
		if (!(r1.reg >= AH && r1.reg <= BH))
		{
			reg = base;
			if (!(base.reg >= AH && base.reg <= BH))
			{
				reg = index;
			}
		}
		// #todo obtuse explanation?
		analyzer_error(self, ppos_range(reg.range),
					   "cannot access '%s' register with a rex instruction",
					   register_x8_names[reg.reg]);
		return false;
	}
	return true;
}

internal bool check_valid_x8_registers(analyzer *self, operand op1, operand op2)
{
	switch (op1.kind)
	{
		case OPER_NONE:
		case OPER_IMMEDIATE:
		case OPER_CC:
		case OPER_RIP_DISPLACEMENT:
		case OPER_DISPLACEMENT:
		case OPER_MEM_LABEL:
		case OPER_LABEL: // #todo?
		{
			return true;
		} break;
		case OPER_REGISTER:
		{
			if (op2.kind == OPER_REGISTER)
			{
				return check_x8_regs(self, op1.reg, op2.reg);
			}
			#if 0
			// #note none of these registers should be 8-bit
			else if (op2.kind == OPER_MEM_REGISTER)
			{
				return check_x8_regs(self, op1.reg, op2.reg);
			}
			else if (op2.kind == OPER_MEM_REGISTER_OFFSET)
			{
				return check_x8_regs(self, op1.reg, op2.mem_offset.base);
			}
			else if (op2.kind == OPER_MEM_REGISTER_INDEXED)
			{
				return check_x8_sib(self, op1.reg, op2.mem_indexed.base, op2.mem_indexed.index);
			}
			else if (op2.kind == OPER_MEM_REGISTER_INDEXED_OFFSET)
			{
				return check_x8_sib(self, op1.reg, op2.mem_indexed_offset.base, op2.mem_indexed_offset.index);
			}
			#endif
			return true;
		}
		case OPER_MEM_REGISTER:
		{
			if (op2.kind == OPER_REGISTER)
			{
				return check_x8_regs(self, op1.reg, op2.reg);
			}
			return true;
		} break;
		case OPER_MEM_REGISTER_OFFSET:
		{
			if (op2.kind == OPER_REGISTER)
			{
				return check_x8_regs(self, op1.mem_offset.base, op2.reg);
			}
			return true;
		} break;
		case OPER_MEM_REGISTER_INDEXED:
		{
			if (op2.kind == OPER_REGISTER)
			{
				return check_x8_sib(self, op2.reg, op1.mem_indexed.base, op1.mem_indexed.index);
			}
			return true;
		} break;
		case OPER_MEM_REGISTER_INDEXED_OFFSET:
		{
			if (op2.kind == OPER_REGISTER)
			{
				return check_x8_sib(self, op2.reg, op1.mem_indexed_offset.base, op1.mem_indexed_offset.index);
			}
			return true;
		} break;
	}
}

// #note op1 is the evaluated oper type, op2 is the check
internal bool check_oper(oper_type op1, oper_type op2)
{
	if (op1 == op2)
	{
		return true;
	}
	else if (op1 == OPTYPE_LABEL && (op2 == OPTYPE_IMM16 || op2 == OPTYPE_IMM32))
	{
		return true;
	}
	else if (op1 == OPTYPE_CL && (op2 == OPTYPE_R8 || op2 == OPTYPE_RM8))
	{
		return true;
	}
	else if((op1 == OPTYPE_R8 && op2 == OPTYPE_RM8) || (op1 == OPTYPE_M8 && op2 == OPTYPE_RM8))
	{
		return true;
	}
	else if ((op1 == OPTYPE_R16 && op2 == OPTYPE_RM16) || (op1 == OPTYPE_M16 && op2 == OPTYPE_RM16))
	{
		return true;
	}
	else if ((op1 == OPTYPE_R32 && op2 == OPTYPE_RM32) || (op1 == OPTYPE_M32 && op2 == OPTYPE_RM32))
	{
		return true;
	}
	else if ((op1 == OPTYPE_R64 && op2 == OPTYPE_RM64) || (op1 == OPTYPE_M64 && op2 == OPTYPE_RM64))
	{
		return true;
	}
	else if (op2 == OPTYPE_IMM16 && op1 == OPTYPE_IMM8)
	{
		// #todo can this be warned for?
		return true;
	}
	else if (op2 == OPTYPE_IMM32 && (op1 == OPTYPE_IMM16 || op1 == OPTYPE_IMM8))
	{
		return true;
	}
	else if (op2 == OPTYPE_IMM64 && (op1 == OPTYPE_IMM32 || op1 == OPTYPE_IMM16 || op1 == OPTYPE_IMM8))
	{
		return true;
	}
	else
	{
		return false;
	}
}

internal void check_instruction(analyzer *self, instruction instr)
{
	oper_type op1 = operand_to_type(instr.op1);
	oper_type op2 = operand_to_type(instr.op2);
	oper_type op3 = operand_to_type(instr.op3);

	const instruction_layout *valid_list = instr_check[instr.op];

	const instruction_layout *current_check = valid_list;
	ASSERT(valid_list != NULL);
	bool operands_are_valid = false;
	while (current_check->op1 != OPTYPE_END)
	{
		if (check_oper(op1, current_check->op1) &&
			check_oper(op2, current_check->op2) &&
			check_oper(op3, current_check->op3))
		{
			operands_are_valid = true;
			break;
		}
		++current_check;
	}

	if (!operands_are_valid)
	{
		// #todo more advanced that checks how close the operands are to correct to find which one is wrong
		if (op1 == OPTYPE_NULL)
		{
			analyzer_error(self, ppos_range(instr.op_range), "expected operand(s) for \"%s\"", op_names[instr.op]);
		}
		else if (op2 == OPTYPE_NULL)
		{
			analyzer_error(self, ppos_range(instr.op1.range), "incorrect operand type (%s) for opcode \"%s\"", oper_type_repr(op1), op_names[instr.op]);
		}
		else if (op3 == OPTYPE_NULL)
		{
			analyzer_error(self, ppos_range2(instr.op1.range, instr.op2.range), "incorrect operand types (%s, %s) for opcode \"%s\"",
						   oper_type_repr(op1), oper_type_repr(op2), op_names[instr.op]);
		}
		else
		{
			analyzer_error(self, ppos_range3(instr.op1.range, instr.op2.range, instr.op3.range), "incorrect operand types (%s, %s, %s) for opcode \"%s\"",
						   oper_type_repr(op1), oper_type_repr(op2), oper_type_repr(op3), op_names[instr.op]);
		}
	}
	else if (!check_operand(self, instr.op1))
	{
		// #note errors handled in function
	}
	else if (!check_operand(self, instr.op2))
	{
		// #note errors handled in function
	}
	else if (!check_operand(self, instr.op3))
	{
		// #note errors handled in function
	}

	else if (instr.op1.width == B8 || instr.op2.width == B8)
	{
		if (!check_valid_x8_registers(self, instr.op1, instr.op2))
		{
			// #todo errors
		}
	}
}

void analyze_statements(analyzer *self, statement *stmts)
{
	for (usize i = 0; i < buf_len(stmts); ++i)
	{
		statement stmt = stmts[i];
		// #todo should other statements be checked?
		if (stmt.kind == STMT_INSTRUCTION)
		{
			check_instruction(self, stmt.instr);
		}
	}
}
