#ifndef XIA_ANALYZER_H
#define XIA_ANALYZER_H

#include "compilation_unit.h"
#include "srcbase.h"

typedef struct analyzer
{
	source_base *srcbase;
	onmsg_fn onmsg;
	u64 errors;
} analyzer;

void analyzer_init(analyzer *self, source_base *srcbase);
void analyze_statements(analyzer *self, statement *stmts);

#endif // !XIA_ANALYZER_H
