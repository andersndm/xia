#ifndef XI_DWARF_H
#define XI_DWARF_H

#include "common/types.h"

typedef enum dwarf_tag
{
	DW_TAG_ARRAY_TYPE				= 0x01,
	DW_TAG_CLASS_TYPE				= 0x02,
	DW_TAG_ENTRY_POINT				= 0x03,
	DW_TAG_ENUMERATION_TYPE			= 0x04,
	DW_TAG_FORMAL_PARAMETER			= 0x05,
	// reserved						= 0x06,
	// reserved						= 0x07,
	DW_TAG_IMPORTED_DECLARATION		= 0x08,
	// reserved						= 0x09,
	DW_TAG_LABEL					= 0x0A,
	DW_TAG_LEXICAL_BLOCK			= 0x0B,
	// reserved						= 0x0C,
	DW_TAG_MEMBER					= 0x0D,
	// reserved						= 0x0E,
	DW_TAG_POINTER_TYPE				= 0x0F,
	DW_TAG_REFERENCE_TYPE			= 0x10,
	DW_TAG_COMPILE_UNIT				= 0x11,
	DW_TAG_STRING_TYPE				= 0x12,
	DW_TAG_STRUCTURE_TYPE			= 0x13,
	// reserved						= 0x14,
	DW_TAG_SUBROUTINE_TYPE			= 0x15,
	DW_TAG_TYPEDEF					= 0x16,
	DW_TAG_UNION_TYPE				= 0x17,
	DW_TAG_UNSPECIFIED_PARAMETERS	= 0x18,
	DW_TAG_VARIANT					= 0x19,
	DW_TAG_COMMON_BLOCK				= 0x1A,
	DW_TAG_COMMON_INCLUSION			= 0x1B,
	DW_TAG_INHERITANCE				= 0x1C,
	DW_TAG_INLINED_SUBROUTINE		= 0x1D,
	DW_TAG_MODULE					= 0x1E,
	DW_TAG_PTR_TO_MEMBER_TYPE		= 0x1F,
	DW_TAG_SET_TYPE					= 0x20,
	DW_TAG_SUBRANGE_TYPE			= 0x21,
	DW_TAG_WITH_STMT				= 0x22,
	DW_TAG_ACCESS_DECLARATION		= 0x23,
	DW_TAG_BASE_TYPE				= 0x24,
	DW_TAG_CATCH_BLOCK				= 0x25,
	DW_TAG_CONST_TYPE				= 0x26,
	DW_TAG_CONSTANT					= 0x27,
	DW_TAG_ENUMERATOR				= 0x28,
	DW_TAG_FILE_TYPE				= 0x29,
	DW_TAG_FRIEND					= 0x2A,
	DW_TAG_NAMELIST					= 0x2B,
	DW_TAG_NAMELIST_ITEM			= 0x2C,
	DW_TAG_PACKED_TYPE				= 0x2D,
	DW_TAG_SUBPROGRAM				= 0x2E,
	DW_TAG_TEMPLATE_TYPE_PARAMETER	= 0x2F,
	DW_TAG_TEMPLATE_VALUE_PARAMETER = 0x30,
	DW_TAG_THROWN_TYPE				= 0x31,
	DW_TAG_TRY_BLOCK				= 0x32,
	DW_TAG_VARIANT_PART				= 0x33,
	DW_TAG_VARIABLE					= 0x34,
	DW_TAG_VOLATILE_TYPE			= 0x35,
	DW_TAG_DWARF_PROCEDURE			= 0x36,
	DW_TAG_RESTRICT_TYPE			= 0x37,
	DW_TAG_INTERFACE_TYPE			= 0x38,
	DW_TAG_NAMESPACE				= 0x39,
	DW_TAG_IMPORTED_MODULE			= 0x3A,
	DW_TAG_UNSPECIFIED_TYPE			= 0x3B,
	DW_TAG_PARTIAL_UNIT				= 0x3C,
	DW_TAG_IMPORTED_UNIT			= 0x3D,
	// reserved						= 0x3E,
	DW_TAG_CONDITION				= 0x3F,
	DW_TAG_SHARED_TYPE				= 0x40,
	DW_TAG_TYPE_UNIT				= 0x41,
	DW_TAG_RVALUE_REFERENCE_TYPE	= 0x42,
	DW_TAG_TEMPLATE_ALIAS			= 0x43,
	DW_TAG_COARRAY_TYPE				= 0x44,
	DW_TAG_GENERIC_SUBRANGE			= 0x45,
	DW_TAG_DYNAMIC_TYPE				= 0x46,
	DW_TAG_ATOMIC_TYPE				= 0x47,
	DW_TAG_CALL_SITE				= 0x48,
	DW_TAG_CALL_SITE_PARAMETER		= 0x49,
	DW_TAG_SKELETON_UNIT			= 0x4A,
	DW_TAG_IMMUTABLE_TYPE			= 0x4B,

	DW_NUM_TAGS,
	
	DW_TAG_LO_USER					= 0x4080,
	DW_TAG_HI_USER					= 0xFFFF
} dwarf_tag;

#define DW_CHILDREN_NO		0x00
#define DW_CHILDREN_YES		0x01

typedef enum dwarf_attribute
{
	DW_AT_SIBLING					= 0x01,
	DW_AT_LOCATION					= 0x02,
	DW_AT_NAME						= 0x03,
	// reserved						= 0x04,
	// reserved						= 0x05,
	// reserved						= 0x06,
	// reserved						= 0x07,
	// reserved						= 0x08,
	DW_AT_ORDERING					= 0x09,
	// reserved						= 0x0A,
	DW_AT_BYTE_SIZE					= 0x0B,
	// reserved						= 0x0C,
	DW_AT_BIT_SIZE					= 0x0D,
	// reserved						= 0x0E,
	// reserved						= 0x0F,
	DW_STMT_LIST					= 0x10,
	DW_AT_LOW_PC					= 0x11,
	DW_AT_HIGH_PC					= 0x12,
	DW_AT_LANGUAGE					= 0x13,
	// reserved						= 0x14,
	DW_AT_DISCR						= 0x15,
	DW_AT_DISRC_VALUE				= 0x16,
	DW_AT_VISIBILITY				= 0x17,
	DW_AT_IMPORT					= 0x18,
	DW_AT_STRING_LENGTH				= 0x19,
	DW_AT_COMMON_REFERENCE			= 0x1A,
	DW_AT_COMP_DIR					= 0x1B,
	DW_AT_CONST_VALUE				= 0x1C,
	DW_AT_CONTAINING_TYPE			= 0x1D,
	DW_AT_DEFAULT_VALUE				= 0x1E,
	// reserved						= 0x1F,
	DW_AT_INLINE					= 0x20,
	DW_AT_IS_OPTIONAL				= 0x21,
	DW_AT_LOWER_BOUND				= 0x22,
	// reserved						= 0x23,
	// reserved						= 0x24,
	DW_AT_PRODUCER					= 0x25,
	// reserved						= 0x26,
	DW_AT_PROTOTYPED				= 0x27,
	// reserved						= 0x28,
	// reserved						= 0x29,
	DW_AT_RETURN_ADDR				= 0x2A,
	// reserved						= 0x2B,
	DW_AT_START_SCOPE				= 0x2C,
	// reserved						= 0x2D,
	DW_AT_BIT_STRIDE				= 0x2E,
	DW_AT_UPPER_BOUND				= 0x2F,
	// reserved						= 0x30,
	DW_AT_ABSTRACT_ORIGIN			= 0x31,
	DW_AT_ACCESSIBILITY				= 0x32,
	DW_AT_ADDRESS_CLASS				= 0x33,
	DW_AT_ARTIFICIAL				= 0x34,
	DW_AT_BASE_TYPES				= 0x35,
	DW_AT_CALLING_CONVENTION		= 0x36,
	DW_AT_COUNT						= 0x37,
	DW_AT_DATA_MEMBER_LOCATION		= 0x38,
	DW_AT_DECL_COLUMN				= 0x39,
	DW_AT_DECL_FILE					= 0x3A,
	DW_AT_DECL_LINE					= 0x3B,
	DW_AT_DECLARATION				= 0x3C,
	DW_AT_DISCR_LIST				= 0x3D,
	DW_AT_ENCODING					= 0x3E,
	DW_AT_EXTERNAL					= 0x3F,
	DW_AT_FRAME_BASE				= 0x40,
	DW_AT_FRIEND					= 0x41,
	DW_AT_IDENTIFIER_CASE			= 0x42,
	// reserved						= 0x43,
	DW_AT_NAMELIST_ITEM				= 0x44,
	DW_AT_PRIORITY					= 0x45,
	DW_AT_SEGMENT					= 0x46,
	DW_AT_SPECIFICATION				= 0x47,
	DW_AT_STATIC_LINK				= 0x48,
	DW_AT_TYPE						= 0x49,
	DW_AT_USE_LOCATION				= 0x4A,
	DW_AT_VARIABLE_PARAMETER		= 0x4B,
	DW_AT_VIRTUALITY				= 0x4C,
	DW_AT_VTABLE_ELEM_LOCATION		= 0x4D,
	DW_AT_ALLOCATED					= 0x4E,
	DW_AT_ASSOCIATED				= 0x4F,
	DW_AT_DATA_LOCATION				= 0x50,
	DW_AT_BYTE_STRIDE				= 0x51,
	DW_AT_ENTRY_PC					= 0x52,
	DW_AT_USE_UTF8					= 0x53,
	DW_AT_EXTENSION					= 0x54,
	DW_AT_RANGES					= 0x55,
	DW_AT_TRAMPOLINE				= 0x56,
	DW_AT_CALL_COLUMN				= 0x57,
	DW_AT_CALL_FILE					= 0x58,
	DW_AT_CALL_LINE					= 0x59,
	DW_AT_DESCRIPTION				= 0x5A,
	DW_AT_BINARY_SCALE				= 0x5B,
	DW_AT_DECIMAL_SCALE				= 0x5C,
	DW_AT_SMALL						= 0x5D,
	DW_AT_DECIMAL_SIGN				= 0x5E,
	DW_AT_DIGIT_COUNT				= 0x5F,
	DW_AT_PICTURE_STRING			= 0x60,
	DW_AT_MUTABLE					= 0x61,
	DW_AT_THREADS_SCALED			= 0x62,
	DW_AT_EXPLICIT					= 0x63,
	DW_AT_OBJECT_POINTER			= 0x64,
	DW_AT_ENDIANITY					= 0x65,
	DW_AT_ELEMENTAL					= 0x66,
	DW_AT_PURE						= 0x67,
	DW_AT_RECURSIVE					= 0x68,
	DW_AT_SIGNATURE					= 0x69,
	DW_AT_MAIN_SUBPROGRAM			= 0x6A,
	DW_AT_DATA_BIT_OFFSET			= 0x6B,
	DW_AT_CONST_EXPR				= 0x6C,
	DW_AT_ENUM_CLASS				= 0x6D,
	DW_AT_LINKAGE_NAME				= 0x6E,
	DW_AT_STRING_LENGTH_BIT_SIZE	= 0x6F,
	DW_AT_STRING_LENGTH_BYTE_SIZE	= 0x70,
	DW_AT_RANK						= 0x71,
	DW_AT_STR_OFFSETS_BASE			= 0x72,
	DW_AT_ADDR_BASE					= 0x73,
	DW_AT_RNGLISTS_BASE				= 0x74,
	// reserved						= 0x75,
	DW_AT_DWO_NAME					= 0x76,
	DW_AT_REFERENCE					= 0x77,
	DW_AT_RVALUE_REFERENCE			= 0x78,
	DW_AT_MACROS					= 0x79,
	DW_AT_CALL_ALL_CALLS			= 0x7A,
	DW_AT_CALL_ALL_SOURCE_CALLS		= 0x7B,
	DW_AT_CALL_ALL_TAIL_CALLS		= 0x7C,
	DW_AT_CALL_RETURN_PC			= 0x7D,
	DW_AT_CALL_VALUE				= 0x7E,
	DW_AT_CALL_ORIGIN				= 0x7F,
	DW_AT_CALL_PARAMETER			= 0x80,
	DW_AT_CALL_PC					= 0x81,
	DW_AT_CALL_TAIL_CALL			= 0x82,
	DW_AT_CALL_TARGET				= 0x83,
	DW_AT_CALL_TARGET_CLOBBERED		= 0x84,
	DW_AT_CALL_DATA_LOCATION		= 0x85,
	DW_AT_CALL_DATA_VALUE			= 0x86,
	DW_AT_NORETURN					= 0x87,
	DW_AT_ALIGNMENT					= 0x88,
	DW_AT_EXPORT_SYMBOLS			= 0x89,
	DW_AT_DELETED					= 0x8A,
	DW_AT_DEFAULTED					= 0x8B,
	DW_AT_LOCLISTS_BASE				= 0x8C,

	DW_NUM_ATTRS,

	DW_AT_LO_USER					= 0x2000,
	DW_AT_HI_USER					= 0x3FFF
} dwarf_attribute;

typedef enum dwarf_form
{
	DW_FORM_ADDR					= 0x01,
	// reserved						= 0x02,
	DW_FORM_BLOCK2					= 0x3,
	DW_FORM_BLOCK4					= 0x04,
	DW_FORM_DATA2					= 0x05,
	DW_FORM_DATA4					= 0x06,
	DW_FORM_DATA8					= 0x07,
	DW_FORM_STRING					= 0x08,
	DW_FORM_BLOCK					= 0x09,
	DW_FORM_BLOCK1					= 0x0A,
	DW_FORM_DATA1					= 0x0B,
	DW_FORM_FLAG					= 0x0C,
	DW_FORM_SDATA					= 0x0D,
	DW_FORM_STRP					= 0x0E,
	DW_FORM_UDATA					= 0x0F,
	DW_FORM_REF_ADDR				= 0x10,
	DW_FORM_REF1					= 0x11,
	DW_FORM_REF2					= 0x12,
	DW_FORM_REF4					= 0x13,
	DW_FORM_REF8					= 0x14,
	DW_FORM_REF_UDATA				= 0x15,
	DW_FORM_INDIRECT				= 0x16,
	DW_FORM_SEC_OFFSET				= 0x17,
	DW_FORM_EXPRLOC					= 0x18,
	DW_FORM_FLAG_PRESENT			= 0x19,
	DW_FORM_STRX					= 0x1A,
	DW_FORM_ADDRX					= 0x1B,
	DW_FORM_REF_SUP4				= 0x1C,
	DW_FORM_STRP_SUP				= 0x1D,
	DW_FORM_DATA16					= 0x1E,
	DW_FORM_LINE_STRP				= 0x1F,
	DW_FORM_REF_SIG8				= 0x20,
	DW_FORM_IMPLICIT_CONST			= 0x21,
	DW_FORM_LOCLISTX				= 0x22,
	DW_FORM_RNGLISTX				= 0x23,
	DW_FORM_REF_SUP8				= 0x24,
	DW_FORM_STRX1					= 0x25,
	DW_FORM_STRX2					= 0x26,
	DW_FORM_STRX3					= 0x27,
	DW_FORM_STRX4					= 0x28,
	DW_FORM_ADDRX1					= 0x29,
	DW_FORM_ADDRX2					= 0x2A,
	DW_FORM_ADDRX3					= 0x2B,
	DW_FORM_ADDRX4					= 0x2C,

	DW_NUM_FORMS,
} dwarf_form;

typedef struct dwarf_form_data
{
	dwarf_form kind;
	union
	{
		uintptr_t addr;
		struct
		{
			u8 length;
			u8 *data;
		} block1;
		struct
		{
			u16 length;
			u8 *data;
		} block2;
		struct
		{
			u32 length;
			u8 *data;
		} block4;
		struct
		{
			u64 length; // uleb128
			u8 *data;
		} block;
		// constant data
		u64 udata; // uleb128
		s64 sdata; // sleb128
		struct
		{
			u64 length; // uleb128
			u8 *expr;
		} exprloc;
		u8 flag; // if dw_form_flag_present implicitly set to true
		u64 sec_offset; // #note u32 for 32-bit
		u8 ref1;
		u16 ref2;
		u32 ref4;
		u64 ref8;
		u64 ref_udata; // uleb128
		u64 ref_addr; // #note u32 for 32-bit
		u64 ref_sig8;
		const char *string;
		u64 strp; // #note u32 for 32-bit
	};
	
} dwarf_form_data;

typedef enum dwarf_operations
{
	// reserved						= 0x01
	// reserved						= 0x02
	DW_OP_ADDR						= 0x03,
	// reserved						= 0x04
	// reserved						= 0x05
	DW_OP_DEREF						= 0x06,
	// reserved						= 0x07
	DW_OP_CONST1U					= 0x08,
	DW_OP_CONST1S					= 0x09,
	DW_OP_CONST2U					= 0x0A,
	DW_OP_CONST2S					= 0x0B,
	DW_OP_CONST4U					= 0x0C,
	DW_OP_CONST4S					= 0x0D,
	DW_OP_CONST8U					= 0x0E,
	DW_OP_CONST8S					= 0x0F,
	DW_OP_CONSTU					= 0x10,
	DW_OP_CONSTS					= 0x11,
	DW_OP_DUP						= 0x12,
	DW_OP_DROP						= 0x13,
	DW_OP_OVER						= 0x14,
	DW_OP_PICK						= 0x15,
	DW_OP_SWAP						= 0x16,
	DW_OP_ROT						= 0x17,
	DW_OP_XDEREF					= 0x18,
	DW_OP_ABS						= 0x19,
	DW_OP_AND						= 0x1A,
	DW_OP_DIV						= 0x1B,
	DW_OP_MINUS						= 0x1C,
	DW_OP_MOD						= 0x1D,
	DW_OP_MUL						= 0x1E,
	DW_OP_NEG						= 0x1F,
	DW_OP_NOT						= 0x20,
	DW_OP_OR						= 0x21,
	DW_OP_PLUS						= 0x22,
	DW_OP_PUSH_CONST				= 0x23,
	DW_OP_SHL						= 0x24,
	DW_OP_SHR						= 0x25,
	DW_OP_SHRA						= 0x26,
	DW_OP_XOR						= 0x27,
	DW_OP_BRA						= 0x28,
	DW_OP_EQ						= 0x29,
	DW_OP_GE						= 0x2A,
	DW_OP_GT						= 0x2B,
	DW_OP_LE						= 0x2C,
	DW_OP_LT						= 0x2D,
	DW_OP_NE						= 0x2E,
	DW_OP_SKIP						= 0x2F,
	DW_OP_LIT0						= 0x30,
	DW_OP_LIT1						= 0x31,
	DW_OP_LIT2						= 0x32,
	DW_OP_LIT3						= 0x33,
	DW_OP_LIT4						= 0x34,
	DW_OP_LIT5						= 0x35,
	DW_OP_LIT6						= 0x36,
	DW_OP_LIT7						= 0x37,
	DW_OP_LIT8						= 0x38,
	DW_OP_LIT9						= 0x39,
	DW_OP_LIT10						= 0x3A,
	DW_OP_LIT11						= 0x3B,
	DW_OP_LIT12						= 0x3C,
	DW_OP_LIT13						= 0x3D,
	DW_OP_LIT14						= 0x3E,
	DW_OP_LIT15						= 0x3F,
	DW_OP_LIT16						= 0x40,
	DW_OP_LIT17						= 0x41,
	DW_OP_LIT18						= 0x42,
	DW_OP_LIT19						= 0x43,
	DW_OP_LIT20						= 0x44,
	DW_OP_LIT21						= 0x45,
	DW_OP_LIT22						= 0x46,
	DW_OP_LIT23						= 0x47,
	DW_OP_LIT24						= 0x48,
	DW_OP_LIT25						= 0x49,
	DW_OP_LIT26						= 0x4A,
	DW_OP_LIT27						= 0x4B,
	DW_OP_LIT28						= 0x4C,
	DW_OP_LIT29						= 0x4D,
	DW_OP_LIT30						= 0x4E,
	DW_OP_LIT31						= 0x4F,
	DW_OP_REG0						= 0x50,
	DW_OP_REG1						= 0x51,
	DW_OP_REG2						= 0x52,
	DW_OP_REG3						= 0x53,
	DW_OP_REG4						= 0x54,
	DW_OP_REG5						= 0x55,
	DW_OP_REG6						= 0x56,
	DW_OP_REG7						= 0x57,
	DW_OP_REG8						= 0x58,
	DW_OP_REG9						= 0x59,
	DW_OP_REG10						= 0x5A,
	DW_OP_REG11						= 0x5B,
	DW_OP_REG12						= 0x5C,
	DW_OP_REG13						= 0x5D,
	DW_OP_REG14						= 0x5E,
	DW_OP_REG15						= 0x5F,
	DW_OP_REG16						= 0x60,
	DW_OP_REG17						= 0x61,
	DW_OP_REG18						= 0x62,
	DW_OP_REG19						= 0x63,
	DW_OP_REG20						= 0x64,
	DW_OP_REG21						= 0x65,
	DW_OP_REG22						= 0x66,
	DW_OP_REG23						= 0x67,
	DW_OP_REG24						= 0x68,
	DW_OP_REG25						= 0x69,
	DW_OP_REG26						= 0x6A,
	DW_OP_REG27						= 0x6B,
	DW_OP_REG28						= 0x6C,
	DW_OP_REG29						= 0x6D,
	DW_OP_REG30						= 0x6E,
	DW_OP_REG31						= 0x6F,
	DW_OP_BREG0						= 0x70,
	DW_OP_BREG1						= 0x71,
	DW_OP_BREG2						= 0x72,
	DW_OP_BREG3						= 0x73,
	DW_OP_BREG4						= 0x74,
	DW_OP_BREG5						= 0x75,
	DW_OP_BREG6						= 0x76,
	DW_OP_BREG7						= 0x77,
	DW_OP_BREG8						= 0x78,
	DW_OP_BREG9						= 0x79,
	DW_OP_BREG10					= 0x7A,
	DW_OP_BREG11					= 0x7B,
	DW_OP_BREG12					= 0x7C,
	DW_OP_BREG13					= 0x7D,
	DW_OP_BREG14					= 0x7E,
	DW_OP_BREG15					= 0x7F,
	DW_OP_BREG16					= 0x80,
	DW_OP_BREG17					= 0x81,
	DW_OP_BREG18					= 0x82,
	DW_OP_BREG19					= 0x83,
	DW_OP_BREG20					= 0x84,
	DW_OP_BREG21					= 0x85,
	DW_OP_BREG22					= 0x86,
	DW_OP_BREG23					= 0x87,
	DW_OP_BREG24					= 0x88,
	DW_OP_BREG25					= 0x89,
	DW_OP_BREG26					= 0x8A,
	DW_OP_BREG27					= 0x8B,
	DW_OP_BREG28					= 0x8C,
	DW_OP_BREG29					= 0x8D,
	DW_OP_BREG30					= 0x8E,
	DW_OP_BREG31					= 0x8F,
	DW_OP_REGX						= 0x90,
	DW_OP_FBREG						= 0x91,
	DW_OP_BREGX						= 0x92,
	DW_OP_PIECE						= 0x93,
	DW_OP_DEREF_SIZE				= 0x94,
	DW_OP_XDEREF_SIZE				= 0x95,
	DW_OP_NOP						= 0x96,
	DW_OP_PUSH_OBJECT_ADDRESS		= 0x97,
	DW_OP_CALL2						= 0x98,
	DW_OP_CALL4						= 0x99,
	DW_OP_CALL_REF					= 0x9A,
	DW_OP_FORM_TLS_ADDRESS			= 0x9B,
	DW_OP_CALL_FRAME_CFA			= 0x9C,
	DW_OP_BIT_PIECE					= 0x9D,
	DW_OP_IMPLICIT_VALUE			= 0x9E,
	DW_OP_STACK_VALUE				= 0x9F,
	DW_OP_IMPLICIT_POINTER			= 0xA0,
	DW_OP_ADDRX						= 0xA1,
	DW_OP_CONSTX					= 0xA2,
	DW_OP_ENTRY_VALUE				= 0xA3,
	DW_OP_CONST_TYPE				= 0xA4,
	DW_OP_REGVAL_TYPE				= 0xA5,
	DW_OP_DEREF_TYPE				= 0xA6,
	DW_OP_XDEREF_TYPE				= 0xA7,
	DW_OP_CONVERT					= 0xA8,
	DW_OP_REINTERPRET				= 0xA9,
	DW_OP_LO_USER					= 0xE0,
	DW_OP_HI_USER					= 0xFF
} dwarf_operations;

typedef enum dwarf_location_list_entry
{
	DW_LLE_END_OF_LIST				= 0x00,
	DW_LLE_BASE_ADDRESSX			= 0x01,
	DW_LLE_STARTX_ENDX				= 0x02,
	DW_LLE_STARTX_LENGTH			= 0x03,
	DW_LLE_OFFSET_PAIR				= 0x04,
	DW_LLE_DEFAULT_LOCATION			= 0x05,
	DW_LLE_BASE_ADDRESS				= 0x06,
	DW_LLE_START_END				= 0x07,
	DW_LLE_START_LENGTH				= 0x08
} dwarf_location_list_entry;

typedef enum dwarf_attribute_type_encodings
{
	DW_ATE_ADDRESS					= 0x01,
	DW_ATE_BOOLEAN					= 0x02,
	DW_ATE_COMPLEX_FLOAT			= 0x03,
	DW_ATE_FLOAT					= 0x04,
	DW_ATE_SIGNED					= 0x05,
	DW_ATE_SIGNED_CHAR				= 0x06,
	DW_ATE_UNSIGNED					= 0x07,
	DW_ATE_UNSIGNED_CHAR			= 0x08,
	DW_ATE_IMAGINARY_FLOAT			= 0x09,
	DW_ATE_PACKED_DECIMAL			= 0x0A,
	DW_ATE_NUMERIC_STRING			= 0x0B,
	DW_ATE_EDITED					= 0x0C,
	DW_ATE_SIGNED_FIXED				= 0x0D,
	DW_ATE_UNSIGNED_FIXED			= 0x0E,
	DW_ATE_DECIMAL_FLOAT			= 0x0F,
	DW_ATE_UTF						= 0x10,
	DW_ATE_UCS						= 0x11,
	DW_ATE_ASCII					= 0x12,
	DW_ATE_LO_USER					= 0x80,
	DW_ATE_HI_USER					= 0xFF
} dwarf_attribute_type_encodings;

typedef enum dwarf_decimal_sign
{
	DW_DS_UNSIGNED					= 0x01,
	DW_DS_LEADING_OVERPUNCH			= 0x02,
	DW_DS_TRAILING_OVERPUNCH		= 0x03,
	DW_DS_LEADING_SEPARATE			= 0x04,
	DW_DS_TRAILING_SEPARATE			= 0x05
} dwarf_decimal_sign;

typedef enum dwarf_endianess
{
	DW_END_DEFAULT					= 0x00,
	DW_END_BIG						= 0x01,
	DW_END_LITTLE					= 0x02,
	DW_END_LO_USER					= 0x40,
	DW_END_HI_USER					= 0xFF
} dwarf_endianess;

typedef enum dwarf_accessibility
{
	DW_ACCESS_PUBLIC				= 0x01,
	DW_ACCESS_PROTECTED				= 0x02,
	DW_ACCESS_PRIVAT				= 0x03
} dwarf_accessibility;

typedef enum dwarf_visibility
{
	DW_VIS_LOCAL					= 0x01,
	DW_VIS_EXPORTED					= 0x02,
	DW_VIS_QUALIFIED				= 0x03
} dwarf_visibility;

typedef enum dwarf_virtuality
{
	DW_VIRTUALITY_NONE				= 0x00,
	DW_VIRTUALITY_VIRTUAL			= 0x01,
	DW_VIRTUALITY_PURE_VIRTUAL		= 0x02,
} dwarf_virtuality;

typedef enum dwarf_language
{
	DW_LANG_C89						= 0x01,
	DW_LANG_C						= 0x02,
	DW_LANG_ADA83					= 0x03,
	DW_LANG_C_PLUS_PLUS				= 0x04,
	DW_LANG_COBOL74					= 0x05,
	DW_LANG_COBOL85					= 0x06,
	DW_LANG_FORTRAN77				= 0x07,
	DW_LANG_FORTRAN90				= 0x08,
	DW_LANG_PASCAL83				= 0x09,
	DW_LANG_MODULA2					= 0x0A,
	DW_LANG_JAVA					= 0x0B,
	DW_LANG_C99						= 0x0C,
	DW_LANG_ADA95					= 0x0D,
	DW_LANG_FORTRAN95				= 0x0E,
	DW_LANG_PLI						= 0x0F,
	DW_LANG_OBJC					= 0x10,
	DW_LANG_OBJC_PLUS_PLUS			= 0x11,
	DW_LANG_UPC						= 0x12,
	DW_LANG_D						= 0x13,
	DW_LANG_PYTHON					= 0x14,
	DW_LANG_OPENCL					= 0x15,
	DW_LANG_GO						= 0x16,
	DW_LANG_MODULA3					= 0x17,
	DW_LANG_HASKELL					= 0x18,
	DW_LANG_C_PLUS_PLUS_03			= 0x19,
	DW_LANG_C_PLUS_PLUS_11			= 0x1A,
	DW_LANG_OCAML					= 0x1B,
	DW_LANG_RUST					= 0x1C,
	DW_LANG_C11						= 0x1D,
	DW_LANG_SWIFT					= 0x1E,
	DW_LANG_JULIA					= 0x1F,
	DW_LANG_DYLAN					= 0x20,
	DW_LANG_C_PLUS_PLUS_14			= 0x21,
	DW_LANG_FORTRAN03				= 0x22,
	DW_LANG_FORTRAN08				= 0x23,
	DW_LANG_RENDERSCRIPT			= 0x24,
	DW_LANG_BLISS					= 0x25,

	DW_NUM_LANGS,

	DW_LANG_LO_USER					= 0x8000,
	DW_LANG_HI_USER					= 0xFFFF
} dwarf_language;

typedef enum dwarf_identifier_case
{
	DW_ID_CASE_SENSITIVE			= 0x00,
	DW_ID_UP_CASE					= 0x01,
	DW_ID_DOWN_CASE					= 0x02,
	DW_ID_CASE_INSENSITIVE			= 0x03
} dwarf_identifier_case;

typedef enum dwarf_calling_convention
{
	DW_CC_NORMAL					= 0x01,
	DW_CC_PROGRAM					= 0x02,
	DW_CC_NOCALL					= 0x03,
	DW_CC_PASS_BY_REFERENCE			= 0x04,
	DW_CC_PASS_BY_VALUE				= 0x05,
	DW_CC_LO_USER					= 0x40,
	DW_CC_HI_USER					= 0xFF
} dwarf_calling_convention;

typedef enum dwarf_inline
{
	DW_INL_NOT_INLINED				= 0x00,
	DW_INL_INLINED					= 0x01,
	DW_INL_DECLARED_NOT_INLINED		= 0x02,
	DW_INL_DECLARED_INLINED			= 0x03
} dwarf_inline;

typedef enum dwarf_array_ordering
{
	DW_ORD_ROW_MAJOR				= 0x00,
	DW_ORD_COL_MAJOR				= 0x01
} dwarf_array_ordering;

typedef enum dwarf_discriminant_lists
{
	DW_DSC_LABEL					= 0x00,
	DW_DSC_RANGE					= 0x01
} dwarf_discriminant_lists;

typedef enum dwarf_name_index
{
	DW_IDX_COMPLILE_UNIT			= 1,
	DW_IDX_TYPE_UNIT				= 2,
	DW_IDX_DIE_OFFSET				= 3,
	DW_IDX_PARENT					= 4,
	DW_IDX_TYPE_HASH				= 5,
	DW_IDX_LO_USER					= 0x2000,
	DW_IDX_HI_USER					= 0x3FFF
} dwarf_name_index;

typedef enum dwarf_default_member
{
	DW_DEFAULTED_NO					= 0x00,
	DW_DEFAULTED_IN_CLASS			= 0x01,
	DW_DEFAULTED_OUT_OF_CLASS		= 0x02
} dwarf_default_member;

typedef enum dwarf_line_number_information
{
	DW_LNS_COPY						= 0x01,
	DW_LNS_ADVANCE_PC				= 0x02,
	DW_LNS_ADVANCE_LINE				= 0x03,
	DW_LNS_SET_FILE					= 0x04,
	DW_LNS_SET_COLUMN				= 0x05,
	DW_LNS_NEGATE_STMT				= 0x06,
	DW_LNS_SET_BASIC_BLOCK			= 0x07,
	DW_LNS_CONST_ADD_PC				= 0x08,
	DW_LNS_FIXED_ADVANCE_PC			= 0x09,
	DW_LNS_SET_PROLOGUE_END			= 0x0A,
	DW_LNS_SET_EPILOGUE_BEGIN		= 0x0B,
	DW_LNS_SET_ISA					= 0x0C
} dwarf_line_number_information;

typedef enum dwarf_line_number_extended_opcodes
{
	DW_LNE_END_SEQUENCE				= 0x01,
	DW_LNE_SET_ADDRESS				= 0x02,
	// reserved						= 0x03
	DW_LNE_SET_DISCRIMINATOR		= 0x04,
	DW_LNE_LO_USER					= 0x80,
	DW_LNE_HI_USER					= 0xFF,
} dwarf_line_number_extended_opcodes;

typedef enum dwarf_line_number_header_entry
{
	DW_LNCT_PATH					= 0x01,
	DW_LNCT_DIRECTORY_INDEX			= 0x02,
	DW_LNCT_TIMESTAMP				= 0x03,
	DW_LNCT_SIZE					= 0x04,
	DW_LNCT_MD5						= 0x05,
	DW_LNCT_LO_USER					= 0x2000,
	DW_LNCT_HI_USER					= 0x3FFF
} dwarf_line_number_header_entry;

typedef enum dwarf_macro_info_entry_type
{
	DW_MACRO_DEFINE					= 0x01,
	DW_MACRO_UNDEF					= 0x02,
	DW_MACRO_START_FILE				= 0x03,
	DW_MACRO_END_FILE				= 0x04,
	DW_MACRO_DEFINE_STRP			= 0x05,
	DW_MACRO_UNDEF_STRP				= 0x06,
	DW_MACRO_IMPORT					= 0x07,
	DW_MACRO_DEFINE_SUP				= 0x08,
	DW_MACRO_UNDEF_SUP				= 0x09,
	DW_MACRO_IMPORT_UP				= 0x0A,
	DW_MACRO_DEFINE_STRX			= 0x0B,
	DW_MACRO_UNDEF_STRX				= 0x0C,
	DW_MACRO_LO_USER				= 0xE0,
	DW_MACRO_HI_USER				= 0xFF
} dwarf_macro_info_entry_type;

typedef enum dwarf_call_frame_encoding
{
	DW_CFA_ADVANCE_LOC				= 0x40, // #note | (delta & 0x3F)
	DW_CFA_OFFSET					= 0x80, // #note | (register & 0x3F), uleb128 offset operand
	DW_CFA_RESTORE					= 0xC0, // #note | (register & 0x3F)

											// operand1				-	operand2
	DW_CFA_NOP						= 0x00,
	DW_CFA_SET_LOC					= 0x01, // address				-	N/A
	DW_CFA_ADVANCE_LOC1				= 0x02, // 1-byte delta			-	N/A
	DW_CFA_ADVANCE_LOC2				= 0x03, // 2-byte delta			-	N/A
	DW_CFA_ADVANCE_LOC4				= 0x04, // 4-byte delta			-	N/A
	DW_CFA_OFFSET_EXTENDED			= 0x05, // uleb128 register		-	uleb128 offset
	DW_CFA_RESTORE_EXTENDED			= 0x06, // uleb128 register		-	N/A
	DW_CFA_UNDEFINED				= 0x07, // uleb128 register		-	N/A
	DW_CFA_SAME_VALUE				= 0x08, // uleb128 register		-	N/A
	DW_CFA_REGISTER					= 0x09, // uleb128 register		-	uleb128 offset
	DW_CFA_REMEMBER_STATE			= 0x0A,
	DW_CFA_RESTORE_STATE			= 0x0B,
	DW_CFA_DEF_CFA					= 0x0C, // uleb128 register		-	uleb128 offset
	DW_CFA_DEF_CFA_REGISTER			= 0x0D, // uleb128 register		-	N/A
	DW_CFA_DEF_CFA_OFFSET			= 0x0E, // uleb128 offset		-	N/A
	DW_CFA_DEF_CFA_EXPRESSION		= 0x0F, // block					-	N/A
	DW_CFA_EXPRESSION				= 0x10, // uleb128 register		-	block
	DW_CFA_OFFSET_EXTENDED_SF		= 0x11, // uleb128 register		-	sleb128 offset
	DW_CFA_DEF_CFA_SF				= 0x12, // uleb128 register		-	sleb128 offset
	DW_CFA_DEF_CFA_OFFSET_SF		= 0x13, // sleb128 offset		-	N/A
	DW_CFA_VAL_OFFSET				= 0x14, // uleb128				-	uleb128
	DW_CFA_VAL_OFFSET_SF			= 0x15, // uleb128				-	sleb128
	DW_CFA_VAL_EXPRESSION			= 0x16, // uleb128				-	block
	DW_CFA_LO_USER					= 0x1C,
	DW_CFA_HI_USER					= 0x3F
} dwarf_call_frame_encoding;

typedef enum dwarf_range_list_entry
{
	DW_RLE_END_OF_LIST				= 0x00,
	DW_RLE_BASE_ADDRESSX			= 0x01,
	DW_RLE_STARTX_ENDX				= 0x02,
	DW_RLE_STARTX_LENGTH			= 0x03,
	DW_RLE_OFFSET_PAIR				= 0x04,
	DW_RLE_BASE_ADDRESS				= 0x05,
	DW_RLE_START_END				= 0x06,
	DW_RLE_START_LENGTH				= 0x07
} dwarf_rang_list_entry;

typedef struct dwarf_abbrev_attr_entry
{
	dwarf_attribute attr;
	dwarf_form form;
} dwarf_abbrev_attr_entry;

typedef struct dwarf_abbrev_entry
{
	u64 index;
	dwarf_tag tag;
	bool has_children;
	dwarf_abbrev_attr_entry *attrs;

} dwarf_abbrev_entry;

typedef struct dwarf_abbrev_section
{
	u64 offset;
	dwarf_abbrev_entry *entries;
} dwarf_abbrev_section;

typedef struct dwarf_info_attr_entry
{
	dwarf_attribute attr;
	dwarf_form_data form;
} dwarf_info_attr_entry;

typedef struct dwarf_info_entry
{
	dwarf_tag tag;
	dwarf_info_attr_entry *attrs;
} dwarf_info_entry;

typedef struct dwarf_info_section
{
	bool is_32_bit;
	u64 length;
	u16 version;
	u8 ptr_size;
	u64 abbrev_offset;
	dwarf_info_entry *entries;
} dwarf_info_section;

typedef enum dwarf_section_kind
{
	DWARF_SECTION_NONE,
	DWARF_SECTION_ABBREV,
	DWARF_SECTION_INFO,
	DWARF_SECTION_LINE,
	DWARF_SECTION_STR
} dwarf_section_kind;

typedef struct dwarf_section_data
{
	dwarf_section_kind kind;
	u8 *data;
	usize size;

	union
	{
		dwarf_abbrev_section *abbrev_sections;
		dwarf_info_section *info_sections;
	};
	
} dwarf_section_data;

typedef struct dwarf_data
{
	dwarf_section_data abbrev;
	dwarf_section_data info;
	dwarf_section_data line;
	dwarf_section_data str;
	dwarf_section_data addr;
} dwarf_data;

typedef struct dwarf_function
{
	const char *name;
	u64 start_address;
	u64 past_end_address;
} dwarf_function;

dwarf_abbrev_section *parse_debug_abbrev(void *data, usize size);
void parse_debug_info(dwarf_data *dw);
void parse_dwarf_data(dwarf_data *dw);
dwarf_function *parse_dwarf_functions(dwarf_data *dw);

// result is u8[10], should be zero initialized
usize uleb128_encode(u64 raw, u8 *result);
// result is u8[10], should be zero initialized
usize sleb128_encode(s64 raw, u8 *result);
// input is u8[10]
usize uleb128_decode(u8 *input, u64 *result);
// input is u8[10]
usize sleb128_decode(u8 *input, s64 *result);

void test_leb128(void);

#endif // !XI_DWARF_H
