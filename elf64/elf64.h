#ifndef XI_ELF64_H
#define XI_ELF64_H

#include "../common/types.h"

#include "header_info.h"

#include "dwarf.h"

#if 0
typedef struct eh_frame_data
{
	u32 length;
	u64 extended_length;
	union
	{
		struct
		{
			u8 version;
			const char *aug_str;
			usize aug_str_len;
			u64 code_alignment_factor;
			s64 data_alignment_factor;
			u64 aug_len;
			u8 *aug_data;
			u8 *call_frame_instructions;
		} cie;
		struct
		{
			u32 cie_pointer;

		} fde;
	};
	
} eh_frame_data;
#endif

typedef struct elf_file
{
	elf64_file_header header;
	dwarf_function *functions;
} elf_file;

elf_file parse_elf(const char *filename);

#endif // !XI_ELF64_H
