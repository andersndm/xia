
#include "print_elf64.h"

#include "common/common.h"

#include "dwarf.h"

#include "ctype.h"

void elf_print_bytes(u8 *data, u8 *offset, u64 num_bytes)
{
	u8 *end = data + num_bytes;

	// calculate the number of digits needed to display the location
	u64 location = data - offset;
	u64 temp = location + num_bytes;
	u64 num_digits = 0;
	while (temp)
	{
		++num_digits;
		temp >>= 0x4;
	}
	if (num_digits % 2) { ++num_digits; }
	char buf[32];
	snprintf(buf, 32, "0x%%0%lulx  ", num_digits);
	while (data < end)
	{
		PC_BLACK(buf, location);
		for (u32 i = 0; i < 4; ++i)
		{
			if (data >= end) { break; }
			for (u32 j = 0; j < 4; ++j)
			{
				if (data >= end) { break; }
				PC_WHITE("%02x ", *data++);
				++location;
			}
			printf(" ");
		}
		printf("\n");
	}
	printf("\n");
}

void elf_print_data(u8 *data, u8 *offset, u64 num_bytes)
{
	u8 *end = data + num_bytes;

	// calculate the number of digits needed to display the location
	u64 location = data - offset;
	u64 temp = location + num_bytes;
	u64 num_digits = 0;
	while (temp)
	{
		++num_digits;
		temp >>= 0x4;
	}
	if (num_digits % 2) { ++num_digits; }
	char buf[32];
	snprintf(buf, 32, "0x%%0%lulx  ", num_digits);
	while (data < end)
	{
		PC_BLACK(buf, location);
		u8 *line_data = data;
		for (u32 i = 0; i < 4; ++i)
		{
			if (data >= end)
			{
				for (u32 j = i; j < 4; ++j)
				{
					printf("         ");
				}
				break;
			}
			for (u32 j = 0; j < 4; ++j)
			{
				if (data >= end)
				{
					for (u32 k = j; k < 4; ++k)
					{
						printf("  ");
					}
					break;
				}
				#if 1
				if (isprint((char)*data))
				{
					printf("%02x", *data++);
				}
				else
				{
					PC_WHITE("%02x", *data++);
				}
				#else
				PC_CYAN("%02x", *data++);
				#endif
				++location;
			}
			printf(" ");
		}
		
		for (u32 i = 0; i < 4; ++i)
		{
			if (line_data >= end) { break; }
			for (u32 j = 0; j < 4; ++j)
			{
				if (line_data >= end) { break; }
				if (isprint((char)*line_data))
				{
					#if 1
					printf("%c", *line_data);
					#else
					printf("%c", *line_data);
					#endif
				}
				else
				{
					PC_WHITE(".");
				}
				++line_data;
				++location;
			}
		}

		printf("\n");
	}
	printf("\n");
}

void elf_print_strings(char *data, u64 num_bytes)
{
	if (num_bytes <= 1) { return; };
	char *end = data + num_bytes;
	while (*data == 0) { ++data; }
	while (data < end)
	{
		PC_YELLOW("    \"%s\"\n", data);
		while (*data != 0) { ++data; }
		++data;
	}
}

void print_elf64_program_header(u8 *file_start, elf64_program_header *ph)
{
	PC_MAGENTA_BOLD("-- Program Header ------------------------------------------\n");
	PC_BLUE("type:        ");
	switch (ph->type)
	{
		case PT_NULL:    { printf("PT_NULL\n");    } break;
		case PT_LOAD:    { printf("PT_LOAD\n");    } break;
		case PT_DYNAMIC: { printf("PT_DYNAMIC\n"); } break;
		case PT_INTERP:  { printf("PT_INTERP\n");  } break;
		case PT_NOTE:    { printf("PT_NOTE\n");    } break;
		case PT_SHLIB:   { printf("PT_SHLIB\n");   } break;
		case PT_PHDR:    { printf("PT_PHDR\n");    } break;
		case PT_LOOS:    { printf("PT_LOOS\n");    } break;
		case PT_HIOS:    { printf("PT_HIOS\n");    } break;
		case PT_LOPROC:  { printf("PT_LOPROC\n");  } break;
		case PT_HIPROC:  { printf("PT_HIPROC\n");  } break;
		default:         { printf("%x <unknown>\n", ph->type);    } break;
	}

	PC_BLUE("permissions: ");
	if (ph->flags & PF_R) { printf("r");   } else { printf("-");   }
	if (ph->flags & PF_W) { printf("w");   } else { printf("-");   }
	if (ph->flags & PF_X) { printf("x\n"); } else { printf("-\n"); }
	PC_BLUE("offset:      "); printf("%lx\n", ph->offset);
	PC_BLUE("vaddr:       "); printf("%lx\n", ph->vaddr);
	PC_BLUE("paddr:       "); printf("%lx\n", ph->paddr);
	PC_BLUE("file_size:   "); printf("%lx\n", ph->file_size);
	PC_BLUE("mem_size:    "); printf("%lx\n", ph->mem_size);
	PC_BLUE("align:       "); printf("%lx\n", ph->align);

	if (ph->type == PT_INTERP)
	{
		#if 1
		printf("    interpreter: ");
		PC_YELLOW_BOLD("\"%s\"\n", (char *)file_start + ph->offset);
		#endif
	}
	else if (ph->type == PT_NOTE)
	{
		#if 0
		u8 *note_data = ((u8 *)file_start + ph->offset);
		u64 name_size = *(u64 *)note_data; note_data += sizeof(u64);
		u64 desc_size = *(u64 *)note_data; note_data += sizeof(u64);
		u64 type = *note_data++;
		PC_BLUE("name size:   "); printf("%lu\n", name_size);
		PC_BLUE("desc size:   "); printf("%lu\n", desc_size);
		PC_BLUE("type:        "); printf("%lu\n", type);
		PC_BLUE("name:        "); printf("%s\n", (char *)note_data);
		note_data = ALIGN_UP_PTR((u8 *)note_data + name_size + 1, 8); // add null terminator
		PC_BLUE("desc:        "); printf("%s\n", (char *)note_data);
		#endif
	}
	else if (ph->type == PT_DYNAMIC)
	{
		#if 0
		elf64_dynamic_entry *dyn = (void *)((u8 *)file_start + ph->offset);
		char *dyn_strtable = NULL;
		u8 *dyn_symtab = NULL;
		for (elf64_dynamic_entry *ptr = dyn; ptr->tag != DT_NULL; ++ptr)
		{
			if (ptr->tag == DT_STRTAB)
			{
				dyn_strtable = (char *)file_start + ptr->ptr;
				break;
			}
		}
		if (dyn_strtable == NULL)
		{
			PC_RED_BOLD("no dyn strtable in program\n");
			return;
		}
		while (dyn->tag != DT_NULL)
		{
			switch (dyn->tag)
			{
				case DT_NULL:
				{
					PC_CYAN_BOLD("    null               ");
				} break;
				case DT_NEEDED:
				{
					PC_CYAN_BOLD("    needed             ");
					printf("%s\n", dyn_strtable + dyn->val);
				} break;
				case DT_PLTRELSZ:
				{
					PC_CYAN_BOLD("    pltrelsz           ");
					printf("%16lx\n", dyn->val);
				} break;
				case DT_PLTGOT:
				{
					PC_CYAN_BOLD("    pltgot             ");
					printf("%016lx\n", dyn->ptr);
				} break;
				case DT_HASH:
				{
					PC_CYAN_BOLD("    hash               ");
					printf("%016lx\n", dyn->ptr);
				} break;
				case DT_STRTAB:
				{
					PC_CYAN_BOLD("    strtab             ");
					printf("%016lx\n", dyn->ptr);
				} break;
				case DT_SYMTAB:
				{
					dyn_symtab = (u8 *)file_start + dyn->ptr;
					PC_CYAN_BOLD("    symtab             ");
					printf("%016lx\n", dyn->ptr);
				} break;
				case DT_RELA:
				{
					PC_CYAN_BOLD("    rela               ");
					printf("%016lx\n", dyn->ptr);
				} break;
				case DT_RELASZ:
				{
					PC_CYAN_BOLD("    relasz             ");
					printf("%16lx\n", dyn->val);
				} break;
				case DT_RELAENT:
				{
					PC_CYAN_BOLD("    relaent            ");
					printf("%16lx\n", dyn->val);
				} break;
				case DT_STRSZ:
				{
					PC_CYAN_BOLD("    strsz              ");
					printf("%16lx\n", dyn->val);
				} break;
				case DT_SYMENT:
				{
					PC_CYAN_BOLD("    syment             ");
					printf("%16lx\n", dyn->val);
				} break;
				case DT_INIT:
				{
					PC_CYAN_BOLD("    init               ");
					printf("%016lx\n", dyn->ptr);
				} break;
				case DT_FINI:
				{
					PC_CYAN_BOLD("    fini               ");
					printf("%016lx\n", dyn->ptr);
				} break;
				case DT_SONAME:
				{
					PC_CYAN_BOLD("    soname             ");
					printf("%s\n", dyn_strtable + dyn->val);
				} break;
				case DT_RPATH:
				{
					PC_CYAN_BOLD("    rpath              ");
					printf("%s\n", dyn_strtable + dyn->val);
				} break;
				case DT_SYMBOLIC:
				{
					PC_CYAN_BOLD("    symbolic           ");
				} break;
				case DT_REL:
				{
					PC_CYAN_BOLD("    rel                ");
					printf("%016lx\n", dyn->ptr);
				} break;
				case DT_RELSZ:
				{
					PC_CYAN_BOLD("    relsz              ");
					printf("%16lx\n", dyn->val);
				} break;
				case DT_RELENT:
				{
					PC_CYAN_BOLD("    relent             ");
					printf("%16lx\n", dyn->val);
				} break;
				case DT_PLTREL:
				{
					PC_CYAN_BOLD("    pltrel             ");
					printf("%16lx\n", dyn->val);
				} break;
				case DT_DEBUG:
				{
					PC_CYAN_BOLD("    debug              ");
					printf("%016lx\n", dyn->ptr);
				} break;
				case DT_TEXTREL:
				{
					PC_CYAN_BOLD("    textrel            ");
				} break;
				case DT_JMPREL:
				{
					PC_CYAN_BOLD("    jmprel             ");
					printf("%016lx\n", dyn->ptr);
				} break;
				case DT_BIND_NOW:
				{
					PC_CYAN_BOLD("    bind_now           ");
				} break;
				case DT_INIT_ARRAY:
				{
					PC_CYAN_BOLD("    init_array         ");
					printf("%016lx\n", dyn->ptr);
				} break;
				case DT_FINI_ARRAY:
				{
					PC_CYAN_BOLD("    fini_array         ");
					printf("%016lx\n", dyn->ptr);
				} break;
				case DT_INIT_ARRAYSZ:
				{
					PC_CYAN_BOLD("    init_arraysz       ");
					printf("%16lx\n", dyn->val);
				} break;
				case DT_FINI_ARRAYSZ:
				{
					PC_CYAN_BOLD("    fini_arraysz       ");
					printf("%16lx\n", dyn->val);
				} break;
				default:
				{
					if (dyn->tag >= DT_LOOS && dyn->tag <= DT_HIOS)
					{
						PC_CYAN_BOLD("    env spec           "); printf("%08lx\n", dyn->tag);
					}
					else if (dyn->tag >= DT_LOPROC && dyn->tag <= DT_HIPROC)
					{
						PC_CYAN_BOLD("    proc spec          "); printf("%08lx\n", dyn->tag);
					}
					else
					{
						PC_CYAN_BOLD("    unknown dyn tag    "); printf("%16lx\n", dyn->val);
					}
				} break;
			}
			++dyn;
		}
		#endif
		#if 0
		if (dyn_symtab)
		{
			int count = 0;
			
			for (u32 i = 0; i < 100; ++i)
			{
				PC_WHITE_BOLD("-- Symbol ----------------------------------------------------\n");
				elf64_symbol sym = *((elf64_symbol *)dyn_symtab + i);
				const char *sym_name = dyn_strtable + sym.name;
				PC_BLUE("name: "); PC_YELLOW_BOLD("\"%s\"\n", sym_name);
				PC_BLUE("binding:    ");
				switch (sym.info >> 4)
				{
					case 0: { printf("STB_LOCAL\n"); } break;
					case 1: { printf("STB_GLOBAL\n"); } break;
					case 2: { printf("STB_WEAK\n"); } break;
					case 10: { printf("STB_LOOS\n"); } break;
					case 12: { printf("STB_HIOS\n"); } break;
					case 13: { printf("STB_LOPROC\n"); } break;
					case 15: { printf("STB_HIPROC\n"); } break;
					default: { printf("unknown binding\n"); } break;
				}
				PC_BLUE("type:       ");
				switch (sym.info & 0x0F)
				{
					case 0: { printf("STT_NOTYPE\n"); } break;
					case 1: { printf("STT_OBJECT\n"); } break;
					case 2: { printf("STT_FUNC\n"); } break;
					case 3: { printf("STT_SECTION\n"); } break;
					case 4: { printf("STT_FILE\n"); } break;
					case 5: { printf("STT_COMMON\n"); } break;
					case 6: { printf("STT_TLS\n"); } break;
					case 10: { printf("STT_LOOS\n"); } break;
					case 12: { printf("STT_HIOS\n"); } break;
					case 13: { printf("STT_LOPROC\n"); } break;
					case 15: { printf("STT_HIPROC\n"); } break;
				}
				PC_BLUE("visibility: ");
				switch (sym.other & 0x3)
				{
					case 0: { printf("STV_DEFAULT\n"); } break;
					case 1: { printf("STV_INTERNAL\n"); } break;
					case 2: { printf("STV_HIDDEN\n"); } break;
					case 3: { printf("STV_PROTECTED\n"); } break;
					default: { printf("unknown visibility\n"); } break;
				}
				PC_BLUE("shndx:      ");
				switch (sym.shndx)
				{
					case 0x0000: { printf("SHN_UNDEF\n"); } break;
					case 0xFF00: { printf("SHN_LORESERVE | SHN_LOPROC | SHN_BEFORE\n"); } break;
					case 0xFF01: { printf("SHN_AFTER\n"); } break;
					case 0xFF02: { printf("SHN_AMD64_LCOMMON\n"); } break;
					case 0xFF1F: { printf("SHN_HIPROC\n"); } break;
					case 0xFF20: { printf("SHN_LOOS\n"); } break;
					case 0xFF3F: { printf("SHN_LOSUNW | SHN_SUNW_IGNORE | SHN_HISUNW | SHN_HIOS\n"); } break;
					case 0xFFF1: { printf("SHN_ABS\n"); } break;
					case 0xFFF2: { printf("SHN_COMMON\n"); } break;
					case 0xFFFF: { printf("SHN_XINDEX | SHN_HIRESERVE\n"); } break;
					default: { printf("%u\n", sym.shndx); } break;
				}
				PC_BLUE("value:      "); printf("0x%08lx\n", sym.value);
				PC_BLUE("size:       "); printf("0x%08lx\n", sym.size);

				printf("\n");
				++count;
			}
			PC_WHITE_BOLD("--------------------------------------------------------------\n");
		}
		#endif
	}
	PC_MAGENTA_BOLD("------------------------------------------------------------\n");
}

void print_elf64_section_header(u8 *file_start, elf64_section_header *sh, elf64_section_header *symtab,
								char *sh_str_start, u16 i)
{
	PC_YELLOW_BOLD("-- Section Header -----------------------------------------\n");
	PC_BLUE("index:       "); printf("%u\n", i);
	if (sh->type == SHT_NULL || sh->type > SHT_LOOS)
	{
		PC_BLUE("type:        ");
		if (sh->type == SHT_NULL) { printf("SHT_NULL\n"); }
		else if (sh->type >= SHT_LOOS && sh->type < SHT_LOPROC)
		{
			printf("Environment Specific\n");
			PC_BLUE("name:        "); PC_YELLOW("%s\n", sh_str_start + sh->name_offset);
		}
		else
		{
			printf("Processor specific\n");
			PC_BLUE("name:        "); PC_YELLOW("%s\n", sh_str_start + sh->name_offset);
		}
		PC_YELLOW_BOLD("-----------------------------------------------------------\n");
		return;
	}

	PC_BLUE("name:        "); PC_YELLOW("%s\n", sh_str_start + sh->name_offset);
	PC_BLUE("type:        ");
	switch (sh->type)
	{
		case 0x0: { printf("SHT_NULL"); } break;
		case 0x1: { printf("SHT_PROGBITS"); } break;
		case 0x2: { printf("SHT_SYMTAB"); } break;
		case 0x3: { printf("SHT_STRTAB"); } break;
		case 0x4: { printf("SHT_RELA"); } break;
		case 0x5: { printf("SHT_HASH"); } break;
		case 0x6: { printf("SHT_DYNAMIC"); } break;
		case 0x7: { printf("SHT_NOTE"); } break;
		case 0x8: { printf("SHT_NOBITS"); } break;
		case 0x9: { printf("SHT_REL"); } break;
		case 0xA: { printf("SHT_SHLIB"); } break;
		case 0xB: { printf("SHT_DYNSYM"); } break;
		case 0xE: { printf("SHT_INIT_ARRAY"); } break;
		case 0xF: { printf("SHT_FINI_ARRAY"); } break;
		case 0x10: { printf("SHT_PREINIT_ARRAY"); } break;
		case 0x11: { printf("SHT_GROUP"); } break;
		case 0x12: { printf("SHT_SYMTAB_SHNDX"); } break;
		case 0x13: { printf("SHT_NUM"); } break;
		case 0x60000000: { printf("SHT_LOOS"); } break;
		default: { printf("unknown section type: 0x%02x\n", sh->type); }
	}
	printf("\n");
	if (sh->flags)
	{
		PC_BLUE("flags:       ");
		if (sh->flags & SHF_WRITE) { printf("SHF_WRITE "); }
		if (sh->flags & SHF_ALLOC) { printf("SHF_ALLOC "); }
		if (sh->flags & SHF_EXECINSTR) { printf("SHF_EXECINSTR "); }
		if (sh->flags & SHF_MERGE) { printf("SHF_MERGE "); }
		if (sh->flags & SHF_STRINGS) { printf("SHF_STRINGS "); }
		if (sh->flags & SHF_INFO_LINK) { printf("SHF_INFO_LINK "); }
		if (sh->flags & SHF_LINK_ORDER) { printf("SHF_LINK_ORDER "); }
		if (sh->flags & SHF_OS_NONCONFORMING) { printf("SHF_OS_NONCONFORMING "); }
		if (sh->flags & SHF_GROUP) { printf("SHF_GROUP "); }
		if (sh->flags & SHF_TLS) { printf("SHF_TLS "); }
		if (sh->flags & SHF_MASKOS) { printf("SHF_MASKOS "); }
		if (sh->flags & SHF_MASKPROC) { printf("SHF_MASKPROC "); }
		if (sh->flags & SHF_ORDERED) { printf("SHF_ORDERED "); }
		if (sh->flags & SHF_EXCLUDE) { printf("SHF_EXCLUDE "); }
		printf("\n");
	}

	if (sh->addr)      { PC_BLUE("addr:        "); printf("0x%08lx\n", sh->addr); }
	if (sh->offset)    { PC_BLUE("offset:      "); printf("0x%016lx\n", sh->offset); }
	if (sh->size)      { PC_BLUE("size:        "); printf("0x%016lx\n", sh->size); }
	if (sh->link)      { PC_BLUE("link:        ");  printf("%u\n", sh->link); }
	if (sh->info)      { PC_BLUE("info:        ");  printf("%u\n", sh->info); }
	if (sh->addralign) { PC_BLUE("addralign:   "); printf("0x%lx\n", sh->addralign); }
	if (sh->entsize)   { PC_BLUE("entsize:     ");  printf("%lu (0x%lx)\n", sh->entsize, sh->entsize); }

	PC_YELLOW_BOLD("-----------------------------------------------------------\n");

	#if 0
	u8 *data = (u8 *)file_start + sh->offset;
	usize size = sh->size;
	const char *name = sh_str_start + sh->name_offset;

	if (sh->type == SHT_RELA)
	{
		while (data < (u8 *)file_start + sh->offset + sh->size)
		{
			elf64_rela rela = *(elf64_rela *)data;
			u32 rela_index = (u32)(rela.info >> 32);
			u32 rela_type = (u32)(rela.info & 0xFFFFFFFF);
			elf64_symbol *sym = (elf64_symbol *)((u8 *)file_start + symtab->offset) + rela_index;
			printf("    rela @ %lx\n", data - (u8 *)file_start);
			PC_GREEN("offset:       "); printf("%16lx\n", rela.offset);
			char *sym_name = str_start + sym->name;
			if (*sym_name == 0)
			{
				PC_GREEN("symbol index: "); printf("%8x\n", rela_index);
			}
			else
			{
				PC_GREEN("symbol name:  "); PC_YELLOW_BOLD("\"%s\"\n", str_start + sym->name);
			}
			if (sym->shndx == SHN_UNDEF)
			{
				printf("   external symbol\n");
			}
			else
			{
				printf("    defined in ");
				PC_YELLOW("\"%s\"\n", sh_str_start + (first_sh + sym->shndx)->name_offset);
			}
			PC_GREEN("rela type:  "); printf("(%x) ", rela_type);
			switch (rela_type)
			{
				case ELF_RELA_TYPE_NONE:
				{
				} break;
				case ELF_RELA_TYPE_64:
				{
					printf("ELF_RELA_TYPE_64");
				} break;
				case ELF_RELA_TYPE_PC32:
				{
					printf("ELF_RELA_TYPE_PC32");
				} break;
				case ELF_RELA_TYPE_GOT32:
				{
					printf("ELF_RELA_TYPE_GOT32");
				} break;
				case ELF_RELA_TYPE_PLT32:
				{
					printf("ELF_RELA_TYPE_PLT32");
				} break;
				case ELF_RELA_TYPE_COPY:
				{
					printf("ELF_RELA_TYPE_COPY");
				} break;
				case ELF_RELA_TYPE_GLOB_DAT:
				{
					printf("ELF_RELA_TYPE_GLOB_DAT");
				} break;
				case ELF_RELA_TYPE_JUMP_SLOT:
				{
					printf("ELF_RELA_TYPE_JUMP_SLOT");
				} break;
				case ELF_RELA_TYPE_RELATIVE:
				{
					printf("ELF_RELA_TYPE_RELATIVE");
				} break;
				case ELF_RELA_TYPE_GOTPCREL:
				{
					printf("ELF_RELA_TYPE_GOTPCREL");
				} break;
				case ELF_RELA_TYPE_32:
				{
					printf("ELF_RELA_TYPE_32");
				} break;
				case ELF_RELA_TYPE_32S:
				{
					printf("ELF_RELA_TYPE_32S");
				} break;
				case ELF_RELA_TYPE_16:
				{
					printf("ELF_RELA_TYPE_16");
				} break;
				case ELF_RELA_TYPE_PC16:
				{
					printf("ELF_RELA_TYPE_PC16");
				} break;
				case ELF_RELA_TYPE_8:
				{
					printf("ELF_RELA_TYPE_8");
				} break;
				case ELF_RELA_TYPE_PC8:
				{
					printf("ELF_RELA_TYPE_PC8");
				} break;
				case ELF_RELA_TYPE_DTPMOD64:
				{
					printf("ELF_RELA_TYPE_DTPMOD64");
				} break;
				case ELF_RELA_TYPE_DTPOFF64:
				{
					printf("ELF_RELA_TYPE_DTPOFF64");
				} break;
				case ELF_RELA_TYPE_TPOFF64:
				{
					printf("ELF_RELA_TYPE_TPOFF64");
				} break;
				case ELF_RELA_TYPE_TLSGD:
				{
					printf("ELF_RELA_TYPE_TLSGD");
				} break;
				case ELF_RELA_TYPE_TLSLD:
				{
					printf("ELF_RELA_TYPE_TLSLD");
				} break;
				case ELF_RELA_TYPE_DTPOFF32:
				{
					printf("ELF_RELA_TYPE_DTPOFF32");
				} break;
				case ELF_RELA_TYPE_GOTTPOFF:
				{
					printf("ELF_RELA_TYPE_GOTTPOFF");
				} break;
				case ELF_RELA_TYPE_TPOFF32:
				{
					printf("ELF_RELA_TYPE_TPOFF32");
				} break;
				case ELF_RELA_TYPE_PC64:
				{
					printf("ELF_RELA_TYPE_PC64");
				} break;
				case ELF_RELA_TYPE_GOTOFF64:
				{
					printf("ELF_RELA_TYPE_GOTOFF64");
				} break;
				case ELF_RELA_TYPE_GOTPC32:
				{
					printf("ELF_RELA_TYPE_GOTPC32");
				} break;
				case ELF_RELA_TYPE_SIZE32:
				{
					printf("ELF_RELA_TYPE_SIZE32");
				} break;
				case ELF_RELA_TYPE_SIZE64:
				{
					printf("ELF_RELA_TYPE_SIZE64");
				} break;
				case ELF_RELA_TYPE_GOTPC32_TLSDESC:
				{
					printf("ELF_RELA_TYPE_GOTPC32_TLSDESC");
				} break;
				case ELF_RELA_TYPE_TLSDESC_CALL:
				{
					printf("ELF_RELA_TYPE_TLSDESC_CALL");
				} break;
				case ELF_RELA_TYPE_TLSDESC:
				{
					printf("ELF_RELA_TYPE_TLSDESC");
				} break;
				case ELF_RELA_TYPE_IRELATIVE:
				{
					printf("ELF_RELA_TYPE_IRELATIVE");
				} break;
				default:
				{
					printf("unknown relocation type");
				} break;
			}
			printf("\n");
			PC_GREEN("addend:       "); printf("%ld - %016lx\n", rela.addend, rela.addend);
			printf("\n");
			data += sizeof(elf64_rela);
		}
	}
	else if (false) //strcmp(name, ".eh_frame") == 0)
	{
		while (data < (u8 *)file_start + sh->offset + sh->size)
		{
			u8 *start_data = data;

			u64 extended_length = 0;
			u32 length = *(u32 *)data;
			data += sizeof(u32);
			if (length == 0)
			{
				// end
			}
			else if (length == 0xFFFFFFFF)
			{
				extended_length = *(u64 *)data;
				data += sizeof(u64);
			}

			u32 CIE_ID = *(u32 *)data;
			data += sizeof(u32);
			ASSERT(CIE_ID == 0); // #note 0 for CIE
			u8 version = *data++;
			ASSERT(version == 1);
			const char *aug_str = (const char *)data;
			usize aug_str_len = strlen(aug_str) + 1;
			data += aug_str_len;

			u64 code_align_factor = 0;
			usize bytes_read = uleb128_decode(data, &code_align_factor);
			data += bytes_read;
			s64 data_align_factor = 0;
			bytes_read = sleb128_decode(data, &data_align_factor);
			data += bytes_read;

			bool contains_z = false;
			for (usize i = 0; i < aug_str_len; ++i)
			{
				if (*(aug_str + i) == 'z')
				{
					contains_z = true;
					break;
				}
			}

			u64 aug_length = 0;
			u8 *aug_data = 0;
			if (contains_z)
			{
				bytes_read = uleb128_decode(data, &aug_length);
				data += bytes_read;
				aug_data = data;
				data += aug_length;
			}

			intptr_t offset = data - start_data;
			usize remaining_bytes = (length + 4) - (data - start_data);

			if (extended_length)
			{
				data += (extended_length + 4 + 8) - (data - start_data);
			}
			else
			{
				data += (length + 4) - (data - start_data);
			}

			u64 fde_extended_length = 0;
			u32 fde_length = *(u32 *)data;
			data += sizeof(u32);
			if (fde_length == 0xFFFFFFFF)
			{
				fde_extended_length = *(u64 *)data;
				data += sizeof(u64);
			}

			u32 cie_pointer = *(u32 *)data;
			data += sizeof(u32);
		}
	}
	#endif
}

void print_elf64_programs(void *file_start, elf64_program_header *first_ph, u16 num_programs)
{
	for (u16 i = 0; i < num_programs; ++i)
	{
		print_elf64_program_header(file_start, first_ph + i);
	}
}

void print_elf64_sections(void *file_start, elf64_section_header *first_sh, u16 num_sections, u16 name_index)
{
	elf64_section_header *name_sh = first_sh + name_index;
	char *sh_str_start = (char *)file_start + name_sh->offset;
	char *str_start = NULL;
	elf64_section_header *symtab = NULL;

	for (u16 i = 0; i < num_sections; ++i)
	{
		elf64_section_header *sh = first_sh + i;
		if (sh->type == SHT_STRTAB)
		{
			str_start = (char *)file_start + sh->offset;
			break;
		}
	}
	ASSERT(str_start);
	for (u16 i = 0; i < num_sections; ++i)
	{
		elf64_section_header *sh = first_sh + i;
		if (sh->type == SHT_SYMTAB)
		{
			symtab = sh;
			break;
		}
	}
	ASSERT(symtab);

	for (u16 i = 0; i < num_sections; ++i)
	{
		elf64_section_header *sh = first_sh + i;
	}
}

void print_elf64_header(elf64_file_header *header)
{
	PC_GREEN_BOLD("-- ELF64 File Header ---------------------------------------\n");
	PC_BLUE("endianness: "); printf("little\n");

	PC_BLUE("osabi:      ");
	switch (header->osabi)
	{
		case 0x00: { printf("System V\n"); } break;
		case 0x01: { printf("HP-UX\n"); } break;
		case 0x02: { printf("NetBSD\n"); } break;
		case 0x03: { printf("Linux\n"); } break;
		case 0x04: { printf("GNU Hurd\n"); } break;
		case 0x06: { printf("Solaris\n"); } break;
		case 0x07: { printf("AIX\n"); } break;
		case 0x08: { printf("IRIX\n"); } break;
		case 0x09: { printf("FreeBSD\n"); } break;
		case 0x0A: { printf("True64\n"); } break;
		case 0x0B: { printf("Novell Modesto\n"); } break;
		case 0x0C: { printf("OpenBSD\n"); } break;
		case 0x0D: { printf("OpenVMS\n"); } break;
		case 0x0E: { printf("NonStop Kernel\n"); } break;
		case 0x0F: { printf("AROS\n"); } break;
		case 0x10: { printf("Fenix OS\n"); } break;
		case 0x11: { printf("CloudABI\n"); } break;
		case 0x12: { printf("Stratus Technologies OpenVOS\n"); } break;
		default: { printf("unknown target operating system\n"); } break;
	}
	if (header->abi_version)
	{
		PC_BLUE("abi version: "); printf("0x%02x\n", header->abi_version);
	}

	PC_BLUE("type:       ");
	switch (header->type)
	{
		case 0x0000: { printf("ET_NONE\n"); } break;
		case 0x0001: { printf("ET_REL\n"); } break;
		case 0x0002: { printf("ET_EXEC\n"); } break;
		case 0x0003: { printf("ET_DYN\n"); } break;
		case 0x0004: { printf("ET_CORE\n"); } break;
		case 0xfe00: { printf("ET_LOOS\n"); } break;
		case 0xfeff: { printf("ET_HIOS\n"); } break;
		case 0xff00: { printf("ET_LOPROC\n"); } break;
		case 0xffff: { printf("ET_HIPROC\n"); } break;
	}

	PC_BLUE("isa:        ");
	switch (header->machine)
	{
		case 0x01: { printf("Unspecified\n"); } break;
		case 0x02: { printf("AT&T WE 32100\n"); } break;
		case 0x03: { printf("SPARC\n"); } break;
		case 0x04: { printf("x86\n"); } break;
		case 0x05: { printf("Motorola 68000 (M68K)\n"); } break;
		case 0x06: { printf("Motorola 88000 (M88K)\n"); } break;
		case 0x07: { printf("Intel MCU\n"); } break;
		case 0x08: { printf("Intel 80860\n"); } break;
		case 0x09: { printf("MIPS\n"); } break;
		case 0x0A: { printf("IBM_System/370\n"); } break;
		case 0x0B: case 0x0C: case 0x0D: { printf("Reserved\n"); } break;
		case 0x0E: { printf("Hewlett-Packard PA-RISC\n"); } break;
		case 0x0F: { printf("Reserved\n"); } break;
		case 0x13: { printf("Intel 80960\n"); } break;
		case 0x14: { printf("PowerPC\n"); } break;
		case 0x15: { printf("PowerPC (64-bit)\n"); } break;
		case 0x16: { printf("S390\n"); } break;
		case 0x28: { printf("ARM (32-bit)\n"); } break;
		case 0x2A: { printf("SuperH\n"); } break;
		case 0x32: { printf("IA-64\n"); } break;
		case 0x3E: { printf("amd64\n"); } break;
		case 0x8C: { printf("TMS320C6000\n"); } break;
		case 0xB7: { printf("ARM (64-bit)\n"); } break;
		case 0xF3: { printf("RISC-V\n"); } break;
	}

	if (header->version2 != 1) { printf("unknown version %u\n", header->version); }
	PC_BLUE("entry:      "); printf("0x%08lx\n", header->entry);
	PC_BLUE("phoff:      "); printf("0x%08lx\n", header->phoff);
	PC_BLUE("shoff:      "); printf("0x%08lx\n", header->shoff);
	if (header->flags != 0)
	{
		printf("flags:      0x%04x\n", header->flags);
	}
	PC_BLUE("ehsize:     "); printf("%u (0x%02x)\n", header->ehsize, header->ehsize);
	PC_BLUE("phentsize:  "); printf("%u (0x%02x)\n", header->phentsize, header->phentsize);
	PC_BLUE("phnum:      "); printf("%u\n", header->phnum);
	PC_BLUE("shentsize:  "); printf("%u (0x%02x)\n", header->shentsize, header->shentsize);
	PC_BLUE("shnum:      "); printf("%u\n", header->shnum);
	PC_BLUE("shstrndx:   "); printf("%u\n", header->shstrndx);
	PC_GREEN_BOLD("------------------------------------------------------------\n");
}
