#ifndef XI_PRINT_ELF64_H
#define XI_PRINT_ELF64_H

#include "common/types.h"

#include "elf64/header_info.h"

void elf_print_bytes(u8 *data, u8 *offset, u64 num_bytes);
void elf_print_data(u8 *data, u8 *offset, u64 num_bytes);
void elf_print_strings(char *data, u64 num_bytes);

void print_elf64_header(elf64_file_header *header);
void print_elf64_program_header(u8 *file_start, elf64_program_header *ph);
void print_elf64_section_header(u8 *file_start, elf64_section_header *sh, elf64_section_header *symtab,
								char *sh_str_start, u16 i);

void print_elf64_sections(void *file_start, elf64_section_header *first_sh, u16 num_sections,
						  u16 name_index);
void print_elf64_programs(void *file_start, elf64_program_header *first_ph, u16 num_programs);

#endif // !XI_PRINT_ELF64_H
