#include "elf64.h"

#include <string.h>

#include "common/common.h"
#include "header_info.h"
#include "print_elf64.h"

#include "dwarf.h"

dwarf_data gather_dwarf_sections(void *file_start, elf64_section_header *first_sh, u16 num_sections, u16 name_index)
{
	dwarf_data result = { 0 };
	elf64_section_header *name_sh = first_sh + name_index;
	char *str_start = (char *)file_start + name_sh->offset;

	for (u16 i = 0; i < num_sections; ++i)
	{
		elf64_section_header *sh = first_sh + i;
		if (sh->type == SHT_NULL || sh->type > SHT_LOOS) { continue; }

		u8 *data = (u8 *)file_start + sh->offset;
		usize size = sh->size;
		const char *name = str_start + sh->name_offset;

		if (strcmp(name, ".debug_abbrev") == 0)
		{
			result.abbrev.data = data;
			result.abbrev.size = size;
		}
		else if (strcmp(name, ".debug_info") == 0)
		{
			result.info.data = data;
			result.info.size = size;
		}
		else if (strcmp(name, ".debug_line") == 0)
		{
			result.line.data = data;
			result.line.size = size;
		}
		else if (strcmp(name, ".debug_str") == 0)
		{
			result.str.data = data;
			result.str.size = size;
		}
		else if (strcmp(name, ".debug_addr") == 0)
		{
			result.addr.data = data;
			result.addr.size = size;
		}
		#if 0
		else if (strcmp(name, ".debug_str_offsets") == 0)
		{
			result.str_offsets.data = data;
			result.str_offsets.size = size;
		}
		#endif
	}
	return result;
}

elf_file parse_elf(const char *filename)
{
	elf_file result = { 0 };
	usize fsize = 0;
	void *elf_contents = read_file(filename, &fsize);
	if (elf_contents == NULL) { return result; }

	//printf("%s - %ld B\n", filename, fsize);

	elf64_file_header *header = elf_contents;

	char *magic_number = (char *)(&header->magic_number);
	if (magic_number[0] != 0x7F || magic_number[1] != 'E' ||
		magic_number[2] != 'L' || magic_number[3] != 'F')
	{
		PC_RED_BOLD("file is not in ELF format\n");
		return result;
	}

	if (header->class != 2)
	{
		PC_RED_BOLD("ELF file is not 64-bit\n");
		return result;
	}

	if (header->data != 1)
	{
		PC_RED_BOLD("ELF file is not little-endian\n");
		return result;
	}

	memcpy(&result.header, header, sizeof(elf64_file_header));

	void *ph_start = (u8 *)elf_contents + result.header.phoff;
	void *sh_start = (u8 *)elf_contents + result.header.shoff;

	print_elf64_header(&result.header);

	print_elf64_programs(elf_contents, ph_start, header->phnum);
	print_elf64_sections(elf_contents, sh_start, header->shnum, header->shstrndx);

	//dwarf_data dw_data = gather_dwarf_sections(elf_contents, sh_start, result.header.shnum, result.header.shstrndx);
	//result.functions = parse_dwarf_functions(&dw_data);

	free(elf_contents);

	return result;
}
