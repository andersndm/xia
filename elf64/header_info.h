#ifndef IA_ELF64_HEADER_INFO_H
#define IA_ELF64_HEADER_INFO_H

#include "common/types.h"
// #note this only supports 64-bit ELF files

/*
    ELF64 File header

    fields:
        |--------------+--------+------+-----------------------------------------------------------|
        | Name         | Offset | Size | Description                                               |
        |--------------+--------+------+-----------------------------------------------------------|
        | magic_number |   0x00 |    4 | Magic number signifying the file is an ELF file, the      |
        |              |        |      | value must be 0x7F454c46 (0x7F 'E' 'L' 'F').              |
        |--------------+--------+------+-----------------------------------------------------------|
        | class        |   0x04 |    1 | 1 for 32-bit, 2 for 64-bit, only 64-bit is supported      |
        |--------------+--------+------+-----------------------------------------------------------|
        | data         |   0x05 |    1 | 1 for little-endian, 2 for big-endian, currently only     |
        |              |        |      | little-endian is supported                                |
        |--------------+--------+------+-----------------------------------------------------------|
        | version      |   0x06 |    1 | 1 for original and current version of elf.                |
        |--------------+--------+------+-----------------------------------------------------------|
        | osabi        |   0x07 |    1 | Identifies the target operating system, often just set to |
        |              |        |      | 0.                                                        |
        |              |        |      |                                                           |
        |              |        |      | Value - ABI                                               |
        |              |        |      |  0x00 - System V                                          |
        |              |        |      |  0x01 - HP-UX                                             |
        |              |        |      |  0x02 - NetBSD                                            |
        |              |        |      |  0x03 - Linux                                             |
        |              |        |      |  0x04 - GNU Hurd                                          |
        |              |        |      |  0x06 - Solaris                                           |
        |              |        |      |  0x07 - AIX                                               |
        |              |        |      |  0x08 - IRIX                                              |
        |              |        |      |  0x09 - FreeBSD                                           |
        |              |        |      |  0x0A - Tru64                                             |
        |              |        |      |  0x0B - Novell Modesto                                    |
        |              |        |      |  0x0C - OpenBSD                                           |
        |              |        |      |  0x0D - OpenVMS                                           |
        |              |        |      |  0x0E - NonStop Kernel                                    |
        |              |        |      |  0x0F - AROS                                              |
        |              |        |      |  0x10 - Fenix OS                                          |
        |              |        |      |  0x11 - CloudABI                                          |
        |              |        |      |  0x12 - Stratus Technologies OpenVOS                      |
        |--------------+--------+------+-----------------------------------------------------------|
        | abi_version  |   0x08 |    1 | Further specifies ABI version, treated as padding on      |
        |              |        |      | Linux, so will be ignored, and set to 0.                  |
        |--------------+--------+------+-----------------------------------------------------------|
        | pad          |   0x09 |    7 | Padding, should be filled with 0's.                       |
        |--------------+--------+------+-----------------------------------------------------------|
        | type         |   0x10 |    2 | Identifies the type of object file.                       |
        |              |        |      |                                                           |
        |              |        |      | Value   - Type      - Notes                               |
        |              |        |      |  0x0000 - ET_NONE   - No file type                        |
        |              |        |      |  0x0001 - ET_REL    - Relocatable file                    |
        |              |        |      |  0x0002 - ET_EXEC   - Executable file                     |
        |              |        |      |  0x0003 - ET_DYN    - Shared object file                  |
        |              |        |      |  0x0004 - ET_CORE   - Core file                           |
        |              |        |      |  0x0006 - ET_LOOS                                         |
        |              |        |      |  0x0007 - ET_HIOS                                         |
        |              |        |      |  0x0008 - ET_LOPROC                                       |
        |              |        |      |  0x0009 - ET_HIPROC                                       |
        |--------------+--------+------+-----------------------------------------------------------|
        | machine      |   0x12 |    2 | Specifies the target instruction set architecture. For    |
        |              |        |      | example:                                                  |
        |              |        |      |                                                           |
        |              |        |      | Value - ISA                                               |
        |              |        |      |  0x00 - Not specified                                     |
        |              |        |      |  0x01 - AT&T WE 32100                                     |
        |              |        |      |  0x02 - SPARC                                             |
        |              |        |      |  0x03 - x86                                               |
        |              |        |      |  0x04 - Motorola 68000 (M68K)                             |
        |              |        |      |  0x05 - Motorola 88000 (m88K)                             |
        |              |        |      |  0x06 - Intel MCU                                         |
        |              |        |      |  0x07 - Intel 80860                                       |
        |              |        |      |  0x08 - MIPS                                              |
        |              |        |      |  0x09 - IBM_System/370                                    |
        |              |        |      |  0x0A - MIPS RS3000 Little-endian                         |
        |              |        |      |  0x0B - Reserved for future use                           |
        |              |        |      |  0x0C - Reserved for future use                           |
        |              |        |      |  0x0D - Reserved for future use                           |
        |              |        |      |  0x0E - Hewlett-Packard PA-RISC                           |
        |              |        |      |  0x0F - Reserved for future use                           |
        |              |        |      |  0x13 - Intel 80960                                       |
        |              |        |      |  0x14 - PowerPC                                           |
        |              |        |      |  0x15 - PowerPC (64-bit)                                  |
        |              |        |      |  0x16 - S390, including S390x                             |
        |              |        |      |  0x28 - ARM (up to ARMv7/Aarch32)                         |
        |              |        |      |  0x2A - SuperH                                            |
        |              |        |      |  0x32 - IA-64                                             |
        |              |        |      |  0x3E - amd64                                             |
        |              |        |      |  0x8C - TMS320C6000 Family                                |
        |              |        |      |  0xB7 - ARM 64-bits (ARMv8/Aarch64)                       |
        |              |        |      |  0xF3 - RISC-V                                            |
        |--------------+--------+------+-----------------------------------------------------------|
        | version2     |   0x14 |    4 | Set to 1 for the original version of ELF.                 |
        |--------------+--------+------+-----------------------------------------------------------|
        | entry        |   0x18 |    8 | Entry point for where the process starts executing.       |
        |--------------+--------+------+-----------------------------------------------------------|
        | phoff        |   0x20 |    8 | Points to the start of the program header table.          |
        |--------------+--------+------+-----------------------------------------------------------|
        | shoff        |   0x28 |    8 | Points to the start of the section header table.          |
        |--------------+--------+------+-----------------------------------------------------------|
        | flags        |   0x30 |    4 | Depends on the target architecture.                       |
        |--------------+--------+------+-----------------------------------------------------------|
        | ehsize       |   0x34 |    2 | Size of this header.                                      |
        |--------------+--------+------+-----------------------------------------------------------|
        | phentsize    |   0x36 |    2 | The size of a program header entry.                       |
        |--------------+--------+------+-----------------------------------------------------------|
        | phnum        |   0x38 |    2 | The number of entries in the program header table.        |
        |--------------+--------+------+-----------------------------------------------------------|
        | shentsize    |   0x3A |    2 | The size of a section header entry.                       |
        |--------------+--------+------+-----------------------------------------------------------|
        | shnum        |   0x3C |    2 | The number of entries in the section header table.        |
        |--------------+--------+------+-----------------------------------------------------------|
        | shstrndx     |   0x3E |    2 | The index of the section header table entry that contains |
        |              |        |      | the names of the sections.                                |
        |--------------+--------+------+-----------------------------------------------------------|
 */

#define ET_NONE   0x0000
#define ET_REL    0x0001
#define ET_EXEC   0x0002
#define ET_DYN    0x0003
#define ET_CORE   0x0004
#define ET_LOOS   0xfe00
#define ET_HIOS   0xfeff
#define ET_LOPROC 0xff00
#define ET_HIPROC 0xffff

typedef struct __attribute__((packed)) elf64_file_header
{
	u32 magic_number;
	u8  class;
    u8  data;
    u8  version;
    u8  osabi;
    u8  abi_version;
    u8  padding[7];
    u16 type;
    u16 machine;
    u32 version2;
    u64 entry;
    u64 phoff;
    u64 shoff;
    u32 flags;
    u16 ehsize;
    u16 phentsize;
    u16 phnum;
    u16 shentsize;
    u16 shnum;
    u16 shstrndx;
} elf64_file_header;

#define ELF_CLASS_32 1
#define ELF_CLASS_64 2

#define ELF_DATA_LITTLE 1
#define ELF_DATA_BIG    2

#define ELF_VERSION_CURRENT 1

#define ELF_OSABI_SYSV                          0
#define ELF_OSABI_HPUX                          1
#define ELF_OSABI_NETBSD                        2
#define ELF_OSABI_LINUX                         3
#define ELF_OSABI_GNUHURD                       4
#define ELF_OSABI_SOLARIS                       6
#define ELF_OSABI_AIX                           7
#define ELF_OSABI_IRIX                          8
#define ELF_OSABI_FREEBSD                       9
#define ELF_OSABI_TRUE64                       10
#define ELF_OSABI_NOVELL_MODESTO               11
#define ELF_OSABI_OPENBSD                      12
#define ELF_OSABI_OPENVMS                      13
#define ELF_OSABI_NONSTOP_KERNEL               14
#define ELF_OSABI_AROS                         15
#define ELF_OSABI_FENIX_OS                     16
#define ELF_OSABI_CLOUDABI                     17
#define ELF_OSABI_STRATUS_TECHNOLOGIES_OPENVOS 18
#define ELF_OSABI_STANDALONE                   255

#define ELF_MACH_AMD64 0x3E

#define PT_NULL    0
#define PT_LOAD    1
#define PT_DYNAMIC 2
#define PT_INTERP  3
#define PT_NOTE    4
#define PT_SHLIB   5
#define PT_PHDR    6
#define PT_LOOS    0x60000000
#define PT_HIOS    0x6FFFFFFF
#define PT_LOPROC  0x70000000
#define PT_HIPROC  0x7FFFFFFF

#define PF_X        0x1
#define PF_W        0x2
#define PF_R        0x4
#define PF_MASKOS   0x00FF0000
#define PF_MASKPROC 0xFF000000

typedef struct __attribute__((packed)) elf64_program_header
{
	u32 type;         // type
	u32 flags;        // attributes
	u64 offset;       // offset in file
	u64 vaddr;        // virtual address in memory
	u64 paddr;        // reserved
	u64 file_size;    // size of segment in file
	u64 mem_size;     // size of segment in memory
	u64 align;        // alignment of segment
} elf64_program_header;

                                 // union:
#define DT_NULL             0    // ignored
#define DT_NEEDED           1    // val
#define DT_PLTRELSZ         2    // val
#define DT_PLTGOT           3    // ptr
#define DT_HASH             4    // ptr
#define DT_STRTAB           5    // ptr
#define DT_SYMTAB           6    // ptr
#define DT_RELA             7    // ptr
#define DT_RELASZ           8    // val
#define DT_RELAENT          9    // val
#define DT_STRSZ            10   // val
#define DT_SYMENT           11   // val
#define DT_INIT             12   // ptr
#define DT_FINI             13   // ptr
#define DT_SONAME           14   // val
#define DT_RPATH            15   // val
#define DT_SYMBOLIC         16   // ignored
#define DT_REL              17   // ptr
#define DT_RELSZ            18   // val
#define DT_RELENT           19   // val
#define DT_PLTREL           20   // val
#define DT_DEBUG            21   // ptr
#define DT_TEXTREL          22   // ignored
#define DT_JMPREL           23   // ptr
#define DT_BIND_NOW         24   // ignored
#define DT_INIT_ARRAY       25   // ptr
#define DT_FINI_ARRAY       26   // ptr
#define DT_INIT_ARRAYSZ     27   // val
#define DT_FINI_ARRAYSZ     28   // val
#define DT_LOOS             0x60000000
#define DT_HIOS             0x6FFFFFFF
#define DT_LOPROC           0x70000000
#define DT_HIPROC           0x7FFFFFFF

typedef struct __attribute__((packed)) elf64_dynamic_entry
{
    u64 tag;
    union
    {
        u64 val;
        u64 ptr;
    };
    
} elf64_dynamic_entry;

typedef struct __attribute__((packed)) elf64_section_header
{
	u32 name_offset;
	u32 type;
	u64 flags;
	u64 addr;
	u64 offset;
	u64 size;
	u32 link;
	u32 info;
	u64 addralign;
	u64 entsize;
} elf64_section_header;

typedef enum section_type
{
	SHT_NULL,
	SHT_PROGBITS,
	SHT_SYMTAB,
	SHT_STRTAB,
	SHT_RELA,
	SHT_HASH,
	SHT_DYNAMIC,
	SHT_NOTE,
	SHT_NOBITS,
	SHT_REL,
	SHT_SHLIB,
	SHT_DYNSYM,
	SHT_INIT_ARRAY = 0x0E,
	SHT_FINI_ARRAY,
	SHT_PREINIT_ARRAY,
	SHT_GROUP,
	SHT_SYMTAB_SHNDX,
	SHT_NUM,
	SHT_LOOS = 0x60000000,
    SHT_LOPROC = 0x70000000
	// ...
} section_type;

#define SHF_WRITE            0x1
#define SHF_ALLOC            0x2
#define SHF_EXECINSTR        0x4
#define SHF_MERGE            0x10
#define SHF_STRINGS          0x20
#define SHF_INFO_LINK        0x40
#define SHF_LINK_ORDER       0x80
#define SHF_OS_NONCONFORMING 0x100
#define SHF_GROUP            0x200
#define SHF_TLS              0x400
#define SHF_MASKOS           0x0FF00000
#define SHF_MASKPROC         0xF0000000
#define SHF_ORDERED          0x40000000
#define SHF_EXCLUDE          0x80000000

#define STB_LOCAL  0
#define STB_GLOBAL 1
#define STB_WEAK   2

#define STT_NOTYPE  0
#define STT_OBJECT  1
#define STT_FUNC    2
#define STT_SECTION 3
#define STT_FILE    4

#define SHN_UNDEF 0

typedef struct __attribute__((packed)) elf64_symbol
{
	u32 name;
	u8  info;
	u8  other;
	u16 shndx;
	u64 value;
	u64 size;
} elf64_symbol;

#define ELF_RELA_TYPE_NONE				 0
#define ELF_RELA_TYPE_64				 1
#define ELF_RELA_TYPE_PC32				 2
#define ELF_RELA_TYPE_GOT32				 3
#define ELF_RELA_TYPE_PLT32				 4
#define ELF_RELA_TYPE_COPY				 5
#define ELF_RELA_TYPE_GLOB_DAT			 6
#define ELF_RELA_TYPE_JUMP_SLOT			 7
#define ELF_RELA_TYPE_RELATIVE			 8
#define ELF_RELA_TYPE_GOTPCREL			 9
#define ELF_RELA_TYPE_32				10
#define ELF_RELA_TYPE_32S				11
#define ELF_RELA_TYPE_16				12
#define ELF_RELA_TYPE_PC16				13
#define ELF_RELA_TYPE_8					14
#define ELF_RELA_TYPE_PC8				15
#define ELF_RELA_TYPE_DTPMOD64			16
#define ELF_RELA_TYPE_DTPOFF64			17
#define ELF_RELA_TYPE_TPOFF64			18
#define ELF_RELA_TYPE_TLSGD				19
#define ELF_RELA_TYPE_TLSLD				20
#define ELF_RELA_TYPE_DTPOFF32			21
#define ELF_RELA_TYPE_GOTTPOFF			22
#define ELF_RELA_TYPE_TPOFF32			23
#define ELF_RELA_TYPE_PC64				24
#define ELF_RELA_TYPE_GOTOFF64			25
#define ELF_RELA_TYPE_GOTPC32			26
#define ELF_RELA_TYPE_SIZE32			32
#define ELF_RELA_TYPE_SIZE64			33
#define ELF_RELA_TYPE_GOTPC32_TLSDESC	34
#define ELF_RELA_TYPE_TLSDESC_CALL		35
#define ELF_RELA_TYPE_TLSDESC			36
#define ELF_RELA_TYPE_IRELATIVE			37

typedef struct __attribute__((packed)) elf64_rela
{
    u64 offset;
    u64 info;
    u64 addend;
} elf64_rela;

#endif // !IA_ELF64_HEADER_INFO_H
