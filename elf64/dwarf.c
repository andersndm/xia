#include "dwarf.h"

#include "common/macros.h"
#include "common/buf.h"
#include "common/color_printing.h"
#include "common/memory.h"

#include <string.h>

const char *dwarf_tag_names[DW_NUM_TAGS] =
{
	[DW_TAG_ARRAY_TYPE] = "DW_TAG_ARRAY_TYPE",
	[DW_TAG_CLASS_TYPE] = "DW_TAG_CLASS_TYPE",
	[DW_TAG_ENTRY_POINT] = "DW_TAG_ENTRY_POINT",
	[DW_TAG_ENUMERATION_TYPE] = "DW_TAG_ENUMERATION_TYPE",
	[DW_TAG_FORMAL_PARAMETER] = "DW_TAG_FORMAL_PARAMETER",
	// reserved						= 0x06,
	// reserved						= 0x07,
	[DW_TAG_IMPORTED_DECLARATION] = "DW_TAG_IMPORTED_DECLARATION",
	// reserved						= 0x09,
	[DW_TAG_LABEL] = "DW_TAG_LABEL",
	[DW_TAG_LEXICAL_BLOCK] = "DW_TAG_LEXICAL_BLOCK",
	// reserved						= 0x0C,
	[DW_TAG_MEMBER] = "DW_TAG_MEMBER",
	// reserved						= 0x0E,
	[DW_TAG_POINTER_TYPE] = "DW_TAG_POINTER_TYPE",
	[DW_TAG_REFERENCE_TYPE] = "DW_TAG_REFERENCE_TYPE",
	[DW_TAG_COMPILE_UNIT] = "DW_TAG_COMPILE_UNIT",
	[DW_TAG_STRING_TYPE] = "DW_TAG_STRING_TYPE",
	[DW_TAG_STRUCTURE_TYPE] = "DW_TAG_STRUCTURE_TYPE",
	// reserved						= 0x14,
	[DW_TAG_SUBROUTINE_TYPE] = "DW_TAG_SUBROUTINE_TYPE",
	[DW_TAG_TYPEDEF] = "DW_TAG_TYPEDEF",
	[DW_TAG_UNION_TYPE] = "DW_TAG_UNION_TYPE",
	[DW_TAG_UNSPECIFIED_PARAMETERS] = "DW_TAG_UNSPECIFIED_PARAMETERS",
	[DW_TAG_VARIANT] = "DW_TAG_VARIANT",
	[DW_TAG_COMMON_BLOCK] = "DW_TAG_COMMON_BLOCK",
	[DW_TAG_COMMON_INCLUSION] = "DW_TAG_COMMON_INCLUSION",
	[DW_TAG_INHERITANCE] = "DW_TAG_INHERITANCE",
	[DW_TAG_INLINED_SUBROUTINE] = "DW_TAG_INLINED_SUBROUTINE",
	[DW_TAG_MODULE] = "DW_TAG_MODULE",
	[DW_TAG_PTR_TO_MEMBER_TYPE] = "DW_TAG_PTR_TO_MEMBER_TYPE",
	[DW_TAG_SET_TYPE] = "DW_TAG_SET_TYPE",
	[DW_TAG_SUBRANGE_TYPE] = "DW_TAG_SUBRANGE_TYPE",
	[DW_TAG_WITH_STMT] = "DW_TAG_WITH_STMT",
	[DW_TAG_ACCESS_DECLARATION] = "DW_TAG_ACCESS_DECLARATION",
	[DW_TAG_BASE_TYPE] = "DW_TAG_BASE_TYPE",
	[DW_TAG_CATCH_BLOCK] = "DW_TAG_CATCH_BLOCK",
	[DW_TAG_CONST_TYPE] = "DW_TAG_CONST_TYPE",
	[DW_TAG_CONSTANT] = "DW_TAG_CONSTANT",
	[DW_TAG_ENUMERATOR] = "DW_TAG_ENUMERATOR",
	[DW_TAG_FILE_TYPE] = "DW_TAG_FILE_TYPE",
	[DW_TAG_FRIEND] = "DW_TAG_FRIEND",
	[DW_TAG_NAMELIST] = "DW_TAG_NAMELIST",
	[DW_TAG_NAMELIST_ITEM] = "DW_TAG_NAMELIST_ITEM",
	[DW_TAG_PACKED_TYPE] = "DW_TAG_PACKED_TYPE",
	[DW_TAG_SUBPROGRAM] = "DW_TAG_SUBPROGRAM",
	[DW_TAG_TEMPLATE_TYPE_PARAMETER] = "DW_TAG_TEMPLATE_TYPE_PARAMETER",
	[DW_TAG_TEMPLATE_VALUE_PARAMETER] = "DW_TAG_TEMPLATE_VALUE_PARAMETER",
	[DW_TAG_THROWN_TYPE] = "DW_TAG_THROWN_TYPE",
	[DW_TAG_TRY_BLOCK] = "DW_TAG_TRY_BLOCK",
	[DW_TAG_VARIANT_PART] = "DW_TAG_VARIANT_PART",
	[DW_TAG_VARIABLE] = "DW_TAG_VARIABLE",
	[DW_TAG_VOLATILE_TYPE] = "DW_TAG_VOLATILE_TYPE",
	[DW_TAG_DWARF_PROCEDURE] = "DW_TAG_DWARF_PROCEDURE",
	[DW_TAG_RESTRICT_TYPE] = "DW_TAG_RESTRICT_TYPE",
	[DW_TAG_INTERFACE_TYPE] = "DW_TAG_INTERFACE_TYPE",
	[DW_TAG_NAMESPACE] = "DW_TAG_NAMESPACE",
	[DW_TAG_IMPORTED_MODULE] = "DW_TAG_IMPORTED_MODULE",
	[DW_TAG_UNSPECIFIED_TYPE] = "DW_TAG_UNSPECIFIED_TYPE",
	[DW_TAG_PARTIAL_UNIT] = "DW_TAG_PARTIAL_UNIT",
	[DW_TAG_IMPORTED_UNIT] = "DW_TAG_IMPORTED_UNIT",
	// reserved						= 0x3E,
	[DW_TAG_CONDITION] = "DW_TAG_CONDITION",
	[DW_TAG_SHARED_TYPE] = "DW_TAG_SHARED_TYPE",
	[DW_TAG_TYPE_UNIT] = "DW_TAG_TYPE_UNIT",
	[DW_TAG_RVALUE_REFERENCE_TYPE] = "DW_TAG_RVALUE_REFERENCE_TYPE",
	[DW_TAG_TEMPLATE_ALIAS] = "DW_TAG_TEMPLATE_ALIAS",
	[DW_TAG_COARRAY_TYPE] = "DW_TAG_COARRAY_TYPE",
	[DW_TAG_GENERIC_SUBRANGE] = "DW_TAG_GENERIC_SUBRANGE",
	[DW_TAG_DYNAMIC_TYPE] = "DW_TAG_DYNAMIC_TYPE",
	[DW_TAG_ATOMIC_TYPE] = "DW_TAG_ATOMIC_TYPE",
	[DW_TAG_CALL_SITE] = "DW_TAG_CALL_SITE",
	[DW_TAG_CALL_SITE_PARAMETER] = "DW_TAG_CALL_SITE_PARAMETER",
	[DW_TAG_SKELETON_UNIT] = "DW_TAG_SKELETON_UNIT",
	[DW_TAG_IMMUTABLE_TYPE] = "DW_TAG_IMMUTABLE_TYPE"
};

const char * dwarf_attr_names[DW_NUM_ATTRS] =
{
	[DW_AT_SIBLING] = "DW_AT_SIBLING",
	[DW_AT_LOCATION] = "DW_AT_LOCATION",
	[DW_AT_NAME] = "DW_AT_NAME",
	[DW_AT_ORDERING] = "DW_AT_ORDERING",
	[DW_AT_BYTE_SIZE] = "DW_AT_BYTE_SIZE",
	[DW_AT_BIT_SIZE] = "DW_AT_BIT_SIZE",
	[DW_STMT_LIST] = "DW_STMT_LIST",
	[DW_AT_LOW_PC] = "DW_AT_LOW_PC",
	[DW_AT_HIGH_PC] = "DW_AT_HIGH_PC",
	[DW_AT_LANGUAGE] = "DW_AT_LANGUAGE",
	[DW_AT_DISCR] = "DW_AT_DISCR",
	[DW_AT_DISRC_VALUE] = "DW_AT_DISRC_VALUE",
	[DW_AT_VISIBILITY] = "DW_AT_VISIBILITY",
	[DW_AT_IMPORT] = "DW_AT_IMPORT",
	[DW_AT_STRING_LENGTH] = "DW_AT_STRING_LENGTH",
	[DW_AT_COMMON_REFERENCE] = "DW_AT_COMMON_REFERENCE",
	[DW_AT_COMP_DIR] = "DW_AT_COMP_DIR",
	[DW_AT_CONST_VALUE] = "DW_AT_CONST_VALUE",
	[DW_AT_CONTAINING_TYPE] = "DW_AT_CONTAINING_TYPE",
	[DW_AT_DEFAULT_VALUE] = "DW_AT_DEFAULT_VALUE",
	[DW_AT_INLINE] = "DW_AT_INLINE",
	[DW_AT_IS_OPTIONAL] = "DW_AT_IS_OPTIONAL",
	[DW_AT_LOWER_BOUND] = "DW_AT_LOWER_BOUND",
	[DW_AT_PRODUCER] = "DW_AT_PRODUCER",
	[DW_AT_PROTOTYPED] = "DW_AT_PROTOTYPED",
	[DW_AT_RETURN_ADDR] = "DW_AT_RETURN_ADDR",
	[DW_AT_START_SCOPE] = "DW_AT_START_SCOPE",
	[DW_AT_BIT_STRIDE] = "DW_AT_BIT_STRIDE",
	[DW_AT_UPPER_BOUND] = "DW_AT_UPPER_BOUND",
	[DW_AT_ABSTRACT_ORIGIN] = "DW_AT_ABSTRACT_ORIGIN",
	[DW_AT_ACCESSIBILITY] = "DW_AT_ACCESSIBILITY",
	[DW_AT_ADDRESS_CLASS] = "DW_AT_ADDRESS_CLASS",
	[DW_AT_ARTIFICIAL] = "DW_AT_ARTIFICIAL",
	[DW_AT_BASE_TYPES] = "DW_AT_BASE_TYPES",
	[DW_AT_CALLING_CONVENTION] = "DW_AT_CALLING_CONVENTION",
	[DW_AT_COUNT] = "DW_AT_COUNT",
	[DW_AT_DATA_MEMBER_LOCATION] = "DW_AT_DATA_MEMBER_LOCATION",
	[DW_AT_DECL_COLUMN] = "DW_AT_DECL_COLUMN",
	[DW_AT_DECL_FILE] = "DW_AT_DECL_FILE",
	[DW_AT_DECL_LINE] = "DW_AT_DECL_LINE",
	[DW_AT_DECLARATION] = "DW_AT_DECLARATION",
	[DW_AT_DISCR_LIST] = "DW_AT_DISCR_LIST",
	[DW_AT_ENCODING] = "DW_AT_ENCODING",
	[DW_AT_EXTERNAL] = "DW_AT_EXTERNAL",
	[DW_AT_FRAME_BASE] = "DW_AT_FRAME_BASE",
	[DW_AT_FRIEND] = "DW_AT_FRIEND",
	[DW_AT_IDENTIFIER_CASE] = "DW_AT_IDENTIFIER_CASE",
	[DW_AT_NAMELIST_ITEM] = "DW_AT_NAMELIST_ITEM",
	[DW_AT_PRIORITY] = "DW_AT_PRIORITY",
	[DW_AT_SEGMENT] = "DW_AT_SEGMENT",
	[DW_AT_SPECIFICATION] = "DW_AT_SPECIFICATION",
	[DW_AT_STATIC_LINK] = "DW_AT_STATIC_LINK",
	[DW_AT_TYPE] = "DW_AT_TYPE",
	[DW_AT_USE_LOCATION] = "DW_AT_USE_LOCATION",
	[DW_AT_VARIABLE_PARAMETER] = "DW_AT_VARIABLE_PARAMETER",
	[DW_AT_VIRTUALITY] = "DW_AT_VIRTUALITY",
	[DW_AT_VTABLE_ELEM_LOCATION] = "DW_AT_VTABLE_ELEM_LOCATION",
	[DW_AT_ALLOCATED] = "DW_AT_ALLOCATED",
	[DW_AT_ASSOCIATED] = "DW_AT_ASSOCIATED",
	[DW_AT_DATA_LOCATION] = "DW_AT_DATA_LOCATION",
	[DW_AT_BYTE_STRIDE] = "DW_AT_BYTE_STRIDE",
	[DW_AT_ENTRY_PC] = "DW_AT_ENTRY_PC",
	[DW_AT_USE_UTF8] = "DW_AT_USE_UTF8",
	[DW_AT_EXTENSION] = "DW_AT_EXTENSION",
	[DW_AT_RANGES] = "DW_AT_RANGES",
	[DW_AT_TRAMPOLINE] = "DW_AT_TRAMPOLINE",
	[DW_AT_CALL_COLUMN] = "DW_AT_CALL_COLUMN",
	[DW_AT_CALL_FILE] = "DW_AT_CALL_FILE",
	[DW_AT_CALL_LINE] = "DW_AT_CALL_LINE",
	[DW_AT_DESCRIPTION] = "DW_AT_DESCRIPTION",
	[DW_AT_BINARY_SCALE] = "DW_AT_BINARY_SCALE",
	[DW_AT_DECIMAL_SCALE] = "DW_AT_DECIMAL_SCALE",
	[DW_AT_SMALL] = "DW_AT_SMALL",
	[DW_AT_DECIMAL_SIGN] = "DW_AT_DECIMAL_SIGN",
	[DW_AT_DIGIT_COUNT] = "DW_AT_DIGIT_COUNT",
	[DW_AT_PICTURE_STRING] = "DW_AT_PICTURE_STRING",
	[DW_AT_MUTABLE] = "DW_AT_MUTABLE",
	[DW_AT_THREADS_SCALED] = "DW_AT_THREADS_SCALED",
	[DW_AT_EXPLICIT] = "DW_AT_EXPLICIT",
	[DW_AT_OBJECT_POINTER] = "DW_AT_OBJECT_POINTER",
	[DW_AT_ENDIANITY] = "DW_AT_ENDIANITY",
	[DW_AT_ELEMENTAL] = "DW_AT_ELEMENTAL",
	[DW_AT_PURE] = "DW_AT_PURE",
	[DW_AT_RECURSIVE] = "DW_AT_RECURSIVE",
	[DW_AT_SIGNATURE] = "DW_AT_SIGNATURE",
	[DW_AT_MAIN_SUBPROGRAM] = "DW_AT_MAIN_SUBPROGRAM",
	[DW_AT_DATA_BIT_OFFSET] = "DW_AT_DATA_BIT_OFFSET",
	[DW_AT_CONST_EXPR] = "DW_AT_CONST_EXPR",
	[DW_AT_ENUM_CLASS] = "DW_AT_ENUM_CLASS",
	[DW_AT_LINKAGE_NAME] = "DW_AT_LINKAGE_NAME",
	[DW_AT_STRING_LENGTH_BIT_SIZE] = "DW_AT_STRING_LENGTH_BIT_SIZE",
	[DW_AT_STRING_LENGTH_BYTE_SIZE] = "DW_AT_STRING_LENGTH_BYTE_SIZE",
	[DW_AT_RANK] = "DW_AT_RANK",
	[DW_AT_STR_OFFSETS_BASE] = "DW_AT_STR_OFFSETS_BASE",
	[DW_AT_ADDR_BASE] = "DW_AT_ADDR_BASE",
	[DW_AT_RNGLISTS_BASE] = "DW_AT_RNGLISTS_BASE",
	[DW_AT_DWO_NAME] = "DW_AT_DWO_NAME",
	[DW_AT_REFERENCE] = "DW_AT_REFERENCE",
	[DW_AT_RVALUE_REFERENCE] = "DW_AT_RVALUE_REFERENCE",
	[DW_AT_MACROS] = "DW_AT_MACROS",
	[DW_AT_CALL_ALL_CALLS] = "DW_AT_CALL_ALL_CALLS",
	[DW_AT_CALL_ALL_SOURCE_CALLS] = "DW_AT_CALL_ALL_SOURCE_CALLS",
	[DW_AT_CALL_ALL_TAIL_CALLS] = "DW_AT_CALL_ALL_TAIL_CALLS",
	[DW_AT_CALL_RETURN_PC] = "DW_AT_CALL_RETURN_PC",
	[DW_AT_CALL_VALUE] = "DW_AT_CALL_VALUE",
	[DW_AT_CALL_ORIGIN] = "DW_AT_CALL_ORIGIN",
	[DW_AT_CALL_PARAMETER] = "DW_AT_CALL_PARAMETER",
	[DW_AT_CALL_PC] = "DW_AT_CALL_PC",
	[DW_AT_CALL_TAIL_CALL] = "DW_AT_CALL_TAIL_CALL",
	[DW_AT_CALL_TARGET] = "DW_AT_CALL_TARGET",
	[DW_AT_CALL_TARGET_CLOBBERED] = "DW_AT_CALL_TARGET_CLOBBERED",
	[DW_AT_CALL_DATA_LOCATION] = "DW_AT_CALL_DATA_LOCATION",
	[DW_AT_CALL_DATA_VALUE] = "DW_AT_CALL_DATA_VALUE",
	[DW_AT_NORETURN] = "DW_AT_NORETURN",
	[DW_AT_ALIGNMENT] = "DW_AT_ALIGNMENT",
	[DW_AT_EXPORT_SYMBOLS] = "DW_AT_EXPORT_SYMBOLS",
	[DW_AT_DELETED] = "DW_AT_DELETED",
	[DW_AT_DEFAULTED] = "DW_AT_DEFAULTED",
	[DW_AT_LOCLISTS_BASE] = "DW_AT_LOCLISTS_BASE"
};

const char *dwarf_form_names[DW_NUM_FORMS] =
{
	[DW_FORM_ADDR] = "DW_FORM_ADDR",
	[DW_FORM_BLOCK2] = "DW_FORM_BLOCK2",
	[DW_FORM_BLOCK4] = "DW_FORM_BLOCK4",
	[DW_FORM_DATA2] = "DW_FORM_DATA2",
	[DW_FORM_DATA4] = "DW_FORM_DATA4",
	[DW_FORM_DATA8] = "DW_FORM_DATA8",
	[DW_FORM_STRING] = "DW_FORM_STRING",
	[DW_FORM_BLOCK] = "DW_FORM_BLOCK",
	[DW_FORM_BLOCK1] = "DW_FORM_BLOCK1",
	[DW_FORM_DATA1] = "DW_FORM_DATA1",
	[DW_FORM_FLAG] = "DW_FORM_FLAG",
	[DW_FORM_SDATA] = "DW_FORM_SDATA",
	[DW_FORM_STRP] = "DW_FORM_STRP",
	[DW_FORM_UDATA] = "DW_FORM_UDATA",
	[DW_FORM_REF_ADDR] = "DW_FORM_REF_ADDR",
	[DW_FORM_REF1] = "DW_FORM_REF1",
	[DW_FORM_REF2] = "DW_FORM_REF2",
	[DW_FORM_REF4] = "DW_FORM_REF4",
	[DW_FORM_REF8] = "DW_FORM_REF8",
	[DW_FORM_REF_UDATA] = "DW_FORM_REF_UDATA",
	[DW_FORM_INDIRECT] = "DW_FORM_INDIRECT",
	[DW_FORM_SEC_OFFSET] = "DW_FORM_SEC_OFFSET",
	[DW_FORM_EXPRLOC] = "DW_FORM_EXPRLOC",
	[DW_FORM_FLAG_PRESENT] = "DW_FORM_FLAG_PRESENT",
	[DW_FORM_STRX] = "DW_FORM_STRX",
	[DW_FORM_ADDRX] = "DW_FORM_ADDRX",
	[DW_FORM_REF_SUP4] = "DW_FORM_REF_SUP4",
	[DW_FORM_STRP_SUP] = "DW_FORM_STRP_SUP",
	[DW_FORM_DATA16] = "DW_FORM_DATA16",
	[DW_FORM_LINE_STRP] = "DW_FORM_LINE_STRP",
	[DW_FORM_REF_SIG8] = "DW_FORM_REF_SIG8",
	[DW_FORM_IMPLICIT_CONST] = "DW_FORM_IMPLICIT_CONST",
	[DW_FORM_LOCLISTX] = "DW_FORM_LOCLISTX",
	[DW_FORM_RNGLISTX] = "DW_FORM_RNGLISTX",
	[DW_FORM_REF_SUP8] = "DW_FORM_REF_SUP8",
	[DW_FORM_STRX1] = "DW_FORM_STRX1",
	[DW_FORM_STRX2] = "DW_FORM_STRX2",
	[DW_FORM_STRX3] = "DW_FORM_STRX3",
	[DW_FORM_STRX4] = "DW_FORM_STRX4",
	[DW_FORM_ADDRX1] = "DW_FORM_ADDRX1",
	[DW_FORM_ADDRX2] = "DW_FORM_ADDRX2",
	[DW_FORM_ADDRX3] = "DW_FORM_ADDRX3",
	[DW_FORM_ADDRX4] = "DW_FORM_ADDRX4"
};

const char *dwarf_lang_names[DW_NUM_LANGS] =
{
	[DW_LANG_C89] = "C89",
	[DW_LANG_C] = "C",
	[DW_LANG_ADA83] = "Ada 83",
	[DW_LANG_C_PLUS_PLUS] = "C++",
	[DW_LANG_COBOL74] = "Cobol 74",
	[DW_LANG_COBOL85] = "Cobol 85",
	[DW_LANG_FORTRAN77] = "Fortran 77",
	[DW_LANG_FORTRAN90] = "ortran N90",
	[DW_LANG_PASCAL83] = "Pascal 83",
	[DW_LANG_MODULA2] = "Modula2",
	[DW_LANG_JAVA] = "Java",
	[DW_LANG_C99] = "C99",
	[DW_LANG_ADA95] = "Ada 95",
	[DW_LANG_FORTRAN95] = "Fortran 95",
	[DW_LANG_PLI] = "PLI",
	[DW_LANG_OBJC] = "OBJ-C",
	[DW_LANG_OBJC_PLUS_PLUS] = "OBJ-C++",
	[DW_LANG_UPC] = "UPC",
	[DW_LANG_D] = "D",
	[DW_LANG_PYTHON] = "Python",
	[DW_LANG_OPENCL] = "OpenCL",
	[DW_LANG_GO] = "Go",
	[DW_LANG_MODULA3] = "Modula 3",
	[DW_LANG_HASKELL] = "Haskell",
	[DW_LANG_C_PLUS_PLUS_03] = "C++ 03",
	[DW_LANG_C_PLUS_PLUS_11] = "C++ 11",
	[DW_LANG_OCAML] = "OCaml",
	[DW_LANG_RUST] = "Rust",
	[DW_LANG_C11] = "C11",
	[DW_LANG_SWIFT] = "Swift",
	[DW_LANG_JULIA] = "Julia",
	[DW_LANG_DYLAN] = "Dylan",
	[DW_LANG_C_PLUS_PLUS_14] = "C++ 14",
	[DW_LANG_FORTRAN03] = "Fortran 03",
	[DW_LANG_FORTRAN08] = "Fortran 08",
	[DW_LANG_RENDERSCRIPT] = "RenderScript",
	[DW_LANG_BLISS] = "Bliss"
};

const char *dwarf_lang_repr(dwarf_language lang)
{
	const char *str = dwarf_lang_names[lang];
	if (str)
	{
		return str;
	}
	else
	{
		return "unknown language";
	}
	
}

const char *dwarf_form_repr(dwarf_form form)
{
	if (form >= DW_NUM_FORMS)
	{
		return "DW_FORM_USER";
	}
	else
	{
		const char *str = dwarf_form_names[form];
		if (str)
		{
			return str;
		}
		else
		{
			return "DW_FORM_RESERVED";
		}
	}
}

const char *dwarf_attr_repr(dwarf_attribute attr)
{
	if (attr >= DW_NUM_ATTRS)
	{
		return "DW_AT_USER";
	}
	else
	{
		const char *str = dwarf_attr_names[attr];
		if (str)
		{
			return str;
		}
		else
		{
			return "DW_AT_RESERVED";
		}
	}
}

const char *dwarf_tag_repr(dwarf_tag tag)
{
	if (tag >= DW_NUM_TAGS)
	{
		return "DW_TAG_USER";
	}
	else
	{
		const char *str = dwarf_tag_names[tag];
		if (str)
		{
			return str;
		}
		else
		{
			return "DW_TAG_RESERVED";
		}
	}
}

void print_dwarf_abbrev(dwarf_abbrev_section *sections)
{
	ASSERT(sections != 0);
	PC_CYAN_BOLD("-- Dwarf Abbrev ----------------------------------------------\n");
	for (usize section_idx = 0; section_idx < buf_len(sections); ++section_idx)
	{
		if (section_idx != 0)
		{
			PC_CYAN_BOLD("--------------------------------------------------------------\n");
		}
		dwarf_abbrev_section section = sections[section_idx];
		printf("Number Tag(0x%lx)\n", section.offset);
		for (u32 i = 0; i < buf_len(section.entries); ++i)
		{
			dwarf_abbrev_entry entry = section.entries[i];
			PC_MAGENTA_BOLD("Entry: ");
			printf("%lu - %s ", entry.index, dwarf_tag_repr(entry.tag));
			if (entry.has_children)
			{
				printf("[has children]\n");
			}
			else
			{
				printf("[no children]\n");
			}
			
			if (buf_len(entry.attrs))
			{
				PC_MAGENTA_BOLD("Attributes:\n");
				for (u32 j = 0; j < buf_len(entry.attrs); ++j)
				{
					dwarf_abbrev_attr_entry attr_entry = entry.attrs[j];
					printf("    %s - %s\n", dwarf_attr_repr(attr_entry.attr), dwarf_form_repr(attr_entry.form));
				}
			}
		}
	}
}

dwarf_abbrev_section *get_abbrev_section(dwarf_abbrev_section *sections, u64 offset)
{
	for (usize i = 0; i < buf_len(sections); ++i)
	{
		if ((sections +i)->offset == offset)
		{
			return sections + i;
		}
	}
	return NULL;
}

dwarf_abbrev_entry *get_abbrev_entry(dwarf_abbrev_entry *entries, u64 index)
{
	u32 len = buf_len(entries);
	for (u32 i = 0; i < len; ++i)
	{
		if ((entries + i)->index == index)
		{
			return entries + i;
		}
	}
	return NULL;
}

dwarf_abbrev_section *parse_debug_abbrev(void *data, usize size)
{
	dwarf_abbrev_section *result = 0;
	u8 *ptr = data;
	while (ptr < (u8 *)data + size)
	{
		dwarf_abbrev_section section = { 0 };
		section.offset = (u64)(ptr - (u8 *)data);
		for (;;)
		{
			dwarf_abbrev_entry entry;
			usize bytes_read = uleb128_decode(ptr, &entry.index);
			ptr += bytes_read;
			// #note null code
			if (entry.index == 0)
			{
				break;
			}
			u64 utag = 0;
			bytes_read = uleb128_decode(ptr, &utag);
			ptr += bytes_read;
			entry.tag = (dwarf_tag)utag;

			entry.attrs = 0;
			entry.has_children = *ptr++;
			// #todo this may segfault on improper implementations of dwarf
			dwarf_abbrev_attr_entry attr_entry;
			for (;;)
			{
				u64 uattr = 0;
				u64 uform = 0;
				bytes_read = uleb128_decode(ptr, &uattr);
				ptr += bytes_read;
				bytes_read = uleb128_decode(ptr, &uform);
				ptr += bytes_read;
				if (uattr == 0)
				{
					ASSERT(uform == 0);
					break;
				}
				attr_entry.attr = (dwarf_attribute)uattr;
				attr_entry.form = (dwarf_form)uform;
				
				buf_push(entry.attrs, attr_entry);
			}
			buf_push(section.entries, entry);
		}
		buf_push(result, section);
	}
	return result;
}

u8 parse_dwarf_form(dwarf_form kind, u8 *data, dwarf_form_data *result, u8 ptr_size, bool is_32_bit, u8 *str_data)
{
	result->kind = kind;
	switch (kind)
	{
		case DW_FORM_ADDR:
		{
			switch (ptr_size)
			{
				case 8:
				{
					result->addr = *(u64 *)data;
					return 8;
				} break;
				case 4:
				{
					result->addr = *(u32 *)data;
					return 4;
				} break;
				case 2:
				{
					result->addr = *(u16 *)data;
					return 2;
				} break;
				case 1:
				{
					result->addr = *(u8 *)data;
					return 1;
				} break;
				default:
				{
					INVALID_CODE_PATH;
				} break;
			}
		} break;
		case DW_FORM_BLOCK2:
		case DW_FORM_BLOCK4:
		{
			INVALID_CODE_PATH;
		} break;
		case DW_FORM_DATA1:
		{
			result->udata = *data;
			result->kind = DW_FORM_UDATA;
			return 1;
		} break;
		case DW_FORM_DATA2:
		{
			result->udata = *(u16 *)data;
			result->kind = DW_FORM_UDATA;
			return 2;
		} break;
		case DW_FORM_DATA4:
		{
			result->udata = *(u32 *)data;
			result->kind = DW_FORM_UDATA;
			return 4;
		} break;
		case DW_FORM_DATA8:
		{
			result->udata = *(u64 *)data;
			result->kind = DW_FORM_UDATA;
			return 8;
		} break;
		case DW_FORM_STRING:
		case DW_FORM_BLOCK:
		case DW_FORM_BLOCK1:
		{
			INVALID_CODE_PATH;
		} break;
		case DW_FORM_FLAG:
		{
			result->flag = *data ? true : false;
			return 1;
		} break;
		case DW_FORM_SDATA:
		{
			INVALID_CODE_PATH;
		} break;
		case DW_FORM_STRP:
		{
			result->kind = DW_FORM_STRING;
			if (is_32_bit)
			{
				result->string = (const char *)str_data + *(u32 *)data;
				return sizeof(u32);
			}
			else
			{
				result->string = (const char *)str_data + *(u64 *)data;
				return sizeof(u64);
			}
		} break;
		case DW_FORM_UDATA:
		{
			usize bytes_read = uleb128_decode(data, &result->udata);
			return bytes_read;
		} break;
		case DW_FORM_REF_ADDR:
		{
			INVALID_CODE_PATH;
		} break;
		case DW_FORM_REF1:
		{
			result->ref1 = *data;
			return 1;
		} break;
		case DW_FORM_REF2:
		{
			result->ref2 = *(u16 *)data;
			return 2;
		} break;
		case DW_FORM_REF4:
		{
			result->ref4 = *(u32 *)data;
			return 4;
		} break;
		case DW_FORM_REF8:
		{
			result->ref8 = *(u64 *)data;
			return 8;
		} break;
		case DW_FORM_REF_UDATA:
		{
			usize bytes_read = uleb128_decode(data, &result->ref_udata);
			return bytes_read;
		} break;
		case DW_FORM_INDIRECT:
		{
			INVALID_CODE_PATH;
		} break;
		case DW_FORM_SEC_OFFSET:
		{
			if (is_32_bit)
			{
				result->sec_offset = *(u32 *)data;
				return sizeof(u32);
			}
			else
			{
				result->sec_offset = *(u64 *)data;
				return sizeof(u64);
			}
		} break;
		case DW_FORM_EXPRLOC:
		{
			usize bytes_read = uleb128_decode(data, &result->exprloc.length);
			result->exprloc.expr = data + bytes_read;
			return bytes_read + result->exprloc.length;
		} break;
		case DW_FORM_FLAG_PRESENT:
		{
			result->kind = DW_FORM_FLAG;
			result->flag = true;
			return 0;
		} break;
		case DW_FORM_STRX:
		{
			INVALID_CODE_PATH;
		} break;
		case DW_FORM_ADDRX:
		{
			usize bytes_read = uleb128_decode(data, &result->addr);
			return bytes_read;
		} break;
		case DW_FORM_REF_SUP4:
		case DW_FORM_STRP_SUP:
		case DW_FORM_DATA16:
		case DW_FORM_LINE_STRP:
		case DW_FORM_REF_SIG8:
		case DW_FORM_IMPLICIT_CONST:
		case DW_FORM_LOCLISTX:
		case DW_FORM_RNGLISTX:
		case DW_FORM_REF_SUP8:
		case DW_FORM_STRX1:
		case DW_FORM_STRX2:
		case DW_FORM_STRX3:
		case DW_FORM_STRX4:
		case DW_FORM_ADDRX1:
		case DW_FORM_ADDRX2:
		case DW_FORM_ADDRX3:
		case DW_FORM_ADDRX4:

		case DW_NUM_FORMS:
		default:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

void print_dwarf_form(dwarf_form_data form)
{
	switch (form.kind)
	{
		case DW_FORM_ADDR:
		{
			printf("%8lx", form.addr);
		} break;
		case DW_FORM_BLOCK2:
		case DW_FORM_BLOCK4:
		{
			INVALID_CODE_PATH;
		} break;
		case DW_FORM_DATA1:
		case DW_FORM_DATA2:
		case DW_FORM_DATA4:
		case DW_FORM_DATA8:
		{
			INVALID_CODE_PATH;
		} break;
		case DW_FORM_UDATA:
		{
			printf("%16lx", form.udata);
		} break;
		case DW_FORM_BLOCK:
		case DW_FORM_BLOCK1:
		{
			INVALID_CODE_PATH;
		} break;
		case DW_FORM_FLAG:
		{
			PC_GREEN_BOLD("%s", form.flag ? "true" : "false");
		} break;
		case DW_FORM_SDATA:
		{
			INVALID_CODE_PATH;
		} break;
		case DW_FORM_STRX1:
		case DW_FORM_STRING:
		case DW_FORM_STRP:
		{
			PC_YELLOW_BOLD("\"%s\"", form.string);
		} break;
		case DW_FORM_REF_ADDR:
		{
			INVALID_CODE_PATH;
		} break;
		case DW_FORM_REF1:
		{
			printf("%2x", form.ref1);
		} break;
		case DW_FORM_REF2:
		{
			printf("%4x", form.ref2);
		} break;
		case DW_FORM_REF4:
		{
			printf("%8x", form.ref4);
		} break;
		case DW_FORM_REF8:
		{
			printf("%16lx", form.ref8);
		} break;
		case DW_FORM_REF_UDATA:
		{
			printf("%16lx", form.ref_udata);
		} break;
		case DW_FORM_INDIRECT:
		{
			INVALID_CODE_PATH;
		} break;
		case DW_FORM_SEC_OFFSET:
		{
			printf("%8lx", form.sec_offset);
		} break;
		case DW_FORM_EXPRLOC:
		{
			if (form.exprloc.length == 1)
			{
				printf("1 byte of data: "); PC_WHITE_BOLD("%02x", *form.exprloc.expr);
			}
			else
			{
				printf("%lx bytes of data: ", form.exprloc.length);
				for (u64 i = 0; i < form.exprloc.length; ++i)
				{
					PC_WHITE_BOLD("%02x ", *(form.exprloc.expr + i));
				}
			}
		} break;
		case DW_FORM_FLAG_PRESENT:
		case DW_FORM_STRX:
		{
			INVALID_CODE_PATH;
		} break;
		case DW_FORM_ADDRX:
		{
			printf("%016lx", form.addr);
		} break;
		case DW_FORM_REF_SUP4:
		case DW_FORM_STRP_SUP:
		case DW_FORM_DATA16:
		case DW_FORM_LINE_STRP:
		case DW_FORM_REF_SIG8:
		case DW_FORM_IMPLICIT_CONST:
		case DW_FORM_LOCLISTX:
		case DW_FORM_RNGLISTX:
		case DW_FORM_REF_SUP8:
		case DW_FORM_STRX2:
		case DW_FORM_STRX3:
		case DW_FORM_STRX4:
		case DW_FORM_ADDRX1:
		case DW_FORM_ADDRX2:
		case DW_FORM_ADDRX3:
		case DW_FORM_ADDRX4:

		case DW_NUM_FORMS:
		default:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

void print_debug_info(dwarf_data *dw)
{
	PC_CYAN_BOLD("-- Dwarf Info ------------------------------------------------\n");
	for (usize section_idx = 0; section_idx < buf_len(dw->info.info_sections); ++section_idx)
	{
		dwarf_info_section section = dw->info.info_sections[section_idx];
		if (section.is_32_bit)
		{
			printf("32-bit\n");
		}
		else
		{
			printf("64-bit\n");
		}
		printf("version: %u\n", section.version);
		printf("pointer size: %u\n", section.ptr_size);
		printf("abbrev offset: %lu\n", section.abbrev_offset);
		
		for (usize i = 0; i < buf_len(section.entries); ++i)
		{
			dwarf_info_entry entry = section.entries[i];
			PC_CYAN_BOLD("%s\n", dwarf_tag_repr(entry.tag));
			for (usize j = 0; j < buf_len(entry.attrs); ++j)
			{
				dwarf_info_attr_entry attr = entry.attrs[j];
				printf("%s: ", dwarf_attr_repr(attr.attr));
				if (attr.attr == DW_AT_LANGUAGE)
				{
					printf("%s", dwarf_lang_repr(attr.form.udata));
				}
				else
				{
					print_dwarf_form(attr.form);
				}
				printf("\n");
			}
			PC_CYAN_BOLD("--------------------------------------------------------------\n");
		}
	}
}

void parse_debug_info(dwarf_data *dw)
{
	ASSERT(dw->abbrev.abbrev_sections != 0);
	if (dw->info.info_sections != 0)
	{
		return;
	}

	u8 *ptr = dw->info.data;
	while (ptr < dw->info.data + dw->info.size)
	{
		dwarf_info_section section = { 0 };
		u32 length_test = *(u32 *)ptr;
		ptr += sizeof(u32);
		if (length_test < 0xFFFFFFF0)
		{
			section.length = length_test;
			section.is_32_bit = true;
		}
		else
		{
			ASSERT(length_test == 0xFFFFFFFF);
			section.length = *(u64 *)ptr;
			ptr += sizeof(u64);
		}
		u8 *section_start = ptr;
		section.version = *(u16 *)ptr;
		ptr += sizeof(u16);
		ASSERT(section.version == 4);
		if (section.is_32_bit)
		{
			section.abbrev_offset = *(u32 *)ptr;
			ptr += sizeof(u32);
		}
		else
		{
			section.abbrev_offset = *(u64 *)ptr;
			ptr += sizeof(u64);
		}

		dwarf_abbrev_section *abbrev_section = get_abbrev_section(dw->abbrev.abbrev_sections,
																 section.abbrev_offset);

		section.ptr_size = *ptr++;
		
		while (ptr < section_start + section.length)
		{
			u64 abbrev_index = 0;
			u8 bytes_read = uleb128_decode(ptr, &abbrev_index);
			ptr += bytes_read;
			if (abbrev_index != 0)
			{
				dwarf_abbrev_entry *entry = get_abbrev_entry(abbrev_section->entries,
															 abbrev_index);
				dwarf_info_entry info = { 0 };
				info.tag = entry->tag;
				for (usize i = 0; i < buf_len(entry->attrs); ++i)
				{
					dwarf_info_attr_entry info_attr;
					info_attr.attr = entry->attrs[i].attr;
					bytes_read = parse_dwarf_form(entry->attrs[i].form, ptr, &info_attr.form, section.ptr_size, section.is_32_bit, dw->str.data);
					ptr += bytes_read;

					buf_push(info.attrs, info_attr);
				}
				buf_push(section.entries, info);
			}
		}
		buf_push(dw->info.info_sections, section);
	}
}

void parse_dwarf_data(dwarf_data *dw)
{
	dw->abbrev.abbrev_sections = parse_debug_abbrev(dw->abbrev.data, dw->abbrev.size);
	print_dwarf_abbrev(dw->abbrev.abbrev_sections);
	parse_debug_info(dw);
}

void print_dwarf_functions(dwarf_function *fns)
{
	for (u32 i = 0; i < buf_len(fns); ++i)
	{
		dwarf_function fn = fns[i];
		printf("function: "); PC_YELLOW_BOLD("\"%s\"\n", fn.name);
		printf("start: %016lx - end: %016lx\n", fn.start_address, fn.past_end_address);
	}
}

dwarf_function *parse_dwarf_functions(dwarf_data *dw)
{
	dw->abbrev.abbrev_sections = parse_debug_abbrev(dw->abbrev.data, dw->abbrev.size);
//	print_dwarf_abbrev(dw->abbrev.abbrev_sections);
	parse_debug_info(dw);
//	print_debug_info(dw);
	dwarf_function *functions = 0;

	for (usize section_idx = 0; section_idx < buf_len(dw->info.info_sections); ++section_idx)
	{
		dwarf_info_section section = dw->info.info_sections[section_idx];
		for (usize entry_idx = 0; entry_idx < buf_len(section.entries); ++entry_idx)
		{
			dwarf_info_entry entry = section.entries[entry_idx];
			if (entry.tag == DW_TAG_SUBPROGRAM)
			{
				dwarf_function fn = { 0 };
				for (usize attr_idx = 0; attr_idx < buf_len(entry.attrs); ++attr_idx)
				{
					dwarf_info_attr_entry attr_entry = entry.attrs[attr_idx];
					if (attr_entry.attr == DW_AT_NAME)
					{
						ASSERT(attr_entry.form.kind == DW_FORM_STRING);
						usize len = strlen(attr_entry.form.string) + 1;
						fn.name = xmalloc(len);
						fn.name = strcpy((char *)fn.name, attr_entry.form.string);
					}
					else if (attr_entry.attr == DW_AT_LOW_PC)
					{
						ASSERT(attr_entry.form.kind == DW_FORM_ADDR);
						fn.start_address = attr_entry.form.addr;
					}
					else if (attr_entry.attr == DW_AT_HIGH_PC)
					{
						ASSERT(attr_entry.form.kind == DW_FORM_UDATA);
						fn.past_end_address = attr_entry.form.addr;
					}
				}

				ASSERT(fn.name != 0);
				ASSERT(fn.start_address != 0);
				ASSERT(fn.past_end_address != 0);
				fn.past_end_address += fn.start_address;
				buf_push(functions, fn);
			}
		}
	}

	//print_dwarf_functions(functions);

	return functions;
}

#define LEB128_PRINT 0

#if LEB128_PRINT
#include "common/color_printing.h"
#endif

// result is u8[10], should be zero initialized, #todo could return a stretchy buffer
usize uleb128_encode(u64 input, u8 *result)
{
	#if LEB128_PRINT
	PC_BLUE_BOLD("encode unsigned leb128 -------------------------------------\n");
	PC_BLUE("input: "); printf("%016lx\n", input);
	#endif
	usize i = 0;
	u64 copy = input;
	for (; i < 10; ++i)
	{
		result[i] = copy & 0x7F;
		copy >>= 7;
		if (copy != 0)
		{
			result[i] |= 0x80;
			#if LEB128_PRINT
			printf("%016lx -> %02x\n", copy, result[i]);
			#endif
		}
		else
		{
			#if LEB128_PRINT
			printf("%016lx -> %02x\n", copy, result[i]);
			#endif
			break;
		}
		
	}
	#if LEB128_PRINT
	PC_BLUE("output: ");
	for (usize j = 0; j < 10; ++j)
	{
		u8 val = result[j];
		if (val == 0 && j != 0) { break; }
		printf("%02x ", val);
	}
	printf("\n");
	#endif
	return i + 1;
}

// input is u8[10]
usize uleb128_decode(u8 *input, u64 *result)
{
	#if LEB128_PRINT
	PC_BLUE_BOLD("decode unsigned leb128 -------------------------------------\n");
	PC_BLUE("input: ");
	for (usize i = 0; i < 10; ++i)
	{
		u8 val = input[i];
		if (val == 0 && i != 0) { break; }
		printf("%02x ", val);
	}
	printf("\n");
	#endif
	
	*result = 0;
	u8 shift = 0;
	usize i = 0;
	for (; i < 10; ++i)
	{
		u8 byte = input[i];
		u64 temp = (byte & 0x7F);
		//temp <<= shift;
		*result |= temp << shift;
		#if LEB128_PRINT
		printf("%016lx <- %02x\n", *result, input[i]);
		#endif
		if ((byte & 0x80) == 0)
		{
			break;
		}
		shift += 7;
		
	}
	#if LEB128_PRINT
	PC_BLUE("output: "); printf("%016lx\n", *result);
	#endif
	return i + 1;
}

// result is u8[10], should be zero initialized
usize sleb128_encode(s64 input, u8 *result)
{
	#if LEB128_PRINT
	PC_BLUE_BOLD("encode signed leb128 ---------------------------------------\n");
	PC_BLUE("input: "); printf("%016lx\n", input);
	#endif
	s64 copy = input;
	usize i = 0;
	for (; i < 10; ++i)
	{
		result[i] = copy & 0x7F;
		copy >>= 7;
		if ((copy == 0 && (result[i] & 0x40) == 0) || (copy == -1 && (result[i] & 0x40)))
		{
			#if LEB128_PRINT
			printf("%016lx -> %02x\n", copy, result[i]);
			#endif
			break;
		}
		else
		{
			#if LEB128_PRINT
			result[i] |= 0x80;
			printf("%016lx -> %02x\n", copy, result[i]);
			#endif
		}
	}
	#if LEB128_PRINT
	PC_BLUE("output: ");
	for (usize j = 0; j < 10; ++j)
	{
		u8 val = result[j];
		if (val == 0 && j != 0) { break; }
		printf("%02x ", val);
	}
	printf("\n");
	#endif
	return i + 1;
}

// input is u8[10]
usize sleb128_decode(u8 *input, s64 *result)
{
	#if LEB128_PRINT
	PC_BLUE_BOLD("decode signed leb128 ---------------------------------------\n");
	PC_BLUE("input: ");
	for (usize i = 0; i < 10; ++i)
	{
		u8 val = input[i];
		if (val == 0) { break; }
		printf("%02x ", val);
	}
	printf("\n");
	#endif

	*result = 0;
	u8 shift = 0;
	s8 byte = 0;
	usize i = 0;

	for (; i < 10; ++i)
	{
		byte = (s8)input[i];
		s64 temp = (byte & 0x7F);
		*result |= temp << shift;
		shift += 7;
		#if LEB128_PRINT
		printf("%016lx <- %02x\n", *result, input[i]);
		#endif
		if ((byte & 0x80) == 0)
		{
			break;
		}
	}
	if ((shift < 64) && (byte & 0x40))
	{
		*result |= (~0 << shift);
	}
	#if LEB128_PRINT
	PC_BLUE("output: "); printf("%016lx\n", *result);
	#endif
	return i + 1;
}

void test_leb128(void)
{
	u8 result[10] = { 0 };
	u64 ures = 0;
	usize bytes_written = uleb128_encode(0xFFFFFFFFFFFFFFFFull, result);
	usize bytes_read = uleb128_decode(result, &ures);
	for (u32 i = 0; i < 10; ++i) { result[i] = 0; }
	bytes_written = uleb128_encode(624485ull, result);
	bytes_read = uleb128_decode(result, &ures);
	for (u32 i = 0; i < 10; ++i) { result[i] = 0; }
	bytes_written = uleb128_encode(0ull, result);
	bytes_read = uleb128_decode(result, &ures);
	for (u32 i = 0; i < 10; ++i) { result[i] = 0; }

	s64 sres = 0;
	bytes_written = sleb128_encode(0x8FFFFFFFFFFFFFFFll, result);
	bytes_read = sleb128_decode(result, &sres);
	for (u32 i = 0; i < 10; ++i) { result[i] = 0; }
	bytes_written = sleb128_encode(0xFFFFFFFFFFFFFFFFll, result);
	bytes_read = sleb128_decode(result, &sres);
	for (u32 i = 0; i < 10; ++i) { result[i] = 0; }
	bytes_written = sleb128_encode(0x7FFFFFFFFFFFFFFFll, result);
	bytes_read = sleb128_decode(result, &sres);
	for (u32 i = 0; i < 10; ++i) { result[i] = 0; }
	bytes_written = sleb128_encode(-123456ll, result);
	bytes_read = sleb128_decode(result, &sres);
	for (u32 i = 0; i < 10; ++i) { result[i] = 0; }
	bytes_written = sleb128_encode(0ll, result);
	bytes_read = sleb128_decode(result, &sres);
}
