#ifndef XIA_DIRECTIVES_H
#define XIA_DIRECTIVES_H

#include "common/types.h"
#include "elf64/header_info.h"

#include "tokens.h"

#define MAX_ALIGNMENT 16

typedef enum directive_kind
{
	DIR_SECTION,
	DIR_GLOBAL,
	DIR_ALIGN,

	DIR_ASCII,

	FIRST_DATA_DIR = DIR_ASCII,
	DIR_I8,
	DIR_I16,
	DIR_I32,
	DIR_I64,

	LAST_DATA_DIR = DIR_I64,

	NUM_DIRECTIVES,
	DIRECTIVE_NONE
} directive_kind;

struct directive_name
{
	const char *name;
	pos_range name_range;
};

typedef struct directives
{
	directive_kind kind;
	position pos;
	union
	{
		struct
		{
			const char *name;
			pos_range name_range;
			section_type type;
			u64 flags;
		} section;
		struct
		{
			const char *name;
			pos_range name_range;
		} global;
		struct
		{
			u8 alignment;
			pos_range alignment_range;
			u8 fill_byte;
			pos_range fill_byte_range;
			u32 bytes_written;
			u64 code_offset;
		} align;

		// data
		const char *str;
		u8 i8;
		u16 i16;
		u32 i32;
		u64 i64;
	};

} directives;

extern const char *directive_names[NUM_DIRECTIVES];

#endif // !XIA_DIRECTIVES_H
