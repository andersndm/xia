#include "registers.h"

const char *register_x64_names[NUM_REGISTERS] =
{
	[RAX] = "rax",
	[RCX] = "rcx",
	[RDX] = "rdx",
	[RBX] = "rbx",
	[RSP] = "rsp",
	[RBP] = "rbp",
	[RSI] = "rsi",
	[RDI] = "rdi",
	[R8]  = "r8",
	[R9]  = "r9",
	[R10] = "r10",
	[R11] = "r11",
	[R12] = "r12",
	[R13] = "r13",
	[R14] = "r14",
	[R15] = "r15",
};

const char *register_x32_names[NUM_REGISTERS] =
{
	[EAX] = "eax",
	[ECX] = "ecx",
	[EDX] = "edx",
	[EBX] = "ebx",
	[ESP] = "esp",
	[EBP] = "ebp",
	[ESI] = "esi",
	[EDI] = "edi",
	[R8d] = "r8d",
	[R9d] = "r9d",
	[R10D] = "r10d",
	[R11D] = "r11d",
	[R12D] = "r12d",
	[R13D] = "r13d",
	[R14D] = "r14d",
	[R15D] = "r15d",
};

const char *register_x16_names[NUM_REGISTERS] =
{
	[AX] = "ax",
	[CX] = "cx",
	[DX] = "dx",
	[BX] = "bx",
	[SP] = "sp",
	[BP] = "bp",
	[SI] = "si",
	[DI] = "di",
	[R8W] = "r8w",
	[R9W] = "r9w",
	[R10W] = "r10w",
	[R11W] = "r11w",
	[R12W] = "r12w",
	[R13W] = "r13w",
	[R14W] = "r14w",
	[R15W] = "r15w",
};

const char *register_x8_names[NUM_REGISTERS] =
{
	[AL] = "al",
	[CL] = "cl",
	[DL] = "dl",
	[BL] = "bl",
	[AH] = "ah",
	[CH] = "ch",
	[DH] = "dh",
	[BH] = "bh",
	[R8B] = "r8b",
	[R9B] = "r9b",
	[R10B] = "r10b",
	[R11B] = "r11b",
	[R12B] = "r12b",
	[R13B] = "r13b",
	[R14B] = "r14b",
	[R15B] = "r15b",
};
