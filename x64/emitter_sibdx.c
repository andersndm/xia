#include "emitter_sibdx.h"

#include "common/macros.h"

// -----------------------------------------------------------------------------
//     #section x8 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x8_emit_sibdb(opcode op, registers base, registers index, log2_scale scale,
							u8 offset)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodes8[op].m);
	emit_indirect_indexed_byte_displaced(opcodes8[op].ext_m, base.reg, index.reg, scale,
										 offset);
}

internal void x8_emit_sibdd(opcode op, registers base, registers index, log2_scale scale,
							u32 offset)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodes8[op].m);
	emit_indirect_indexed_displaced(opcodes8[op].ext_m, base.reg, index.reg, scale,
										 offset);
}

internal void x8_emit_sibdb_r(opcode op, registers base, registers index, log2_scale scale, u8 offset, registers src)
{
	ASSERT(src.width == B8);
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(src.reg, base.reg, index.reg);
	emit_byte(opcodes8[op].r);
	emit_indirect_indexed_byte_displaced(src.reg, base.reg, index.reg, scale,
										 offset);
}

internal void x8_emit_sibdd_r(opcode op, registers base, registers index, log2_scale scale, u32 offset, registers src)
{
	ASSERT(src.width == B8);
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(src.reg, base.reg, index.reg);
	emit_byte(opcodes8[op].r);
	emit_indirect_indexed_displaced(src.reg, base.reg, index.reg, scale,
										 offset);
}

internal void x8_emit_sibdb_ib(opcode op, registers base, registers index, log2_scale scale,
								u8 offset, u8 ib)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodes8[op].ib);
	emit_indirect_indexed_byte_displaced(opcodes8[op].ext_i, base.reg, index.reg, scale,
										 offset);
	emit_byte(ib);
}

internal void x8_emit_sibdd_ib(opcode op, registers base, registers index, log2_scale scale,
								u32 offset, u8 ib)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodes8[op].ib);
	emit_indirect_indexed_displaced(opcodes8[op].ext_i, base.reg, index.reg, scale,
										 offset);
	emit_byte(ib);
}

// }}}
// -----------------------------------------------------------------------------
//     #section x16 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x16_emit_sibdb(opcode op, registers base, registers index, log2_scale scale,
							u8 offset)
{
	ASSERT(base.width == index.width);
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].m);
	emit_indirect_indexed_byte_displaced(opcodesX[op].ext_m, base.reg, index.reg, scale,
										 offset);
}

internal void x16_emit_sibdd(opcode op, registers base, registers index, log2_scale scale,
							u32 offset)
{
	ASSERT(base.width == index.width);
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].m);
	emit_indirect_indexed_displaced(opcodesX[op].ext_m, base.reg, index.reg, scale,
										 offset);
}

internal void x16_emit_sibdb_r(opcode op, registers base, registers index, log2_scale scale, u8 offset, registers src)
{
	ASSERT(src.width == B16);
	ASSERT(base.width == index.width);
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(src.reg, base.reg, index.reg);
	emit_byte(opcodesX[op].r);
	emit_indirect_indexed_byte_displaced(src.reg, base.reg, index.reg, scale,
										 offset);
}

internal void x16_emit_sibdd_r(opcode op, registers base, registers index, log2_scale scale, u32 offset, registers src)
{
	ASSERT(src.width == B16);
	ASSERT(base.width == index.width);
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(src.reg, base.reg, index.reg);
	emit_byte(opcodesX[op].r);
	emit_indirect_indexed_displaced(src.reg, base.reg, index.reg, scale,
										 offset);
}

internal void x16_emit_sibdb_ib(opcode op, registers base, registers index, log2_scale scale,
								u8 offset, u8 ib)
{
	ASSERT(base.width == index.width);
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].ib);
	emit_indirect_indexed_byte_displaced(opcodesX[op].ext_i, base.reg, index.reg, scale,
										 offset);
	emit_byte(ib);
}

internal void x16_emit_sibdb_iw(opcode op, registers base, registers index, log2_scale scale,
								u8 offset, u16 iw)
{
	ASSERT(base.width == index.width);
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].ix);
	emit_indirect_indexed_byte_displaced(opcodesX[op].ext_i, base.reg, index.reg, scale,
										 offset);
	emit_word(iw);
}

internal void x16_emit_sibdd_ib(opcode op, registers base, registers index, log2_scale scale,
								u32 offset, u8 ib)
{
	ASSERT(base.width == index.width);
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].ib);
	emit_indirect_indexed_displaced(opcodesX[op].ext_i, base.reg, index.reg, scale,
										 offset);
	emit_byte(ib);
}

internal void x16_emit_sibdd_iw(opcode op, registers base, registers index, log2_scale scale,
								u32 offset, u16 iw)
{
	ASSERT(base.width == index.width);
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].ix);
	emit_indirect_indexed_displaced(opcodesX[op].ext_i, base.reg, index.reg, scale,
										 offset);
	emit_word(iw);
}

// }}}
// -----------------------------------------------------------------------------
//     #section x32 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x32_emit_sibdb(opcode op, registers base, registers index, log2_scale scale,
							u8 offset)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].m);
	emit_indirect_indexed_byte_displaced(opcodesX[op].ext_m, base.reg, index.reg, scale,
										 offset);
}

internal void x32_emit_sibdd(opcode op, registers base, registers index, log2_scale scale,
							u32 offset)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].m);
	emit_indirect_indexed_displaced(opcodesX[op].ext_m, base.reg, index.reg, scale,
										 offset);
}

internal void x32_emit_sibdb_r(opcode op, registers base, registers index, log2_scale scale, u8 offset, registers src)
{
	ASSERT(src.width == B32);
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(src.reg, base.reg, index.reg);
	emit_byte(opcodesX[op].r);
	emit_indirect_indexed_byte_displaced(src.reg, base.reg, index.reg, scale,
										 offset);
}

internal void x32_emit_sibdd_r(opcode op, registers base, registers index, log2_scale scale, u32 offset, registers src)
{
	ASSERT(src.width == B32);
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(src.reg, base.reg, index.reg);
	emit_byte(opcodesX[op].r);
	emit_indirect_indexed_displaced(src.reg, base.reg, index.reg, scale,
										 offset);
}

internal void x32_emit_sibdb_ib(opcode op, registers base, registers index, log2_scale scale,
								u8 offset, u8 ib)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].ib);
	emit_indirect_indexed_byte_displaced(opcodesX[op].ext_i, base.reg, index.reg, scale,
										 offset);
	emit_byte(ib);
}

internal void x32_emit_sibdb_id(opcode op, registers base, registers index, log2_scale scale,
								u8 offset, u32 id)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].ix);
	emit_indirect_indexed_byte_displaced(opcodesX[op].ext_i, base.reg, index.reg, scale,
										 offset);
	emit_dword(id);
}

internal void x32_emit_sibdd_ib(opcode op, registers base, registers index, log2_scale scale,
								u32 offset, u8 ib)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].ib);
	emit_indirect_indexed_displaced(opcodesX[op].ext_i, base.reg, index.reg, scale,
										 offset);
	emit_byte(ib);
}

internal void x32_emit_sibdd_id(opcode op, registers base, registers index, log2_scale scale,
								u32 offset, u32 id)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].ix);
	emit_indirect_indexed_displaced(opcodesX[op].ext_i, base.reg, index.reg, scale,
										 offset);
	emit_dword(id);
}

// }}}
// -----------------------------------------------------------------------------
//     #section x64 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x64_emit_sibdb(opcode op, registers base, registers index, log2_scale scale,
							u8 offset)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].m);
	emit_indirect_indexed_byte_displaced(opcodesX[op].ext_m, base.reg, index.reg, scale,
										 offset);
}

internal void x64_emit_sibdd(opcode op, registers base, registers index, log2_scale scale,
							u32 offset)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].m);
	emit_indirect_indexed_displaced(opcodesX[op].ext_m, base.reg, index.reg, scale,
										 offset);
}

internal void x64_emit_sibdb_r(opcode op, registers base, registers index, log2_scale scale, u8 offset, registers src)
{
	ASSERT(src.width == B64);
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_rex_indexed(src.reg, base.reg, index.reg);
	emit_byte(opcodesX[op].r);
	emit_indirect_indexed_byte_displaced(src.reg, base.reg, index.reg, scale,
										 offset);
}

internal void x64_emit_sibdd_r(opcode op, registers base, registers index, log2_scale scale, u32 offset, registers src)
{
	ASSERT(src.width == B64);
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_rex_indexed(src.reg, base.reg, index.reg);
	emit_byte(opcodesX[op].r);
	emit_indirect_indexed_displaced(src.reg, base.reg, index.reg, scale,
										 offset);
}

internal void x64_emit_sibdb_ib(opcode op, registers base, registers index, log2_scale scale,
								u8 offset, u8 ib)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].ib);
	emit_indirect_indexed_byte_displaced(opcodesX[op].ext_i, base.reg, index.reg, scale,
										 offset);
	emit_byte(ib);
}

internal void x64_emit_sibdb_id(opcode op, registers base, registers index, log2_scale scale,
								u8 offset, u32 id)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].ix);
	emit_indirect_indexed_byte_displaced(opcodesX[op].ext_i, base.reg, index.reg, scale,
										 offset);
	emit_dword(id);
}

internal void x64_emit_sibdd_ib(opcode op, registers base, registers index, log2_scale scale,
								u32 offset, u8 ib)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].ib);
	emit_indirect_indexed_displaced(opcodesX[op].ext_i, base.reg, index.reg, scale,
										 offset);
	emit_byte(ib);
}

internal void x64_emit_sibdd_id(opcode op, registers base, registers index, log2_scale scale,
								u32 offset, u32 id)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].ix);
	emit_indirect_indexed_displaced(opcodesX[op].ext_i, base.reg, index.reg, scale,
										 offset);
	emit_dword(id);
}

// }}}
// -----------------------------------------------------------------------------
//     #section width dispatching
// -----------------------------------------------------------------------------
// {{{

void emit_sibdx(bit_width width, opcode op, registers base, registers index, log2_scale scale,
				immediate_op offset)
{
	switch (offset.kind)
	{
		case IB:
		{
			switch (width)
			{
				case B8:
				{
					x8_emit_sibdb(op, base, index, scale, offset.ib);
				} break;
				case B16:
				{
					x16_emit_sibdb(op, base, index, scale, offset.ib);
				} break;
				case B32:
				{
					x32_emit_sibdb(op, base, index, scale, offset.ib);
				} break;
				case B64:
				{
					x64_emit_sibdb(op, base, index, scale, offset.ib);
				} break;
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
		} break;
		case ID:
		{
			switch (width)
			{
				case B8:
				{
					x8_emit_sibdd(op, base, index, scale, offset.id);
				} break;
				case B16:
				{
					x16_emit_sibdd(op, base, index, scale, offset.id);
				} break;
				case B32:
				{
					x32_emit_sibdd(op, base, index, scale, offset.id);
				} break;
				case B64:
				{
					x64_emit_sibdd(op, base, index, scale, offset.id);
				} break;
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
		} break;
		case IW:
		case IQ:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

void emit_sibdx_r(bit_width width, opcode op, registers base, registers index, log2_scale scale,
				  immediate_op offset, registers src)
{
	switch (offset.kind)
	{
		case IB:
		{
			switch (width)
			{
				case B8:
				{
					x8_emit_sibdb_r(op, base, index, scale, offset.ib, src);
				} break;
				case B16:
				{
					x16_emit_sibdb_r(op, base, index, scale, offset.ib, src);
				} break;
				case B32:
				{
					x32_emit_sibdb_r(op, base, index, scale, offset.ib, src);
				} break;
				case B64:
				{
					x64_emit_sibdb_r(op, base, index, scale, offset.ib, src);
				} break;
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
		} break;
		case ID:
		{
			switch (width)
			{
				case B8:
				{
					x8_emit_sibdd_r(op, base, index, scale, offset.id, src);
				} break;
				case B16:
				{
					x16_emit_sibdd_r(op, base, index, scale, offset.id, src);
				} break;
				case B32:
				{
					x32_emit_sibdd_r(op, base, index, scale, offset.id, src);
				} break;
				case B64:
				{
					x64_emit_sibdd_r(op, base, index, scale, offset.id, src);
				} break;
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
		} break;
		case IW:
		case IQ:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

void emit_sibdx_ix(bit_width width, opcode op, registers base, registers index, log2_scale scale,
				   immediate_op offset, immediate_op imm_op)
{
	switch (offset.kind)
	{
		case IB:
		{
			switch (width)
			{
				case B8:
				{
					ASSERT(imm_op.kind == IB);
					x8_emit_sibdb_ib(op, base, index, scale, offset.ib, imm_op.ib);
				} break;
				case B16:
				{
					if (imm_op.kind == IB)
					{
						x16_emit_sibdb_ib(op, base, index, scale, offset.ib, imm_op.ib);
					}
					else
					{
						ASSERT(imm_op.kind == IW);
						x16_emit_sibdb_iw(op, base, index, scale, offset.ib, imm_op.iw);
					}
				} break;
				case B32:
				{
					if (imm_op.kind == IB)
					{
						x32_emit_sibdb_ib(op, base, index, scale, offset.ib, imm_op.ib);
					}
					else
					{
						if (imm_op.kind == IW)
						{
							imm_op.kind = ID;
							imm_op.id = (u32)(s32)imm_op.iw;
						}
						ASSERT(imm_op.kind == ID);
						x32_emit_sibdb_id(op, base, index, scale, offset.ib, imm_op.id);
					}
				} break;
				case B64:
				{
					if (imm_op.kind == IB)
					{
						x64_emit_sibdb_ib(op, base, index, scale, offset.ib, imm_op.ib);
					}
					else
					{
						if (imm_op.kind == IW)
						{
							imm_op.kind = ID;
							imm_op.id = (u32)(s32)imm_op.iw;
						}
						ASSERT(imm_op.kind == ID);
						x64_emit_sibdb_id(op, base, index, scale, offset.ib, imm_op.id);
					}
				} break;
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
		} break;
		case ID:
		{
			switch (width)
			{
				case B8:
				{
					ASSERT(imm_op.kind == IB);
					x8_emit_sibdd_ib(op, base, index, scale, offset.id, imm_op.ib);
				} break;
				case B16:
				{
					if (imm_op.kind == IB)
					{
						x16_emit_sibdd_ib(op, base, index, scale, offset.id, imm_op.ib);
					}
					else
					{
						ASSERT(imm_op.kind == IW);
						x16_emit_sibdd_iw(op, base, index, scale, offset.id, imm_op.iw);
					}
				} break;
				case B32:
				{
					if (imm_op.kind == IB)
					{
						x32_emit_sibdd_ib(op, base, index, scale, offset.id, imm_op.ib);
					}
					else
					{
						ASSERT(imm_op.kind == ID);
						x32_emit_sibdd_id(op, base, index, scale, offset.id, imm_op.id);
					}
				} break;
				case B64:
				{
					if (imm_op.kind == IB)
					{
						x64_emit_sibdd_ib(op, base, index, scale, offset.id, imm_op.ib);
					}
					else
					{
						ASSERT(imm_op.kind == ID);
						x64_emit_sibdd_id(op, base, index, scale, offset.id, imm_op.id);
					}
				} break;
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
		} break;
		case IW:
		case IQ:
		{
			INVALID_CODE_PATH;
		} break;
	}
}
