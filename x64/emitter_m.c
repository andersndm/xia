#include "emitter_m.h"

#include "common/macros.h"

// -----------------------------------------------------------------------------
//     #section x8 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x8_emit_m(opcode op, registers base)
{
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodes8[op].m);
	emit_xm(opcodes8[op].ext_m, base.reg);
}

internal void x8_emit_m_r(opcode op, registers base, registers src)
{
	ASSERT(src.width == B8);
	emit_addressing_mode(base.width);
	emit_optional_rex(src.reg, base.reg);
	emit_byte(opcodes8[op].r);
	emit_xm(src.reg, base.reg);
}

internal void x8_emit_m_ib(opcode op, registers base, u8 ib)
{
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodes8[op].ib);
	emit_xm(opcodes8[op].ext_i, base.reg);
	emit_byte(ib);
}

// }}}
// -----------------------------------------------------------------------------
//     #section x16 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x16_emit_m(opcode op, registers base)
{
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodesX[op].m);
	emit_xm(opcodesX[op].ext_m, base.reg);
}

internal void x16_emit_m_r(opcode op, registers base, registers src)
{
	ASSERT(src.width == B16);
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex(src.reg, base.reg);
	emit_byte(opcodesX[op].r);
	emit_xm(src.reg, base.reg);
}

internal void x16_emit_m_ib(opcode op, registers base, u8 ib)
{
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodesX[op].ib);
	emit_xm(opcodesX[op].ext_i, base.reg);
	emit_byte(ib);
}

internal void x16_emit_m_iw(opcode op, registers base, u16 iw)
{
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodesX[op].ix);
	emit_xm(opcodesX[op].ext_i, base.reg);
	emit_word(iw);
}

// }}}
// -----------------------------------------------------------------------------
//     #section x32 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x32_emit_m(opcode op, registers base)
{
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodesX[op].m);
	emit_xm(opcodesX[op].ext_m, base.reg);
}

internal void x32_emit_m_r(opcode op, registers base, registers src)
{
	ASSERT(src.width == B32);
	emit_addressing_mode(base.width);
	emit_optional_rex(src.reg, base.reg);
	emit_byte(opcodesX[op].r);
	emit_xm(src.reg, base.reg);
}

internal void x32_emit_m_ib(opcode op, registers base, u8 ib)
{
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodesX[op].ib);
	emit_xm(opcodesX[op].ext_i, base.reg);
	emit_byte(ib);
}

internal void x32_emit_m_id(opcode op, registers base, u32 id)
{
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodesX[op].ix);
	emit_xm(opcodesX[op].ext_i, base.reg);
	emit_dword(id);
}

// }}}
// -----------------------------------------------------------------------------
//     #section x64 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x64_emit_m(opcode op, registers base)
{
	emit_addressing_mode(base.width);
	emit_rex(0x00, base.reg);
	emit_byte(opcodesX[op].m);
	emit_xm(opcodesX[op].ext_m, base.reg);
}

internal void x64_emit_m_r(opcode op, registers base, registers src)
{
	ASSERT(src.width == B64);
	emit_addressing_mode(base.width);
	emit_rex(src.reg, base.reg);
	emit_byte(opcodesX[op].r);
	emit_xm(src.reg, base.reg);
}

internal void x64_emit_m_ib(opcode op, registers base, u8 ib)
{
	emit_addressing_mode(base.width);
	emit_rex(0x00, base.reg);
	emit_byte(opcodesX[op].ib);
	emit_xm(opcodesX[op].ext_i, base.reg);
	emit_byte(ib);
}

internal void x64_emit_m_id(opcode op, registers base, u32 id)
{
	emit_addressing_mode(base.width);
	emit_rex(0x00, base.reg);
	emit_byte(opcodesX[op].ix);
	emit_xm(opcodesX[op].ext_i, base.reg);
	emit_dword(id);
}

// }}}
// -----------------------------------------------------------------------------
//     #section width dispatching
// -----------------------------------------------------------------------------
// {{{

void emit_m(bit_width width, opcode op, registers base)
{
	switch (width)
	{
		case B8:
		{
			x8_emit_m(op, base);
		} break;
		case B16:
		{
			x16_emit_m(op, base);
		} break;
		case B32:
		{
			x32_emit_m(op, base);
		} break;
		case B64:
		{
			x64_emit_m(op, base);
		} break;
		case BNONE:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

void emit_m_r(bit_width width, opcode op, registers base, registers src)
{
	switch (width)
	{
		case B8:
		{
			x8_emit_m_r(op, base, src);
		} break;
		case B16:
		{
			x16_emit_m_r(op, base, src);
		} break;
		case B32:
		{
			x32_emit_m_r(op, base, src);
		} break;
		case B64:
		{
			x64_emit_m_r(op, base, src);
		} break;
		case BNONE:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

void emit_m_ix(bit_width width, opcode op, registers base, immediate_op imm_op)
{
	switch (width)
	{
		case B8:
		{
			ASSERT(imm_op.kind == IB);
			x8_emit_m_ib(op, base, imm_op.ib);
		} break;
		case B16:
		{
			if (imm_op.kind == IB)
			{
				x16_emit_m_ib(op, base, imm_op.ib);
			}
			else
			{
				ASSERT(imm_op.kind == IW);
				x16_emit_m_iw(op, base, imm_op.iw);
			}
		} break;
		case B32:
		{
			// #todo may be cases where x32 can take a 16-bit immediate
			if (imm_op.kind == IB)
			{
				x32_emit_m_ib(op, base, imm_op.ib);
			}
			else
			{
				ASSERT(imm_op.kind == ID);
				x32_emit_m_id(op, base, imm_op.id);
			}
		} break;
		case B64:
		{
			// #todo may be cases where x64 can take a 16-bit immediate
			// #todo rare cases for 64-bit immediate
			if (imm_op.kind == IB)
			{
				x64_emit_m_ib(op, base, imm_op.ib);
			}
			else
			{			
				ASSERT(imm_op.kind == ID);
				x64_emit_m_id(op, base, imm_op.id);
			}
		} break;
		case BNONE:
		{
			INVALID_CODE_PATH;
		} break;
	}
}
