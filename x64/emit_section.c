#include "emit_section.h"

#include "common/buf.h"
#include "common/macros.h"
#include "common/memory.h"

#include "emitter_r.h"
#include "emitter_m.h"
#include "emitter_mdx.h"
#include "emitter_sib.h"
#include "emitter_sibdx.h"
#include "emitter_disp.h"
#include "emitter_rip_disp.h"
#include "emitter_special.h"

// #todo should specify sign-extension
immediate_op promote_immediate(immediate_op imm, immediate_kind kind)
{
	if (imm.kind == kind)
	{
		return imm;
	}

	ASSERT(kind > imm.kind);
	if (kind == IW)
	{
		imm.kind = IW;
		imm.iw = (u16)(s16)imm.ib;
	}
	else if (kind == ID)
	{
		if (imm.kind == IB)
		{
			imm.id = (u32)(s32)imm.ib;
		}
		else if (imm.kind == IW)
		{
			imm.id = (u32)(s32)imm.iw;
		}
		imm.kind = ID;
	}
	else
	{
		ASSERT(kind == IQ);
		if (imm.kind == IB)
		{
			imm.iq = (u64)(s64)imm.ib;
		}
		else if (imm.kind == IW)
		{
			imm.iq = (u64)(s64)imm.iw;
		}
		else
		{
			imm.iq = (u64)(s64)imm.id;
		}
		imm.kind = IQ;
	}
	return imm;
}

internal void emit_instruction_r_x(bit_width width, opcode op, registers dest, operand src)
{
	if (src.kind != OPER_NONE)
	{
		bit_width width = src.width;
		ASSERT(width == B8 || width == B16 || width == B32 || width == B64);
	}
	switch (src.kind)
	{
		case OPER_NONE:
		{
			emit_r(width, op, dest);
		} break;
		case OPER_IMMEDIATE:
		{
			emit_r_ix(width, op, dest, src.imm_op);
		} break;
		case OPER_DISPLACEMENT:
		{
			emit_r_disp(width, op, dest, src.displacement);
		} break;
		case OPER_MEM_LABEL:
		{
			if (dest.width == B8)
			{
				emit_optional_rex(dest.reg, 0x00);
				emit_byte(opcodes8[op].m);
			}
			else
			{
				if (dest.width == B16)
				{
					emit_x16_prefix();
					emit_optional_rex(dest.reg, 0x00);
				}
				else if (dest.width == B32)
				{
					emit_optional_rex(dest.reg, 0x00);
				}
				else
				{
					ASSERT(dest.width == B64);
					emit_rex(dest.reg, 0x00);
				}
				emit_byte(opcodesX[op].m);
			}

			emit_indirect_displaced_rip(dest.reg, 0);
		} break;
		case OPER_RIP_DISPLACEMENT:
		{
			emit_r_rip_disp(width, op, dest, src.displacement);
		} break;
		case OPER_CC:
		{
			INVALID_CODE_PATH;
		} break;
		case OPER_REGISTER:
		{
			emit_r_r(width, op, dest, src.reg);
		} break;
		case OPER_MEM_REGISTER:
		{
			emit_r_m(width, op, dest, src.reg);
		} break;
		case OPER_MEM_REGISTER_OFFSET:
		{
			switch (src.mem_offset.offset.kind)
			{
				case IB:
				{
					emit_r_mdb(width, op, dest, src.mem_offset.base, src.mem_offset.offset.ib);
				} break;
				case IW:
				{
					emit_r_mdd(width, op, dest, src.mem_offset.base, (u32)src.mem_offset.offset.iw);
				} break;
				case ID:
				{
					emit_r_mdd(width, op, dest, src.mem_offset.base, src.mem_offset.offset.id);
				} break;
				case IQ:
				{
					INVALID_CODE_PATH;
				} break;
			}
		} break;
		case OPER_MEM_REGISTER_INDEXED:
		{
			emit_r_sib(width, op, dest, src.mem_indexed.base, src.mem_indexed.index,
					   src.mem_indexed.scale);
		} break;
		case OPER_MEM_REGISTER_INDEXED_OFFSET:
		{
			switch (src.mem_indexed_offset.offset.kind)
			{
				case IB:
				{
					emit_r_sibdb(width, op, dest, src.mem_indexed_offset.base,
								 src.mem_indexed_offset.index, src.mem_indexed_offset.scale,
								 src.mem_indexed_offset.offset.ib);
				} break;
				case IW:
				{
					emit_r_sibdd(width, op, dest, src.mem_indexed_offset.base,
								 src.mem_indexed_offset.index, src.mem_indexed_offset.scale,
								 (u32)src.mem_indexed_offset.offset.iw);
				} break;
				case ID:
				{
					emit_r_sibdd(width, op, dest, src.mem_indexed_offset.base,
								 src.mem_indexed_offset.index, src.mem_indexed_offset.scale,
								 src.mem_indexed_offset.offset.id);
				} break;
				case IQ:
				{
					INVALID_CODE_PATH;
				} break;
			}
		} break;
		case OPER_LABEL:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

internal void emit_instruction_m_x(bit_width width, opcode op, registers base, operand src)
{
	if (src.kind != OPER_NONE)
	{
		bit_width width = src.width;
		ASSERT(width == B8 || width == B16 || width == B32 || width == B64);
	}
	switch (src.kind)
	{
		case OPER_NONE:
		{
			emit_m(width, op, base);
		} break;
		case OPER_IMMEDIATE:
		{
			emit_m_ix(width, op, base, src.imm_op);
		} break;
		case OPER_REGISTER:
		{
			emit_m_r(width, op, base, src.reg);
		} break;
		case OPER_LABEL:
		case OPER_MEM_LABEL:
		case OPER_DISPLACEMENT:
		case OPER_RIP_DISPLACEMENT:
		case OPER_CC:
		case OPER_MEM_REGISTER:
		case OPER_MEM_REGISTER_OFFSET:
		case OPER_MEM_REGISTER_INDEXED:
		case OPER_MEM_REGISTER_INDEXED_OFFSET:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

internal void emit_instruction_mdx_x(bit_width width, opcode op, registers base,
							immediate_op offset, operand src)
{
	if (src.kind != OPER_NONE)
	{
		bit_width width = src.width;
		ASSERT(width == B8 || width == B16 || width == B32 || width == B64);
	}
	switch (src.kind)
	{
		case OPER_NONE:
		{
			emit_mdx(width, op, base, offset);
		} break;
		case OPER_IMMEDIATE:
		{
			emit_mdx_ix(width, op, base, offset, src.imm_op);
		} break;
		case OPER_REGISTER:
		{
			emit_mdx_r(width, op, base, offset, src.reg);
		} break;
		case OPER_LABEL:
		case OPER_MEM_LABEL:
		case OPER_DISPLACEMENT:
		case OPER_RIP_DISPLACEMENT:
		case OPER_CC:
		case OPER_MEM_REGISTER:
		case OPER_MEM_REGISTER_OFFSET:
		case OPER_MEM_REGISTER_INDEXED:
		case OPER_MEM_REGISTER_INDEXED_OFFSET:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

internal void emit_instruction_sib_x(bit_width width, opcode op, registers base,
							registers index, log2_scale scale, operand src)
{
	if (src.kind != OPER_NONE)
	{
		bit_width width = src.width;
		ASSERT(width == B8 || width == B16 || width == B32 || width == B64);
	}
	switch (src.kind)
	{
		case OPER_NONE:
		{
			emit_sib(width, op, base, index, scale);
		} break;
		case OPER_IMMEDIATE:
		{
			emit_sib_ix(width, op, base, index, scale, src.imm_op);
		} break;
		case OPER_REGISTER:
		{
			emit_sib_r(width, op, base, index, scale, src.reg);
		} break;
		case OPER_LABEL:
		case OPER_MEM_LABEL:
		case OPER_DISPLACEMENT:
		case OPER_RIP_DISPLACEMENT:
		case OPER_CC:
		case OPER_MEM_REGISTER:
		case OPER_MEM_REGISTER_OFFSET:
		case OPER_MEM_REGISTER_INDEXED:
		case OPER_MEM_REGISTER_INDEXED_OFFSET:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

internal void emit_instruction_sibdx_x(bit_width width, opcode op, registers base, registers index,
						   log2_scale scale, immediate_op offset, operand src)
{
	if (src.kind != OPER_NONE)
	{
		bit_width width = src.width;
		ASSERT(width == B8 || width == B16 || width == B32 || width == B64);
	}
	switch (src.kind)
	{
		case OPER_NONE:
		{
			emit_sibdx(width, op, base, index, scale, offset);
		} break;
		case OPER_IMMEDIATE:
		{
			emit_sibdx_ix(width, op, base, index, scale, offset, src.imm_op);
		} break;
		case OPER_REGISTER:
		{
			emit_sibdx_r(width, op, base, index, scale, offset, src.reg);
		} break;
		case OPER_LABEL:
		case OPER_MEM_LABEL:
		case OPER_DISPLACEMENT:
		case OPER_RIP_DISPLACEMENT:
		case OPER_CC:
		case OPER_MEM_REGISTER:
		case OPER_MEM_REGISTER_OFFSET:
		case OPER_MEM_REGISTER_INDEXED:
		case OPER_MEM_REGISTER_INDEXED_OFFSET:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

internal void emit_instruction_disp_x(bit_width width, opcode op, u32 displacement, operand src)
{
	if (src.kind != OPER_NONE)
	{
		bit_width width = src.width;
		ASSERT(width == B8 || width == B16 || width == B32 || width == B64);
	}
	switch (src.kind)
	{
		case OPER_NONE:
		{
			emit_disp(width, op, displacement);
		} break;
		case OPER_REGISTER:
		{
			emit_disp_r(width, op, displacement, src.reg);
		} break;
		case OPER_IMMEDIATE:
		{
			emit_disp_ix(width, op, displacement, src.imm_op);
		} break;
		case OPER_LABEL:
		case OPER_MEM_LABEL:
		case OPER_DISPLACEMENT:
		case OPER_RIP_DISPLACEMENT:
		case OPER_CC:
		case OPER_MEM_REGISTER:
		case OPER_MEM_REGISTER_OFFSET:
		case OPER_MEM_REGISTER_INDEXED:
		case OPER_MEM_REGISTER_INDEXED_OFFSET:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

internal void emit_rip_disp_x(bit_width width, opcode op, u32 displacement, operand src)
{
	if (src.kind != OPER_NONE)
	{
		bit_width width = src.width;
		ASSERT(width == B8 || width == B16 || width == B32 || width == B64);
	}
	switch (src.kind)
	{
		case OPER_NONE:
		{
			emit_rip_disp(width, op, displacement);
		} break;
		case OPER_REGISTER:
		{
			emit_rip_disp_r(width, op, displacement, src.reg);
		} break;
		case OPER_IMMEDIATE:
		{
			emit_rip_disp_ix(width, op, displacement, src.imm_op);
		} break;
		case OPER_LABEL:
		case OPER_MEM_LABEL:
		case OPER_DISPLACEMENT:
		case OPER_RIP_DISPLACEMENT:
		case OPER_CC:
		case OPER_MEM_REGISTER:
		case OPER_MEM_REGISTER_OFFSET:
		case OPER_MEM_REGISTER_INDEXED:
		case OPER_MEM_REGISTER_INDEXED_OFFSET:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

void emit_instruction(instruction *instr)
{
	instr->code_offset = emit_ptr - code;
	if (instr->op == JMP)
	{
		emit_jmp(instr->code_offset, instr->op1, instr->op2);
	}
	else if (instr->op == CALL)
	{
		ASSERT(instr->op2.kind == OPER_NONE);
		emit_call(instr->code_offset, instr->op1);
	}
	else if (instr->op == MOVSX || instr->op == MOVZX || (instr->op == MOV && instr->op1.kind == OPER_CC))
	{
		emit_escaped(instr);
	}
	else if (instr->op == MOV)
	{
		emit_mov(instr);
	}
	else if (instr->op == INT)
	{
		ASSERT(instr->op2.kind == OPER_NONE);
		ASSERT(instr->op1.kind == OPER_IMMEDIATE);
		ASSERT(instr->op1.imm_op.kind == IB);
		emit_int(instr->op1.imm_op.ib);
	}
	else if (instr->op == NOP)
	{
		emit_byte(0x90);
	}
	else if (instr->op == POP)
	{
		ASSERT(instr->op1.kind != OPER_NONE);
		ASSERT(instr->op2.kind == OPER_NONE);
		ASSERT(instr->op3.kind == OPER_NONE);
		emit_pop(instr->op1);
	}
	else if (instr->op == PUSH)
	{
		ASSERT(instr->op1.kind != OPER_NONE);
		ASSERT(instr->op2.kind == OPER_NONE);
		ASSERT(instr->op3.kind == OPER_NONE);
		emit_push(instr->op1);
	}
	else if (instr->op == RET)
	{
		ASSERT(instr->op2.kind == OPER_NONE);
		ASSERT(instr->op3.kind == OPER_NONE);
		emit_ret(instr->op1);
	}
	else if (instr->op == SAL || instr->op == SAR || instr->op == SHL || instr->op == SHR)
	{
		ASSERT(instr->op3.kind == OPER_NONE);
		emit_shift(instr->op, instr->op1, instr->op2);
	}
	else if (instr->op == SET)
	{
		ASSERT(instr->op1.kind == OPER_CC);
		ASSERT(instr->op2.kind != OPER_NONE);
		ASSERT(instr->op3.kind == OPER_NONE);
		emit_setcc(instr->op1.cc, instr->op2);
	}
	else if (instr->op == SYSCALL)
	{
		emit_byte(0x0F);
		emit_byte(0x05);
	}
	else
	{
		if (instr->op1.kind != OPER_NONE && instr->op1.kind != OPER_CC)
		{
			bit_width width = instr->op1.width;
			ASSERT(width == B8 || width == B16 || width == B32 || width == B64);
		}
		switch (instr->op1.kind)
		{
			case OPER_NONE:
			{
				INVALID_CODE_PATH;
			} break;
			case OPER_IMMEDIATE:
			{
				INVALID_CODE_PATH;
			} break;
			case OPER_DISPLACEMENT:
			{
				emit_instruction_disp_x(instr->op1.width, instr->op, instr->op1.displacement, instr->op2);
			} break;
			case OPER_MEM_LABEL:
			{
				emit_rip_disp_x(instr->op1.width, instr->op, 0, instr->op2);
			} break;
			case OPER_RIP_DISPLACEMENT:
			{
				emit_rip_disp_x(instr->op1.width, instr->op, instr->op1.displacement, instr->op2);
			} break;
			case OPER_CC:
			{
				INVALID_CODE_PATH;
			} break;
			case OPER_REGISTER:
			{
				emit_instruction_r_x(instr->op1.width, instr->op, instr->op1.reg, instr->op2);
			} break;
			case OPER_MEM_REGISTER:
			{
				emit_instruction_m_x(instr->op1.width, instr->op, instr->op1.reg, instr->op2);
			} break;
			case OPER_MEM_REGISTER_OFFSET:
			{
				emit_instruction_mdx_x(instr->op1.width, instr->op, instr->op1.mem_offset.base,
									instr->op1.mem_offset.offset, instr->op2);
			} break;
			case OPER_MEM_REGISTER_INDEXED:
			{
				emit_instruction_sib_x(instr->op1.width, instr->op, instr->op1.mem_indexed.base,
									instr->op1.mem_indexed.index, instr->op1.mem_indexed.scale,
									instr->op2);
			} break;
			case OPER_MEM_REGISTER_INDEXED_OFFSET:
			{
				emit_instruction_sibdx_x(instr->op1.width, instr->op,
										instr->op1.mem_indexed_offset.base,
										instr->op1.mem_indexed_offset.index,
										instr->op1.mem_indexed_offset.scale,
										instr->op1.mem_indexed_offset.offset,
										instr->op2);
			} break;
			case OPER_LABEL:
			{
				INVALID_CODE_PATH;
			} break;
		}
	}
	instr->code_len = (uintptr_t)(emit_ptr - (code + instr->code_offset));
}

void emit_directive(directives *dir)
{
	switch (dir->kind)
	{
		case DIR_GLOBAL:
		{
			// #note do nothing
		} break;
		case DIR_ALIGN:
		{
			dir->align.code_offset = emit_ptr - code;
			u8 align = dir->align.alignment;
			u8 fill_byte = dir->align.fill_byte;
			u8 current_section_offset = (u8)((u64)(emit_ptr - code) & 0xFF);
			while (current_section_offset % align)
			{
				emit_byte(fill_byte);
				++dir->align.bytes_written;
				current_section_offset = (u8)((u64)(emit_ptr - code) & 0xFF);
			}
		} break;
		case DIR_ASCII:
		{
			const char *str = dir->str;
			while (*str)
			{
				emit_byte(*str++);
			}
			emit_byte(0);
		} break;
		case DIR_I8:
		{
			emit_byte(dir->i8);
		} break;
		case DIR_I16:
		{
			emit_word(dir->i16);
		} break;
		case DIR_I32:
		{
			emit_dword(dir->i32);
		} break;
		case DIR_I64:
		{
			emit_qword(dir->i64);
		} break;
		case DIRECTIVE_NONE:
		case NUM_DIRECTIVES:
		case DIR_SECTION:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

void emit_section(section *sec)
{
	max_code_len = sec->max_code_len;
	sec->code = xmalloc(max_code_len);
	emit_ptr = sec->code;
	code = sec->code;

	for (statement *stmt = sec->stmts; stmt != buf_end(sec->stmts); ++stmt)
	{
		switch (stmt->kind)
		{
			case STMT_DIRECTIVE:
			{
				emit_directive(&stmt->dir);
			} break;
			case STMT_LABEL:
			{
				stmt->lab.section_relative_offset = (u64)(emit_ptr - code);
				label *l = section_get_label(sec, stmt->lab.name);
				l->section_relative_offset = (u64)(emit_ptr - code);
			} break;
			case STMT_INSTRUCTION:
			{
				emit_instruction(&stmt->instr);
			} break;
			case STMT_NONE:
			{
				INVALID_CODE_PATH;
			} break;
		}
	}

	sec->code_len = emit_ptr - code;

	max_code_len = 0;
	emit_ptr = NULL;
	code = NULL;
}
