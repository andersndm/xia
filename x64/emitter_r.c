#include "emitter_r.h"

#include "common/macros.h"

// }}}
// -----------------------------------------------------------------------------
//     #section x8 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x8_emit_r(opcode op, registers src)
{
	ASSERT(src.width == B8);
	emit_optional_rex(0x00, src.reg);
	emit_byte(opcodes8[op].r);
	emit_direct(opcodes8[op].ext_m, src.reg);
}

internal void x8_emit_r_ib(opcode op, registers dest, u8 ib)
{
	ASSERT(dest.width == B8);
	if (op == MOV)
	{
		emit_optional_rex(0x00, dest.reg);
		emit_byte(0xB0 + (dest.reg & 0x7));
	}
	else if (dest.reg == RAX && opcodes8[op].rax_ib != 0)
	{
		emit_byte(opcodes8[op].rax_ib);
	}
	else
	{
		emit_optional_rex(0x00, dest.reg);
		emit_byte(opcodes8[op].ib);
		emit_direct(opcodes8[op].ext_i, dest.reg);
	}
	emit_byte(ib);
}

/*
  illegal combinations: ah-bh, r8b-r15b
                        r8b-r15b, ah-bh
 */
// should they pass register width?
internal void x8_emit_r_r(opcode op, registers dest, registers src)
{
	ASSERT(dest.width == B8);
	ASSERT(src.width == B8);
	emit_optional_rex(src.reg, dest.reg);
	emit_byte(opcodes8[op].r);
	emit_direct(src.reg, dest.reg);
}

/*
  it is required to have a 32-bit or 64-bit register as base
 */
internal void x8_emit_r_m(opcode op, registers dest, registers base)
{
	ASSERT(dest.width == B8);
	emit_addressing_mode(base.width);
	emit_optional_rex(dest.reg, base.reg);
	emit_byte(opcodes8[op].m);
	emit_xm(dest.reg, base.reg);
}

internal void x8_emit_r_mdb(opcode op, registers dest, registers base, u8 offset)
{
	ASSERT(dest.width == B8);
	emit_addressing_mode(base.width);
	emit_optional_rex(dest.reg, base.reg);
	emit_byte(opcodes8[op].m);
	emit_xmdb(dest.reg, base.reg, offset);
}

internal void x8_emit_r_mdd(opcode op, registers dest, registers base, u32 offset)
{
	ASSERT(dest.width == B8);
	emit_addressing_mode(base.width);
	emit_optional_rex(dest.reg, base.reg);
	emit_byte(opcodes8[op].m);
	emit_xmdd(dest.reg, base.reg, offset);
}

internal void x8_emit_r_sib(opcode op, registers dest, registers base, registers index, log2_scale scale)
{
	ASSERT(dest.width == B8);
	emit_addressing_mode(base.width);
	ASSERT(index.width == base.width);
	emit_optional_rex_indexed(dest.reg, base.reg, index.reg);
	emit_byte(opcodes8[op].m);
	emit_xsib(dest.reg, base.reg, index.reg, scale);
}

internal void x8_emit_r_sibdb(opcode op, registers dest, registers base, registers index, log2_scale scale,
					 u8 offset)
{
	ASSERT(dest.width == B8);
	emit_addressing_mode(base.width);
	ASSERT(index.width == base.width);
	ASSERT(index.reg != RSP);
	emit_optional_rex_indexed(dest.reg, base.reg, index.reg);
	emit_byte(opcodes8[op].m);
	emit_indirect_indexed_byte_displaced(dest.reg, base.reg, index.reg, scale, offset);
}

internal void x8_emit_r_sibdd(opcode op, registers dest, registers base, registers index, log2_scale scale,
					 u32 offset)
{
	ASSERT(dest.width == B8);
	emit_addressing_mode(base.width);
	ASSERT(index.width == base.width);
	ASSERT(index.reg != RSP);
	emit_optional_rex_indexed(dest.reg, base.reg, index.reg);
	emit_byte(opcodes8[op].m);
	emit_indirect_indexed_displaced(dest.reg, base.reg, index.reg, scale, offset);
}

internal void x8_emit_r_disp(opcode op, registers dest, u32 displacement)
{
	ASSERT(dest.width == B8);
	emit_optional_rex(dest.reg, 0x00);
	emit_byte(opcodes8[op].m);
	emit_displaced(dest.reg, displacement);
}

internal void x8_emit_r_rip_disp(opcode op, registers dest, u32 displacement)
{
	ASSERT(dest.width == B8);
	emit_optional_rex(dest.reg, 0x00);
	emit_byte(opcodes8[op].m);
	// #note instruction length
	displacement -= 6;
	if (dest.reg >= R8)
	{
		displacement -= 1;
	}
	emit_indirect_displaced_rip(dest.reg, displacement);
}

// }}}
// -----------------------------------------------------------------------------
//     #section x16 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x16_emit_r(opcode op, registers src)
{
	ASSERT(src.width == B16);
	emit_x16_prefix();
	emit_optional_rex(0x00, src.reg);
	emit_byte(opcodesX[op].r);
	emit_direct(opcodesX[op].ext_m, src.reg);
}

internal void x16_emit_r_ib(opcode op, registers dest, u8 ib)
{
	ASSERT(dest.width == B16);
	emit_x16_prefix();
	emit_optional_rex(0x00, dest.reg);
	emit_byte(opcodesX[op].ib);
	emit_direct(opcodes8[op].ext_i, dest.reg);
	emit_byte(ib);
}

internal void x16_emit_r_iw(opcode op, registers dest, u16 iw)
{
	ASSERT(dest.width == B16);
	emit_x16_prefix();
	if (op == MOV)
	{
		emit_optional_rex(0x00, dest.reg);
		emit_byte(0xB8 + (dest.reg & 0x7));
	}
	else if (dest.reg == RAX && opcodesX[op].rax_ix != 0)
	{
		emit_byte(opcodesX[op].rax_ix);
	}
	else
	{
		emit_optional_rex(0x00, dest.reg);
		emit_byte(opcodesX[op].ix);
		emit_direct(opcodes8[op].ext_i, dest.reg);
	}
	emit_word(iw);
}

internal void x16_emit_r_r(opcode op, registers dest, registers src)
{
	emit_x16_prefix();
	emit_optional_rex(src.reg, dest.reg);
	if (op == XCHG && dest.reg == AX)
	{
		emit_byte(0x90 + (src.reg & 0x7));
	}
	else if (op == XCHG && src.reg == AX)
	{
		emit_byte(0x90 + (dest.reg & 0x7));
	}
	else
	{
		emit_byte(opcodesX[op].r);
		emit_direct(src.reg, dest.reg);
	}
}

internal void x16_emit_r_m(opcode op, registers dest, registers base)
{
	emit_x16_prefix();
	ASSERT(dest.width == B16);
	emit_addressing_mode(base.width);
	emit_optional_rex(dest.reg, base.reg);
	emit_byte(opcodesX[op].m);
	emit_xm(dest.reg, base.reg);
}

internal void x16_emit_r_mdb(opcode op, registers dest, registers base, u8 offset)
{
	emit_x16_prefix();
	ASSERT(dest.width == B16);
	emit_addressing_mode(base.width);
	emit_optional_rex(dest.reg, base.reg);
	emit_byte(opcodesX[op].m);
	emit_xmdb(dest.reg, base.reg, offset);
}

internal void x16_emit_r_mdd(opcode op, registers dest, registers base, u32 offset)
{
	emit_x16_prefix();
	ASSERT(dest.width == B16);
	emit_addressing_mode(base.width);
	emit_optional_rex(dest.reg, base.reg);
	emit_byte(opcodesX[op].m);
	emit_xmdd(dest.reg, base.reg, offset);
}

internal void x16_emit_r_sib(opcode op, registers dest, registers base, registers index, log2_scale scale)
{
	emit_x16_prefix();
	ASSERT(dest.width == B16);
	emit_addressing_mode(base.width);
	ASSERT(index.width == base.width);
	emit_optional_rex_indexed(dest.reg, base.reg, index.reg);
	emit_byte(opcodesX[op].m);
	emit_xsib(dest.reg, base.reg, index.reg, scale);
}

internal void x16_emit_r_sibdb(opcode op, registers dest, registers base, registers index, log2_scale scale,
					 u8 offset)
{
	emit_x16_prefix();
	ASSERT(dest.width == B16);
	emit_addressing_mode(base.width);
	ASSERT(index.width == base.width);
	emit_optional_rex_indexed(dest.reg, base.reg, index.reg);
	emit_byte(opcodesX[op].m);
	emit_indirect_indexed_byte_displaced(dest.reg, base.reg, index.reg, scale, offset);
}

internal void x16_emit_r_sibdd(opcode op, registers dest, registers base, registers index, log2_scale scale,
					 u32 offset)
{
	emit_x16_prefix();
	ASSERT(dest.width == B16);
	emit_addressing_mode(base.width);
	ASSERT(index.width == base.width);
	emit_optional_rex_indexed(dest.reg, base.reg, index.reg);
	emit_byte(opcodesX[op].m);
	emit_indirect_indexed_displaced(dest.reg, base.reg, index.reg, scale, offset);
}

internal void x16_emit_r_disp(opcode op, registers dest, u32 displacement)
{
	ASSERT(dest.width == B16);
	emit_x16_prefix();
	emit_optional_rex(dest.reg, 0x00);
	emit_byte(opcodesX[op].m);
	emit_displaced(dest.reg, displacement);
}

internal void x16_emit_r_rip_disp(opcode op, registers dest, u32 displacement)
{
	ASSERT(dest.width == B16);
	emit_x16_prefix();
	emit_optional_rex(dest.reg, 0x00);
	emit_byte(opcodesX[op].m);
	// #note instruction length
	displacement -= 7;
	if (dest.reg >= R8)
	{
		displacement -= 1;
	}
	emit_indirect_displaced_rip(dest.reg, displacement);
}

// }}}
// -----------------------------------------------------------------------------
//     #section x32 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x32_emit_r(opcode op, registers src)
{
	ASSERT(src.width == B32);
	emit_optional_rex(0x00, src.reg);
	emit_byte(opcodesX[op].r);
	emit_direct(opcodesX[op].ext_m, src.reg);
}

internal void x32_emit_r_ib(opcode op, registers dest, u8 ib)
{
	ASSERT(dest.width == B32);
	emit_optional_rex(0x00, dest.reg);
	emit_byte(opcodesX[op].ib);
	emit_direct(opcodesX[op].ext_i, dest.reg);
	emit_byte(ib);
}

internal void x32_emit_r_id(opcode op, registers dest, u32 id)
{
	ASSERT(dest.width == B32);
	if (op == MOV)
	{
		emit_optional_rex(0x00, dest.reg);
		emit_byte(0xB8 + (dest.reg & 0x7));
	}
	else if (dest.reg == RAX && opcodesX[op].rax_ix != 0)
	{
		emit_byte(opcodesX[op].rax_ix);
	}
	else
	{
		emit_optional_rex(0x00, dest.reg);
		emit_byte(opcodesX[op].ix);
		emit_direct(opcodes8[op].ext_i, dest.reg);
	}
	emit_dword(id);
}

internal void x32_emit_r_r(opcode op, registers dest, registers src)
{
	ASSERT(dest.width == B32);
	emit_optional_rex(src.reg, dest.reg);
	if (op == XCHG && dest.reg == EAX)
	{
		emit_byte(0x90 + (src.reg & 0x7));
	}
	else if (op == XCHG && src.reg == EAX)
	{
		emit_byte(0x90 + (dest.reg & 0x7));
	}
	else
	{
		emit_byte(opcodesX[op].r);
		emit_direct(src.reg, dest.reg);
	}
}

internal void x32_emit_r_m(opcode op, registers dest, registers base)
{
	ASSERT(dest.width == B32);
	emit_addressing_mode(base.width);
	emit_byte(opcodesX[op].m);
	emit_xm(dest.reg, base.reg);
}

internal void x32_emit_r_mdb(opcode op, registers dest, registers base, u8 offset)
{
	ASSERT(dest.width == B32);
	emit_addressing_mode(base.width);
	emit_optional_rex(dest.reg, base.reg);
	emit_byte(opcodesX[op].m);
	emit_xmdb(dest.reg, base.reg, offset);
}

internal void x32_emit_r_mdd(opcode op, registers dest, registers base, u32 offset)
{
	ASSERT(dest.width == B32);
	emit_addressing_mode(base.width);
	emit_optional_rex(dest.reg, base.reg);
	emit_byte(opcodesX[op].m);
	emit_xmdd(dest.reg, base.reg, offset);
}

internal void x32_emit_r_sib(opcode op, registers dest, registers base, registers index, log2_scale scale)
{
	ASSERT(dest.width == B32);
	emit_addressing_mode(base.width);
	ASSERT(index.width == base.width);
	emit_optional_rex_indexed(dest.reg, base.reg, index.reg);
	emit_byte(opcodesX[op].m);
	emit_xsib(dest.reg, base.reg, index.reg, scale);
}

internal void x32_emit_r_sibdb(opcode op, registers dest, registers base, registers index, log2_scale scale,
					 u8 offset)
{
	ASSERT(dest.width == B32);
	emit_addressing_mode(base.width);
	ASSERT(index.width == base.width);
	emit_optional_rex_indexed(dest.reg, base.reg, index.reg);
	emit_byte(opcodesX[op].m);
	emit_indirect_indexed_byte_displaced(dest.reg, base.reg, index.reg, scale, offset);
}

internal void x32_emit_r_sibdd(opcode op, registers dest, registers base, registers index, log2_scale scale,
					 u32 offset)
{
	ASSERT(dest.width == B32);
	emit_addressing_mode(base.width);
	ASSERT(index.width == base.width);
	emit_optional_rex_indexed(dest.reg, base.reg, index.reg);
	emit_byte(opcodesX[op].m);
	emit_indirect_indexed_displaced(dest.reg, base.reg, index.reg, scale, offset);
}

internal void x32_emit_r_disp(opcode op, registers dest, u32 displacement)
{
	ASSERT(dest.width == B32);
	emit_optional_rex(dest.reg, 0x00);
	emit_byte(opcodesX[op].m);
	emit_displaced(dest.reg, displacement);
}

internal void x32_emit_r_rip_disp(opcode op, registers dest, u32 displacement)
{
	ASSERT(dest.width == B32);
	emit_optional_rex(dest.reg, 0x00);
	emit_byte(opcodesX[op].m);
	// #note instruction length
	displacement -= 6;
	if (dest.reg >= R8)
	{
		displacement -= 1;
	}
	emit_indirect_displaced_rip(dest.reg, displacement);
}

// }}}
// -----------------------------------------------------------------------------
//     #section x64 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x64_emit_r(opcode op, registers src)
{
	ASSERT(src.width == B64);
	emit_rex(0x00, src.reg);
	emit_byte(opcodesX[op].r);
	emit_direct(opcodesX[op].ext_m, src.reg);
}

internal void x64_emit_r_ib(opcode op, registers dest, u8 ib)
{
	ASSERT(dest.width == B64);
	emit_rex(0x00, dest.reg);
	emit_byte(opcodesX[op].ib);
	emit_direct(opcodesX[op].ext_i, dest.reg);
	emit_byte(ib);
}

internal void x64_emit_r_id(opcode op, registers dest, u32 id)
{
	ASSERT(dest.width == B64);
	emit_rex(0x00, dest.reg);
	if (dest.reg == RAX && opcodesX[op].rax_ix != 0)
	{
		emit_byte(opcodesX[op].rax_ix);
	}
	else
	{
		emit_byte(opcodesX[op].ix);
		emit_direct(opcodes8[op].ext_i, dest.reg);
	}
	emit_dword(id);
}

internal void x64_emit_r_iq(opcode op, registers dest, u64 iq)
{
	ASSERT(dest.width == B64);
	emit_rex(0x00, dest.reg);
	emit_byte(0xB8 + (dest.reg & 0x7));
	emit_qword(iq);
}

internal void x64_emit_r_r(opcode op, registers dest, registers src)
{
	emit_rex(src.reg, dest.reg);
	if (op == XCHG && dest.reg == RAX)
	{
		emit_byte(0x90 + (src.reg & 0x7));
	}
	else if (op == XCHG && src.reg == RAX)
	{
		emit_byte(0x90 + (dest.reg & 0x7));
	}
	else
	{

		emit_byte(opcodesX[op].r);
		emit_direct(src.reg, dest.reg);
	}
}

internal void x64_emit_r_m(opcode op, registers dest, registers base)
{
	ASSERT(dest.width == B64);
	emit_addressing_mode(base.width);
	emit_rex(dest.reg, base.reg);
	emit_byte(opcodesX[op].m);
	emit_xm(dest.reg, base.reg);
}

internal void x64_emit_r_mdb(opcode op, registers dest, registers base, u8 offset)
{
	ASSERT(dest.width == B64);
	emit_addressing_mode(base.width);
	emit_xmdb(dest.reg, base.reg, offset);
}

internal void x64_emit_r_mdd(opcode op, registers dest, registers base, u32 offset)
{
	ASSERT(dest.width == B64);
	emit_addressing_mode(base.width);
	emit_rex(dest.reg, base.reg);
	emit_byte(opcodesX[op].m);
	emit_xmdd(dest.reg, base.reg, offset);
}

internal void x64_emit_r_sib(opcode op, registers dest, registers base, registers index, log2_scale scale)
{
	ASSERT(dest.width == B64);
	emit_addressing_mode(base.width);
	ASSERT(index.width == base.width);
	emit_rex_indexed(dest.reg, base.reg, index.reg);
	emit_byte(opcodesX[op].m);
	emit_xsib(dest.reg, base.reg, index.reg, scale);
}

internal void x64_emit_r_sibdb(opcode op, registers dest, registers base, registers index, log2_scale scale,
					 u8 offset)
{
	ASSERT(dest.width == B64);
	emit_addressing_mode(base.width);
	ASSERT(index.width == base.width);
	emit_rex_indexed(dest.reg, base.reg, index.reg);
	emit_byte(opcodesX[op].m);
	emit_indirect_indexed_byte_displaced(dest.reg, base.reg, index.reg, scale, offset);
}

internal void x64_emit_r_sibdd(opcode op, registers dest, registers base, registers index, log2_scale scale,
					 u32 offset)
{
	ASSERT(dest.width == B64);
	emit_addressing_mode(base.width);
	ASSERT(index.width == base.width);
	emit_rex_indexed(dest.reg, base.reg, index.reg);
	emit_byte(opcodesX[op].m);
	emit_indirect_indexed_displaced(dest.reg, base.reg, index.reg, scale, offset);
}

internal void x64_emit_r_disp(opcode op, registers dest, u32 displacement)
{
	ASSERT(dest.width == B64);
	emit_rex(dest.reg, 0x00);
	emit_byte(opcodesX[op].m);
	emit_displaced(dest.reg, displacement);
}

internal void x64_emit_r_rip_disp(opcode op, registers dest, u32 displacement)
{
	ASSERT(dest.width == B64);
	emit_rex(dest.reg, 0x00);
	emit_byte(opcodesX[op].m);
	// #note instruction length
	displacement -= 7;
	emit_indirect_displaced_rip(dest.reg, displacement);
}

// }}}
// -----------------------------------------------------------------------------
//     #section width dispatching
// -----------------------------------------------------------------------------
// {{{

void emit_r(bit_width width, opcode op, registers src)
{
	switch (width)
	{
		case B8:
		{
			x8_emit_r(op, src);
		} break;
		case B16:
		{
			x16_emit_r(op, src);
		} break;
		case B32:
		{
			x32_emit_r(op, src);
		} break;
		case B64:
		{
			x64_emit_r(op, src);
		} break;
		case BNONE: { INVALID_CODE_PATH; } break;
	}
}

void emit_r_ix(bit_width width, opcode op, registers dest, immediate_op imm_op)
{
	switch (width)
	{
		case B8:
		{
			ASSERT(imm_op.kind == IB);
			x8_emit_r_ib(op, dest, imm_op.ib);
		} break;
		case B16:
		{
			if (imm_op.kind == IB)
			{
				x16_emit_r_ib(op, dest, imm_op.ib);
			}
			else
			{
				ASSERT(imm_op.kind == IW);
				x16_emit_r_iw(op, dest, imm_op.iw);
			}
		} break;
		case B32:
		{
			// #todo may be cases where x32 can take a 16-bit immediate
			if (imm_op.kind == IB)
			{
				x32_emit_r_ib(op, dest, imm_op.ib);
			}
			else
			{
				ASSERT(imm_op.kind == ID);
				x32_emit_r_id(op, dest, imm_op.id);
			}
		} break;
		case B64:
		{
			// #todo may be cases where x64 can take a 16-bit immediate
			// #todo rare cases for 64-bit immediate
			if (imm_op.kind == IB)
			{
				x64_emit_r_ib(op, dest, imm_op.ib);
			}
			else if (imm_op.kind == ID)
			{
				x64_emit_r_id(op, dest, imm_op.id);
			}
			else
			{
				ASSERT(imm_op.kind == IQ && op == MOV);
				x64_emit_r_iq(op, dest, imm_op.iq);
			}
		} break;
		case BNONE:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

void emit_r_r(bit_width width, opcode op, registers dest, registers src)
{
	switch (width)
	{
		case B8:
		{
			x8_emit_r_r(op, dest, src);
		} break;
		case B16:
		{
			x16_emit_r_r(op, dest, src);
		} break;
		case B32:
		{
			x32_emit_r_r(op, dest, src);
		} break;
		case B64:
		{
			x64_emit_r_r(op, dest, src);
		} break;
		case BNONE: { INVALID_CODE_PATH; } break;
	}
}

void emit_r_m(bit_width width, opcode op, registers dest, registers base)
{
	switch (width)
	{
		case B8:
		{
			x8_emit_r_m(op, dest, base);
		} break;
		case B16:
		{
			x16_emit_r_m(op, dest, base);
		} break;
		case B32:
		{
			x32_emit_r_m(op, dest, base);
		} break;
		case B64:
		{
			x64_emit_r_m(op, dest, base);
		} break;
		case BNONE: { INVALID_CODE_PATH; } break;
	}
}

void emit_r_mdb(bit_width width, opcode op, registers dest, registers base, u8 offset)
{
	switch (width)
	{
		case B8:
		{
			x8_emit_r_mdb(op, dest, base, offset);
		} break;
		case B16:
		{
			x16_emit_r_mdb(op, dest, base, offset);
		} break;
		case B32:
		{
			x32_emit_r_mdb(op, dest, base, offset);
		} break;
		case B64:
		{
			x64_emit_r_mdb(op, dest, base, offset);
		} break;
		case BNONE: { INVALID_CODE_PATH; } break;
	}
}

void emit_r_mdd(bit_width width, opcode op, registers dest, registers base, u32 offset)
{
	switch (width)
	{
		case B8:
		{
			x8_emit_r_mdd(op, dest, base, offset);
		} break;
		case B16:
		{
			x16_emit_r_mdd(op, dest, base, offset);
		} break;
		case B32:
		{
			x32_emit_r_mdd(op, dest, base, offset);
		} break;
		case B64:
		{
			x64_emit_r_mdd(op, dest, base, offset);
		} break;
		case BNONE: { INVALID_CODE_PATH; } break;
	}
}

void emit_r_sib(bit_width width, opcode op, registers dest, registers base, registers index,
				log2_scale scale)
{
	switch (width)
	{
		case B8:
		{
			x8_emit_r_sib(op, dest, base, index, scale);
		} break;
		case B16:
		{
			x16_emit_r_sib(op, dest, base, index, scale);
		} break;
		case B32:
		{
			x32_emit_r_sib(op, dest, base, index, scale);
		} break;
		case B64:
		{
			x64_emit_r_sib(op, dest, base, index, scale);
		} break;
		case BNONE: { INVALID_CODE_PATH; } break;
	}
}

void emit_r_sibdb(bit_width width, opcode op, registers dest, registers base, registers index,
				  log2_scale scale, u8 offset)
{
	switch (width)
	{
		case B8:
		{
			x8_emit_r_sibdb(op, dest, base, index, scale, offset);
		} break;
		case B16:
		{
			x16_emit_r_sibdb(op, dest, base, index, scale, offset);
		} break;
		case B32:
		{
			x32_emit_r_sibdb(op, dest, base, index, scale, offset);
		} break;
		case B64:
		{
			x64_emit_r_sibdb(op, dest, base, index, scale, offset);
		} break;
		case BNONE: { INVALID_CODE_PATH; } break;
	}
}

void emit_r_sibdd(bit_width width, opcode op, registers dest, registers base, registers index,
				  log2_scale scale, u32 offset)
{
	switch (width)
	{
		case B8:
		{
			x8_emit_r_sibdd(op, dest, base, index, scale, offset);
		} break;
		case B16:
		{
			x16_emit_r_sibdd(op, dest, base, index, scale, offset);
		} break;
		case B32:
		{
			x32_emit_r_sibdd(op, dest, base, index, scale, offset);
		} break;
		case B64:
		{
			x64_emit_r_sibdd(op, dest, base, index, scale, offset);
		} break;
		case BNONE: { INVALID_CODE_PATH; } break;
	}
}

void emit_r_rip_disp(bit_width width, opcode op, registers dest, u32 displacement)
{
	switch (width)
	{
		case B8:
		{
			x8_emit_r_rip_disp(op, dest, displacement);
		} break;
		case B16:
		{
			x16_emit_r_rip_disp(op, dest, displacement);
		} break;
		case B32:
		{
			x32_emit_r_rip_disp(op, dest, displacement);
		} break;
		case B64:
		{
			x64_emit_r_rip_disp(op, dest, displacement);
		} break;
		case BNONE: { INVALID_CODE_PATH; } break;
	}
}

void emit_r_disp(bit_width width, opcode op, registers dest, u32 displacement)
{
	switch (width)
	{
		case B8:
		{
			x8_emit_r_disp(op, dest, displacement);
		} break;
		case B16:
		{
			x16_emit_r_disp(op, dest, displacement);
		} break;
		case B32:
		{
			x32_emit_r_disp(op, dest, displacement);
		} break;
		case B64:
		{
			x64_emit_r_disp(op, dest, displacement);
		} break;
		case BNONE: { INVALID_CODE_PATH; } break;
	}
}

// }}}
