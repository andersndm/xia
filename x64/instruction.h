#ifndef IA_INSTRUCTION_H
#define IA_INSTRUCTION_H

#include "common/types.h"
#include "registers.h"
#include "opcodes.h"

#include "xia/tokens.h"

typedef enum log2_scale
{
	X1 = 0,
	X2 = 1,
	X4 = 2,
	X8 = 3
} log2_scale;

typedef enum immediate_kind
{
	IB,
	IW,
	ID,
	IQ
} immediate_kind;

typedef enum operand_kind
{
	OPER_NONE,
	OPER_IMMEDIATE,
	OPER_DISPLACEMENT,
	OPER_RIP_DISPLACEMENT,
	OPER_CC,
	OPER_REGISTER,
	OPER_MEM_REGISTER,
	OPER_MEM_LABEL,
	OPER_MEM_REGISTER_OFFSET,
	OPER_MEM_REGISTER_INDEXED,
	OPER_MEM_REGISTER_INDEXED_OFFSET,
	OPER_LABEL,
} operand_kind;

typedef struct immediate_op
{
	immediate_kind kind;
	bool negative;
	union
	{
		u8 ib;
		u16 iw;
		u32 id;
		u64 iq;
	};
	pos_range range;
} immediate_op;

typedef struct operand
{
	operand_kind kind;
	bit_width width;
	pos_range range;
	union
	{
		registers reg;
		condition_codes cc;
		immediate_op imm_op;
		u32 displacement;
		const char *label;
		struct
		{
			registers base;
			immediate_op offset;
		} mem_offset;
		struct
		{
			registers base;
			registers index;
			log2_scale scale;
		} mem_indexed;
		struct
		{
			registers base;
			registers index;
			log2_scale scale;
			immediate_op offset;
		} mem_indexed_offset;
	};
} operand;

typedef struct instruction
{
	// #todo it does not seem like this width is being set or used
	bit_width width;
	opcode op;
	pos_range op_range;
	operand op1;
	operand op2;
	operand op3;
	u64 code_offset;
	u64 code_len;
} instruction;

immediate_op promote_immediate(immediate_op, immediate_kind kind);

#endif // !IA_INSTRUCTION_H
