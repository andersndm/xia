#include "opcodes.h"

const char *op_names[NUM_OPCODES] =
{
	[ADC] = "adc",
	[ADD] = "add",
	[AND] = "and",
	[CALL] = "call",
	[CMP] = "cmp",
	[DEC] = "dec",
	[DIV] = "div",
	[IDIV] = "idiv",
	[IMUL] = "imul",
	[INC] = "inc",
	[INT] = "int",
	[JMP] = "jmp",

	[JO] = "jo",
	[JNO] = "jno",
	[JB] = "jb",
	[JNB] = "jnb",
	[JE] = "je",
	[JNE] = "jne",
	[JNA] = "jna",
	[JA] = "ja",
	[JS] = "js",
	[JNS] = "jns",
	[JP] = "jp",
	[JNP] = "jnp",
	[JL] = "jl",
	[JNL] = "jnl",
	[JNG] = "jng",
	[JG] = "jg",
	[JNAE] = "jnae",
	[JC] = "jc",
	[JAE] = "jae",
	[JNC] = "jnc",
	[JZ] = "jz",
	[JNZ] = "jnz",
	[JBE] = "jbe",
	[JNBE] = "jnbe",
	[JPE] = "jpe",
	[JPO] = "jpo",
	[JNGE] = "jnge",
	[JGE] = "jge",
	[JLE] = "jle",
	[JNLE] = "jnle",

	[LEA] = "lea",
	[MOV] = "mov",

	[CMOVO] = "cmovo",
	[CMOVNO] = "cmovno",
	[CMOVB] = "cmovb",
	[CMOVNB] = "cmovnb",
	[CMOVE] = "cmove",
	[CMOVNE] = "cmovne",
	[CMOVNA] = "cmovna",
	[CMOVA] = "cmova",
	[CMOVS] = "cmovs",
	[CMOVNS] = "cmovns",
	[CMOVP] = "cmovp",
	[CMOVNP] = "cmovnp",
	[CMOVL] = "cmovl",
	[CMOVNL] = "cmovnl",
	[CMOVNG] = "cmovng",
	[CMOVG] = "cmovg",
	[CMOVNAE] = "cmovnae",
	[CMOVC] = "cmovc",
	[CMOVAE] = "cmovae",
	[CMOVNC] = "cmovnc",
	[CMOVZ] = "cmovz",
	[CMOVNZ] = "cmovnz",
	[CMOVBE] = "cmovbe",
	[CMOVNBE] = "cmovnbe",
	[CMOVPE] = "cmovpe",
	[CMOVPO] = "cmovpo",
	[CMOVNGE] = "cmovnge",
	[CMOVGE] = "cmovge",
	[CMOVLE] = "cmovle",
	[CMOVNLE] = "cmovnle",

	[MOVSX] = "movsx",
	[MOVZX] = "movzx",
	[MUL] = "mul",
	[NEG] = "neg",
	[NOP] = "nop",
	[NOT] = "not",
	[OR] = "or",
	[POP] = "pop",
	[PUSH] = "push",
	[RET] = "ret",
	[SAL] = "sal",
	[SAR] = "sar",
	[SBB] = "sbb",

	// #todo/#note this op should never actually be used
	[SET] = "set",

	[SETO]    = "seto",
	[SETNO]   = "setno",
	[SETB]    = "setb",
	[SETC]    = "setc",
	[SETNAE]  = "setnae",
	[SETAE]   = "setae",
	[SETNB]   = "setnb",
	[SETNC]   = "setnc",
	[SETE]    = "sete",
	[SETZ]    = "setz",
	[SETNZ]   = "setnz",
	[SETNE]   = "setne",
	[SETBE]   = "setbe",
	[SETNA]   = "setna",
	[SETA]    = "seta",
	[SETNBE]  = "setnbe",
	[SETS]    = "sets",
	[SETNS]   = "setns",
	[SETP]    = "setp",
	[SETPE]   = "setpe",
	[SETPO]   = "setpo",
	[SETNP]   = "setnp",
	[SETL]    = "setl",
	[SETNGE]  = "setnge",
	[SETGE]   = "setge",
	[SETNL]   = "setnl",
	[SETLE]   = "setle",
	[SETNG]   = "setng",
	[SETG]    = "setg",
	[SETNLE]  = "setnle",

	[SHL] = "shl",
	[SHR] = "shr",
	[SUB] = "sub",
	[SYSCALL] = "syscall",
	[TEST] = "test",
	[XCHG] = "xchg",
	[XOR] = "xor",
};

condition_codes op_to_cc[NUM_OPCODES] =
{
	[JO]   = O,
	[JNO]  = NO,
	[JB]   = B,
	[JC]   = C,
	[JNAE] = NAE,
	[JAE]  = AE,
	[JNB]  = NB,
	[JNC]  = NC,
	[JE]   = E,
	[JZ]   = Z,
	[JNE]  = NE,
	[JNZ]  = NZ,
	[JBE]  = BE,
	[JNA]  = NA,
	[JA]   = A,
	[JNBE] = NBE,
	[JS]   = S,
	[JNS]  = NS,
	[JP]   = P,
	[JPE]  = PE,
	[JPO]  = PO,
	[JNP]  = NP,
	[JL]   = L,
	[JNGE] = NGE,
	[JGE]  = GE,
	[JNL]  = NL,
	[JLE]  = LE,
	[JNG]  = NG,
	[JG]   = G,
	[JNLE] = NLE,

	[CMOVO]   = O,
	[CMOVNO]  = NO,
	[CMOVB]   = B,
	[CMOVC]   = C,
	[CMOVNAE] = NAE,
	[CMOVAE]  = AE,
	[CMOVNB]  = NB,
	[CMOVNC]  = NC,
	[CMOVE]   = E,
	[CMOVZ]   = Z,
	[CMOVNE]  = NE,
	[CMOVNZ]  = NZ,
	[CMOVBE]  = BE,
	[CMOVNA]  = NA,
	[CMOVA]   = A,
	[CMOVNBE] = NBE,
	[CMOVS]   = S,
	[CMOVNS]  = NS,
	[CMOVP]   = P,
	[CMOVPE]  = PE,
	[CMOVPO]  = PO,
	[CMOVNP]  = NP,
	[CMOVL]   = L,
	[CMOVNGE] = NGE,
	[CMOVGE]  = GE,
	[CMOVNL]  = NL,
	[CMOVLE]  = LE,
	[CMOVNG]  = NG,
	[CMOVG]   = G,
	[CMOVNLE] = NLE,

	[SETO]    = O,
	[SETNO]   = NO,
	[SETB]    = B,
	[SETC]    = C,
	[SETNAE]  = NAE,
	[SETAE]   = AE,
	[SETNB]   = NB,
	[SETNC]   = NC,
	[SETE]    = E,
	[SETZ]    = Z,
	[SETNZ]   = NZ,
	[SETNE]   = NE,
	[SETBE]   = BE,
	[SETNA]   = NA,
	[SETA]    = A,
	[SETNBE]  = NBE,
	[SETS]    = S,
	[SETNS]   = NS,
	[SETP]    = P,
	[SETPE]   = PE,
	[SETPO]   = PO,
	[SETNP]   = NP,
	[SETL]    = L,
	[SETNGE]  = NGE,
	[SETGE]   = GE,
	[SETNL]   = NL,
	[SETLE]   = LE,
	[SETNG]   = NG,
	[SETG]    = G,
	[SETNLE]  = NLE,
};

#define OP8(op, r, m, ext_m, rax_ib, ib, ext_i) [op] = { r, m, ext_m, rax_ib, ib, ext_i },

struct op8_data opcodes8[NUM_OPCODES] =
{
	//  op    r     m     ext_m  rax_ib ib    ext_i
	OP8(ADC,  0x10, 0x12, 0x00,  0x14,  0x80, 0x02)
	OP8(ADD,  0x00, 0x02, 0x00,  0x04,  0x80, 0x00)
	OP8(AND,  0x20, 0x22, 0x00,  0x24,  0x80, 0x04)
	OP8(CMP,  0x38, 0x3A, 0x00,  0x3C,  0x80, 0x07)
	OP8(DEC,  0xFE, 0xFE, 0x01,  0x00,  0x00, 0x00)
	OP8(DIV,  0xF6, 0xF6, 0x06,  0x00,  0x00, 0x00)
	OP8(IDIV, 0xF6, 0xF6, 0x07,  0x00,  0x00, 0x00)
	OP8(IMUL, 0xF6, 0xF6, 0x05,  0x00,  0x00, 0x00)
	OP8(INC,  0xFE, 0xFE, 0x00,  0x00,  0x00, 0x00)
	OP8(MOV,  0x88, 0x8A, 0x00,  0x00,  0xC6, 0x00)
	OP8(MUL,  0xF6, 0xF6, 0x04,  0x00,  0x00, 0x00)
	OP8(NEG,  0xF6, 0xF6, 0x03,  0x00,  0x00, 0x00)
	OP8(NOT,  0xF6, 0xF6, 0x02,  0x00,  0x00, 0x00)
	OP8(OR,   0x08, 0x0A, 0x00,  0x0C,  0x80, 0x01)
	OP8(SBB,  0x18, 0x1A, 0x00,  0x1C,  0x80, 0x03)
	OP8(SUB,  0x28, 0x2A, 0x00,  0x2C,  0x80, 0x05)
	OP8(TEST, 0x84, 0x00, 0x00,  0xA8,  0xF6, 0x00)
	OP8(XCHG, 0x86, 0x86, 0x00,  0x00,  0x00, 0x00)
	OP8(XOR,  0x30, 0x32, 0x00,  0x34,  0x80, 0x06)
};

#undef OP8

#define OPX(op, r, m, ext_m, rax_ix, ib, ix, ext_i) [op] = { r, m, ext_m, rax_ix, ib, ix, ext_i },

struct opX_data opcodesX[NUM_OPCODES] =
{
	//  op    r     m     ext_m  rax_ix ib    ix    ext_i
	OPX(ADC,  0x11, 0x13, 0x00,  0x15,  0x83, 0x81, 0x02)
	OPX(ADD,  0x01, 0x03, 0x00,  0x05,  0x83, 0x81, 0x00)
	OPX(AND,  0x21, 0x23, 0x00,  0x25,  0x83, 0x81, 0x04)
	OPX(CMP,  0x39, 0x3B, 0x00,  0x3D,  0x83, 0x81, 0x07)
	OPX(DEC,  0xFF, 0xFF, 0x01,  0x00,  0x00, 0x00, 0x00)
	OPX(DIV,  0xF7, 0xF7, 0x06,  0x00,  0x00, 0x00, 0x00)
	OPX(IDIV, 0xF7, 0xF7, 0x07,  0x00,  0x00, 0x00, 0x00)
	OPX(IMUL, 0xF7, 0xF7, 0x05,  0x00,  0x6B, 0x69, 0x00) // #note ib and ix are for imul r, rm, ib/ix
	OPX(INC,  0xFF, 0xFF, 0x00,  0x00,  0x00, 0x00, 0x00)
	OPX(LEA,  0x00, 0x8D, 0x00,  0x00,  0x00, 0x00, 0x00)
	OPX(MOV,  0x89, 0x8B, 0x00,  0x00,  0x00, 0xC7, 0x00)
	OPX(MUL,  0xF7, 0xF7, 0x04,  0x00,  0x00, 0x00, 0x00)
	OPX(NEG,  0xF7, 0xF7, 0x03,  0x00,  0x00, 0x00, 0x00)
	OPX(NOT,  0xF7, 0xF7, 0x02,  0x00,  0x00, 0x00, 0x00)
	OPX(OR,   0x09, 0x0B, 0x00,  0x0D,  0x83, 0x81, 0x01)
	OPX(POP,  0x58, 0x8F, 0x00,  0x00,  0x00, 0x00, 0x00)
	OPX(SBB,  0x19, 0x1B, 0x00,  0x1D,  0x83, 0x81, 0x03)
	OPX(SUB,  0x29, 0x2B, 0x00,  0x2D,  0x83, 0x81, 0x05)
	OPX(TEST, 0x85, 0x00, 0x00,  0xA9,  0x00, 0xF7, 0x00)
	OPX(XCHG, 0x87, 0x87, 0x00,  0x00,  0x00, 0x00, 0x00)
	OPX(XOR,  0x31, 0x33, 0x00,  0x35,  0x83, 0x81, 0x06)
};

#undef OPX
