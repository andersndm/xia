#ifndef IA_PRINT_DATA_H
#define IA_PRINT_DATA_H

#include "instruction.h"
#include "xia/compilation_unit.h"

// #todo should this be moved to xia instead of xia/x64?

void print_instruction(instruction instr, label *labels);
void print_instruction_bytes(instruction instr, section *sec, u8 width);
void print_code(void);
void print_section(section *sec);

#endif // !IA_PRINT_DATA_H
