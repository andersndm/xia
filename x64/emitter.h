#ifndef IA_EMITTER_H
#define IA_EMITTER_H

#include "common/types.h"
#include "instruction.h"

typedef enum mode
{
	INDIRECT = 0,
	BYTE_DISPLACED_INDIRECT = 1,
	DISPLACED_INDIRECT = 2,
	DIRECT = 3
} mode;

// #section basic emitter
void emit_byte(u8 byte);
void emit_word(u16 word);
void emit_dword(u32 dword);
void emit_qword(u64 qword);

// #section modr/m and sib emitter
void emit_mod_rx_rm(u8 mod, u8 rx, u8 rm);
void emit_direct(u8 rx, u8 rm);
void emit_indirect(u8 rx, u8 base);
void emit_indirect_byte_displaced(u8 rx, u8 base, u8 displacement);
void emit_indirect_displaced(u8 rx, u8 base, u32 displacement);
void emit_indirect_displaced_rip(u8 rx, u32 displacement);
void emit_indirect_indexed(u8 rx, u8 base, u8 index, log2_scale scale);
void emit_indirect_indexed_byte_displaced(u8 rx, u8 base, u8 index, log2_scale scale, u8 displacement);
void emit_indirect_indexed_displaced(u8 rx, u8 base, u8 index, log2_scale scale, u32 displacement);
void emit_displaced(u8 rx, u32 displacement);

// #section rex emitter
void emit_optional_rex(u8 rx, u8 base);
void emit_optional_rex_indexed(u8 rx, u8 base, u8 index);
void emit_rex(u8 rx, u8 base);
void emit_rex_indexed(u8 rx, u8 base, u8 index);
void emit_x16_prefix(void);
void emit_addressing_mode(bit_width width);

void emit_xm(u8 x, u8 base);
void emit_xmdb(u8 x, u8 base, u8 offset);
void emit_xmdd(u8 x, u8 base, u32 offset);
void emit_xsib(u8 x, u8 base, u8 index, log2_scale scale);

extern u64 max_code_len;
extern u8 *code;
extern u8 *emit_ptr;

#endif // !IA_EMITTER_H
