#include "emitter_m.h"

#include "common/macros.h"

// -----------------------------------------------------------------------------
//     #section x8 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x8_emit_sib(opcode op, registers base, registers index, log2_scale scale)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodes8[op].m);
	emit_xsib(opcodes8[op].ext_m, base.reg, index.reg, scale);
}

internal void x8_emit_sib_r(opcode op, registers base, registers index, log2_scale scale, registers src)
{
	ASSERT(src.width == B8);
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(src.reg, base.reg, index.reg);
	emit_byte(opcodes8[op].r);
	emit_xsib(src.reg, base.reg, index.reg, scale);
}

internal void x8_emit_sib_ib(opcode op, registers base, registers index, log2_scale scale, u8 ib)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodes8[op].ib);
	emit_xsib(opcodes8[op].ext_i, base.reg, index.reg, scale);
	emit_byte(ib);
}

// }}}
// -----------------------------------------------------------------------------
//     #section x16 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x16_emit_sib(opcode op, registers base, registers index, log2_scale scale)
{
	ASSERT(base.width == index.width);
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].m);
	emit_xsib(opcodesX[op].ext_m, base.reg, index.reg, scale);
}

internal void x16_emit_sib_r(opcode op, registers base, registers index, log2_scale scale, registers src)
{
	ASSERT(src.width == B16);
	ASSERT(base.width == index.width);
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(src.reg, base.reg, index.reg);
	emit_byte(opcodesX[op].r);
	emit_xsib(src.reg, base.reg, index.reg, scale);
}

internal void x16_emit_sib_ib(opcode op, registers base, registers index, log2_scale scale, u8 ib)
{
	ASSERT(base.width == index.width);
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].ib);
	emit_xsib(opcodesX[op].ext_i, base.reg, index.reg, scale);
	emit_byte(ib);
}

internal void x16_emit_sib_iw(opcode op, registers base, registers index, log2_scale scale, u16 iw)
{
	ASSERT(base.width == index.width);
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].ix);
	emit_xsib(opcodesX[op].ext_i, base.reg, index.reg, scale);
	emit_word(iw);
}

// }}}
// -----------------------------------------------------------------------------
//     #section x32 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x32_emit_sib(opcode op, registers base, registers index, log2_scale scale)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].m);
	emit_xsib(opcodesX[op].ext_m, base.reg, index.reg, scale);
}

internal void x32_emit_sib_r(opcode op, registers base, registers index, log2_scale scale, registers src)
{
	ASSERT(src.width == B32);
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(src.reg, base.reg, index.reg);
	emit_byte(opcodesX[op].r);
	emit_xsib(src.reg, base.reg, index.reg, scale);
}

internal void x32_emit_sib_ib(opcode op, registers base, registers index, log2_scale scale, u8 ib)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].ib);
	emit_xsib(opcodesX[op].ext_i, base.reg, index.reg, scale);
	emit_byte(ib);
}

internal void x32_emit_sib_id(opcode op, registers base, registers index, log2_scale scale, u32 id)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_optional_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].ix);
	emit_xsib(opcodesX[op].ext_i, base.reg, index.reg, scale);
	emit_dword(id);
}

// }}}
// -----------------------------------------------------------------------------
//     #section x64 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x64_emit_sib(opcode op, registers base, registers index, log2_scale scale)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].m);
	emit_xsib(opcodesX[op].ext_m, base.reg, index.reg, scale);
}

internal void x64_emit_sib_r(opcode op, registers base, registers index, log2_scale scale, registers src)
{
	ASSERT(src.width == B64);
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_rex_indexed(src.reg, base.reg, index.reg);
	emit_byte(opcodesX[op].r);
	emit_xsib(src.reg, base.reg, index.reg, scale);
}

internal void x64_emit_sib_ib(opcode op, registers base, registers index, log2_scale scale, u8 ib)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].ib);
	emit_xsib(opcodesX[op].ext_i, base.reg, index.reg, scale);
	emit_byte(ib);
}

internal void x64_emit_sib_id(opcode op, registers base, registers index, log2_scale scale, u32 id)
{
	ASSERT(base.width == index.width);
	emit_addressing_mode(base.width);
	emit_rex_indexed(0x00, base.reg, index.reg);
	emit_byte(opcodesX[op].ix);
	emit_xsib(opcodesX[op].ext_i, base.reg, index.reg, scale);
	emit_dword(id);
}

// }}}
// -----------------------------------------------------------------------------
//     #section width dispatching
// -----------------------------------------------------------------------------
// {{{

void emit_sib(bit_width width, opcode op, registers base, registers index, log2_scale scale)
{
	switch (width)
	{
		case B8:
		{
			x8_emit_sib(op, base, index, scale);
		} break;
		case B16:
		{
			x16_emit_sib(op, base, index, scale);
		} break;
		case B32:
		{
			x32_emit_sib(op, base, index, scale);
		} break;
		case B64:
		{
			x64_emit_sib(op, base, index, scale);
		} break;
		case BNONE:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

void emit_sib_r(bit_width width, opcode op, registers base, registers index, log2_scale scale,
				registers src)
{
	switch (width)
	{
		case B8:
		{
			x8_emit_sib_r(op, base, index, scale, src);
		} break;
		case B16:
		{
			x16_emit_sib_r(op, base, index, scale, src);
		} break;
		case B32:
		{
			x32_emit_sib_r(op, base, index, scale, src);
		} break;
		case B64:
		{
			x64_emit_sib_r(op, base, index, scale, src);
		} break;
		case BNONE:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

void emit_sib_ix(bit_width width, opcode op, registers base, registers index, log2_scale scale,
				 immediate_op imm_op)
{
	switch (width)
	{
		case B8:
		{
			ASSERT(imm_op.kind == IB);
			x8_emit_sib_ib(op, base, index, scale, imm_op.ib);
		} break;
		case B16:
		{
			if (imm_op.kind == IB)
			{
				x16_emit_sib_ib(op, base, index, scale, imm_op.ib);
			}
			else
			{
				ASSERT(imm_op.kind == IW);
				x16_emit_sib_iw(op, base, index, scale, imm_op.iw);
			}
		} break;
		case B32:
		{
			// #todo may be cases where x32 can take a 16-bit immediate
			if (imm_op.kind == IB)
			{
				x32_emit_sib_ib(op, base, index, scale, imm_op.ib);
			}
			else
			{
				ASSERT(imm_op.kind == ID);
				x32_emit_sib_id(op, base, index, scale, imm_op.id);
			}
		} break;
		case B64:
		{
			// #todo may be cases where x64 can take a 16-bit immediate
			// #todo rare cases for 64-bit immediate
			if (imm_op.kind == IB)
			{
				x64_emit_sib_ib(op, base, index, scale, imm_op.ib);
			}
			else
			{			
				ASSERT(imm_op.kind == ID);
				x64_emit_sib_id(op, base, index, scale, imm_op.id);
			}
		} break;
		case BNONE:
		{
			INVALID_CODE_PATH;
		} break;
	}
}
