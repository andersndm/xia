#include "emitter_disp.h"

#include "common/macros.h"

internal void x8_emit_disp(opcode op, u32 displacement)
{
	emit_byte(opcodes8[op].m);
	emit_displaced(opcodes8[op].ext_m, displacement);
}

internal void x8_emit_disp_r(opcode op, u32 displacement, registers src)
{
	ASSERT(src.width == B8);
	emit_optional_rex(src.reg, 0x00);
	emit_byte(opcodes8[op].r);
	emit_displaced(src.reg, displacement);
}

internal void x8_emit_disp_ib(opcode op, u32 displacement, u8 ib)
{
	emit_byte(opcodes8[op].ib);
	emit_displaced(opcodes8[op].ext_i, displacement);
	emit_byte(ib);
}

internal void x16_emit_disp(opcode op, u32 displacement)
{
	emit_x16_prefix();
	emit_byte(opcodesX[op].m);
	emit_displaced(opcodesX[op].ext_m, displacement);
}

internal void x16_emit_disp_r(opcode op, u32 displacement, registers src)
{
	ASSERT(src.width == B16);
	emit_x16_prefix();
	emit_optional_rex(src.reg, 0x00);
	emit_byte(opcodesX[op].r);
	emit_displaced(src.reg, displacement);
}

internal void x16_emit_disp_ib(opcode op, u32 displacement, u8 ib)
{
	emit_x16_prefix();
	emit_byte(opcodesX[op].ib);
	emit_displaced(opcodesX[op].ext_i, displacement);
	emit_byte(ib);
}

internal void x16_emit_disp_iw(opcode op, u32 displacement, u16 iw)
{
	emit_x16_prefix();
	emit_byte(opcodesX[op].ix);
	emit_displaced(opcodesX[op].ext_i, displacement);
	emit_word(iw);
}

internal void x32_emit_disp(opcode op, u32 displacement)
{
	emit_byte(opcodesX[op].m);
	emit_displaced(opcodesX[op].ext_m, displacement);
}

internal void x32_emit_disp_r(opcode op, u32 displacement, registers src)
{
	ASSERT(src.width == B32);
	emit_optional_rex(src.reg, 0x00);
	emit_byte(opcodesX[op].r);
	emit_displaced(src.reg, displacement);
}

internal void x32_emit_disp_ib(opcode op, u32 displacement, u8 ib)
{
	emit_byte(opcodesX[op].ib);
	emit_displaced(opcodesX[op].ext_i, displacement);
	emit_byte(ib);
}

internal void x32_emit_disp_id(opcode op, u32 displacement, u32 id)
{
	emit_byte(opcodesX[op].ix);
	emit_displaced(opcodesX[op].ext_i, displacement);
	emit_dword(id);
}

internal void x64_emit_disp(opcode op, u32 displacement)
{
	emit_rex(0x00, 0x00);
	emit_byte(opcodesX[op].m);
	emit_displaced(opcodesX[op].ext_m, displacement);
}

internal void x64_emit_disp_r(opcode op, u32 displacement, registers src)
{
	ASSERT(src.width == B64);
	emit_rex(src.reg, 0x00);
	emit_byte(opcodesX[op].r);
	emit_displaced(src.reg, displacement);
}

internal void x64_emit_disp_ib(opcode op, u32 displacement, u8 ib)
{
	emit_rex(0x00, 0x00);
	emit_byte(opcodesX[op].ib);
	emit_displaced(opcodesX[op].ext_i, displacement);
	emit_byte(ib);
}

internal void x64_emit_disp_id(opcode op, u32 displacement, u32 id)
{
	emit_rex(0x00, 0x00);
	emit_byte(opcodesX[op].ix);
	emit_displaced(opcodesX[op].ext_i, displacement);
	emit_dword(id);
}

void emit_disp(bit_width width, opcode op, u32 displacement)
{
	switch (width)
	{
		case B8:
		{
			x8_emit_disp(op, displacement);
		} break;
		case B16:
		{
			x16_emit_disp(op, displacement);
		} break;
		case B32:
		{
			x32_emit_disp(op, displacement);
		} break;
		case B64:
		{
			x64_emit_disp(op, displacement);
		} break;
		case BNONE:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

void emit_disp_r(bit_width width, opcode op, u32 displacement, registers src)
{
	switch (width)
	{
		case B8:
		{
			x8_emit_disp_r(op, displacement, src);
		} break;
		case B16:
		{
			x16_emit_disp_r(op, displacement, src);
		} break;
		case B32:
		{
			x32_emit_disp_r(op, displacement, src);
		} break;
		case B64:
		{
			x64_emit_disp_r(op, displacement, src);
		} break;
		case BNONE:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

void emit_disp_ix(bit_width width, opcode op, u32 displacement, immediate_op imm_op)
{
	switch (width)
	{
		case B8:
		{
			ASSERT(imm_op.kind == IB);
			x8_emit_disp_ib(op, displacement, imm_op.ib);
		} break;
		case B16:
		{
			if (imm_op.kind == IB)
			{
				x16_emit_disp_ib(op, displacement, imm_op.ib);
			}
			else
			{
				ASSERT(imm_op.kind == IW);
				x16_emit_disp_iw(op, displacement, imm_op.iw);
			}
		} break;
		case B32:
		{
			if (imm_op.kind == IB)
			{
				x32_emit_disp_ib(op, displacement, imm_op.ib);
			}
			else
			{
				ASSERT(imm_op.kind == ID);
				x32_emit_disp_id(op, displacement, imm_op.id);
			}
		} break;
		case B64:
		{
			if (imm_op.kind == IB)
			{
				x64_emit_disp_ib(op, displacement, imm_op.ib);
			}
			else
			{
				ASSERT(imm_op.kind == ID);
				x64_emit_disp_id(op, displacement, imm_op.id);
			}
		} break;
		case BNONE:
		{
			INVALID_CODE_PATH;
		} break;
	}
}
