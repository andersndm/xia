#ifndef IA_OPCODES_H
#define IA_OPCODES_H

#include "common/types.h"

typedef enum opcode
{
	ADC, // done
	ADD, // done
	AND, // done
	CALL,// done

	CMP, // done
	DEC, // done
	DIV, // done
	IDIV,// done
	IMUL,// done
	INC, // done
	INT, // done
	JMP, // done

	JO,              // 80
	FIRST_JCC = JO,
	JNO,             // 81
	JB,              // 82
	JC,              // 82
	JNAE,            // 82
	JAE,             // 83
	JNB,             // 83
	JNC,             // 83
	JE,              // 84
	JZ,              // 84
	JNE,             // 85
	JNZ,             // 85
	JBE,             // 86
	JNA,             // 86
	JA,              // 87
	JNBE,            // 87
	JS,              // 88
	JNS,             // 89
	JP,              // 8A
	JPE,             // 8A
	JPO,             // 8B
	JNP,             // 8B
	JL,              // 8C
	JNGE,            // 8C
	JGE,             // 8D
	JNL,             // 8D
	JLE,             // 8E
	JNG,             // 8E
	JG,              // 8F
	JNLE,            // 8F
	LAST_JCC = JNLE,

	LEA,
	MOV,

	CMOVO,                 // 40
	FIRST_CMOVCC = CMOVO,
	CMOVNO,                // 41
	CMOVB,                 // 42
	CMOVC,                 // 42
	CMOVNAE,               // 42
	CMOVAE,                // 43
	CMOVNB,                // 43
	CMOVNC,                // 43
	CMOVE,                 // 44
	CMOVZ,                 // 44
	CMOVNE,                // 45
	CMOVNZ,                // 45
	CMOVBE,                // 46
	CMOVNA,                // 46
	CMOVA,                 // 47
	CMOVNBE,               // 47
	CMOVS,                 // 48
	CMOVNS,                // 49
	CMOVP,                 // 4A
	CMOVPE,                // 4A
	CMOVPO,                // 4B
	CMOVNP,                // 4B
	CMOVL,                 // 4C
	CMOVNGE,               // 4C
	CMOVGE,                // 4D
	CMOVNL,                // 4D
	CMOVLE,                // 4E
	CMOVNG,                // 4E
	CMOVG,                 // 4F
	CMOVNLE,               // 4F
	LAST_CMOVCC = CMOVNLE,

	MOVSX, // done
	MOVZX, // done
	MUL,  // done
	NEG,  // done
	NOP,  // done
	NOT,  // done
	OR,   // done
	POP,  // done
	PUSH, // done
	RET,  // done
	SAL,  // done
	SAR,  // done
	SBB,  // done

	SET,

	SETO,                 // 90
	FIRST_SETCC = SETO,
	SETNO,                // 91
	SETB,                 // 92
	SETC,                 // 92
	SETNAE,               // 92
	SETAE,                // 93
	SETNB,                // 93
	SETNC,                // 93
	SETE,                 // 94
	SETZ,                 // 94
	SETNZ,                // 95
	SETNE,                // 95
	SETBE,                // 96
	SETNA,                // 96
	SETA,                 // 97
	SETNBE,               // 97
	SETS,                 // 98
	SETNS,                // 99
	SETP,                 // 9A
	SETPE,                // 9A
	SETPO,                // 9B
	SETNP,                // 9B
	SETL,                 // 9C
	SETNGE,               // 9C
	SETGE,                // 9D
	SETNL,                // 9D
	SETLE,                // 9E
	SETNG,                // 9E
	SETG,                 // 9F
	SETNLE,               // 9F
	LAST_SETCC = SETNLE,

	SHL, // done
	SHR, // done
	SUB, // done
	SYSCALL, // done
	TEST, // done
	XCHG, // done
	XOR,

	NUM_OPCODES,
	OP_NONE
} opcode;

typedef enum condition_codes
{
	O,          // overflow
	NO,         // no overflow
	B,          // below
	NB,         // not below
	E,          // equal
	NE,         // not equal
	NA,         // not above
	A,          // above
	S,          // sign
	NS,         // no sign
	P,          // parity
	NP,         // no parity
	L,          // less than
	NL,         // not less than
	NG,         // not greater than
	G,          // greater than
	NAE = B,    // not above or equal
	C = B,      // carry
	AE = NB,    // above or equal
	NC = NB,    // no carry
	Z = E,      // zero
	NZ = NE,    // not zero
	BE = NA,    // below or equal
	NBE = A,    // not below or equal
	PE = P,     // parity even
	PO = NP,    // parity odd
	NGE = L,    // not greater than or equal
	GE = NL,    // greater than or equal
	LE = NG,    // less than or equal
	NLE = G     // not less than or equal
} condition_codes;

struct op8_data
{
	u8 r;
	u8 m;
	u8 ext_m;
	u8 rax_ib;
	u8 ib;
	u8 ext_i;
};

struct opX_data
{
	u8 r;
	u8 m;
	u8 ext_m;
	u8 rax_ix;
	u8 ib;
	u8 ix;
	u8 ext_i;
};

extern const char *op_names[NUM_OPCODES];
extern struct op8_data opcodes8[NUM_OPCODES];
extern struct opX_data opcodesX[NUM_OPCODES];
extern condition_codes op_to_cc[NUM_OPCODES];

#endif // !IA_OPCODES_H
