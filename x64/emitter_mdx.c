#include "emitter_m.h"

#include "common/macros.h"

// -----------------------------------------------------------------------------
//     #section x8 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x8_emit_mdb(opcode op, registers base, u8 offset)
{
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodes8[op].m);
	emit_xmdb(opcodes8[op].ext_m, base.reg, offset);
}

internal void x8_emit_mdd(opcode op, registers base, u32 offset)
{
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodes8[op].m);
	emit_xmdd(opcodes8[op].ext_m, base.reg, offset);
}

internal void x8_emit_mdb_r(opcode op, registers base, u8 offset, registers src)
{
	ASSERT(src.width == B8);
	emit_addressing_mode(base.width);
	emit_optional_rex(src.reg, base.reg);
	emit_byte(opcodes8[op].r);
	emit_xmdb(src.reg, base.reg, offset);
}

internal void x8_emit_mdd_r(opcode op, registers base, u32 offset, registers src)
{
	ASSERT(src.width == B8);
	emit_addressing_mode(base.width);
	emit_optional_rex(src.reg, base.reg);
	emit_byte(opcodes8[op].r);
	emit_xmdd(src.reg, base.reg, offset);
}

internal void x8_emit_mdb_ib(opcode op, registers base, u8 offset, u8 ib)
{
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodes8[op].ib);
	emit_xmdb(opcodes8[op].ext_i, base.reg, offset);
	emit_byte(ib);
}

internal void x8_emit_mdd_ib(opcode op, registers base, u32 offset, u8 ib)
{
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodes8[op].ib);
	emit_xmdd(opcodes8[op].ext_i, base.reg, offset);
	emit_byte(ib);
}

// }}}
// -----------------------------------------------------------------------------
//     #section x16 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x16_emit_mdb(opcode op, registers base, u8 offset)
{
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodesX[op].m);
	emit_xmdb(opcodesX[op].ext_m, base.reg, offset);
}

internal void x16_emit_mdd(opcode op, registers base, u32 offset)
{
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodesX[op].m);
	emit_xmdd(opcodesX[op].ext_m, base.reg, offset);
}

internal void x16_emit_mdb_r(opcode op, registers base, u8 offset, registers src)
{
	ASSERT(src.width == B16);
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex(src.reg, base.reg);
	emit_byte(opcodesX[op].r);
	emit_xmdb(src.reg, base.reg, offset);
}

internal void x16_emit_mdd_r(opcode op, registers base, u32 offset, registers src)
{
	ASSERT(src.width == B16);
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex(src.reg, base.reg);
	emit_byte(opcodesX[op].r);
	emit_xmdd(src.reg, base.reg, offset);
}

internal void x16_emit_mdb_ib(opcode op, registers base, u8 offset, u8 ib)
{
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodesX[op].ib);
	emit_xmdb(opcodesX[op].ext_i, base.reg, offset);
	emit_byte(ib);
}

internal void x16_emit_mdb_iw(opcode op, registers base, u8 offset, u16 iw)
{
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodesX[op].ix);
	emit_xmdb(opcodesX[op].ext_i, base.reg, offset);
	emit_word(iw);
}

internal void x16_emit_mdd_ib(opcode op, registers base, u32 offset, u8 ib)
{
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodesX[op].ib);
	emit_xmdd(opcodesX[op].ext_i, base.reg, offset);
	emit_byte(ib);
}

internal void x16_emit_mdd_iw(opcode op, registers base, u32 offset, u16 iw)
{
	emit_x16_prefix();
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodesX[op].ix);
	emit_xmdd(opcodesX[op].ext_i, base.reg, offset);
	emit_word(iw);
}

// }}}
// -----------------------------------------------------------------------------
//     #section x32 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x32_emit_mdb(opcode op, registers base, u8 offset)
{
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodesX[op].m);
	emit_xmdb(opcodesX[op].ext_m, base.reg, offset);
}

internal void x32_emit_mdd(opcode op, registers base, u32 offset)
{
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodesX[op].m);
	emit_xmdd(opcodesX[op].ext_m, base.reg, offset);
}

internal void x32_emit_mdb_r(opcode op, registers base, u8 offset, registers src)
{
	ASSERT(src.width == B32);
	emit_addressing_mode(base.width);
	emit_optional_rex(src.reg, base.reg);
	emit_byte(opcodesX[op].r);
	emit_xmdb(src.reg, base.reg, offset);
}

internal void x32_emit_mdd_r(opcode op, registers base, u32 offset, registers src)
{
	ASSERT(src.width == B32);
	emit_addressing_mode(base.width);
	emit_optional_rex(src.reg, base.reg);
	emit_byte(opcodesX[op].r);
	emit_xmdd(src.reg, base.reg, offset);
}

internal void x32_emit_mdb_ib(opcode op, registers base, u8 offset, u8 ib)
{
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodesX[op].ib);
	emit_xmdb(opcodesX[op].ext_i, base.reg, offset);
	emit_byte(ib);
}

internal void x32_emit_mdb_id(opcode op, registers base, u8 offset, u32 id)
{
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodesX[op].ix);
	emit_xmdb(opcodesX[op].ext_i, base.reg, offset);
	emit_dword(id);
}

internal void x32_emit_mdd_ib(opcode op, registers base, u32 offset, u8 ib)
{
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodesX[op].ib);
	emit_xmdd(opcodesX[op].ext_i, base.reg, offset);
	emit_byte(ib);
}

internal void x32_emit_mdd_id(opcode op, registers base, u32 offset, u32 id)
{
	emit_addressing_mode(base.width);
	emit_optional_rex(0x00, base.reg);
	emit_byte(opcodesX[op].ix);
	emit_xmdd(opcodesX[op].ext_i, base.reg, offset);
	emit_dword(id);
}

// }}}
// -----------------------------------------------------------------------------
//     #section x64 instruction emitter
// -----------------------------------------------------------------------------
// {{{

internal void x64_emit_mdb(opcode op, registers base, u8 offset)
{
	emit_addressing_mode(base.width);
	emit_rex(0x00, base.reg);
	emit_byte(opcodesX[op].m);
	emit_xmdb(opcodesX[op].ext_m, base.reg, offset);
}

internal void x64_emit_mdd(opcode op, registers base, u32 offset)
{
	emit_addressing_mode(base.width);
	emit_rex(0x00, base.reg);
	emit_byte(opcodesX[op].m);
	emit_xmdd(opcodesX[op].ext_m, base.reg, offset);
}


internal void x64_emit_mdb_r(opcode op, registers base, u8 offset, registers src)
{
	ASSERT(src.width == B64);
	emit_addressing_mode(base.width);
	emit_rex(src.reg, base.reg);
	emit_byte(opcodesX[op].r);
	emit_xmdb(src.reg, base.reg, offset);
}

internal void x64_emit_mdd_r(opcode op, registers base, u32 offset, registers src)
{
	ASSERT(src.width == B64);
	emit_addressing_mode(base.width);
	emit_rex(src.reg, base.reg);
	emit_byte(opcodesX[op].r);
	emit_xmdd(src.reg, base.reg, offset);
}

internal void x64_emit_mdb_ib(opcode op, registers base, u8 offset, u8 ib)
{
	emit_addressing_mode(base.width);
	emit_rex(0x00, base.reg);
	emit_byte(opcodesX[op].ib);
	emit_xmdb(opcodesX[op].ext_i, base.reg, offset);
	emit_byte(ib);
}

internal void x64_emit_mdb_id(opcode op, registers base, u8 offset, u32 id)
{
	emit_addressing_mode(base.width);
	emit_rex(0x00, base.reg);
	emit_byte(opcodesX[op].ix);
	emit_xmdb(opcodesX[op].ext_i, base.reg, offset);
	emit_dword(id);
}

internal void x64_emit_mdd_ib(opcode op, registers base, u32 offset, u8 ib)
{
	emit_addressing_mode(base.width);
	emit_rex(0x00, base.reg);
	emit_byte(opcodesX[op].ib);
	emit_xmdd(opcodesX[op].ext_i, base.reg, offset);
	emit_byte(ib);
}

internal void x64_emit_mdd_id(opcode op, registers base, u32 offset, u32 id)
{
	emit_addressing_mode(base.width);
	emit_rex(0x00, base.reg);
	emit_byte(opcodesX[op].ix);
	emit_xmdd(opcodesX[op].ext_i, base.reg, offset);
	emit_dword(id);
}

// }}}
// -----------------------------------------------------------------------------
//     #section width dispatching
// -----------------------------------------------------------------------------
// {{{

void emit_mdx(bit_width width, opcode op, registers base, immediate_op offset)
{
	switch (offset.kind)
	{
		case IB:
		{
			switch (width)
			{
				case B8:
				{
					x8_emit_mdb(op, base, offset.ib);
				} break;
				case B16:
				{
					x16_emit_mdb(op, base, offset.ib);
				} break;
				case B32:
				{
					x32_emit_mdb(op, base, offset.ib);
				} break;
				case B64:
				{
					x64_emit_mdb(op, base, offset.ib);
				} break;
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
		} break;
		case ID:
		{
			switch (width)
			{
				case B8:
				{
					x8_emit_mdd(op, base, offset.id);
				} break;
				case B16:
				{
					x16_emit_mdd(op, base, offset.id);
				} break;
				case B32:
				{
					x32_emit_mdd(op, base, offset.id);
				} break;
				case B64:
				{
					x64_emit_mdd(op, base, offset.id);
				} break;
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
		} break;
		case IW:
		case IQ:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

void emit_mdx_r(bit_width width, opcode op, registers base, immediate_op offset,
				registers src)
{
	switch (offset.kind)
	{
		case IB:
		{
			switch (width)
			{
				case B8:
				{
					x8_emit_mdb_r(op, base, offset.ib, src);
				} break;
				case B16:
				{
					x16_emit_mdb_r(op, base, offset.ib, src);
				} break;
				case B32:
				{
					x32_emit_mdb_r(op, base, offset.ib, src);
				} break;
				case B64:
				{
					x64_emit_mdb_r(op, base, offset.ib, src);
				} break;
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
		} break;
		case ID:
		{
			switch (width)
			{
				case B8:
				{
					x8_emit_mdd_r(op, base, offset.id, src);
				} break;
				case B16:
				{
					x16_emit_mdd_r(op, base, offset.id, src);
				} break;
				case B32:
				{
					x32_emit_mdd_r(op, base, offset.id, src);
				} break;
				case B64:
				{
					x64_emit_mdd_r(op, base, offset.id, src);
				} break;
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
		} break;
		case IW:
		case IQ:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

void emit_mdx_ix(bit_width width, opcode op, registers base, immediate_op offset,
				 immediate_op imm_op)
{
	switch (offset.kind)
	{
		case IB:
		{
			switch (width)
			{
				case B8:
				{
					ASSERT(imm_op.kind == IB);
					x8_emit_mdb_ib(op, base, offset.ib, imm_op.ib);
				} break;
				case B16:
				{
					if (imm_op.kind == IB)
					{
						x16_emit_mdb_ib(op, base, offset.ib, imm_op.ib);
					}
					else
					{
						ASSERT(imm_op.kind == IW);
						x16_emit_mdb_iw(op, base, offset.ib, imm_op.iw);
					}
				} break;
				case B32:
				{
					if (imm_op.kind == IB)
					{
						x32_emit_mdb_ib(op, base, offset.ib, imm_op.ib);
					}
					else
					{
						if (imm_op.kind == IW)
						{
							imm_op.kind = ID;
							imm_op.id = (u32)(s32)imm_op.iw;
						}
						ASSERT(imm_op.kind == ID);
						x32_emit_mdb_id(op, base, offset.ib, imm_op.id);
					}
				} break;
				case B64:
				{
					if (imm_op.kind == IB)
					{
						x64_emit_mdb_ib(op, base, offset.ib, imm_op.ib);
					}
					else
					{
						if (imm_op.kind == IW)
						{
							imm_op.kind = ID;
							imm_op.id = (u32)(s32)imm_op.iw;
						}
						ASSERT(imm_op.kind == ID);
						x64_emit_mdb_id(op, base, offset.ib, imm_op.id);
					}
				} break;
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
		} break;
		case ID:
		{
			switch (width)
			{
				case B8:
				{
					ASSERT(imm_op.kind == IB);
					x8_emit_mdd_ib(op, base, offset.id, imm_op.ib);
				} break;
				case B16:
				{
					if (imm_op.kind == IB)
					{
						x16_emit_mdd_ib(op, base, offset.id, imm_op.ib);
					}
					else
					{
						ASSERT(imm_op.kind == IW);
						x16_emit_mdd_iw(op, base, offset.id, imm_op.iw);
					}
				} break;
				case B32:
				{
					if (imm_op.kind == IB)
					{
						x32_emit_mdd_ib(op, base, offset.id, imm_op.ib);
					}
					else
					{
						if (imm_op.kind == IW)
						{
							imm_op.kind = ID;
							imm_op.id = (u32)(s32)imm_op.iw;
						}
						ASSERT(imm_op.kind == ID);
						x32_emit_mdd_id(op, base, offset.id, imm_op.id);
					}
				} break;
				case B64:
				{
					if (imm_op.kind == IB)
					{
						x64_emit_mdd_ib(op, base, offset.id, imm_op.ib);
					}
					else
					{
						if (imm_op.kind == IW)
						{
							imm_op.kind = ID;
							imm_op.id = (u32)(s32)imm_op.iw;
						}
						ASSERT(imm_op.kind == ID);
						x64_emit_mdd_id(op, base, offset.id, imm_op.id);
					}
				} break;
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
		} break;
		case IW:
		case IQ:
		{
			INVALID_CODE_PATH;
		} break;
	}
}
