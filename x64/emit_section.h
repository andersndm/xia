#ifndef IA_EMIT_SECTION_H
#define IA_EMIT_SECTION_H

#include "xia/compilation_unit.h"

void emit_instruction(instruction *instr);
void emit_section(section *sec);

#endif // !IA_EMIT_SECTION_H
