#ifndef XIA_EMITTER_SIBDX_H
#define XIA_EMITTER_SIBDX_H

#include "emitter.h"

void emit_sibdx(bit_width width, opcode op, registers base, registers index, log2_scale scale,
				immediate_op offset);
void emit_sibdx_r(bit_width width, opcode op, registers base, registers index, log2_scale scale,
				  immediate_op offset, registers src);
void emit_sibdx_ix(bit_width width, opcode op, registers base, registers index, log2_scale scale,
				   immediate_op offset, immediate_op imm_op);

#endif // !XIA_EMITTER_SIBDX_H
