#include "emitter.h"

#include "common/macros.h"
#include "registers.h"

u64 max_code_len;
u8 *code;
u8 *emit_ptr;

// -----------------------------------------------------------------------------
//     #section basic emitter
// -----------------------------------------------------------------------------
// {{{

void emit_byte(u8 byte)
{
	*emit_ptr = byte;
	++emit_ptr;

	if ((u64)(emit_ptr - code) > max_code_len)
	{
		PC_RED("error: "); printf("exceeded code size\n");
		ASSERT(0);
	}
}

void emit_word(u16 word)
{
	// #note must be little-endian!
	emit_byte(word & 0xFF);
	emit_byte((word >> 8) & 0xFF);
}

void emit_dword(u32 dword)
{
	// #note must be little-endian!
	emit_byte(dword & 0xFF);
	emit_byte((dword >> 8) & 0xFF);
	emit_byte((dword >> 16) & 0xFF);
	emit_byte((dword >> 24) & 0xFF);
}

void emit_qword(u64 qword)
{
	// #note must be little-endian!
	emit_byte(qword & 0xFF);
	emit_byte((qword >> 8) & 0xFF);
	emit_byte((qword >> 16) & 0xFF);
	emit_byte((qword >> 24) & 0xFF);
	emit_byte((qword >> 32) & 0xFF);
	emit_byte((qword >> 40) & 0xFF);
	emit_byte((qword >> 48) & 0xFF);
	emit_byte((qword >> 56) & 0xFF);
}

// }}}

// -----------------------------------------------------------------------------
//     #section modr/m and sib emitter
// -----------------------------------------------------------------------------
// {{{

void emit_mod_rx_rm(u8 mod, u8 rx, u8 rm)
{
	ASSERT(mod < 4);
	ASSERT(rx < NUM_REGISTERS);
	ASSERT(rm < NUM_REGISTERS);
	emit_byte((mod << 6) | ((rx & 0x7) << 3) | (rm & 0x7));
}

// if indirect no displacement => mod = 0
// if indirect 1-byte displacement => mod = 1
// if indirect multi-byte displacement => mod = 2
// else direct => mod = 3

// if indirect, base register goes in rm
// unless base register is rsp, then need a sib encoding with base = 4, index = 4, scale = whatever (0)

// add rax, rcx
// rx = rax, rm = rcx
void emit_direct(u8 rx, u8 rm)
{
	emit_mod_rx_rm(DIRECT, rx, rm);
}

// add rax, [rcx]
// rx = rax, base = rcx
void emit_indirect(u8 rx, u8 base)
{
	// #todo should probably be 16 for all registers
	#if 0 // does not apply to 8-bit
	ASSERT((base & 7) != RSP);
	ASSERT((base & 7) != RBP);
	#endif
	emit_mod_rx_rm(INDIRECT, rx, base);
}

// add rax, [rcx + 0x12]
// rx = rax, base = rcx, displacement = 0x12
void emit_indirect_byte_displaced(u8 rx, u8 base, u8 displacement)
{
	// #todo should probably be 16 for all registers
	#if 0 // does not apply to 8-bit
	ASSERT((base & 7) != RSP);
	#endif
	emit_mod_rx_rm(BYTE_DISPLACED_INDIRECT, rx, base);
	emit_byte(displacement);
}

// add rax, [rcx + 0x12345678]
// rx = rax, base = rcx, displacement = 0x12345678
void emit_indirect_displaced(u8 rx, u8 base, u32 displacement)
{
	// #todo should probably be 16 for all registers
	ASSERT(rx < NUM_REGISTERS); ASSERT(base < NUM_REGISTERS);
	#if 0
	ASSERT((base & 7) != RSP);
	#endif
	emit_mod_rx_rm(DISPLACED_INDIRECT, rx, base);
	emit_dword(displacement);
}

// add rax, [rip + 0x12345678]
// rx = rax, displacement = 0x12345678
void emit_indirect_displaced_rip(u8 rx, u32 displacement)
{
	emit_mod_rx_rm(INDIRECT, rx, RBP);
	emit_dword(displacement);
}

// add rax, [rcx + 4 * rdx]
// rx = rax, base = rcx, index = rdx, scale = X4
void emit_indirect_indexed(u8 rx, u8 base, u8 index, log2_scale scale)
{
	// #todo should probably be 16 for all registers
	ASSERT(rx < NUM_REGISTERS); ASSERT(base < NUM_REGISTERS);
	#if 0 // does not apply to 8-bit
	ASSERT((base & 7) != RBP);
	#endif
	emit_mod_rx_rm(INDIRECT, rx, RSP);
	emit_mod_rx_rm(scale, index, base);
}

// add rax, [rcx + 4 * rdx + 0x12]
// rx = rax, base = rcx, index = rdx, scale = X4, displacement = 0x12
void emit_indirect_indexed_byte_displaced(u8 rx, u8 base, u8 index, log2_scale scale, u8 displacement)
{
	emit_mod_rx_rm(BYTE_DISPLACED_INDIRECT, rx, RSP);
	emit_mod_rx_rm(scale, index, base);
	emit_byte(displacement);
}

// add rax, [rcx + 4 * rdx + 0x12345678]
// rx = rax, base = rcx, index = rdx, scale = X4, displacement = 0x12345678
void emit_indirect_indexed_displaced(u8 rx, u8 base, u8 index, log2_scale scale, u32 displacement)
{
	emit_mod_rx_rm(DISPLACED_INDIRECT, rx, RSP);
	emit_mod_rx_rm(scale, index, base);
	emit_dword(displacement);
}

// add rax, [0x12345678]
// rx = rax, displacement = 0x12345678
void emit_displaced(u8 rx, u32 displacement)
{
	emit_mod_rx_rm(INDIRECT, rx, RSP);
	emit_mod_rx_rm(X1, RSP, RBP);
	emit_dword(displacement);
}

// }}}
// -----------------------------------------------------------------------------
//     #section rex emitter
// -----------------------------------------------------------------------------
// {{{

void emit_optional_rex(u8 rx, u8 base)
{
	if (rx >= R8 || base >= R8)
	{
		emit_byte(0x40 | (base >> 3) | (( rx >> 3) << 2));
	}
}

void emit_optional_rex_indexed(u8 rx, u8 base, u8 index)
{
	if (rx >= R8 || base >= R8 || index >= R8)
	{
		emit_byte(0x40 | (base >> 3) | ((index >> 3) << 1) | ((rx >> 3 ) << 2));
	}
}

void emit_rex(u8 rx, u8 base)
{
	emit_byte(0x48 | (base >> 3) | ((rx >> 3 ) << 2));
}

void emit_rex_indexed(u8 rx, u8 base, u8 index)
{
	emit_byte(0x48 | (base >> 3) | ((index >> 3) << 1) | ((rx >> 3 ) << 2));
}

void emit_x16_prefix(void)
{
	emit_byte(0x66);
}

void emit_addressing_mode(bit_width width)
{
	if (width == B32)
	{
		emit_byte(0x67);
	}
	else
	{
		ASSERT(width == B64);
	}
}

// }}}
// -----------------------------------------------------------------------------
//     #section emitter snippets
// -----------------------------------------------------------------------------
// {{{
void emit_xm(u8 dest, u8 base)
{
	if ((base & 0x7) == RBP)
	{
		emit_indirect_byte_displaced(dest, base, 0x00);
	}
	else if ((base & 0x7) == RSP)
	{
		emit_indirect_indexed(dest, base, RSP, X1);
	}
	else
	{
		emit_indirect(dest, base);
	}
}

void emit_xmdb(u8 dest, u8 base, u8 offset)
{
	if ((base & 0x7) == RSP)
	{
		emit_indirect_indexed_byte_displaced(dest, base, RSP, X1, offset);
	}
	else
	{
		emit_indirect_byte_displaced(dest, base, offset);
	}
}

void emit_xmdd(u8 dest, u8 base, u32 offset)
{
	if ((base & 0x7) == RSP)
	{
		emit_indirect_indexed_displaced(dest, base, RSP, X1, offset);
	}
	else
	{
		emit_indirect_displaced(dest, base, offset);
	}
}

void emit_xsib(u8 dest, u8 base, u8 index, log2_scale scale)
{
	if ((base & 0x7) == RBP)
	{
		emit_indirect_indexed_byte_displaced(dest, base, index, scale, 0x00);
	}
	else
	{
		emit_indirect_indexed(dest, base, index, scale);
	}
}
// }}}

