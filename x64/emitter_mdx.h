#ifndef IA_EMITTER_MDX_H
#define IA_EMITTER_MDX_H

#include "emitter.h"

void emit_mdx(bit_width width, opcode op, registers base, immediate_op offset);
void emit_mdx_r(bit_width width, opcode op, registers base, immediate_op offset,
				registers dest);
void emit_mdx_ix(bit_width width, opcode op, registers base, immediate_op offset,
				 immediate_op imm_op);

#endif // !IA_EMITTER_MDX_H
