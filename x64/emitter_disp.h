#ifndef XIA_EMITTER_DISP_H
#define XIA_EMITTER_DISP_H

#include "emitter.h"

void emit_disp(bit_width width, opcode op, u32 displacement);
void emit_disp_r(bit_width width, opcode op, u32 displacement, registers src);
void emit_disp_ix(bit_width width, opcode op, u32 displacement, immediate_op imm_op);

#endif // !XIA_EMITTER_DISP_H
