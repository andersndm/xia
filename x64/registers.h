#ifndef XIA_REGISTERS_H
#define XIA_REGISTERS_H

#include "xia/tokens.h"

typedef enum register_x64
{
	RAX,
	RCX,
	RDX,
	RBX,
	RSP,
	RBP,
	RSI,
	RDI,
	R8,
	R9,
	R10,
	R11,
	R12,
	R13,
	R14,
	R15,

	NUM_REGISTERS,

	R_NONE
} register_x64;

typedef enum register_x32
{
	EAX,
	ECX,
	EDX,
	EBX,
	ESP,
	EBP,
	ESI,
	EDI,
	R8d,
	R9d,
	R10D,
	R11D,
	R12D,
	R13D,
	R14D,
	R15D,
} register_x32;

typedef enum register_x16
{
	AX,
	CX,
	DX,
	BX,
	SP,
	BP,
	SI,
	DI,
	R8W,
	R9W,
	R10W,
	R11W,
	R12W,
	R13W,
	R14W,
	R15W,
} register_x16;

typedef enum register_x8
{
	AL,
	CL,
	DL,
	BL,
	AH,
	CH,
	DH,
	BH,
	R8B,
	R9B,
	R10B,
	R11B,
	R12B,
	R13B,
	R14B,
	R15B,
} register_x8;

typedef enum bit_width
{
	BNONE,
	B8,
	B16,
	B32,
	B64
} bit_width;

typedef struct registers
{
	register_x64 reg;
	bit_width width;
	pos_range range;
} registers;

extern const char *register_x64_names[NUM_REGISTERS];
extern const char *register_x32_names[NUM_REGISTERS];
extern const char *register_x16_names[NUM_REGISTERS];
extern const char *register_x8_names[NUM_REGISTERS];
#endif // !XIA_REGISTERS_H
