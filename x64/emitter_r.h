#ifndef IA_EMITTER_R_H
#define IA_EMITTER_R_H

#include "emitter.h"

void emit_r(bit_width width, opcode op, registers src);
void emit_r_ix(bit_width width, opcode op, registers dest, immediate_op imm_op);
void emit_r_r(bit_width width, opcode op, registers dest, registers src);
void emit_r_m(bit_width width, opcode op, registers dest, registers base);
void emit_r_mdb(bit_width width, opcode op, registers dest, registers base, u8 offset);
void emit_r_mdd(bit_width width, opcode op, registers dest, registers base, u32 offset);
void emit_r_sib(bit_width width, opcode op, registers dest, registers base, registers index, log2_scale scale);
void emit_r_sibdb(bit_width width, opcode op, registers dest, registers base, registers index,
				  log2_scale scale, u8 offset);
void emit_r_sibdd(bit_width width, opcode op, registers dest, registers base, registers index,
				  log2_scale scale, u32 offset);
// #todo untested
void emit_r_disp(bit_width width, opcode op, registers dest, u32 displacement);
void emit_r_rip_disp(bit_width width, opcode op, registers dest, u32 displacement);

#endif // !IA_EMITTER_R_H
