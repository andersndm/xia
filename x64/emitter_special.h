#ifndef XIA_EMITTER_SPECIAL_H
#define XIA_EMITTER_SPECIAL_H

#include "emitter.h"

void emit_jmp(u64 code_pos, operand dest_op, operand src_op);
void emit_call(u64 code_pos, operand dest_op);
void emit_cmov(condition_codes cc, operand dest_op, operand src_op);
void emit_movx(opcode op, registers dest, operand src_op);
void emit_escaped(instruction *instr);
void emit_pop(operand src_op);
void emit_push(operand src_op);
void emit_int(u8 ib);
void emit_ret(operand src_op);
void emit_shift(opcode op, operand dest_op, operand amount);
void emit_setcc(condition_codes cc, operand dest_op);
void emit_mov(instruction *instr);

#endif // !XIA_EMITTER_SPECIAL_H
