#include "emitter_special.h"

#include "common/macros.h"
#include "common/warnings.h"

void emit_int(u8 ib)
{
	if (ib == 3)
	{
		emit_byte(0xCC);
	}
	else
	{
		emit_byte(0xCD);
		emit_byte(ib);
	}
}

void emit_call(u64 code_pos, operand oper)
{
	// #todo only difference between call and jmp is the oper_cc, and the codes
	switch (oper.kind)
	{
		case OPER_NONE:
		{
			INVALID_CODE_PATH;
		} break;
		case OPER_DISPLACEMENT:
		{
			emit_byte(0xFF);
			emit_displaced(0x02, oper.displacement);
		} break;
		case OPER_RIP_DISPLACEMENT:
		{
			emit_byte(0xFF);
			emit_indirect_displaced_rip(0x02, oper.displacement - 6);
		} break;
		case OPER_CC:
		{
			INVALID_CODE_PATH;
		} break;
		case OPER_IMMEDIATE:
		{
			u32 instr_length = 5;
			emit_byte(0xE8);
			u32 value = 0;
			if (oper.imm_op.kind == IB)
			{
				value = oper.imm_op.ib - code_pos - instr_length;
			}
			else if (oper.imm_op.kind == IW)
			{
				value = oper.imm_op.iw - code_pos - instr_length;
			}
			else if (oper.imm_op.kind == ID)
			{
				value = oper.imm_op.id - code_pos - instr_length;
			}
			else
			{
				ASSERT(oper.imm_op.iq - code_pos - instr_length <= 0xFFFFFFFF);
				value = oper.imm_op.iq - code_pos - instr_length;
			}

			emit_dword(value);
		} break;
		case OPER_REGISTER:
		{
			ASSERT(oper.reg.width == B64);
			emit_optional_rex(0x00, oper.reg.reg);
			emit_byte(0xFF);
			emit_direct(0x02, oper.reg.reg);
		} break;
		case OPER_MEM_REGISTER:
		{
			ASSERT(oper.width == B64);
			ASSERT(oper.reg.width == B32 || oper.reg.width == B64);
			emit_addressing_mode(oper.reg.width);
			emit_optional_rex(0x00, oper.reg.reg);
			emit_byte(0xFF);
			emit_xm(0x02, oper.reg.reg);
		} break;
		case OPER_MEM_REGISTER_OFFSET:
		{
			ASSERT(oper.width == B64);
			ASSERT(oper.mem_offset.base.width == B32 || oper.mem_offset.base.width == B64);
			emit_addressing_mode(oper.mem_offset.base.width);
			emit_optional_rex(0x00, oper.mem_offset.base.reg);
			emit_byte(0xFF);
			if (oper.mem_offset.offset.kind == IB)
			{
				emit_xmdb(0x02, oper.mem_offset.base.reg, oper.mem_offset.offset.ib);
			}
			else
			{
				ASSERT(oper.mem_offset.offset.kind == ID);
				emit_xmdd(0x02, oper.mem_offset.base.reg, oper.mem_offset.offset.id);
			}
		} break;
		case OPER_MEM_REGISTER_INDEXED:
		{
			ASSERT(oper.width == B64);
			ASSERT(oper.mem_indexed.base.width == B32 || oper.mem_indexed.base.width == B64);
			ASSERT(oper.mem_indexed.base.width == oper.mem_indexed.index.width);
			emit_addressing_mode(oper.mem_offset.base.width);
			emit_optional_rex_indexed(0x00, oper.mem_indexed.base.reg, oper.mem_indexed.index.reg);
			emit_byte(0xFF);
			emit_xsib(0x02, oper.mem_indexed.base.reg, oper.mem_indexed.index.reg, oper.mem_indexed.scale);
		} break;
		case OPER_MEM_REGISTER_INDEXED_OFFSET:
		{
			ASSERT(oper.width == B64);
			ASSERT(oper.mem_indexed_offset.base.width == B32 || oper.mem_indexed_offset.base.width == B64);
			ASSERT(oper.mem_indexed.base.width == oper.mem_indexed.index.width);
			emit_addressing_mode(oper.mem_offset.base.width);
			emit_optional_rex_indexed(0x00, oper.mem_indexed.base.reg, oper.mem_indexed.index.reg);
			emit_byte(0xFF);
			if (oper.mem_indexed_offset.offset.kind == IB)
			{
				emit_indirect_indexed_byte_displaced(0x02, oper.mem_indexed_offset.base.reg,
													oper.mem_indexed_offset.index.reg,
													oper.mem_indexed_offset.scale,
													oper.mem_indexed_offset.offset.ib);
			}
			else
			{
				ASSERT(oper.mem_indexed_offset.offset.kind == ID);
				emit_indirect_indexed_displaced(0x02, oper.mem_indexed_offset.base.reg,
												oper.mem_indexed_offset.index.reg,
												oper.mem_indexed_offset.scale,
												oper.mem_indexed_offset.offset.id);
			}
		} break;
		case OPER_LABEL:
		{
			// #note this will be replaced once the section is complete
			emit_byte(0xE8);
			emit_dword(0);
		} break;
		case OPER_MEM_LABEL:
		{
			// #todo is this valid?
			INVALID_CODE_PATH;
		} break;
	}
}

WRNG_IGNORE_PUSH(WRNG_SWITCH_ENUM)
void emit_jmp(u64 code_pos, operand dest_op, operand src_op)
{
	switch (dest_op.kind)
	{
		case OPER_NONE:
		{
			INVALID_CODE_PATH;
		} break;
		case OPER_DISPLACEMENT:
		{
			ASSERT(src_op.kind == OPER_NONE);
			emit_byte(0xFF);
			emit_displaced(0x04, dest_op.displacement);
		} break;
		case OPER_RIP_DISPLACEMENT:
		{
			ASSERT(src_op.kind == OPER_NONE);
			emit_byte(0xFF);
			emit_indirect_displaced_rip(0x04, dest_op.displacement - 6);
		} break;
		case OPER_CC:
		{
			#if 0
			ASSERT(src_op.kind == OPER_IMMEDIATE);
			u32 instr_length = 6;
			emit_byte(0x0F);
			emit_byte(0x80 + dest_op.cc);
			u32 value = 0;
			if (src_op.imm_op.kind == IB)
			{
				value = src_op.imm_op.ib - code_pos - instr_length;
			}
			else if (src_op.imm_op.kind == IW)
			{
				value = src_op.imm_op.iw - code_pos - instr_length;
			}
			else if (src_op.imm_op.kind == ID)
			{
				value = src_op.imm_op.id - code_pos - instr_length;
			}
			else
			{
				ASSERT(src_op.imm_op.iq - code_pos - instr_length <= 0xFFFFFFFF);
				value = dest_op.imm_op.iq - code_pos - instr_length;
			}
			emit_dword(value);
			#endif
			emit_byte(0x0F);
			emit_byte(0x80 + dest_op.cc);
			if (src_op.kind == OPER_IMMEDIATE)
			{
				ASSERT(src_op.imm_op.kind == ID);
				emit_dword(src_op.imm_op.id);
			}
			else
			{
				ASSERT(src_op.kind == OPER_LABEL);
				emit_dword(0);
			}
		} break;
		case OPER_IMMEDIATE:
		{
			ASSERT(src_op.kind == OPER_NONE);
			#if 0
			// #todo 1 byte jumps
			if (dest_op.imm_op.kind == IB)
			{
				emit_byte(0xEB);
				emit_byte(dest_op.imm_op.ib - 2);
			}
			else
			#endif
			{
				u32 instr_length = 5;
				emit_byte(0xE9);
				u32 value = 0;
				if (dest_op.imm_op.kind == IB)
				{
					value = dest_op.imm_op.ib - code_pos - instr_length;
				}
				else if (dest_op.imm_op.kind == IW)
				{
					value = dest_op.imm_op.iw - code_pos - instr_length;
				}
				else if (dest_op.imm_op.kind == ID)
				{
					value = dest_op.imm_op.id - code_pos - instr_length;
				}
				else
				{
					ASSERT(dest_op.imm_op.iq - code_pos - instr_length <= 0xFFFFFFFF);
					value = dest_op.imm_op.iq - code_pos - instr_length;
				}

				emit_dword(value);
			}
		} break;
		case OPER_REGISTER:
		{
			ASSERT(src_op.kind == OPER_NONE);
			ASSERT(dest_op.reg.width == B64);
			emit_optional_rex(0x00, dest_op.reg.reg);
			emit_byte(0xFF);
			emit_direct(0x04, dest_op.reg.reg);
		} break;
		case OPER_MEM_REGISTER:
		{
			ASSERT(src_op.kind == OPER_NONE);
			ASSERT(dest_op.width == B64);
			ASSERT(dest_op.reg.width == B32 || dest_op.reg.width == B64);
			emit_addressing_mode(dest_op.reg.width);
			emit_optional_rex(0x00, dest_op.reg.reg);
			emit_byte(0xFF);
			emit_xm(0x04, dest_op.reg.reg);
		} break;
		case OPER_MEM_REGISTER_OFFSET:
		{
			ASSERT(src_op.kind == OPER_NONE);
			ASSERT(dest_op.width == B64);
			ASSERT(dest_op.mem_offset.base.width == B32 || dest_op.mem_offset.base.width == B64);
			emit_addressing_mode(dest_op.mem_offset.base.width);
			emit_optional_rex(0x00, dest_op.mem_offset.base.reg);
			emit_byte(0xFF);
			if (dest_op.mem_offset.offset.kind == IB)
			{
				emit_xmdb(0x04, dest_op.mem_offset.base.reg, dest_op.mem_offset.offset.ib);
			}
			else
			{
				ASSERT(dest_op.mem_offset.offset.kind == ID);
				emit_xmdd(0x04, dest_op.mem_offset.base.reg, dest_op.mem_offset.offset.id);
			}
		} break;
		case OPER_MEM_REGISTER_INDEXED:
		{
			ASSERT(src_op.kind == OPER_NONE);
			ASSERT(dest_op.width == B64);
			ASSERT(dest_op.mem_indexed.base.width == B32 || dest_op.mem_indexed.base.width == B64);
			ASSERT(dest_op.mem_indexed.base.width == dest_op.mem_indexed.index.width);
			emit_addressing_mode(dest_op.mem_offset.base.width);
			emit_optional_rex_indexed(0x00, dest_op.mem_indexed.base.reg, dest_op.mem_indexed.index.reg);
			emit_byte(0xFF);
			emit_xsib(0x04, dest_op.mem_indexed.base.reg, dest_op.mem_indexed.index.reg, dest_op.mem_indexed.scale);
		} break;
		case OPER_MEM_REGISTER_INDEXED_OFFSET:
		{
			ASSERT(src_op.kind == OPER_NONE);
			ASSERT(dest_op.width == B64);
			ASSERT(dest_op.mem_indexed_offset.base.width == B32 || dest_op.mem_indexed_offset.base.width == B64);
			ASSERT(dest_op.mem_indexed.base.width == dest_op.mem_indexed.index.width);
			emit_addressing_mode(dest_op.mem_offset.base.width);
			emit_optional_rex_indexed(0x00, dest_op.mem_indexed.base.reg, dest_op.mem_indexed.index.reg);
			emit_byte(0xFF);
			if (dest_op.mem_indexed_offset.offset.kind == IB)
			{
				emit_indirect_indexed_byte_displaced(0x04, dest_op.mem_indexed_offset.base.reg,
													dest_op.mem_indexed_offset.index.reg,
													dest_op.mem_indexed_offset.scale,
													dest_op.mem_indexed_offset.offset.ib);
			}
			else
			{
				ASSERT(dest_op.mem_indexed_offset.offset.kind == ID);
				emit_indirect_indexed_displaced(0x04, dest_op.mem_indexed_offset.base.reg,
												dest_op.mem_indexed_offset.index.reg,
												dest_op.mem_indexed_offset.scale,
												dest_op.mem_indexed_offset.offset.id);
			}
		} break;
		case OPER_LABEL:
		{
			emit_byte(0xE9);
			emit_dword(0);
		} break;
		case OPER_MEM_LABEL:
		{
			// #todo is this invalid?
			INVALID_CODE_PATH;
		} break;
	}
}
WRNG_IGNORE_POP

internal void emit_escape_op(u8 op)
{
	if (op != 0x63)
	{
		emit_byte(0x0F);
	}
	emit_byte(op);
}

void emit_escaped(instruction *instr)
{
	operand dest_op;
	operand src_op;
	u8 op_byte = 0;
	if (instr->op == MOV)
	{
		ASSERT(instr->op1.kind == OPER_CC);
		op_byte = 0x40 + instr->op1.cc;
		dest_op = instr->op2;
		src_op = instr->op3;
		ASSERT(dest_op.width == src_op.width);
	}
	else if (instr->op == MOVSX)
	{
		dest_op = instr->op1;
		src_op = instr->op2;
		op_byte = 0xBE;
		if (src_op.width == B16)
		{
			op_byte = 0xBF;
		}
		else if (src_op.width == B32)
		{
			// #note special case, no escape byte
			op_byte = 0x63;
		}
	}
	else if (instr->op == MOVZX)
	{
		dest_op = instr->op1;
		src_op = instr->op2;
		op_byte = 0xB6;
		if (src_op.width == B16)
		{
			op_byte = 0xB7;
		}
	}
	else
	{
		INVALID_CODE_PATH;
	}
	
	ASSERT(dest_op.kind == OPER_REGISTER);
	registers dest = dest_op.reg;
	// #note dest_op must be a register
	switch (src_op.kind)
	{
		case OPER_REGISTER:
		{
			registers src = src_op.reg;
			switch (dest.width)
			{
				case B16:
				{
					emit_x16_prefix();
					emit_optional_rex(dest.reg, src.reg);
				} break;
				case B32:
				{
					emit_optional_rex(dest.reg, src.reg);
				} break;
				case B64:
				{
					emit_rex(dest.reg, src.reg);
				} break;
				case B8:
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
			emit_escape_op(op_byte);
			emit_direct(dest.reg, src.reg);
		} break;
		case OPER_MEM_REGISTER:
		{
			registers base = src_op.reg;
			switch (dest.width)
			{
				case B16:
				{
					emit_x16_prefix();
					emit_addressing_mode(base.width);
					emit_optional_rex(dest.reg, base.reg);
				} break;
				case B32:
				{
					emit_addressing_mode(base.width);
					emit_optional_rex(dest.reg, base.reg);
				} break;
				case B64:
				{
					emit_addressing_mode(base.width);
					emit_rex(dest.reg, base.reg);
				} break;
				case B8:
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
			emit_escape_op(op_byte);
			emit_xm(dest.reg, base.reg);
		} break;
		case OPER_MEM_REGISTER_OFFSET:
		{
			registers base = src_op.mem_offset.base;
			immediate_op offset = src_op.mem_offset.offset;
			switch (dest.width)
			{
				case B16:
				{
					emit_x16_prefix();
					emit_addressing_mode(base.width);
					emit_optional_rex(dest.reg, base.reg);
					emit_escape_op(op_byte);
				} break;
				case B32:
				{
					emit_addressing_mode(base.width);
					emit_optional_rex(dest.reg, base.reg);
					emit_escape_op(op_byte);
				} break;
				case B64:
				{
					emit_addressing_mode(base.width);
					emit_rex(dest.reg, base.reg);
					emit_escape_op(op_byte);
				} break;
				case B8:
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
			emit_escape_op(op_byte);
			if (offset.kind == IB)
			{
				emit_xmdb(dest.reg, base.reg, offset.ib);
			}
			else
			{
				offset = promote_immediate(offset, ID);
				ASSERT(offset.kind == ID);
				emit_xmdd(dest.reg, base.reg, offset.id);
			}
		} break;
		case OPER_MEM_REGISTER_INDEXED:
		{
			registers base = src_op.mem_indexed.base;
			registers index = src_op.mem_indexed.index;
			log2_scale scale = src_op.mem_indexed.scale;
			ASSERT(base.width == index.width);
			switch (dest.width)
			{
				case B16:
				{
					emit_x16_prefix();
					emit_addressing_mode(base.width);
					emit_optional_rex_indexed(dest.reg, base.reg, index.reg);
					emit_escape_op(op_byte);
				} break;
				case B32:
				{
					emit_addressing_mode(base.width);
					emit_optional_rex_indexed(dest.reg, base.reg, index.reg);
					emit_escape_op(op_byte);
				} break;
				case B64:
				{
					emit_addressing_mode(base.width);
					emit_rex_indexed(dest.reg, base.reg, index.reg);
					emit_escape_op(op_byte);
				} break;
				case B8:
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
			emit_escape_op(op_byte);
			emit_xsib(dest.reg, base.reg, index.reg, scale);
		} break;
		case OPER_MEM_REGISTER_INDEXED_OFFSET:
		{
			registers base = src_op.mem_indexed_offset.base;
			registers index = src_op.mem_indexed_offset.index;
			log2_scale scale = src_op.mem_indexed_offset.scale;
			immediate_op offset = src_op.mem_indexed_offset.offset;
			ASSERT(base.width == index.width);
			switch (dest.width)
			{
				case B16:
				{
					emit_x16_prefix();
					emit_addressing_mode(base.width);
					emit_optional_rex_indexed(dest.reg, base.reg, index.reg);
				} break;
				case B32:
				{
					emit_addressing_mode(base.width);
					emit_optional_rex_indexed(dest.reg, base.reg, index.reg);
				} break;
				case B64:
				{
					emit_addressing_mode(base.width);
					emit_rex_indexed(dest.reg, base.reg, index.reg);
				} break;
				case B8:
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
			emit_escape_op(op_byte);
			if (offset.kind == IB)
			{
				emit_indirect_indexed_byte_displaced(dest.reg, base.reg, index.reg, scale, offset.ib);
			}
			else
			{
				offset = promote_immediate(offset, ID);
				ASSERT(offset.kind == ID);
				emit_indirect_indexed_displaced(dest.reg, base.reg, index.reg, scale, offset.id);
			}
		} break;
		case OPER_DISPLACEMENT:
		{
			u32 displacement = src_op.displacement;
			switch (dest.width)
			{
				case B16:
				{
					emit_x16_prefix();
					emit_optional_rex(dest.reg, 0x00);
				} break;
				case B32:
				{
					emit_optional_rex(dest.reg, 0x00);
				} break;
				case B64:
				{
					emit_rex(dest.reg, 0x00);
				} break;
				case B8:
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
			emit_escape_op(op_byte);
			emit_displaced(dest.reg, displacement);
		} break;
		case OPER_RIP_DISPLACEMENT:
		{
			u32 displacement = src_op.displacement;
			switch (dest.width)
			{
				case B16:
				{
					emit_x16_prefix();
					emit_optional_rex(dest.reg, 0x00);
					emit_escape_op(op_byte);
					displacement -= 8;
					if (dest.reg >= R8)
					{
						--displacement;
					}
				} break;
				case B32:
				{
					emit_optional_rex(dest.reg, 0x00);
					emit_escape_op(op_byte);
					displacement -= 7;
					if (dest.reg >= R8)
					{
						--displacement;
					}
				} break;
				case B64:
				{
					emit_rex(dest.reg, 0x00);
					emit_escape_op(op_byte);
					displacement -= 8;
				} break;
				case B8:
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
			emit_indirect_displaced_rip(dest.reg, displacement);
		} break;
		case OPER_MEM_LABEL:
		{
			u32 displacement = 0;
			switch (dest.width)
			{
				case B16:
				{
					emit_x16_prefix();
				} break;
				case B32:
				{
					emit_optional_rex(dest.reg, 0x00);
				} break;
				case B64:
				{
					emit_rex(dest.reg, 0x00);
				} break;
				case B8:
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
			emit_escape_op(op_byte);
			emit_indirect_displaced_rip(dest.reg, displacement);
		} break;
		case OPER_NONE:
		case OPER_CC:
		case OPER_IMMEDIATE:
		case OPER_LABEL:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

void emit_pop(operand src_op)
{
	if (src_op.width == B16)
	{
		emit_x16_prefix();
	}
	else
	{
		ASSERT(src_op.width == B64);
	}
	switch (src_op.kind)
	{
		case OPER_REGISTER:
		{
			registers src = src_op.reg;
			emit_optional_rex(0x00, src.reg);
			emit_byte(0x58 + (src.reg & 0x7));
		} break;
		case OPER_MEM_REGISTER:
		{
			registers base = src_op.reg;
			emit_addressing_mode(base.width);
			emit_optional_rex(0x00, base.reg);
			emit_byte(0x8F);
			emit_xm(0x00, base.reg);
		} break;
		case OPER_MEM_REGISTER_OFFSET:
		{
			registers base = src_op.mem_offset.base;
			immediate_op offset = src_op.mem_offset.offset;
			emit_addressing_mode(base.width);
			emit_optional_rex(0x00, base.reg);
			emit_byte(0x8F);
			if (offset.kind == IB)
			{
				emit_xmdb(0x00, base.reg, offset.ib);
			}
			else
			{
				offset = promote_immediate(offset, ID);
				ASSERT(offset.kind == ID);
				emit_xmdd(0x00, base.reg, offset.id);
			}
		} break;
		case OPER_MEM_REGISTER_INDEXED:
		{
			registers base = src_op.mem_indexed.base;
			registers index = src_op.mem_indexed.index;
			log2_scale scale = src_op.mem_indexed.scale;
			ASSERT(base.width == index.width);
			emit_addressing_mode(base.width);
			emit_optional_rex_indexed(0x00, base.reg, index.reg);
			emit_byte(0x8F);
			emit_xsib(0x00, base.reg, index.reg, scale);
		} break;
		case OPER_MEM_REGISTER_INDEXED_OFFSET:
		{
			registers base = src_op.mem_indexed_offset.base;
			registers index = src_op.mem_indexed_offset.index;
			log2_scale scale = src_op.mem_indexed_offset.scale;
			immediate_op offset = src_op.mem_indexed_offset.offset;
			emit_addressing_mode(base.width);
			emit_optional_rex_indexed(0x00, base.reg, index.reg);
			emit_byte(0x8F);
			if (offset.kind == IB)
			{
				emit_indirect_indexed_byte_displaced(0x00, base.reg, index.reg, scale, offset.ib);
			}
			else
			{
				offset = promote_immediate(offset, ID);
				ASSERT(offset.kind == ID);
				emit_indirect_indexed_displaced(0x00, base.reg, index.reg, scale, offset.id);
			}
		} break;
		case OPER_DISPLACEMENT:
		{
			emit_byte(0x8F);
			emit_displaced(0x00, src_op.displacement);
		} break;
		case OPER_RIP_DISPLACEMENT:
		{
			u32 displacement = src_op.displacement;
			displacement -= 6;
			if (src_op.width == B16)
			{
				--displacement;
			}
			emit_byte(0x8F);
			emit_indirect_displaced_rip(0x00, displacement);
		} break;
		case OPER_MEM_LABEL:
		{
			emit_byte(0x8F);
			emit_indirect_displaced_rip(0x00, 0);
		} break;
		case OPER_NONE:
		case OPER_IMMEDIATE:
		case OPER_CC:
		case OPER_LABEL:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

void emit_push(operand src_op)
{
	if (src_op.kind == OPER_IMMEDIATE)
	{
		immediate_op imm = src_op.imm_op;
		if (imm.kind == IB)
		{
			emit_byte(0x6A);
			emit_byte(imm.ib);
		}
		else if (imm.kind == IW)
		{
			emit_x16_prefix();
			emit_byte(0x68);
			emit_word(imm.iw);
		}
		else
		{
			ASSERT(imm.kind == ID);
			emit_byte(0x68);
			emit_dword(imm.id);
		}
	}
	else
	{
		if (src_op.width == B16)
		{
			emit_x16_prefix();
		}
		else
		{
			ASSERT(src_op.width == B64);
		}
		switch (src_op.kind)
		{
			case OPER_REGISTER:
			{
				registers src = src_op.reg;
				emit_optional_rex(0x00, src.reg);
				emit_byte(0x50 + (src.reg & 0x7));
			} break;
			case OPER_MEM_REGISTER:
			{
				registers base = src_op.reg;
				emit_addressing_mode(base.width);
				emit_optional_rex(0x00, base.reg);
				emit_byte(0xFF);
				emit_xm(0x06, base.reg);
			} break;
			case OPER_MEM_REGISTER_OFFSET:
			{
				registers base = src_op.mem_offset.base;
				immediate_op offset = src_op.mem_offset.offset;
				emit_addressing_mode(base.width);
				emit_optional_rex(0x00, base.reg);
				emit_byte(0xFF);
				if (offset.kind == IB)
				{
					emit_xmdb(0x06, base.reg, offset.ib);
				}
				else
				{
					offset = promote_immediate(offset, ID);
					ASSERT(offset.kind == ID);
					emit_xmdd(0x06, base.reg, offset.id);
				}
			} break;
			case OPER_MEM_REGISTER_INDEXED:
			{
				registers base = src_op.mem_indexed.base;
				registers index = src_op.mem_indexed.index;
				log2_scale scale = src_op.mem_indexed.scale;
				ASSERT(base.width == index.width);
				emit_addressing_mode(base.width);
				emit_optional_rex_indexed(0x00, base.reg, index.reg);
				emit_byte(0xFF);
				emit_xsib(0x06, base.reg, index.reg, scale);
			} break;
			case OPER_MEM_REGISTER_INDEXED_OFFSET:
			{
				registers base = src_op.mem_indexed_offset.base;
				registers index = src_op.mem_indexed_offset.index;
				log2_scale scale = src_op.mem_indexed_offset.scale;
				immediate_op offset = src_op.mem_indexed_offset.offset;
				ASSERT(base.width == index.width);
				if (offset.kind == IW)
				{
					offset.kind = ID;
					offset.id = (u32)(s32)offset.iw;
				}
				emit_addressing_mode(base.width);
				emit_optional_rex_indexed(0x06, base.reg, index.reg);
				emit_byte(0xFF);
				if (offset.kind == IB)
				{
					emit_indirect_indexed_byte_displaced(0x06, base.reg, index.reg, scale, offset.ib);
				}
				else
				{
					offset = promote_immediate(offset, ID);
					ASSERT(offset.kind == ID);
					emit_indirect_indexed_displaced(0x06, base.reg, index.reg, scale, offset.id);
				}
			} break;
			case OPER_DISPLACEMENT:
			{
				emit_byte(0xFF);
				emit_displaced(0x06, src_op.displacement);
			} break;
			case OPER_RIP_DISPLACEMENT:
			{
				u32 displacement = src_op.displacement;
				displacement -= 6;
				if (src_op.width == B16)
				{
					--displacement;
				}
				emit_byte(0xFF);
				emit_indirect_displaced_rip(0x06, displacement);
			} break;
			case OPER_MEM_LABEL:
			{
				emit_byte(0xFF);
				emit_indirect_displaced_rip(0x06, 0);
			} break;
			case OPER_NONE:
			case OPER_IMMEDIATE:
			case OPER_CC:
			case OPER_LABEL:
			{
				INVALID_CODE_PATH;
			} break;
		}
	}
}

void emit_ret(operand src_op)
{
	if (src_op.kind == OPER_NONE)
	{
		emit_byte(0xC3);
	}
	else
	{
		ASSERT(src_op.kind == OPER_IMMEDIATE);
		if (src_op.imm_op.kind == IB)
		{
			src_op.imm_op.kind = IW;
			src_op.imm_op.iw = (u16)src_op.imm_op.ib;
		}
		ASSERT(src_op.imm_op.kind == IW);
		emit_byte(0xC2);
		emit_word(src_op.imm_op.iw);
	}
}

void emit_shift(opcode op, operand dest_op, operand amount)
{
	u8 op_byte;
	u8 ext;
	u8 ib = 0;
	bool immediate = false;
	if (op == SAL || op == SHL)
	{
		ext = 0x04;
	}
	else if (op == SAR)
	{
		ext = 0x07;
	}
	else
	{
		ASSERT(op == SHR);
		ext = 0x05;
	}

	switch (dest_op.width)
	{
		case B8:
		{
			if (amount.kind == OPER_IMMEDIATE)
			{
				ASSERT(amount.imm_op.kind == IB);
				if (amount.imm_op.ib == 1)
				{
					op_byte = 0xD0;
				}
				else
				{
					op_byte = 0xC0;
					ib = amount.imm_op.ib;
					immediate = true;
				}
				
			}
			else
			{
				ASSERT(amount.kind == OPER_REGISTER);
				ASSERT(amount.reg.reg == CL);
				op_byte = 0xD2;
			}
		} break;
		case B32:
		case B64:
		case B16:
		{
			if (amount.kind == OPER_IMMEDIATE)
			{
				ASSERT(amount.imm_op.kind == IB);
				if (amount.imm_op.ib == 1)
				{
					op_byte = 0xD1;
				}
				else
				{
					op_byte = 0xC1;
					ib = amount.imm_op.ib;
					immediate = true;
				}
			}
			else
			{
				ASSERT(amount.kind == OPER_REGISTER);
				ASSERT(amount.reg.reg == CL);
				op_byte = 0xD3;
			}
		} break;
		case BNONE:
		{
			INVALID_CODE_PATH;
		} break;
	}

	switch (dest_op.kind)
	{
		case OPER_REGISTER:
		{
			registers dest = dest_op.reg;
			switch (dest_op.width)
			{
				case B8:
				{
					emit_optional_rex(0x00, dest.reg);
				} break;
				case B16:
				{
					emit_x16_prefix();
					emit_optional_rex(0x00, dest.reg);
				} break;
				case B32:
				{
					emit_optional_rex(0x00, dest.reg);
				} break;
				case B64:
				{
					emit_rex(0x00, dest.reg);
				} break;
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
			emit_byte(op_byte);
			emit_direct(ext, dest.reg);
		} break;
		case OPER_MEM_REGISTER:
		{
			registers base = dest_op.reg;
			switch (dest_op.width)
			{
				case B8:
				{
					emit_addressing_mode(base.width);
					emit_optional_rex(0x00, base.reg);
				} break;
				case B16:
				{
					emit_x16_prefix();
					emit_addressing_mode(base.width);
					emit_optional_rex(0x00, base.reg);
				} break;
				case B32:
				{
					emit_addressing_mode(base.width);
					emit_optional_rex(0x00, base.reg);
				} break;
				case B64:
				{
					emit_addressing_mode(base.width);
					emit_rex(0x00, base.reg);
				} break;
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
			emit_byte(op_byte);
			emit_xm(ext, base.reg);
		} break;
		case OPER_MEM_REGISTER_OFFSET:
		{
			registers base = dest_op.mem_offset.base;
			immediate_op offset = dest_op.mem_offset.offset;
			if (offset.kind == IW)
			{
				offset.kind = ID;
				offset.id = (u32)(s32)offset.iw;
			}
			switch (dest_op.width)
			{
				case B8:
				{
					emit_addressing_mode(base.width);
					emit_optional_rex(0x00, base.reg);
				} break;
				case B16:
				{
					emit_x16_prefix();
					emit_addressing_mode(base.width);
					emit_optional_rex(0x00, base.reg);
				} break;
				case B32:
				{
					emit_addressing_mode(base.width);
					emit_optional_rex(0x00, base.reg);
				} break;
				case B64:
				{
					emit_addressing_mode(base.width);
					emit_rex(0x00, base.reg);
				} break;
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
			emit_byte(op_byte);
			if (offset.kind == IB)
			{
				emit_xmdb(ext, base.reg, offset.ib);
			}
			else
			{
				offset = promote_immediate(offset, ID);
				ASSERT(offset.kind == ID);
				emit_xmdd(ext, base.reg, offset.id);
			}
		} break;
		case OPER_MEM_REGISTER_INDEXED:
		{
			registers base = dest_op.mem_indexed.base;
			registers index = dest_op.mem_indexed.index;
			log2_scale scale = dest_op.mem_indexed.scale;
			ASSERT(base.width == index.width);
			switch (dest_op.width)
			{
				case B8:
				{
					emit_addressing_mode(base.width);
					emit_optional_rex_indexed(0x00, base.reg, index.reg);
				} break;
				case B16:
				{
					emit_x16_prefix();
					emit_addressing_mode(base.width);
					emit_optional_rex_indexed(0x00, base.reg, index.reg);
				} break;
				case B32:
				{
					emit_addressing_mode(base.width);
					emit_optional_rex_indexed(0x00, base.reg, index.reg);
				} break;
				case B64:
				{
					emit_addressing_mode(base.width);
					emit_rex_indexed(0x00, base.reg, index.reg);
				} break;
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
			emit_byte(op_byte);
			emit_xsib(ext, base.reg, index.reg, scale);
		} break;
		case OPER_MEM_REGISTER_INDEXED_OFFSET:
		{
			registers base = dest_op.mem_indexed_offset.base;
			registers index = dest_op.mem_indexed_offset.index;
			log2_scale scale = dest_op.mem_indexed_offset.scale;
			immediate_op offset = dest_op.mem_indexed_offset.offset;
			ASSERT(base.width == index.width);
			switch (dest_op.width)
			{
				case B8:
				{
					emit_addressing_mode(base.width);
					emit_optional_rex_indexed(0x00, base.reg, index.reg);
				} break;
				case B16:
				{
					emit_x16_prefix();
					emit_addressing_mode(base.width);
					emit_optional_rex_indexed(0x00, base.reg, index.reg);
				} break;
				case B32:
				{
					emit_addressing_mode(base.width);
					emit_optional_rex_indexed(0x00, base.reg, index.reg);
				} break;
				case B64:
				{
					emit_addressing_mode(base.width);
					emit_rex_indexed(0x00, base.reg, index.reg);
				} break;
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
			emit_byte(op_byte);
			if (offset.kind == IB)
			{
				emit_indirect_indexed_byte_displaced(ext, base.reg, index.reg, scale, offset.ib);
			}
			else
			{
				offset = promote_immediate(offset, ID);
				ASSERT(offset.kind == ID);
				emit_indirect_indexed_displaced(ext, base.reg, index.reg, scale, offset.id);
			}
		} break;
		case OPER_DISPLACEMENT:
		{
			u32 displacement = dest_op.displacement;
			switch (dest_op.width)
			{
				case B8:
				case B32:
				{
				} break;
				case B16:
				{
					emit_x16_prefix();
				} break;
				
				case B64:
				{
					emit_rex(0x00, 0x00);
				} break;
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
			emit_byte(op_byte);
			emit_displaced(ext, displacement);
		} break;
		case OPER_RIP_DISPLACEMENT:
		{
			u32 displacement = dest_op.displacement;
			switch (dest_op.width)
			{
				case B8:
				case B32:
				{
				} break;
				case B16:
				{
					emit_x16_prefix();
					--displacement;
				} break;
				
				case B64:
				{
					emit_rex(0x00, 0x00);
					--displacement;
				} break;
				case BNONE:
				{
					INVALID_CODE_PATH;
				} break;
			}
			displacement -= 6;
			if (immediate)
			{
				--displacement;
			}
			emit_byte(op_byte);
			emit_indirect_displaced_rip(ext, displacement);
		} break;
		case OPER_MEM_LABEL:
		{
			if (dest_op.width == B16)
			{
				emit_x16_prefix();
			}
			else if (dest_op.width == B64)
			{
				emit_rex(0x00, 0x00);
			}
			emit_byte(op_byte);
			emit_indirect_displaced_rip(ext, 0);
		} break;
		case OPER_NONE:
		case OPER_IMMEDIATE:
		case OPER_CC:
		case OPER_LABEL:
		{
			INVALID_CODE_PATH;
		} break;
	}
	if (immediate)
	{
		emit_byte(ib);
	}
}

void emit_setcc(condition_codes cc, operand dest_op)
{
	ASSERT(dest_op.width == B8);
	u8 op_byte = 0x90 + cc;
	switch (dest_op.kind)
	{
		case OPER_REGISTER:
		{
			registers dest = dest_op.reg;
			emit_optional_rex(0x00, dest.reg);
			emit_escape_op(op_byte);
			emit_direct(0x00, dest.reg);
			
		} break;
		case OPER_MEM_REGISTER:
		{
			registers base = dest_op.reg;
			emit_addressing_mode(base.width);
			emit_optional_rex(0x00, base.reg);
			emit_escape_op(op_byte);
			emit_xm(0x00, base.reg);
		} break;
		case OPER_MEM_REGISTER_OFFSET:
		{
			registers base = dest_op.mem_offset.base;
			immediate_op offset = dest_op.mem_offset.offset;
			emit_addressing_mode(base.width);
			emit_optional_rex(0x00, base.reg);
			emit_escape_op(op_byte);
			if (offset.kind == IB)
			{
				emit_xmdb(0x00, base.reg, offset.ib);
			}
			else
			{
				offset = promote_immediate(offset, ID);
				ASSERT(offset.kind == ID);
				emit_xmdd(0x00, base.reg, offset.id);
			}
		} break;
		case OPER_MEM_REGISTER_INDEXED:
		{
			registers base = dest_op.mem_indexed.base;
			registers index = dest_op.mem_indexed.index;
			log2_scale scale = dest_op.mem_indexed.scale;
			emit_addressing_mode(base.width);
			emit_optional_rex_indexed(0x00, base.reg, index.reg);
			emit_escape_op(op_byte);
			emit_xsib(0x00, base.reg, index.reg, scale);
		} break;
		case OPER_MEM_REGISTER_INDEXED_OFFSET:
		{
			registers base = dest_op.mem_indexed_offset.base;
			registers index = dest_op.mem_indexed_offset.index;
			log2_scale scale = dest_op.mem_indexed_offset.scale;
			immediate_op offset = dest_op.mem_indexed_offset.offset;

			emit_addressing_mode(base.width);
			emit_optional_rex_indexed(0x00, base.reg, index.reg);
			emit_escape_op(op_byte);
			if (offset.kind == IB)
			{
				emit_indirect_indexed_byte_displaced(0x00, base.reg, index.reg, scale, offset.ib);
			}
			else
			{
				offset = promote_immediate(offset, ID);
				ASSERT(offset.kind == ID);
				emit_indirect_indexed_displaced(0x00, base.reg, index.reg, scale, offset.id);
			}
		} break;
		case OPER_DISPLACEMENT:
		{
			emit_escape_op(op_byte);
			emit_displaced(0x00, dest_op.displacement);
		} break;
		case OPER_RIP_DISPLACEMENT:
		{
			emit_escape_op(op_byte);
			emit_indirect_displaced_rip(0x00, dest_op.displacement - 7);
		} break;
		case OPER_MEM_LABEL:
		{
			emit_escape_op(op_byte);
			emit_indirect_displaced_rip(0x00, 0);
		} break;
		case OPER_NONE:
		case OPER_CC:
		case OPER_IMMEDIATE:
		case OPER_LABEL:
		{
			INVALID_CODE_PATH;
		} break;
	}
}

void emit_mov(instruction *instr)
{
	ASSERT(instr->op1.kind != OPER_NONE);
	ASSERT(instr->op2.kind != OPER_NONE);
	ASSERT(instr->op3.kind == OPER_NONE);
	operand dest_op = instr->op1;
	operand src_op = instr->op2;
	WRNG_IGNORE_PUSH(WRNG_SWITCH_ENUM)
	switch (dest_op.kind)
	{
		case OPER_REGISTER:
		{
			registers dest = dest_op.reg;
			WRNG_IGNORE_PUSH(WRNG_SWITCH_ENUM)
			switch (src_op.kind)
			{
				case OPER_REGISTER:
				{
					registers src = src_op.reg;
					if (dest.width == B8)
					{
						emit_optional_rex(dest.reg, src.reg);
						emit_byte(0x8A);
					}
					else if (dest.width == B16)
					{
						emit_x16_prefix();
						emit_optional_rex(dest.reg, src.reg);
						emit_byte(0x8B);
					}
					else if (dest.width == B32)
					{
						emit_optional_rex(dest.reg, src.reg);
						emit_byte(0x8B);
					}
					else
					{
						ASSERT(dest.width == B64);
						emit_rex(dest.reg, src.reg);
						emit_byte(0x8B);
					}
					emit_direct(dest.reg, src.reg);
				} break;
				case OPER_MEM_REGISTER:
				{
					registers base = src_op.reg;
					if (dest.width == B8)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex(dest.reg, base.reg);
						emit_byte(0x8A);
					}
					else if (dest.width == B16)
					{
						emit_x16_prefix();
						emit_addressing_mode(base.width);
						emit_optional_rex(dest.reg, base.reg);
						emit_byte(0x8B);
					}
					else if (dest.width == B32)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex(dest.reg, base.reg);
						emit_byte(0x8B);
					}
					else
					{
						ASSERT(dest.width == B64);
						emit_addressing_mode(base.width);
						emit_rex(dest.reg, base.reg);
						emit_byte(0x8B);
					}
					emit_xm(dest.reg, base.reg);
				} break;
				case OPER_MEM_REGISTER_OFFSET:
				{
					registers base = src_op.mem_offset.base;
					immediate_op offset = src_op.mem_offset.offset;
					if (dest.width == B8)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex(dest.reg, base.reg);
						emit_byte(0x8A);
					}
					else if (dest.width == B16)
					{
						emit_x16_prefix();
						emit_addressing_mode(base.width);
						emit_optional_rex(dest.reg, base.reg);
						emit_byte(0x8B);
					}
					else if (dest.width == B32)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex(dest.reg, base.reg);
						emit_byte(0x8B);
					}
					else
					{
						ASSERT(dest.width == B64);
						emit_addressing_mode(base.width);
						emit_rex(dest.reg, base.reg);
						emit_byte(0x8B);
					}
					if (offset.kind == IB)
					{
						emit_xmdb(dest.reg, base.reg, offset.ib);
					}
					else
					{
						offset = promote_immediate(offset, ID);
						emit_xmdd(dest.reg, base.reg, offset.id);
					}
				} break;
				case OPER_MEM_REGISTER_INDEXED:
				{
					registers base = src_op.mem_indexed.base;
					registers index = src_op.mem_indexed.index;
					log2_scale scale = src_op.mem_indexed.scale;
					if (dest.width == B8)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex_indexed(dest.reg, base.reg, index.reg);
						emit_byte(0x8A);
					}
					else if (dest.width == B16)
					{
						emit_x16_prefix();
						emit_addressing_mode(base.width);
						emit_optional_rex_indexed(dest.reg, base.reg, index.reg);
						emit_byte(0x8B);
					}
					else if (dest.width == B32)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex_indexed(dest.reg, base.reg, index.reg);
						emit_byte(0x8B);
					}
					else
					{
						ASSERT(dest.width == B64);
						emit_addressing_mode(base.width);
						emit_rex_indexed(dest.reg, base.reg, index.reg);
						emit_byte(0x8B);
					}
					emit_xsib(dest.reg, base.reg, index.reg, scale);
				} break;
				case OPER_MEM_REGISTER_INDEXED_OFFSET:
				{
					registers base = src_op.mem_indexed_offset.base;
					registers index = src_op.mem_indexed_offset.index;
					log2_scale scale = src_op.mem_indexed_offset.scale;
					immediate_op offset = src_op.mem_indexed_offset.offset;
					if (dest.width == B8)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex_indexed(dest.reg, base.reg, index.reg);
						emit_byte(0x8A);
					}
					else if (dest.width == B16)
					{
						emit_x16_prefix();
						emit_addressing_mode(base.width);
						emit_optional_rex_indexed(dest.reg, base.reg, index.reg);
						emit_byte(0x8B);
					}
					else if (dest.width == B32)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex_indexed(dest.reg, base.reg, index.reg);
						emit_byte(0x8B);
					}
					else
					{
						ASSERT(dest.width == B64);
						emit_addressing_mode(base.width);
						emit_rex_indexed(dest.reg, base.reg, index.reg);
						emit_byte(0x8B);
					}
					if (offset.kind == IB)
					{
						emit_indirect_indexed_byte_displaced(dest.reg, base.reg, index.reg, scale, offset.ib);
					}
					else
					{
						offset = promote_immediate(offset, ID);
						emit_indirect_indexed_displaced(dest.reg, base.reg, index.reg, scale, offset.id);
					}
					
				} break;
				case OPER_IMMEDIATE:
				{
					immediate_op imm = src_op.imm_op;
					if (dest.width == B8)
					{
						emit_optional_rex(dest.reg, 0x00);
						ASSERT(imm.kind == IB);
						emit_byte(0xB0 + (dest.reg & 0x7));
						emit_byte(imm.ib);
					}
					else if (dest.width == B16)
					{
						emit_x16_prefix();
						emit_optional_rex(dest.reg, 0x00);
						imm = promote_immediate(imm, IW);
						emit_byte(0xB8 + (dest.reg & 0x7));
						emit_word(imm.iw);
					}
					else if (dest.width == B32)
					{
						emit_optional_rex(dest.reg, 0x00);
						imm = promote_immediate(imm, ID);
						emit_byte(0xB8 + (dest.reg & 0x7));
						emit_dword(imm.id);
					}
					else
					{
						ASSERT(dest.width == B64);
						emit_rex(dest.reg, 0x00);
						if (imm.kind == IQ)
						{
							emit_byte(0xB8 + (dest.reg & 0x7));
							emit_qword(imm.iq);
						}
						else
						{
							imm = promote_immediate(imm, ID);
							emit_byte(0xC7);
							emit_direct(0x00, dest.reg);
							emit_dword(imm.id);
						}
					}
				} break;
				case OPER_LABEL:
				{
					// #todo
					UNIMPLEMENTED;
					u32 displacement = 0;
					if (dest.width == B8)
					{
					}
					else if (dest.width == B16)
					{
					}
					else if (dest.width == B32)
					{
					}
					else
					{
						ASSERT(dest.width == B64);
					}
				} break;
				default:
				{
					INVALID_CODE_PATH;
				} break;
			}
			WRNG_IGNORE_POP
		} break;
		case OPER_MEM_REGISTER:
		{
			registers base = dest_op.reg;
			WRNG_IGNORE_PUSH(WRNG_SWITCH_ENUM)
			switch (src_op.kind)
			{
				case OPER_REGISTER:
				{
					registers src = src_op.reg;
					if (dest_op.width == B8)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex(src.reg, base.reg);
						emit_byte(0x88);
					}
					else if (dest_op.width == B16)
					{
						emit_x16_prefix();
						emit_addressing_mode(base.width);
						emit_optional_rex(src.reg, base.reg);
						emit_byte(0x89);
					}
					else if (dest_op.width == B32)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex(src.reg, base.reg);
						emit_byte(0x89);
					}
					else
					{
						ASSERT(dest_op.width == B64)
						emit_addressing_mode(base.width);
						emit_rex(src.reg, base.reg);
						emit_byte(0x89);
					}
					emit_xm(src.reg, base.reg);
				} break;
				case OPER_IMMEDIATE:
				{
					immediate_op imm = src_op.imm_op;
					if (dest_op.width == B8)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex(0x00, base.reg);
						emit_byte(0xC6);
					}
					else if (dest_op.width == B16)
					{
						emit_x16_prefix();
						emit_addressing_mode(base.width);
						emit_optional_rex(0x00, base.reg);
						emit_byte(0xC7);
					}
					else if (dest_op.width == B32)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex(0x00, base.reg);
						emit_byte(0xC7);
					}
					else
					{
						ASSERT(dest_op.width == B64)
						emit_addressing_mode(base.width);
						emit_rex(0x00, base.reg);
						emit_byte(0xC7);
					}
					emit_xm(0x00, base.reg);
					if (dest_op.width == B8)
					{
						ASSERT(imm.kind == IB);
						emit_byte(imm.ib);
					}
					else if (dest_op.width == B16)
					{
						imm = promote_immediate(imm, IW);
						emit_word(imm.iw);
					}
					else
					{
						ASSERT(dest_op.width == B32 || dest_op.width == B64);
						imm = promote_immediate(imm, ID);
						emit_dword(imm.id);
					}
				} break;
				default:
				{
					INVALID_CODE_PATH;
				} break;
			}
			WRNG_IGNORE_POP
		} break;
		case OPER_MEM_REGISTER_OFFSET:
		{
			registers base = dest_op.mem_offset.base;
			immediate_op offset = dest_op.mem_offset.offset;
			WRNG_IGNORE_PUSH(WRNG_SWITCH_ENUM)
			switch (src_op.kind)
			{
				case OPER_REGISTER:
				{
					registers src = src_op.reg;
					if (dest_op.width == B8)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex(src.reg, base.reg);
						emit_byte(0x88);
					}
					else if (dest_op.width == B16)
					{
						emit_x16_prefix();
						emit_addressing_mode(base.width);
						emit_optional_rex(src.reg, base.reg);
						emit_byte(0x89);
					}
					else if (dest_op.width == B32)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex(src.reg, base.reg);
						emit_byte(0x89);
					}
					else
					{
						ASSERT(dest_op.width == B64)
						emit_addressing_mode(base.width);
						emit_rex(src.reg, base.reg);
						emit_byte(0x89);
					}
					if (offset.kind == IB)
					{
						emit_xmdb(src.reg, base.reg, offset.ib);
					}
					else
					{
						offset = promote_immediate(offset, ID);
						emit_xmdd(src.reg, base.reg, offset.id);
					}
				} break;
				case OPER_IMMEDIATE:
				{
					immediate_op imm = src_op.imm_op;
					if (dest_op.width == B8)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex(0x00, base.reg);
						emit_byte(0xC6);
					}
					else if (dest_op.width == B16)
					{
						emit_x16_prefix();
						emit_addressing_mode(base.width);
						emit_optional_rex(0x00, base.reg);
						emit_byte(0xC7);
					}
					else if (dest_op.width == B32)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex(0x00, base.reg);
						emit_byte(0xC7);
					}
					else
					{
						ASSERT(dest_op.width == B64)
						emit_addressing_mode(base.width);
						emit_rex(0x00, base.reg);
						emit_byte(0xC7);
					}
					if (offset.kind == IB)
					{
						emit_xmdb(0x00, base.reg, offset.ib);
					}
					else
					{
						offset = promote_immediate(offset, ID);
						emit_xmdd(0x00, base.reg, offset.id);
					}
					if (dest_op.width == B8)
					{
						ASSERT(imm.kind == IB);
						emit_byte(imm.ib);
					}
					else if (dest_op.width == B16)
					{
						imm = promote_immediate(imm, IW);
						emit_word(imm.iw);
					}
					else
					{
						ASSERT(dest_op.width == B32 || dest_op.width == B64);
						imm = promote_immediate(imm, ID);
						emit_dword(imm.id);
					}
				} break;
				default:
				{
					INVALID_CODE_PATH;
				} break;
			}
			WRNG_IGNORE_POP
		} break;
		case OPER_MEM_REGISTER_INDEXED:
		{
			registers base = dest_op.mem_indexed.base;
			registers index = dest_op.mem_indexed.index;
			log2_scale scale = dest_op.mem_indexed.scale;
			WRNG_IGNORE_PUSH(WRNG_SWITCH_ENUM)
			switch (src_op.kind)
			{
				case OPER_REGISTER:
				{
					registers src = src_op.reg;
					if (dest_op.width == B8)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex_indexed(src.reg, base.reg, index.reg);
						emit_byte(0x88);
					}
					else if (dest_op.width == B16)
					{
						emit_x16_prefix();
						emit_addressing_mode(base.width);
						emit_optional_rex_indexed(src.reg, base.reg, index.reg);
						emit_byte(0x89);
					}
					else if (dest_op.width == B32)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex_indexed(src.reg, base.reg, index.reg);
						emit_byte(0x89);
					}
					else
					{
						ASSERT(dest_op.width == B64)
						emit_addressing_mode(base.width);
						emit_rex_indexed(src.reg, base.reg, index.reg);
						emit_byte(0x89);
					}
					emit_xsib(src.reg, base.reg, index.reg, scale);
				} break;
				case OPER_IMMEDIATE:
				{
					immediate_op imm = src_op.imm_op;
					if (dest_op.width == B8)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex_indexed(0x00, base.reg, index.reg);
						emit_byte(0xC6);
					}
					else if (dest_op.width == B16)
					{
						emit_x16_prefix();
						emit_addressing_mode(base.width);
						emit_optional_rex_indexed(0x00, base.reg, index.reg);
						emit_byte(0xC7);
					}
					else if (dest_op.width == B32)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex_indexed(0x00, base.reg, index.reg);
						emit_byte(0xC7);
					}
					else
					{
						ASSERT(dest_op.width == B64)
						emit_addressing_mode(base.width);
						emit_rex_indexed(0x00, base.reg, index.reg);
						emit_byte(0xC7);
					}
					emit_xsib(0x00, base.reg, index.reg, scale);
					if (dest_op.width == B8)
					{
						ASSERT(imm.kind == IB);
						emit_byte(imm.ib);
					}
					else if (dest_op.width == B16)
					{
						imm = promote_immediate(imm, IW);
						emit_word(imm.iw);
					}
					else
					{
						ASSERT(dest_op.width == B32 || dest_op.width == B64);
						imm = promote_immediate(imm, ID);
						emit_dword(imm.id);
					}
				} break;
				default:
				{
					INVALID_CODE_PATH;
				} break;
			}
			WRNG_IGNORE_POP
		} break;
		case OPER_MEM_REGISTER_INDEXED_OFFSET:
		{
			registers base = dest_op.mem_indexed_offset.base;
			registers index = dest_op.mem_indexed_offset.index;
			log2_scale scale = dest_op.mem_indexed_offset.scale;
			immediate_op offset = dest_op.mem_indexed_offset.offset;
			WRNG_IGNORE_PUSH(WRNG_SWITCH_ENUM)
			switch (src_op.kind)
			{
				case OPER_REGISTER:
				{
					registers src = src_op.reg;
					if (dest_op.width == B8)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex_indexed(src.reg, base.reg, index.reg);
						emit_byte(0x88);
					}
					else if (dest_op.width == B16)
					{
						emit_x16_prefix();
						emit_addressing_mode(base.width);
						emit_optional_rex_indexed(src.reg, base.reg, index.reg);
						emit_byte(0x89);
					}
					else if (dest_op.width == B32)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex_indexed(src.reg, base.reg, index.reg);
						emit_byte(0x89);
					}
					else
					{
						ASSERT(dest_op.width == B64)
						emit_addressing_mode(base.width);
						emit_rex_indexed(src.reg, base.reg, index.reg);
						emit_byte(0x89);
					}
					if (offset.kind == IB)
					{
						emit_indirect_indexed_byte_displaced(src.reg, base.reg, index.reg, scale, offset.ib);
					}
					else
					{
						offset = promote_immediate(offset, ID);
						emit_indirect_indexed_displaced(src.reg, base.reg, index.reg, scale, offset.id);
					}
				} break;
				case OPER_IMMEDIATE:
				{
					immediate_op imm = src_op.imm_op;
					if (dest_op.width == B8)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex_indexed(0x00, base.reg, index.reg);
						emit_byte(0xC6);
					}
					else if (dest_op.width == B16)
					{
						emit_x16_prefix();
						emit_addressing_mode(base.width);
						emit_optional_rex_indexed(0x00, base.reg, index.reg);
						emit_byte(0xC7);
					}
					else if (dest_op.width == B32)
					{
						emit_addressing_mode(base.width);
						emit_optional_rex_indexed(0x00, base.reg, index.reg);
						emit_byte(0xC7);
					}
					else
					{
						ASSERT(dest_op.width == B64)
						emit_addressing_mode(base.width);
						emit_rex_indexed(0x00, base.reg, index.reg);
						emit_byte(0xC7);
					}
					if (offset.kind == IB)
					{
						emit_indirect_indexed_byte_displaced(0x00, base.reg, index.reg, scale, offset.ib);
					}
					else
					{
						emit_indirect_indexed_displaced(0x00, base.reg, index.reg, scale, offset.id);
					}
					if (dest_op.width == B8)
					{
						ASSERT(imm.kind == IB);
						emit_byte(imm.ib);
					}
					else if (dest_op.width == B16)
					{
						imm = promote_immediate(imm, IW);
						emit_word(imm.iw);
					}
					else
					{
						ASSERT(dest_op.width == B32 || dest_op.width == B64);
						imm = promote_immediate(imm, ID);
						emit_dword(imm.id);
					}
				} break;
				default:
				{
					INVALID_CODE_PATH;
				} break;
			}
			WRNG_IGNORE_POP
		} break;
		case OPER_LABEL:
		{
			// #todo
			UNIMPLEMENTED;
			WRNG_IGNORE_PUSH(WRNG_SWITCH_ENUM)
			switch (src_op.kind)
			{
				u32 displacement = 0;
				case OPER_REGISTER:
				{
					registers src = src_op.reg;
				} break;
				case OPER_IMMEDIATE:
				{
					immediate_op imm = src_op.imm_op;
				} break;
				default:
				{
					INVALID_CODE_PATH;
				} break;
			}
			WRNG_IGNORE_POP
		} break;
		default:
		{
			INVALID_CODE_PATH;
		} break;
	}
	WRNG_IGNORE_POP
}
