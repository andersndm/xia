#include "print_data.h"

#include <string.h>

#include "common/buf.h"
#include "common/color_printing.h"
#include "common/macros.h"

#include "emitter.h"
#include "registers.h"

void print_register(registers reg)
{
	switch (reg.width)
	{
		case B8:
		{
			PC_BLUE_BOLD("%s", register_x8_names[reg.reg]);
		} break;
		case B16:
		{
			PC_BLUE_BOLD("%s", register_x16_names[reg.reg]);
		} break;
		case B32:
		{
			PC_BLUE_BOLD("%s", register_x32_names[reg.reg]);
		} break;
		case B64:
		{
			PC_BLUE_BOLD("%s", register_x64_names[reg.reg]);
		} break;
		case BNONE: { INVALID_CODE_PATH; } break;
	}
}

void print_width(bit_width width)
{
	switch (width)
	{
		case B8:  { PC_GREEN_BOLD("byte "); } break;
		case B16: { PC_GREEN_BOLD("word "); } break;
		case B32: { PC_GREEN_BOLD("dword "); } break;
		case B64: { PC_GREEN_BOLD("qword "); } break;
		case BNONE: { INVALID_CODE_PATH; } break;
	}
}

void print_operand(operand oper)
{
	switch (oper.kind)
	{
		case OPER_NONE:
		{
			//PC_RED_BOLD("none\n");
			return;
		} break;
		case OPER_IMMEDIATE:
		{
			switch (oper.imm_op.kind)
			{
				case IB:
				{
					PC_WHITE_BOLD("%02xh", oper.imm_op.ib);
				} break;
				case IW:
				{
					PC_WHITE_BOLD("%04xh", oper.imm_op.iw);
				} break;
				case ID:
				{
					PC_WHITE_BOLD("%08xh", oper.imm_op.id);
				} break;
				case IQ:
				{
					PC_WHITE_BOLD("%016lxh", oper.imm_op.iq);
				} break;
			}
		} break;
		case OPER_REGISTER:
		{
			print_register(oper.reg);
		} break;
		case OPER_DISPLACEMENT:
		{
			PC_WHITE_BOLD("ds:%08xh", oper.displacement);
		} break;
		case OPER_RIP_DISPLACEMENT:
		{
			print_width(oper.width);
			printf("[");
			PC_BLUE_BOLD("rip");
			printf("+");
			PC_WHITE_BOLD("%08xh", oper.displacement);
			printf("]");
		} break;
		case OPER_CC:
		{
			PC_RED_BOLD("cc");
		} break;
		case OPER_MEM_REGISTER:
		{
			print_width(oper.width);
			printf("[");
			print_register(oper.reg);
			printf("]");
		} break;
		case OPER_MEM_REGISTER_OFFSET:
		{
			print_width(oper.width);
			printf("[");
			print_register(oper.mem_offset.base);
			if (oper.mem_offset.offset.negative)
			{
				printf("-");
			}
			else
			{
				printf("+");
			}
			switch (oper.mem_offset.offset.kind)
			{
				case IB:
				{
					u8 val = oper.mem_offset.offset.ib;
					if (oper.mem_offset.offset.negative)
					{
						val = (u8)((s8)val * -1);
					}
					PC_WHITE_BOLD("%02xh", val);
				} break;
				case IW:
				{
					immediate_op imm = promote_immediate(oper.mem_offset.offset, ID);
					u32 val = imm.id;
					if (oper.mem_offset.offset.negative)
					{
						val = (u32)((s32)val * -1);
					}
					PC_WHITE_BOLD("%08xh", val);
				} break;
				case ID:
				{
					u32 val = oper.mem_offset.offset.id;
					if (oper.mem_offset.offset.negative)
					{
						val = (u32)((s32)val * -1);
					}
					PC_WHITE_BOLD("%08xh", val);
				} break;
				case IQ:
				{
					INVALID_CODE_PATH;
				} break;
			}
			printf("]");
		} break;
		case OPER_MEM_REGISTER_INDEXED:
		{
			print_width(oper.width);
			printf("[");
			print_register(oper.mem_indexed.base);
			printf("+");
			PC_WHITE_BOLD("%d", 1 << oper.mem_indexed.scale);
			printf("*");
			print_register(oper.mem_indexed.index);
			printf("]");
		} break;
		case OPER_MEM_REGISTER_INDEXED_OFFSET:
		{
			print_width(oper.width);
			printf("[");
			print_register(oper.mem_indexed_offset.base);
			printf("+");
			PC_WHITE_BOLD("%d", 1 << oper.mem_indexed.scale);
			printf("*");
			print_register(oper.mem_indexed_offset.index);
			printf("+");
			switch (oper.mem_indexed_offset.offset.kind)
			{
				case IB:
				{
					PC_WHITE_BOLD("%02xh", oper.mem_indexed_offset.offset.ib);
				} break;
				case ID:
				{
					PC_WHITE_BOLD("%08xh", oper.mem_indexed_offset.offset.id);
				} break;
				case IW:
				case IQ:
				{
					INVALID_CODE_PATH;
				} break;
			}
			printf("]");
		} break;
		case OPER_LABEL:
		{
			PC_YELLOW_BOLD("%s", oper.label);
		} break;
		case OPER_MEM_LABEL:
		{
			printf("[");
			PC_YELLOW_BOLD("%s", oper.label);
			printf("]");
		} break;
	}
}

#define OP_NAME_WIDTH 6
void print_instruction(instruction instr, label *labels)
{
	if (instr.op == JMP && instr.op1.kind == OPER_CC)
	{
		const char *op_name = op_names[instr.op1.cc + FIRST_JCC];
		u32 len = strlen(op_name) + 1;
		PC_CYAN_BOLD("%s ", op_name);
		for (u32 i = len; i < OP_NAME_WIDTH; ++i)
		{
			printf(" ");
		}
		ASSERT(instr.op2.kind != OPER_NONE);
		print_operand(instr.op2);
		for (label *lab = labels; lab != buf_end(labels); ++lab)
		{
			//if (lab->section_relative_offset == instr.code_offset + instr.code_len + (s32)instr.op1.imm_op.id)
			if (lab->section_relative_offset == instr.op2.imm_op.id)
			{
				PC_YELLOW_BOLD("<%s>", lab->name);
				break;
			}
		}
		//PC_BLACK_BOLD(" #%x ", instr.code_offset + instr.code_len + (s32)instr.op1.imm_op.id);
		PC_BLACK_BOLD(" #%x ", instr.op2.imm_op.id);
		printf("\n");
	}
	else
	{
		const char *op_name = op_names[instr.op];
		u32 len = strlen(op_name) + 1;
		PC_CYAN_BOLD("%s ", op_name);
		for (u32 i = len; i < OP_NAME_WIDTH; ++i)
		{
			printf(" ");
		}
		if (instr.op == JMP || instr.op == CALL)
		{
			print_operand(instr.op1);
			for (label *lab = labels; lab != buf_end(labels); ++lab)
			{
				//if (lab->section_relative_offset == instr.code_offset + instr.code_len + (s32)instr.op1.imm_op.id)
				if (lab->section_relative_offset == instr.op1.imm_op.id)
				{
					PC_YELLOW_BOLD("<%s>", lab->name);
					break;
				}
			}
			//PC_BLACK_BOLD(" #%x ", instr.code_offset + instr.code_len + (s32)instr.op1.imm_op.id);
			PC_BLACK_BOLD(" #%x ", instr.op1.imm_op.id);
		}
		else
		{
			print_operand(instr.op1);
			if (instr.op2.kind != OPER_NONE)
			{
				printf(", ");
				print_operand(instr.op2);
				if (instr.op3.kind != OPER_NONE)
				{
					printf(", ");
					print_operand(instr.op3);
				}
			}
		}
		printf("\n");
	}
}
#undef OP_NAME_WIDTH

void print_instruction_bytes(instruction instr, section *sec, u8 width)
{
	u8 *data = sec->code + instr.code_offset;
	u8 num_bytes = instr.code_len;
	u8 *end = data + num_bytes;
	char buf[32];
	snprintf(buf, 32, "%% %ulx:  ", width);
	u32 count = 0;
	PC_RED_BOLD(buf, instr.code_offset);
	while (data < end && count < 8)
	{
		printf("%02x ", *data++);
		++count;
	}

	for (u32 i = count; i < 9; ++i)
	{
		printf("   ");
	}

	print_instruction(instr, sec->labels);

	if (data < end)
	{
		for (u8 i = 0; i < width; ++i)
		{
			printf(" ");
		}
		printf("   ");
		while (data < end)
		{
			printf("%02x ", *data++);
		}
		printf("\n");
	}
}

u8 get_hex_width(u64 hex)
{
	u8 result = 0;
	while (hex)
	{
		hex >>= 4;
		++result;
	}

	if (result == 0)
	{
		return 1;
	}
	return result;
}

void print_section(section *sec)
{
	PC_MAGENTA_BOLD("section: "); printf("%s\n", sec->name);
	u8 width = get_hex_width(sec->max_code_len);

	for (statement *stmt = sec->stmts; stmt != buf_end(sec->stmts); ++stmt)
	{
		switch (stmt->kind)
		{
			case STMT_DIRECTIVE:
			{
				if (stmt->dir.kind == DIR_ALIGN)
				{
					if (stmt->dir.align.bytes_written)
					{
						char buf[32];
						snprintf(buf, 32, "%% %ulx:  ", width);
						PC_RED_BOLD(buf, stmt->dir.align.code_offset);
						for (u32 i = 0; i < stmt->dir.align.bytes_written; ++i)
						{
							PC_MAGENTA("%02x ", stmt->dir.align.fill_byte);
						}
						printf("\n");
					}
				}
			} break;
			case STMT_LABEL:
			{
				printf("%016lx ", stmt->lab.section_relative_offset);
				PC_YELLOW_BOLD("<%s>", stmt->lab.name);
				printf(":\n");
			} break;
			case STMT_INSTRUCTION:
			{
				print_instruction_bytes(stmt->instr, sec, width);
			} break;
			case STMT_NONE:
			{
				INVALID_CODE_PATH;
			} break;
		}
	}
}

void print_code(void)
{
	// #todo eventually replace the width
	PC_RED("0x0000:  ");
	u32 count = 0;
	for (u8 *ptr = code; ptr != emit_ptr; ++ptr, ++count)
	{
		if (count != 0)
		{
			if (count % 16 == 0)
			{
				PC_RED("\n0x%04X:  ", count);
			}
			else if (count % 4 == 0)
			{
				printf(" ");
			}
		}
		printf("%02X ", *ptr);
	}
	printf("\n");
}
