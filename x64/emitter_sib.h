#ifndef XIA_EMITTER_SIB_H
#define XIA_EMITTER_SIB_H

#include "emitter.h"

void emit_sib(bit_width width, opcode op, registers base, registers index, log2_scale scale);
void emit_sib_r(bit_width width, opcode op, registers base, registers index, log2_scale scale,
				registers src);
void emit_sib_ix(bit_width width, opcode op, registers base, registers index, log2_scale scale,
				 immediate_op imm_op);

#endif // !XIA_EMITTER_SIB_H
