#ifndef XIA_EMITTER_M_H
#define XIA_EMITTER_M_H

#include "emitter.h"

void emit_m(bit_width width, opcode op, registers base);
void emit_m_r(bit_width width, opcode op, registers base, registers src);
void emit_m_ix(bit_width width, opcode op, registers base, immediate_op imm_op);

#endif // !XIA_EMITTER_M_H
