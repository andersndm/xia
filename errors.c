#include "errors.h"
#include "common/buf.h"
#include "common/macros.h"
#include "common/color_printing.h"

void error_handler_init(xia_error_handler *self, bool warnings_are_errors, xia_error_kind *ignored_warnings)
{
	self->error_buf = NULL;
	self->num_errors = 0;
	self->num_warnings = 0;
	self->warnings_are_errors = warnings_are_errors;
	self->ignored_warngings = ignored_warnings;
}

void error_add(xia_error_handler *self, xia_error_kind kind, printing_pos pp)
{
	xia_error error;
	error.kind = kind;
	error.pp = pp;
	error.type = XIA_ERROR_TYPE_NONE;
	error.data = NULL;
	if (kind > XIA_WARNINGS_START && kind < XIA_WARNINGS_END)
	{
		if (self->warnings_are_errors)
		{
			++self->num_errors;
		}
		else
		{
			++self->num_warnings;
		}
	}
	else if (kind > XIA_ERRORS_START)
	{
		++self->num_errors;
	}
	buf_push(self->error_buf, error);
}

void error_str_add(xia_error_handler *self, xia_error_kind kind, printing_pos pp, const char *fmt, ...)
{
	xia_error error;
	error.kind = kind;
	error.pp = pp;
	error.type = XIA_ERROR_TYPE_STRING;

	va_list args;
	va_list args_copy;
	va_copy(args_copy, args);

	va_start(args_copy, fmt);
	usize size = vsnprintf(0, 0, fmt, args_copy) + 1;
	va_end(args_copy);
	char *str = xmalloc(size);
	va_start(args, fmt);
	vsnprintf(str, size, fmt, args);
	va_end(args);
	
	error.data = str;
	if (kind > XIA_WARNINGS_START && kind < XIA_WARNINGS_END)
	{
		if (self->warnings_are_errors)
		{
			++self->num_errors;
		}
		else
		{
			++self->num_warnings;
		}
	}
	else if (kind > XIA_ERRORS_START)
	{
		++self->num_errors;
	}
	buf_push(self->error_buf, error);
}

void print_errors(xia_error_handler handler, source_base *srcbase)
{
	for (xia_error *error = handler.error_buf; error != buf_end(handler.error_buf); ++error)
	{
		if (error->kind < XIA_NOTES_END)
		{
			PC_WHITE_BOLD("note: ");
		}
		else if (error->kind > XIA_WARNINGS_START && error->kind < XIA_WARNINGS_END)
		{
			if (handler.warnings_are_errors)
			{
				PC_RED_BOLD("warning error: ");
			}
			else
			{
				PC_MAGENTA_BOLD("warning: ");
			}
		}
		else
		{
			ASSERT(error->kind > XIA_ERRORS_START)
			PC_RED_BOLD("error: ");
		}
		switch (error->kind)
		{
			case XIA_NOTE_PREVIOUS_DEFINITION:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("previous definition\n");
			} break;

			case XIA_WARNING_EXCEEDS_ALIGNMENT_CAP:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_STRING);
				printf("value exceeds alignment cap, %s\n", (char *)error->data);
			} break;
			case XIA_WARNING_REGLOBAL:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_STRING);
				printf("label %s already declared global\n", (char *)error->data);
			} break;
			case XIA_WARNING_VALUE_EXCEEDS_BYTE:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("value exceeds range of byte\n");
			} break;

			case XIA_ERROR_UNEXPECTED_TOKEN:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_STRING);
				printf("%s\n", (char *)error->data);
			} break;
			case XIA_ERROR_EXPECTED_IDENTIFIER:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("expected an identifier\n");
			} break;
			case XIA_ERROR_EXPECTED_INTEGER:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("expected an integer\n");
			} break;
			case XIA_ERROR_EXPECTED_REGISTER:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("expected a register\n");
			} break;
			case XIA_ERROR_EXPECTED_RBRACKET:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("expected a ']' token\n");
			} break;
			case XIA_ERROR_EXPECTED_PLUS_OR_RBRACKET:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("expected a '+' or ']' token\n");
			} break;
			case XIA_ERROR_EXPECTED_REGISTER_OR_INTEGER:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("expected a register or integer\n");
			} break;
			case XIA_ERROR_EXPECTED_SIZE_SPECIFIER:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("expected a size specifier\n");
			} break;
			case XIA_ERROR_EXPECTED_PLUS_OR_MINUS:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("expected a '+' or '-' token\n");
			} break;
			case XIA_ERROR_EXPECTED_REGISTER_OR_LBRACKET:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("expected a register or '[' token\n");
			} break;
			case XIA_ERROR_EXPECTED_OP:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("expected an instruction\n");
			} break;
			case XIA_ERROR_LARGER_THAN_BYTE:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("value exceeds range of byte\n");
			} break;
			case XIA_ERROR_LARGER_THAN_2_BYTES:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("value exceeds range of word\n");
			} break;
			case XIA_ERROR_LARGER_THAN_4_BYTES:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("value exceeds range of dword\n");
			} break;
			case XIA_ERROR_LARGER_THAN_8_BYTES:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("value exceeds range of qword\n");
			} break;
			case XIA_ERROR_INVALID_SCALAR:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("invalid scalar, only 1, 2, 4, and 8 are permitted\n");
			} break;
			case XIA_ERROR_INVALID_OFFSET_SIZE:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("invalid offset size, maximum allowed is 32-bits\n");
			} break;
			case XIA_ERROR_UNKNOWN_DIRECTIVE:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_STRING);
				printf("unknown directive %s\n", (char *)error->data);
			} break;
			case XIA_ERROR_LABEL_REDEFINITON:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("redefiniton of label\n");
			} break;
			case XIA_ERROR_EXPECTED_SECTION:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("expected a .section directive\n");
			} break;
			case XIA_ERROR_EXPECTED_STATEMENT:
			{
				ASSERT(error->type == XIA_ERROR_TYPE_NONE);
				printf("expected a directive, label, or instruction\n");
			} break;

			case XIA_NOTES_START:
			case XIA_WARNINGS_START:
			case XIA_ERRORS_START:
			{
				INVALID_CODE_PATH;
			} break;
		}

		print_pos(srcbase, error->pp);
	}
}

