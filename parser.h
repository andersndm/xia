#ifndef XIA_PARSER_H
#define XIA_PARSER_H

#include "srcbase.h"
#include "lexer.h"
#include "compilation_unit.h"
#include "errors.h"

typedef struct parser
{
	source_base *srcbase;
	source *src;
	lexer *lex;
	onmsg_fn onmsg;
	xia_error_handler *error_handler;

	compilation_unit unit;
} parser;

void parser_init(parser *self, source_base *srcbase, source *src, lexer *lex, xia_error_handler *error_handler);
void parse_file(parser *self);

#endif // !IA_PARSER_H
