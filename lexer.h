#ifndef XIA_LEXER_H
#define XIA_LEXER_H

#include "tokens.h"
#include "srcbase.h"

typedef struct lexer
{
	const char *str;
	token tok;

	position start_pos;
	const char *start_str;

	void *userdata;

	online_fn online;
	onstr_fn onstr;
	onidentifier_fn onidentifier;
	onmsg_fn onmsg;
} lexer;

position lexer_pos(lexer *self);
void lexer_init(lexer *self, position pos, const char *str);
void skip_to_end_of_line(lexer *self);
void lexer_start_from_pos(lexer *self, position pos);
void lexer_start_from_token(lexer *self, token tok);
bool next_token(lexer *self);

#endif // !XIA_LEXER_H
