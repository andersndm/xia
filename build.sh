#! /bin/bash

wrng="-Wall -Wextra -Wpedantic -pedantic-errors "
wrng+="-Wswitch-enum -Wswitch "
wrng+="-Wno-unused-parameter -Wno-unused-variable"

inc="-I ./ -I ../"

cflags="-g -std=gnu11 $wrng $inc"
target="xia"
cc=clang

src_list="*.c common/*.c x64/*.c elf64/*.c"
declare -a src_array=()

for src in $src_list;
do
	# echo $src
	src_array+=($src)
	$cc -c $cflags $src
done

obj_list="*.o"
$cc -o data/$target $obj_list

rm *.o

ctags ./*.h ./*.c --exclude=out_ x64/*.c
ctags -e ./*.h ./*.c --exclude=out_ x64/*.c

json_comm="$cc -c $cflags $src"

if [ -f "compile_commands.json" ]; then
	rm compile_commands.json
fi

echo "[" >> compile_commands.json
for src in "${src_array[@]}";
do
	if [ "$src" = "${src_array[0]}" ]; then
		echo "    {" >> compile_commands.json
	else
		echo "    }," >> compile_commands.json
		echo "    {" >> compile_commands.json
	fi
	echo "        \"command\": \"$cc -c $cfalgs $src\"," >> compile_commands.json
	echo "        \"directory\": \"$PWD\"," >> compile_commands.json
	echo "        \"file\": \"$src\"" >> compile_commands.json
done
echo "    }" >> compile_commands.json
echo "]" >> compile_commands.json
