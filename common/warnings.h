#ifndef IA_COMMON_WARNINGS_H
#define IA_COMMON_WARNINGS_H

#ifdef __clang__
#define WRNG_MISSING_PROTOTYPES "clang diagnostic ignored \"-Wmissing-prototypes\""
#define WRNG_NOT_INLINED "clang diagnostic ignored \"-Wunknown\""
#define WRNG_ADDRESS_OF_PACKED_MEMBER "clang diagnostic ignored \"-Waddress-of-packed-member\""
#define WRNG_UNUSED_PARAMETER "clang diagnostic ignored \"-Wunused-parameter\""
#define WRNG_UNUSED_FUNCTION "clang diagnostic ignored \"-Wunused-function\""
#define WRNG_SWITCH_ENUM "clang diagnostic ignored \"-Wswitch-enum\""
#define WRNG_SWITCH "clang diagnostic ignored \"-Wswitch\""

#define WRNG_IGNORE(wrng) _Pragma(wrng)
#define WRNG_IGNORE_PUSH(wrng) _Pragma("clang diagnostic push") _Pragma(wrng)
#define WRNG_IGNORE_POP _Pragma("clang diagnostic pop")

#else

// #todo other compilers
#define WRNG_MISSING_PROTOTYPES
#define WRNG_NOT_INLINED
#define WRNG_ADDRESS_OF_PACKED_MEMBER
#define WRNG_UNUSED_PARAMETER
#define WRNG_SWITCH_ENUM
#define WRNG_SWITCH

#define WRNG_IGNORE(wrng)
#define WRNG_IGNORE_PUSH(wrng)
#define WRNG_IGNORE_POP
#endif

#endif // !IA_COMMON_WARNINGS_H
