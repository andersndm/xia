#ifndef XI_COMMON_PROFILING_H
#define XI_COMMON_PROFILING_H

#include <x86intrin.h>

typedef struct timer_data
{
	uint64_t *next_token_times;
} timer_data;

extern timer_data __timer;

//#define TIMER_START TIMER_START_FUNCTION(__func__)
#define TIMER_START_FUNCTION(name) \
	uint64_t __##name##_timer_start = __rdtsc(); \

//#define TIMER_END TIMER_END_FUNCTION(__func__)
#define TIMER_END_FUNCTION(name) \
	uint64_t __##name##_timer_diff = __rdtsc() - __##name##_timer_start; \
	buf_push(__timer.name##_times, __##name##_timer_diff);

#endif // !XI_COMMON_PROFILING_H
