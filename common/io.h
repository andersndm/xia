#ifndef XI_COMMON_IO_H
#define XI_COMMON_IO_H

// #note optional length, if 0 will be ignored, sets to file length
char *read_file(const char *path, usize *len);
bool write_file(const char *path, const char *buf, usize len);

#endif // !XI_COMMON_IO_H
