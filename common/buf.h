#ifndef XI_COMMON_BUF_H
#define XI_COMMON_BUF_H

#include "types.h"

typedef struct buf_header
{
	//allocator *allocor;
	size_t len;
	size_t cap;
	char buffer[];
} buf_header;

#define __buf_hdr(buf) ((buf_header *)((u8 *)(buf) - offsetof(buf_header, buffer)))

#define buf_len(buf) ((buf) ? __buf_hdr(buf)->len : 0)
#define buf_cap(buf) ((buf) ? __buf_hdr(buf)->cap : 0)
#define buf_end(buf) ((buf) + buf_len(buf))
#define buf_sizeof(buf) ((buf) ? buf_len(buf) * sizeof(*buf) : 0)

#define buf_free(buf) ((buf) ? (xfree(__buf_hdr(buf)), (buf) = 0) : 0)
#define buf_fit(buf, n) ((n) <= buf_cap(buf) ? 0 : ((buf) = __buf_grow((buf), (n), sizeof(*(buf)))))
#define buf_push(buf, ...) (buf_fit((buf), 1 + buf_len(buf)), (buf)[__buf_hdr(buf)->len++] = (__VA_ARGS__))
#define buf_printf(buf, ...) ((buf) = __buf_printf((buf), __VA_ARGS__))
#define buf_clear(buf) ((buf) ? __buf_hdr(buf)->len = 0 : 0)

void *__buf_grow(const void *buf, usize new_len, usize elem_size);
char *__buf_printf(char *buf, const char *fmt, ...);

#endif // !XI_COMMON_BUF_H
