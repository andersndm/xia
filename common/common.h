#ifndef XI_COMMON_H
#define XI_COMMON_H

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif // !_CRT_SECURE_NO_WARNINGS

#define _POSIX_C_SOURCE 200809L
#define _XOPEN_SOURCE 700

#include <stdio.h>
#include <string.h>

#if 0
#include <math.h>
#include <limits.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>

#endif // 0

#include "buf.h"
#include "color_printing.h"
#include "hashmap.h"
#include "io.h"
#include "macros.h"
#include "memory.h"
#include "strings.h"
#include "types.h"
#include "warnings.h"
#include "profiling.h"

// --------------------------------------------------------------------------------------------------
// #section errors
// --------------------------------------------------------------------------------------------------
// {{{

#ifdef _WIN32
#define error_printer
#else
#define error_printer __attribute__((__format__ (__printf__, 1, 0)))
#endif

void error_printer io_error(const char *fmt, ...);
void error_printer command_line_error(const char *fmt, ...);
void noreturn error_printer fatal_error(const char *fmt, ...);
void noreturn xi_exit(int exit_code);

// }}}
// --------------------------------------------------------------------------------------------------
// #section miscellaneous
// --------------------------------------------------------------------------------------------------
// {{{

u32 safecast_s32_to_u32(s32 val);
s32 safecast_u32_to_s32(u32 val);
u64 safecast_s64_to_u64(s64 val);
s64 safecast_u64_to_s64(u64 val);

// }}}

#endif // !XI_COMMON_H
