#ifndef XI_COMMON_COLOR_PRINTING_H
#define XI_COMMON_COLOR_PRINTING_H

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
extern HANDLE console_handle;
extern WORD saved_console_attributes;
#else
#include <stdio.h>
#endif // _WIN32

typedef enum console_color
{
	CC_BLACK = 30,
	CC_RED,
	CC_GREEN,
	CC_YELLOW,
	CC_BLUE,
	CC_MAGENTA,
	CC_CYAN,
	CC_WHITE
} console_color;

#ifndef _WIN32
__attribute__((__format__ (__printf__, 3, 0)))
#endif // !_WIN32
void print_color(console_color color, int bold, const char* fmt, ...);

#define PC_BLACK(...)  print_color(CC_BLACK, 0, __VA_ARGS__)
#define PC_RED(...) print_color(CC_RED, 0, __VA_ARGS__)
#define PC_GREEN(...) print_color(CC_GREEN, 0, __VA_ARGS__)
#define PC_YELLOW(...) print_color(CC_YELLOW, 0, __VA_ARGS__)
#define PC_BLUE(...) print_color(CC_BLUE, 0, __VA_ARGS__)
#define PC_MAGENTA(...) print_color(CC_MAGENTA, 0, __VA_ARGS__)
#define PC_CYAN(...) print_color(CC_CYAN, 0, __VA_ARGS__)
#define PC_WHITE(...) print_color(CC_WHITE, 0, __VA_ARGS__)
#define PC_BLACK_BOLD(...) print_color(CC_BLACK, 1, __VA_ARGS__)
#define PC_RED_BOLD(...) print_color(CC_RED, 1, __VA_ARGS__)
#define PC_GREEN_BOLD(...) print_color(CC_GREEN, 1, __VA_ARGS__)
#define PC_YELLOW_BOLD(...) print_color(CC_YELLOW, 1, __VA_ARGS__)
#define PC_BLUE_BOLD(...) print_color(CC_BLUE, 1, __VA_ARGS__)
#define PC_MAGENTA_BOLD(...) print_color(CC_MAGENTA, 1, __VA_ARGS__)
#define PC_CYAN_BOLD(...) print_color(CC_CYAN, 1, __VA_ARGS__)
#define PC_WHITE_BOLD(...) print_color(CC_WHITE, 1, __VA_ARGS__)

#endif // !XI_COMMON_COLOR_PRINTING_H
