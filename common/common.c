#include "common.h"

#include <ctype.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

timer_data __timer;

#ifdef _WIN32
HANDLE console_handle = INVALID_HANDLE_VALUE;
WORD saved_console_attributes = 0;
#endif // _WIN32

usize next_pow2(usize n)
{
	if (n && !(n & (n - 1)))
	{
		return n;
	}

	usize count = 0;
	while (n)
	{
		n >>= 1;
		++count;
	}

	return (usize)1 << count;
}

void set_print_color(console_color color, bool bold)
{
#ifdef _WIN32
	// #todo
#else
	if (bold > 0) { printf("\x1b[%d;1m", color); }
	else { printf("\x1b[%dm", color); }
	printf("\x1b[0m");
#endif
}

#ifndef _WIN32
__attribute__((__format__ (__printf__, 3, 0)))
#endif // !_WIN32
void print_color(console_color color, int bold, const char* fmt, ...)
{
#ifdef _WIN32
	if(console_handle == INVALID_HANDLE_VALUE)
	{
		console_handle = GetStdHandle(STD_OUTPUT_HANDLE);
		CONSOLE_SCREEN_BUFFER_INFO console_info;
		GetConsoleScreenBufferInfo(console_handle, &console_info);
		saved_console_attributes = console_info.wAttributes;
	}

	WORD win32_color = 0;
	if (bold) { win32_color = FOREGROUND_INTENSITY; }
	switch (color)
	{
		case CC_BLACK:
		{
			SetConsoleTextAttribute(console_handle, win32_color);
		} break;
		case CC_RED:
		{
			win32_color |= FOREGROUND_RED;
			SetConsoleTextAttribute(console_handle, win32_color);
		} break;
		case CC_GREEN:
		{
			win32_color |= FOREGROUND_GREEN;
			SetConsoleTextAttribute(console_handle, win32_color);
		} break;
		case CC_YELLOW:
		{
			win32_color |= FOREGROUND_RED | FOREGROUND_GREEN;
			SetConsoleTextAttribute(console_handle, win32_color);
		} break;
		case CC_BLUE:
		{
			win32_color |= FOREGROUND_BLUE;
			SetConsoleTextAttribute(console_handle, win32_color);
		} break;
		case CC_MAGENTA:
		{
			win32_color |= FOREGROUND_RED | FOREGROUND_BLUE;
			SetConsoleTextAttribute(console_handle, win32_color);
		} break;
		case CC_CYAN:
		{
			win32_color |= FOREGROUND_GREEN | FOREGROUND_BLUE;
			SetConsoleTextAttribute(console_handle, win32_color);
		} break;
		case CC_WHITE:
		{
			win32_color |= FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE;
			SetConsoleTextAttribute(console_handle, win32_color);
		} break;
		default:
		{
			SetConsoleTextAttribute(console_handle, saved_console_attributes);
		} break;

		va_list args;
		va_start(args, fmt);
		vprintf(fmt, args);
		va_end(args);

		SetConsoleTextAttribute(console_handle, saved_console_attributes);
	}
#else
	if (bold > 0) { printf("\x1b[%d;1m", color); }
	else { printf("\x1b[%dm", color); }
	va_list args;
	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);
	printf("\x1b[0m");
#endif // PL_WIN
}

void noreturn xi_exit(int exit_code)
{
	if (exit_code != 0)
	{
		DEBUG_BREAK;
	}
	exit(exit_code);
}

void error_printer io_error(const char *fmt, ...)
{
	PC_WHITE_BOLD("io error: ");
	va_list args;
	va_start(args, fmt);
	vprintf(fmt, args);
	printf("\n");
	va_end(args);
}

void error_printer command_line_error(const char *fmt, ...)
{
	PC_WHITE_BOLD("command line error: ");
	va_list args;
	va_start(args, fmt);
	vprintf(fmt, args);
	printf("\n");
	va_end(args);
}

void noreturn error_printer fatal_error(const char *fmt, ...)
{
	PC_RED_BOLD("fatal: ");
	va_list args;
	va_start(args, fmt);
	vprintf(fmt, args);
	printf("\n");
	va_end(args);
	xi_exit(1);
}

void *xmalloc(usize num_bytes)
{
	void *ptr = malloc(num_bytes);
	if (!ptr)
	{
		perror("malloc failed");
		xi_exit(1);
	}
	return ptr;
}

void *xcalloc(usize num_elems, usize elem_size)
{
	void *ptr = calloc(num_elems, elem_size);
	if (!ptr)
	{
		perror("calloc failed");
		xi_exit(1);
	}
	return ptr;
}

void *xrealloc(void *ptr, usize num_bytes)
{
	ptr = realloc(ptr, num_bytes);
	if (!ptr)
	{
		perror("realloc failed");
		xi_exit(1);
	}
	return ptr;
}

void xfree(void *ptr)
{
	if (ptr)
	{
		free(ptr);
	}
	else
	{
		printf("attempted free on null pointer\n");
		xi_exit(1);
	}
}

void *memdup(void *src, usize size)
{
	void *dest = xmalloc(size);
	memcpy(dest, src, size);
	return dest;
}

void *__buf_grow(const void *buf, usize new_len, usize elem_size)
{
	ASSERT(buf_cap(buf) <= (SIZE_MAX - 1) / 2);
	usize new_cap = CLAMP_MIN(2 * buf_cap(buf), MAX(new_len, 16));
	ASSERT(new_len <= new_cap);
	ASSERT(new_cap <= (SIZE_MAX - offsetof(buf_header, buffer)) / elem_size);
	usize new_size = offsetof(buf_header, buffer) + new_cap * elem_size;

	buf_header *new_hdr;
	if (buf)
	{
		new_hdr = xrealloc(__buf_hdr(buf), new_size);
	}
	else
	{
		new_hdr = xmalloc(new_size);
		new_hdr->len = 0;
	}
	new_hdr->cap = new_cap;
	return new_hdr->buffer;
}

char *__buf_printf(char *buf, const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	usize cap = buf_cap(buf) - buf_len(buf);
	usize n = 1 + (usize)vsnprintf(buf_end(buf), cap, fmt, args);
	va_end(args);

	if (n > cap)
	{
		buf_fit(buf, n + buf_len(buf));
		va_start(args, fmt);
		usize new_cap = buf_cap(buf) - buf_len(buf);
		n = 1 + (usize)vsnprintf(buf_end(buf), new_cap, fmt, args);
		ASSERT(n <= new_cap);
		va_end(args);
	}

	__buf_hdr(buf)->len += n - 1;
	return buf;
}

memarena intern_arena;
hashmap interns;
usize intern_memory_usage;

void memarena_grow(memarena *arena, usize min_size)
{
	usize size = ALIGN_UP(CLAMP_MIN(min_size, MEMARENA_BLOCK_SIZE), MEMARENA_ALIGNMENT);
	arena->ptr = xmalloc(size);
	ASSERT(arena->ptr == ALIGN_DOWN_PTR(arena->ptr, MEMARENA_ALIGNMENT));
	arena->end = arena->ptr + size;
	buf_push(arena->blocks, arena->ptr);
}

void *memarena_alloc(memarena *arena, usize size)
{
	if (size > (usize)(arena->end - arena->ptr))
	{
		memarena_grow(arena, size);
		ASSERT(size <= (usize)(arena->end - arena->ptr));
	}
	void *ptr = arena->ptr;
	arena->ptr = ALIGN_UP_PTR(arena->ptr + size, MEMARENA_ALIGNMENT);
	ASSERT(arena->ptr <= arena->end);
	ASSERT(ptr == ALIGN_DOWN_PTR(ptr, MEMARENA_ALIGNMENT));
	return ptr;
}

void memarena_free(memarena *arena)
{
	for (u8 **it = arena->blocks; it != buf_end(arena->blocks); ++it)
	{
		free(*it);
	}
	buf_free(arena->blocks);
	arena->ptr = 0;
	arena->end = 0;
	arena->blocks = 0;
}

u64 hash_u64(u64 hash)
{
	hash ^= (hash * 0xff51afd7ed558ccd) >> 32;
	return hash;
}

u64 hash_ptr(const void *ptr)
{
	return hash_u64((uintptr_t)ptr);
}

u64 hash_mix(u64 x, u64 y)
{
	x ^= y;
	x *= 0xff51afd7ed558ccd;
	x ^= x >> 32;
	return x;
}

u64 hash_bytes(const void *ptr, usize len)
{
	u64 hash = 0xcbf29ce484222325;
	const char *buf = (const char *)ptr;
	for (usize i = 0; i < len; ++i)
	{
		hash ^= (u64)buf[i];
		hash *= 0x100000001b3;
		hash ^= hash >> 32;
	}
	return hash;
}

u64 hash_bytes_range(const char *start, const char *end)
{
	u64 hash = 0xcbf29ce484222325ull;
	while (start != end)
	{
		hash ^= (u64)*start++;
		hash *= 0x100000001b3;
		hash ^= hash >> 32;
	}
	return hash;
}

u64 map_get_u64_from_u64(hashmap *map, u64 key)
{
	if (map->len == 0)
	{
		return 0;
	}
	ASSERT(IS_POW2(map->cap));
	usize i = (usize)hash_u64(key);
	ASSERT(map->len < map->cap);
	for (;;)
	{
		i &= map->cap - 1;
		if (map->keys[i] == key)
		{
			return map->vals[i];
		}
		else if (!map->keys[i])
		{
			return 0;
		}
		++i;
	}

	return 0;
}

void map_put_u64_from_u64(hashmap *map, u64 key, u64 val);

void map_grow(hashmap *map, usize new_cap)
{
	new_cap = CLAMP_MIN(new_cap, 16);
	hashmap new_map = (hashmap)
	{
		.keys = xcalloc(new_cap, sizeof(void *)),
		.vals = xmalloc(new_cap * sizeof(void *)),
		.len = 0,
		.cap = new_cap
	};

	for (usize i = 0; i < map->cap; ++i)
	{
		if (map->keys[i])
		{
			map_put_u64_from_u64(&new_map, map->keys[i], map->vals[i]);
		}
	}

	if (map->keys)
	{
		xfree((void *)map->keys);
	}
	if (map->vals)
	{
		xfree(map->vals);
	}

	*map = new_map;
}

void map_put_u64_from_u64(hashmap *map, u64 key, u64 val)
{
	ASSERT(key);
	if (!val) { return; }
	if (2 * map->len >= map->cap)
	{
		map_grow(map, 2 * map->cap);
	}
	ASSERT(2 * map->len < map->cap);
	ASSERT(IS_POW2(map->cap));
	usize i = (usize)hash_u64(key);
	for (;;)
	{
		i &= map->cap - 1;
		if (!map->keys[i])
		{
			++map->len;
			map->keys[i] = key;
			map->vals[i] = val;
			return;
		}
		else if (map->keys[i] == key)
		{
			map->vals[i] = val;
			return;
		}
		++i;
	}
}

void *map_get(hashmap *map, const void *key)
{
	return (void *)(uintptr_t)map_get_u64_from_u64(map, (u64)(uintptr_t)key);
}

void map_put(hashmap *map, const void *key, void *val)
{
	map_put_u64_from_u64(map, (u64)(uintptr_t)key, (u64)(uintptr_t)val);
}

void *map_get_from_u64(hashmap *map, u64 key)
{
	return (void *)(uintptr_t)map_get_u64_from_u64(map, key);
}

void map_put_from_u64(hashmap *map, u64 key, void *val)
{
	map_put_u64_from_u64(map, key, (u64)(uintptr_t)val);
}

u64 map_get_u64(hashmap *map, void *key)
{
	return map_get_u64_from_u64(map, (u64)(uintptr_t)key);
}

void map_put_u64(hashmap *map, void *key, u64 val)
{
	map_put_u64_from_u64(map, (u64)(uintptr_t)key, val);
}

void map_clear(hashmap *map)
{
	if (map->keys)
	{
		xfree((void *)map->keys);
		map->keys = 0;
	}
	if (map->vals)
	{
		xfree(map->vals);
		map->vals = 0;
	}
	map->cap = 0;
	map->len = 0;
}

const char *str_intern_range(strmap *self, const char *start, const char *end)
{
	usize len = (usize)(end - start);
	u64 hash = hash_bytes_range(start, end);
	u64 key = hash ? hash : 1;
	interned_str *intern = map_get_from_u64(&self->interns, key);
	for (interned_str *it = intern; it; it = it->next)
	{
		if (it->len == len && strncmp(it->str, start, len) == 0)
		{
			return it->str;
		}
	}

	interned_str *new_intern = memarena_alloc(&self->arena, offsetof(interned_str, str) + len + 1);
	new_intern->len = len;
	new_intern->next = intern;
	memcpy(new_intern->str, start, len);
	new_intern->str[len] = 0;
	self->memory_usage += sizeof(interned_str) + len + 1 + 16; // 16 is estimate of hash table cost
 	map_put_from_u64(&self->interns, key, new_intern);

	return new_intern->str;
}

const char *str_intern(strmap *self, const char *str, u32 len)
{
	// #todo a faster implementation, not piggybacking off of ^^
	return str_intern_range(self, str, str + len);
}

void clear_interned_strs(strmap *self)
{
	memarena_free(&self->arena);
	map_clear(&self->interns);
}

bool str_islower(const char *str)
{
	while (*str)
	{
		if (isalpha(*str) && !islower(*str))
		{
			return false;
		}
		++str;
	}
	return true;
}

u32 safecast_s32_to_u32(s32 val)
{
	if (val < 0)
	{
		ASSERT(!"attempting to cast a negative s32 to u32");
	}
	return (u32)val;
}

s32 safecast_u32_to_s32(u32 val)
{
	if (val > 0x8FFFFFFF)
	{
		ASSERT(!"attempting to cast a u32 that is larger than S32_MAX to s32");
	}
	return (s32)val;
}

u64 safecast_s64_to_u64(s64 val)
{
	if (val < 0)
	{
		ASSERT(!"attempting to cast a negative s64 to u64");
	}
	return (u64)val;
}

s64 safecast_u64_to_s64(u64 val)
{
	if (val > 0x8FFFFFFFFFFFFFFF)
	{
		ASSERT(!"attempting to cast a u32 that is larger than S32_MAX to s32");
	}
	return (s32)val;
}

char *strf(const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	usize n = 1 + (usize)safecast_s32_to_u32(vsnprintf(0, 0, fmt, args));
	va_end(args);
	char *str = xmalloc(n);
	va_start(args, fmt);
	vsnprintf(str, n, fmt, args);
	va_end(args);
	return str;
}

const char *get_ext(const char *path)
{
	const char *ext = 0;
	for (; *path; ++path)
	{
		if (*path == '.')
		{
			ext = path + 1;
		}
	}
	return ext;
}

const char *replace_ext(const char *path, const char *new_ext)
{
	const char *ext = get_ext(path);
	if (!ext)
	{
		return 0;
	}
	usize base_len = safecast_s64_to_u64(ext - path);
	usize new_ext_len = strlen(new_ext);
	usize new_path_len = base_len + new_ext_len;
	char *new_path = xmalloc(new_path_len + 1);
	memcpy(new_path, path, base_len);
	memcpy(new_path + base_len, new_ext, new_ext_len);
	*(new_path + new_path_len) = 0;
	return new_path;
}

// #todo cross platform?
char *read_file(const char *path, usize *file_len)
{
#ifdef _WIN32
	FILE* fp = fopen(path, "rb");
	if (fp == NULL)
	{
		io_error("failed to open file '%s' for read", path);
		return NULL;
	}
	fseek(fp, 0, SEEK_END);
	long len = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	char* buf = xmalloc(len + 1);
	size_t n = fread(buf, len, 1, fp);
	if (len && n != 1)
	{
		fclose(fp);
		free(buf);
		return NULL;
	}
	fclose(fp);
	buf[len] = 0;
	if (file_len != NULL)
	{
		*file_len = len;
	}
	return buf;
#else
	char *result = 0;
	s32 file_ptr = open(path, O_RDONLY);
	if (file_ptr <= 0)
	{
		io_error("failed to open file '%s' for read", path);
		return 0;
	}
	struct stat file_stat = { 0 };
	fstat(file_ptr, &file_stat);
	if (file_stat.st_size <= 0)
	{
		io_error("file '%s' is empty", path);
		return "";
	}
	else
	{
		u64 size = safecast_s64_to_u64(file_stat.st_size);
		result = xmalloc(size + 1);
		ssize_t bytes_read = read(file_ptr, result, size);
		if (safecast_s64_to_u64(bytes_read) != size)
		{
			io_error("failed to read entire file, '%s'", path);
			xfree(result);
			result = 0;
		}
		else
		{
			*(result + size) = 0;
			if (file_len != NULL)
			{
				*file_len = size;
			}
		}
	}
	close(file_ptr);
	return result;
#endif
}

bool write_file(const char *path, const char *buf, usize len)
{
#ifdef _WIN32
	FILE* fp = fopen(path, "w");
	if (fp == NULL)
	{
		return false;
	}
	size_t n = fwrite(buf, len, 1, fp);
	fclose(fp);
	return n == 1;
#else
	// #todo if the entire file isn't replaced, add O_TRUNC
	// #note should set the permissions to -rw-r--r--
	s32 file_ptr = open(path, O_WRONLY | O_CREAT | O_TRUNC,
						S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (file_ptr <= 0)
	{
		io_error("failed to open file '%s' for write", path);
		return false;
	}
	bool result = true;
	ssize_t bytes_written = write(file_ptr, buf, len);
	if (safecast_s64_to_u64(bytes_written) != len)
	{
		io_error("incorrect number of bytes written to file '%s'", path);
		result = false;
	}

	close(file_ptr);
	return result;
#endif
}
