#ifndef XI_COMMON_TYPES_H
#define XI_COMMON_TYPES_H

#include <inttypes.h>
#include <stddef.h>

#define false (0 != 0)
#define true (0 == 0)

typedef uint8_t byte;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef __uint128_t u128;

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;
typedef __int128_t s128;

typedef size_t usize;

typedef int8_t bool;

typedef float f32;
typedef double f64;

#endif // !XI_COMMON_TYPES_H
