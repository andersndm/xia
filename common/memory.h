#ifndef XI_COMMON_MEMORY_H
#define XI_COMMON_MEMORY_H

void *xmalloc(usize num_bytes);
void *xcalloc(usize num_elems, usize elem_size);
void *xrealloc(void *ptr, usize num_bytes);
void xfree(void *ptr);
void *memdup(void *src, usize size);

typedef struct memarena
{
	u8 *ptr;
	u8 *end;
	u8 **blocks;
} memarena;

#define MEMARENA_ALIGNMENT 8
#define MEMARENA_BLOCK_SIZE (1024 * 1024)

void memarena_grow(memarena *arena, usize min_size);
void *memarena_alloc(memarena *arena, usize size);
void memarena_free(memarena *arena);

#if 0
typedef void *(*allocate_fn)(void *, usize, usize);
typedef void (*free_fn)(void *, void *);

typedef struct allocator
{
	allocate_fn allocate;
	free_fn free;
} allocator;

WRNG_IGNORE_PUSH(WRNG_UNUSED_PARAMETER)
void *default_allocate(void *allocor, usize size, usize align)
{
	#if 0
	void *ptr = NULL;
	int res = posix_memalign(&ptr, align, size);
	if (res == 0)
	{
		return ptr;
	}
	else
	{
		DEBUG_BREAK;
	}
	return NULL;
	#else
	void *ptr = malloc(size);
	if (ptr == NULL)
	{
		DEBUG_BREAK;
	}
	return ptr;
	#endif
}

void default_free(void *allocor, void *ptr)
{
	free(ptr);
}

void noop_free(void *data, void *ptr)
{
}
WRNG_IGNORE_POP

allocator *current_allocator = &(allocator){ default_allocate, default_free };

void *generic_allocate(allocator *allocor, usize size, usize align)
{
	if (!size)
	{
		return 0;
	}
	if (!allocor)
	{
		allocor = current_allocator;
	}
	return allocor->allocate(allocor, size, align);
}

void generic_free(allocator *allocor, void *ptr)
{
	if (!allocor)
	{
		allocor = current_allocator;
	}
	allocor->free(allocor, ptr);
}

typedef struct temp_alloctaor
{
	allocator base;
	void *start;
	void *next;
	void *end;
} temp_allocator;

typedef struct temp_mark
{
	void *ptr;
} temp_mark;

void *temp_allocate(void *allocor, usize size, usize align)
{
	temp_allocator *self = allocor;
	u64 aligned = ((uintptr_t)(self->next) + align - 1) & ~(align - 1);
	u64 next = aligned + size;
	if (next > (uintptr_t)self->end)
	{
		return 0;
	}
	self->next = (void *)next;
	return (void *)aligned;
}

temp_allocator gen_temp_allocator(void *buf, usize size)
{
	return (temp_allocator){ (allocator){ temp_allocate, noop_free }, buf, buf, (u8 *)buf + size };
}

temp_mark temp_begin(temp_allocator *self)
{
	return (temp_mark){ self->next };
}

void temp_end(temp_allocator *self, temp_mark mark)
{
	void *ptr = mark.ptr;
	ASSERT(self->start <= ptr && ptr <= self->end);
	self->next = ptr;
}

typedef struct arena_allocater
{
	allocator base;
	allocator *allocor;
	usize block_size;
	u8 *next;
	u8 *end;
	u8 *blocks[];
} arena_allocator;

const usize ARENA_MIN_BLOCK_SIZE = KILOBYTES(4);
const usize ARENA_MIN_BLOCK_ALIGN = sizeof(u64);

void *arena_allocator_grow(arena_allocator *self, usize size, usize align)
{
	usize block_size = 2 * self->block_size;
	if (block_size < size)
	{
		block_size = size;
	}

	usize block_align = ARENA_MIN_BLOCK_ALIGN;
	if (block_align < align)
	{
		block_align = align;
	}

	u8 *block = generic_allocate(self->allocor, block_size, block_align);
	if (!block)
	{
		return 0;
	}
	buf_push(self->blocks, block);
	self->block_size = block_size;
	self->next = block + size;
	self->end = block + block_size;
	return block;
}

void *arena_allocate(void *allocor, usize size, usize align)
{
	arena_allocator *self = allocor;
	usize aligned = ((uintptr_t)(self->next) + align - 1) & ~(align - 1);
	usize next = aligned + size;
	if (next > (uintptr_t)self->end)
	{
		return arena_allocator_grow(self, size, align);
	}
	self->next = (u8 *)next;
	return (void *)aligned;
}

void arena_free(arena_allocator *self)
{
	for (usize i = 0; i < buf_len(self->blocks); ++i)
	{
		generic_free(self->allocor, self->blocks[i]);
	}
	return buf_free(self->blocks);
}

arena_allocator gen_arena_allocator(void *allocor, usize min_block_size)
{
	return (arena_allocator){
		(allocator){ arena_allocate, noop_free },
		allocor, min_block_size ? min_block_size : ARENA_MIN_BLOCK_SIZE };
}

typedef enum allocator_event_kind
{
	EVENT_ALLOC,
	EVENT_FREE
} allocator_event_kind;

typedef struct allocator_event
{
	allocator_event_kind kind;
	time_t time;
	void *ptr;
	usize size;
	usize align;
} allocator_event;

typedef struct trace_allocator
{
	allocator base;
	allocator *allocor;
	allocator_event events[];
} trace_allocator;

void *trace_allocate(void *allocor, usize size, usize align)
{
	trace_allocator *self = allocor;
	void *ptr = generic_allocate(self->allocor, size, align);
	buf_push(self->events, (allocator_event){ EVENT_ALLOC, time(0), ptr, size, align });
	return ptr;
}

void trace_free(void *allocor, void *ptr)
{
	trace_allocator *self = allocor;
	generic_free(self->allocor, ptr);
	buf_push(self->events, (allocator_event){ EVENT_FREE, time(0), ptr, size, align });
	return ptr;
}
#endif

#endif // !XI_COMMON_MEMORY_H
