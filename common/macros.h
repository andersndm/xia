#ifndef XI_COMMON_MACROS_H
#define XI_COMMON_MACROS_H

//#define global static
#define internal static
#define persist static

#include "color_printing.h"

#ifdef _WIN32
#define DEBUG_BREAK __debugbreak()
#define noreturn __declspec(noreturn)
#else
#define DEBUG_BREAK __asm__ volatile("int $0x03")
#define noreturn __attribute__((__noreturn__))
#endif // _WIN32

void noreturn xi_exit(int exit_code);

#define ASSERT(expression)							\
	if(!(expression))								\
	{												\
		PC_WHITE_BOLD("%s:%d", __FILE__, __LINE__);	\
		printf(": ");								\
		PC_RED("assertion failed ");				\
		printf("in function ");						\
		PC_BLUE_BOLD("%s()\n", __func__);			\
		PC_WHITE_BOLD("expression: ");				\
		printf("(%s) == false\n", #expression);		\
		DEBUG_BREAK;								\
		xi_exit(1);									\
	}

#define INVALID_CODE_PATH							\
	PC_RED_BOLD("invalid code path reached!\n");	\
	PC_WHITE_BOLD("%s:%d\n", __FILE__, __LINE__);	\
	xi_exit(1);

#define UNIMPLEMENTED							\
	PC_MAGENTA_BOLD("unimplemented");			\
	PC_WHITE_BOLD("%s:%d", __FILE__, __LINE__); \
	xi_exit(1);

#define ARRAY_COUNT(array) (sizeof(array) / sizeof((array)[0]))

#define MIN(x, y) ((x) <= (y) ? (x) : (y))
#define MAX(x, y) ((x) >= (y) ? (x) : (y))

#define CLAMP_MIN(x, min) MAX(x, min)
#define CLAMP_MAX(x, max) MIN(x, max)

#define IS_POW2(x) (((x) != 0) && (((x) & ((x) - 1)) == 0))

// a should be align
#define ALIGN_DOWN(n, a) (u64)((n) & (u64)~((s64)(a) - 1))
#define ALIGN_UP(n, a) ALIGN_DOWN((n) + (u64)((s64)(a) - 1), (a))
#define ALIGN_DOWN_PTR(p, a) (((void *)ALIGN_DOWN((uintptr_t)(p), (a))))
#define ALIGN_UP_PTR(p, a) ((void *)ALIGN_UP((uintptr_t)(p), (a)))

#define KILOBYTES(a) (a * 1024)
#define MEGABYTES(a) (KILOBYTES(a) * 1024)
#define GIGABYTES(a) (MEGABYTES(a) * 1024)

#endif // !XI_COMMON_MACROS_H
