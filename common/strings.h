#ifndef XI_COMMON_STRINGS_H
#define XI_COMMON_STRINGS_H

#include "memory.h"
#include "hashmap.h"

typedef struct interned_str
{
	usize len;
	struct interned_str *next;
	char str[];
} interned_str;

typedef struct strmap
{
	memarena arena;
	hashmap interns;
	usize memory_usage;
} strmap;

const char *str_intern_range(strmap *self, const char *start, const char *end);
const char *str_intern(strmap *self, const char *str, u32 len);
void clear_interned_strs(strmap *self);

bool str_islower(const char *str);

#endif // !XI_COMMON_STRINGS_H
