#ifndef XI_COMMON_HASHMAP_H
#define XI_COMMON_HASHMAP_H

u64 hash_u64(u64 hash);
u64 hash_ptr(const void *ptr);
u64 hash_mix(u64 x, u64 y);
u64 hash_bytes(const void *ptr, usize len);

typedef struct hashmap
{
	u64 *keys;
	u64 *vals;
	usize len;
	usize cap;
} hashmap;

void *map_get(hashmap *map, const void *key);
void map_put(hashmap *map, const void *key, void *val);
void *map_get_from_u64(hashmap *map, u64 key);
void map_put_from_u64(hashmap *map, u64 key, void *val);
void map_put_u64(hashmap *map, void *key, u64 val);
u64 map_get_u64(hashmap *map, void *key);
void map_clear(hashmap *map);

#endif // !XI_COMMON_HASHMAP_H
