#include "directives.h"

const char *directive_names[NUM_DIRECTIVES] =
{
	[DIR_SECTION] = ".section",
	[DIR_GLOBAL] = ".global",
	[DIR_ALIGN] = ".align",

	[DIR_ASCII] = ".ascii",
	[DIR_I8] = ".i8",
	[DIR_I16] = ".i16",
	[DIR_I32] = ".i32",
	[DIR_I64] = ".i64",
};
