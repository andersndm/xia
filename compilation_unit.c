#include "compilation_unit.h"

#include "common/common.h"

#include "x64/emitter.h"
#include "x64/emit_section.h"

#include "elf64/header_info.h"

/*!
  Allocates memory in a buffer for the concatenated string that must be freed
 */
internal const char *concat(const char *a, const char *b)
{
	char *buf = NULL;
	while (*a != 0)
	{
		buf_push(buf, *a++);
	}
	while (*b != 0)
	{
		buf_push(buf, *b++);
	}
	buf_push(buf, 0);
	return buf;
}

internal void section_error(section *self, printing_pos pp, char const *fmt, ...)
{
	++self->errors;
	va_list args;
	va_start(args, fmt);
	va_list args_copy;
	va_copy(args_copy, args);
	usize size = vsnprintf(0, 0, fmt, args) + 1;
	char *str = xmalloc(size);
	vsnprintf(str, size, fmt, args_copy);
	va_end(args);
	va_end(args_copy);
	PC_RED_BOLD("error: ");
	printf("%s\n", str);
	xfree(str);
	print_pos(self->srcbase, pp);
}

label *section_get_label(section *self, const char *name)
{
	for (u32 i = 0; i < buf_len(self->labels); ++i)
	{
		if (name == self->labels[i].name)
		{
			return self->labels + i;
		}
	}
	return NULL;
}

label *sections_get_label(section *sections, const char *name)
{
	for (u32 i = 0; i < buf_len(sections); ++i)
	{
		label *l = section_get_label(sections + i, name);
		if (l != NULL)
		{
			return l;
		}
	}
	return NULL;
}

internal void section_find_label_and_relocate(section *sec, operand *oper)
{
	ASSERT(oper->kind == OPER_LABEL);
	label *l = section_get_label(sec, oper->label);
	if (l == NULL)
	{
		section_error(sec, ppos_range(oper->range), "use of undefined label");
	}
	else
	{
		oper->kind = OPER_IMMEDIATE;
		oper->imm_op.kind = ID;
		// #note the emitter will handle the relative position, so give it the absolute
		// #todo add a u64 rel immediate so that it can handle larger addresses, as
		// long as the relative position is under u32_max, or stop the emitter from
		// calculating the relative offset
		oper->imm_op.id = (u32)l->section_relative_offset;
	}
}

internal void section_reemit_label_instruction(section *sec, instruction *instr)
{
	u64 code_len = instr->code_len;
	code = sec->code;
	emit_ptr = code + instr->code_offset;
	max_code_len = sec->max_code_len;

	emit_instruction(instr);

	code = NULL;
	emit_ptr = NULL;
	max_code_len = 0;
	ASSERT(code_len == instr->code_len);
}

// #todo remove include
#include "x64/print_data.h"

void section_replace_labels(section *sec, section *sections)
{
	for (statement *stmt = sec->stmts; stmt != buf_end(sec->stmts); ++stmt)
	{
		if (stmt->kind == STMT_INSTRUCTION)
		{
			instruction *instr = &stmt->instr;
			if (instr->op == CALL)
			{
				if (instr->op1.kind == OPER_LABEL)
				{
					ASSERT(instr->op2.kind == OPER_NONE && instr->op3.kind == OPER_NONE);
					relocation rel;
					rel.kind = RELOCATION_CALL;
					rel.call.lab = section_get_label(sec, instr->op1.label);
					if (rel.call.lab == NULL)
					{
						section_error(sec, ppos_range(instr->op1.range), "unresolved label");
					}
					// #note call should have a 1-byte opcode for call rel32
					ASSERT(instr->code_len > 4);
					rel.call.offset = instr->code_offset + instr->code_len - 4;
					buf_push(sec->relocations, rel);
				}
			}
			else if (instr->op1.kind == OPER_MEM_LABEL || instr->op2.kind == OPER_MEM_LABEL)
			{
				const char *label_name = instr->op1.label;
				pos_range range = instr->op1.range;
				if (instr->op2.kind == OPER_MEM_LABEL)
				{
					label_name = instr->op2.label;
					range = instr->op2.range;
				}
				print_instruction_bytes(*instr, sec, 8);
				u64 rela_offset = instr->code_offset + instr->code_len - 4;
				relocation rel;
				rel.kind = RELOCATION_DATA;
				rel.data.lab = sections_get_label(sections, label_name);
				if (rel.data.lab == NULL)
				{
					section_error(sec, ppos_range(range), "unresolved label");
				}
				ASSERT(instr->code_len > 4);
				rel.data.offset = instr->code_offset + instr->code_len - 4;
				buf_push(sec->relocations, rel);
			}
			else
			{
				operand *oper = NULL;
				if (instr->op1.kind == OPER_LABEL)
				{
					oper = &instr->op1;
				}
				else if (instr->op2.kind == OPER_LABEL)
				{
					oper = &instr->op2;
				}
				else if (instr->op3.kind == OPER_LABEL)
				{
					oper = &instr->op3;
				}

				if (oper != NULL)
				{
					section_find_label_and_relocate(sec, oper);
					section_reemit_label_instruction(sec, instr);
				}
			}
		}
	}
}

#include "elf64/print_elf64.h"

internal u32 push_str_to_table(char **table, const char *str)
{
	if (*table == NULL)
	{
		buf_push(*table, '\0');
	}
	u32 index = buf_len(*table);
	while (*str != 0)
	{
		buf_push(*table, *str++);
	}
	buf_push(*table, '\0');
	return index;
}

internal void write_to_buffer(u8 **buffer, u8* data, usize num_bytes)
{
	for (usize i = 0; i < num_bytes; ++i)
	{
		buf_push(*buffer, *data++);
	}
}

internal elf64_symbol *create_symbol_table(compilation_unit unit, char **str_table, u32 *first_non_local)
{
	elf64_symbol *symbol_table = NULL;
	// null symbol
	elf64_symbol symbol = { 0 };
	buf_push(symbol_table, symbol);

	// section symbols
	symbol.info = (STB_LOCAL << 4) | (STT_SECTION);
	for (u32 i = 0; i < buf_len(unit.sections); ++i)
	{
		symbol.shndx = i + 1; // .text section index
		buf_push(symbol_table, symbol);
	}

	// all local symbols
	for (u32 sec_index = 0; sec_index < buf_len(unit.sections); ++sec_index)
	{
		section *sec = unit.sections + sec_index;
		for (label *l = sec->labels; l != buf_end(sec->labels); ++l)
		{
			if (!l->global)
			{
				l->symbol_index = buf_len(symbol_table);
				symbol.name = push_str_to_table(str_table, l->name);
				symbol.info = STT_NOTYPE | (STB_LOCAL << 4);
				symbol.shndx = sec_index + 1; // .text index
				symbol.value = l->section_relative_offset;
				symbol.size = 0;
				buf_push(symbol_table, symbol);
			}
		}
	}

	*first_non_local = buf_len(symbol_table);

	// all global symbols
	for (u32 sec_index = 0; sec_index < buf_len(unit.sections); ++sec_index)
	{
		section *sec = unit.sections + sec_index;
		for (label *l = sec->labels; l != buf_end(sec->labels); ++l)
		{
			if (l->global)
			{
				l->symbol_index = buf_len(symbol_table);
				symbol.name = push_str_to_table(str_table, l->name);
				symbol.info = STT_NOTYPE | (STB_GLOBAL << 4);
				symbol.shndx = sec_index + 1; // .text index
				symbol.value = l->section_relative_offset;
				symbol.size = 0;
				buf_push(symbol_table, symbol);
			}
		}
	}
	return symbol_table;
}

void compilation_unit_output_obj(compilation_unit unit, const char *out_path)
{
	u32 num_sections = buf_len(unit.sections);
	u32 current_section_index = 1; // #note skipping over null section
	elf64_section_header *section_headers = NULL;

	elf64_file_header header = { 0 };
	header.magic_number = 0x464C457F;
	header.class = ELF_CLASS_64;
	header.data = ELF_DATA_LITTLE;
	header.version = ELF_VERSION_CURRENT;
	header.osabi = ELF_OSABI_SYSV;
	header.abi_version = 0;
	header.type = ET_REL;
	header.machine = ELF_MACH_AMD64;
	header.version2 = ELF_VERSION_CURRENT;
	header.entry = 0;
	header.phoff = 0;
	header.flags = 0;
	header.ehsize = sizeof(header);
	header.phentsize = 0;
	header.phnum = 0;
	header.shoff = 0; // #note set later
	header.shentsize = sizeof(elf64_section_header);
	header.shnum = 0; // #note set later
	header.shstrndx = 0; // #note set later

	u32 current_offset = sizeof(header);

	char *str_table = NULL;
	char *sh_str_table = NULL;

	for (u32 sec_index = 0; sec_index < buf_len(unit.sections); ++sec_index)
	{
		section *sec = unit.sections + sec_index;
		elf64_section_header header;
		header.name_offset = push_str_to_table(&sh_str_table, sec->name);
		header.type = sec->type;
		header.flags = sec->flags;
		header.addr = 0;
		header.offset = current_offset;
		header.size = sec->code_len;
		header.link = 0;
		header.info = 0;
		header.addralign = 0x1;
		// #todo section should hold onto some alingment data
		if (sec->max_align)
		{
			header.addralign = sec->max_align;
		}
		header.entsize = 0;

		current_offset += header.size;
		buf_push(section_headers, header);
		++current_section_index;
	}

	for (u32 sec_index = 0; sec_index < buf_len(unit.sections); ++sec_index)
	{
		section *sec = unit.sections + sec_index;
		// #note currently the relocation section must immediately follow the relevant section, this is likely
		// not an issue and seems common, at least clang also does it
		if (sec->relocations)
		{
			// #note actual relocations are computed later, when they are being written
			elf64_section_header rela_header;
			const char *joined_str = concat(".rela", sec->name);
			rela_header.name_offset = push_str_to_table(&sh_str_table, joined_str);
			rela_header.type = SHT_RELA;
			rela_header.flags = SHF_INFO_LINK;
			rela_header.addr = 0;
			rela_header.offset = current_offset;
			rela_header.size = buf_len(sec->relocations) * sizeof(elf64_rela);
			// #todo symbol table index, maybe go through all sections and check for rela sections and set then
			rela_header.link = 0;
			rela_header.info = sec_index + 1; // #section with relocations, +1 for the null section header
			rela_header.addralign = 0x8;
			rela_header.entsize = sizeof(elf64_rela);

			buf_push(section_headers, rela_header);
			current_offset += rela_header.size;
			++current_section_index;

			buf_free(joined_str);
			joined_str = NULL;
		}
	}

	u32 first_non_local_symbol = 0;
	// #todo perhaps some structure should contain a link between sections and section headers, then
	// relocations can be added right after the section
	elf64_symbol *symbol_table = create_symbol_table(unit, &str_table, &first_non_local_symbol);

	elf64_section_header symbol_table_header;
	symbol_table_header.name_offset = push_str_to_table(&sh_str_table, ".symtab");
	symbol_table_header.type = SHT_SYMTAB;
	symbol_table_header.flags = 0;
	symbol_table_header.addr = 0;
	symbol_table_header.offset = current_offset;
	symbol_table_header.size = sizeof(elf64_symbol) * buf_len(symbol_table);
	symbol_table_header.link = current_section_index + 1;
	symbol_table_header.info = first_non_local_symbol; // index of first non-local symbol
	symbol_table_header.addralign = 0x8;
	symbol_table_header.entsize = sizeof(elf64_symbol);

	current_offset += symbol_table_header.size;

	// #note now that the symbol table section header is set, it needs to be set as the link
	// field of relocation sections
	for (elf64_section_header *sec = section_headers; sec != buf_end(section_headers); ++sec)
	{
		if (sec->type == SHT_RELA)
		{
			sec->link = current_section_index;
		}
	}

	++current_section_index;

	elf64_section_header str_table_header;
	str_table_header.name_offset = push_str_to_table(&sh_str_table, ".strtab");
	str_table_header.type = SHT_STRTAB;
	str_table_header.flags = 0;
	str_table_header.addr = 0;
	str_table_header.offset = current_offset;
	str_table_header.size = buf_len(str_table);
	str_table_header.link = 0;
	str_table_header.info = 0;
	str_table_header.addralign = 0x1;
	str_table_header.entsize = 0;

	current_offset += str_table_header.size;
	++current_section_index;

	elf64_section_header sh_str_table_header;
	sh_str_table_header.name_offset = push_str_to_table(&sh_str_table, ".shstrtab");
	sh_str_table_header.type = SHT_STRTAB;
	sh_str_table_header.flags = 0;
	sh_str_table_header.addr = 0;
	sh_str_table_header.offset = current_offset;
	sh_str_table_header.size = buf_len(sh_str_table);
	sh_str_table_header.link = 0;
	sh_str_table_header.info = 0;
	sh_str_table_header.addralign = 0x1;
	sh_str_table_header.entsize = 0;
	++current_section_index;

	current_offset += sh_str_table_header.size;

	header.shoff = current_offset;
	header.shnum = current_section_index;
	header.shstrndx = current_section_index - 1;

	u8 *file_buffer = NULL;

	// write header
	write_to_buffer(&file_buffer, (u8 *)&header, sizeof(header));
	// write section data data
	for (section *sec = unit.sections; sec != buf_end(unit.sections); ++sec)
	{
		if (sec->type != SHT_NOBITS)
		{
			write_to_buffer(&file_buffer, (u8 *)sec->code, sec->code_len);
		}
	}
	for (section *sec = unit.sections; sec != buf_end(unit.sections); ++sec)
	{
		if (sec->relocations)
		{
			// #todo handle relocation data
			elf64_rela *relas = NULL;
			for (u32 i = 0; i < buf_len(sec->relocations); ++i)
			{
				relocation rel = sec->relocations[i];
				elf64_rela rela;
				if (rel.kind == RELOCATION_CALL)
				{
					rela.offset = rel.call.offset;
					rela.info = ((u64)rel.call.lab->symbol_index << 32) | ELF_RELA_TYPE_PLT32;
					rela.addend = 0xFFFFFFFFFFFFFFFC;
				}
				else if (rel.kind == RELOCATION_DATA)
				{
					rela.offset = rel.data.offset;
					rela.info = ((u64)rel.data.lab->symbol_index << 32) | ELF_RELA_TYPE_PC32;
					rela.addend = 0xFFFFFFFFFFFFFFFC;
				}
				else
				{
					ASSERT(0);
				}
				buf_push(relas, rela);
			}
			write_to_buffer(&file_buffer, (u8 *)relas, buf_len(relas) * sizeof(elf64_rela));
			buf_free(relas);
		}
	}
	// write symbol table data
	write_to_buffer(&file_buffer, (u8 *)symbol_table, buf_len(symbol_table) * sizeof(elf64_symbol));
	// write string table data
	write_to_buffer(&file_buffer, (u8 *)str_table, buf_len(str_table));
	// write sh string table data
	write_to_buffer(&file_buffer, (u8 *)sh_str_table, buf_len(sh_str_table));

	// write null section header
	elf64_section_header null_header = { 0 };
	write_to_buffer(&file_buffer, (u8 *)&null_header, sizeof(elf64_section_header));
	// write section headers
	for (elf64_section_header *hdr = section_headers; hdr != buf_end(section_headers); ++hdr)
	{
		write_to_buffer(&file_buffer, (u8 *)hdr, sizeof(elf64_section_header));
	}
	// write symbol table section header
	write_to_buffer(&file_buffer, (u8 *)&symbol_table_header, sizeof(elf64_section_header));
	// write string table section header
	write_to_buffer(&file_buffer, (u8 *)&str_table_header, sizeof(elf64_section_header));
	// write sh string table section header
	write_to_buffer(&file_buffer, (u8 *)&sh_str_table_header, sizeof(elf64_section_header));

	write_file(out_path, (const char *)file_buffer, buf_len(file_buffer));
	printf("writing %zu bytes to %s\n", buf_len(file_buffer), out_path);

	buf_free(file_buffer);

	buf_free(str_table);
	buf_free(sh_str_table);
	buf_free(symbol_table);
}
