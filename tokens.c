#include "tokens.h"

#include "common/macros.h"

char const *token_kind_names[NUM_TOKEN_KINDS] =
{
	[TOK_EOF] = "EOF",
	[TOK_UNKNOWN] = "<unknown>",
	[TOK_NEWLINE] = "newline",

	[TOK_INTEGER] = "integer",
	[TOK_IDENTIFIER] = "identifier",
	[TOK_STRING] = "string",
	[TOK_KEYWORD] = "keyword",
	[TOK_DIRECTIVE] = "directive",
	[TOK_REGISTER] = "register",
	[TOK_OP] = "opcode",
	[TOK_LABEL] = "label",

	[TOK_PLUS] = "+",
	[TOK_MINUS] = "-",
	[TOK_MUL] = "*",

	[TOK_COLON] = ":",
	[TOK_DOLLAR] = "$",

	[TOK_LBRACKET] = "[",
	[TOK_RBRACKET] = "]",

	[TOK_COMMA] = ",",
	[TOK_DOT] = "."
};

char const *suffix_names[NUM_TOKEN_SUFFIXES] =
{
	[TOK_NONE] = "",
	[TOK_S8] = "s8",
	[TOK_U8] = "u8",
	[TOK_S16] = "s16",
	[TOK_U16] = "u16",
	[TOK_S32] = "s32",
	[TOK_U32] = "u32",
	[TOK_S64] = "s64",
	[TOK_U64] = "u64"
};

const char *token_kind_name(token_kind kind)
{
	if (kind < sizeof(token_kind_names) / sizeof(*token_kind_names))
	{
		return token_kind_names[kind];
	}
	else
	{
		ASSERT(0);
		return "<unknown>";
	}
}

const char *token_repr(token tok)
{
	if (tok.kind == TOK_IDENTIFIER && tok.tidentifier)
	{
		return tok.tidentifier;
	}
	else
	{
		return token_kind_name(tok.kind);
	}
}
