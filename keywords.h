#ifndef XIA_KEYWORDS_H
#define XIA_KEYWORDS_H

typedef enum keywords
{
	/* directives not keywords
	KEYWORD_GLOBAL,

	KEYWORD_ALIGN,
	KEYWORD_RESB,

	KEYWORD_DB,
	KEYWORD_DW,
	KEYWORD_DD,
	KEYWORD_QD,
	*/

	KEYWORD_BYTE,
	KEYWORD_WORD,
	KEYWORD_DWORD,
	KEYWORD_QWORD,

	KEYWORD_REL,

	NUM_KEYWORDS,
	KEYWORD_NONE
} keywords;

extern const char *keyword_names[NUM_KEYWORDS];

#endif // !XIA_KEYWORDS_H
