#include "lexer.h"

#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>

#include "common/memory.h"
#include "common/buf.h"

void lexer_msgv(lexer *self, msg_kind msg, printing_pos pp, char const *fmt, va_list args)
{
	if (self->onmsg)
	{
		usize size = vsnprintf(0, 0, fmt, args) + 1;
		// #todo custom allocator
		char *str = xmalloc(size);
		vsnprintf(str, size, fmt, args);
		self->onmsg(self->userdata, pp, msg, str);
		xfree(str);
	}
}

position lexer_pos(lexer *self)
{
	return self->start_pos + (self->str - self->start_str);
}

void lexer_warn(lexer *self, char const *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	lexer_msgv(self, MSG_WARNING, ppos_pos(lexer_pos(self)), fmt, args);
	va_end(args);
}

void lexer_error(lexer *self, const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	lexer_msgv(self, MSG_ERROR, ppos_pos(lexer_pos(self)), fmt, args);
	va_end(args);
}

void online(lexer *self)
{
	if (self->online)
	{
		self->online(self->userdata, lexer_pos(self) + 1);
	}
}

const char *onstr(lexer *self, const char *str, usize len)
{
	if (self->onstr)
	{
		return self->onstr(self->userdata, ppos_pos(lexer_pos(self)), str, len);
	}
	return 0;
}

const char *onidentifier(lexer *self, const char *str, usize len)
{
	if (self->onidentifier)
	{
		return self->onidentifier(self->userdata, ppos_pos(lexer_pos(self)), str, len);
	}
	return 0;
}

void lexer_init(lexer *self, position pos, const char *str)
{
	self->start_pos = pos;
	self->start_str = str;
	self->str = str;
}

void skip_to_end_of_line(lexer *self)
{
	while (*self->str != '\n' && *self->str != 0)
	{
		++self->str;
	}
	if (*self->str == 0)
	{
		self->tok.kind = TOK_EOF;
	}
	else
	{
		next_token(self);
	}
}

void lexer_start_from_pos(lexer *self, position pos)
{
	self->str = self->start_str + (pos - self->start_pos);
}

void lexer_start_from_token(lexer *self, token tok)
{
	lexer_start_from_pos(self, tok.range.start);
}

u8 char_to_digit[256] =
{
	['0'] = 0,
	['1'] = 1,
	['2'] = 2,
	['3'] = 3,
	['4'] = 4,
	['5'] = 5,
	['6'] = 6,
	['7'] = 7,
	['8'] = 8,
	['9'] = 9,
	['a'] = 10, ['A'] = 10,
	['b'] = 11, ['B'] = 11,
	['c'] = 12, ['C'] = 12,
	['d'] = 13, ['D'] = 13,
	['e'] = 14, ['E'] = 14,
	['f'] = 15, ['F'] = 15,
};

token next_int(lexer *self)
{
	const char *start = self->str;
	const char *str = self->str;
	u32 base = 10;

	if (*str == '0')
	{
		++str;
		if (tolower(*str) == 'x')
		{
			++str;
			base = 16;
			start = str;
		}
		else if (tolower(*str) == 'o')
		{
			++str;
			base = 8;
			start = str;
		}
		else if (tolower(*str) == 'b')
		{
			++str;
			base = 2;
			start = str;
		}
		else
		{
			str = start;
			// lexer_warn(self, "leading 0");
			// start = str;
		}
	}

	u64 val = 0;
	for (;;)
	{
		if (*str == '_')
		{
			++str;
			continue;
		}
		u8 digit = char_to_digit[(u8)(*str)];
		if (digit == 0 && *str != '0')
		{
			break;
		}
		if (digit >= base)
		{
			lexer_error(self, "digit '%c' out of range of base %d", *str, base);
			digit = 0;
		}
		if (val > (0xFFFFFFFFFFFFFFFF - digit) / base)
		{
			lexer_error(self, "integer literal overflow");
			if (base > 16)
			{
				while (isdigit(*str))
				{
					++str;
				}
			}
			else
			{
				while (isalnum(*str))
				{
					++str;
				}
			}
			val = 0;
			break;
		}
		val = val * base + digit;
		++str;
	}

	if (str == start)
	{
		lexer_error(self, "expected base %d digit, got '%c'", base, *str);
	}

	token_suffix suffix = TOK_NONE;
	if (*str == ':')
	{
		if (tolower(str[1]) == 'u' && str[2] == '8')
		{
			suffix = TOK_U8;
			if (val > 0xFF)
			{
				lexer_warn(self, "u8 literal overflow");
			}
			val = val & 0xFF;
			str += 3;
		}
		else if (tolower(str[1]) == 's' && str[2] == '8')
		{
			suffix = TOK_S8;
			if (val > 0xFF)
			{
				lexer_warn(self, "s8 literal overflow");
			}
			val = val & 0xFF;
			str += 3;
		}
		else if (tolower(str[1]) == 'u' && str[2] == '1' && str[3] == '6')
		{
			suffix = TOK_U16;
			if (val > 0xFFFF)
			{
				lexer_warn(self, "u16 literal overflow");
			}
			val = val & 0xFFFF;
			str += 4;
		}
		else if (tolower(str[1]) == 's' && str[2] == '1' && str[3] == '6')
		{
			suffix = TOK_S16;
			if (val > 0xFFFF)
			{
				lexer_warn(self, "s16 literal overflow");
			}
			val = val & 0xFFFF;
			str += 4;
		}
		else if (tolower(str[1]) == 'u' && str[2] == '3' && str[3] == '2')
		{
			suffix = TOK_U32;
			if (val > 0xFFFFFFFF)
			{
				lexer_warn(self, "u32 literal overflow");
			}
			val = val & 0xFFFFFFFF;
			str += 4;
		}
		else if (tolower(str[1]) == 's' && str[2] == '3' && str[3] == '2')
		{
			suffix = TOK_S32;
			if (val > 0xFFFFFFFF)
			{
				lexer_warn(self, "s32 literal overflow");
			}
			val = val & 0xFFFFFFFF;
			str += 4;
		}
		else if (tolower(str[1]) == 'u' && str[2] == '6' && str[3] == '4')
		{
			suffix = TOK_U64;
			str += 4;
		}
		else if (tolower(str[1]) == 's' && str[2] == '6' && str[3] == '4')
		{
			suffix = TOK_S64;
			str += 4;
		}
		else
		{
			++str;
			const char *suffix_start = str;
			while (isalpha(*str) || isdigit(*str))
			{
				++str;
			}
			lexer_error(self, "invalid integer suffix '*.%s'", suffix_start - str, suffix_start);
		}
	}

	if (suffix == TOK_NONE)
	{
		if (val <= 0xFF)
		{
			suffix = TOK_U8;
		}
		else if (val <= 0xFFFF)
		{
			suffix = TOK_U16;
		}
		else if (val <= 0xFFFFFFFF)
		{
			suffix = TOK_U32;
		}
		else
		{
			suffix = TOK_U64;
		}
	}

	self->str = str;
	return (token){ .kind = TOK_INTEGER, .suffix = suffix, .tint = val };
}

char skip_space(const char **str)
{
	while (isspace(**str))
	{
		if (**str == '\n')
		{
			return **str;
		}
		++*str;
	}
	return **str;
}

char escape_to_char[256] =
{
	['0'] = '\0',
	['\''] = '\'',
	['"'] = '"',
	['\\'] = '\\',
	['n'] = '\n',
	['r'] = '\r',
	['t'] = '\t',
	['v'] = '\v',
	['b'] = '\b',
	['a'] = '\a'
};

s32 next_hex_escape(lexer *self)
{
	const char *str = self->str;
	++str;
	s32 val = char_to_digit[(unsigned char)*str];
	if (val == 0 && *str != '0')
	{
		lexer_error(self, "\\x needs at least 1 hex digit");
	}
	++str;
	s32 digit = char_to_digit[(unsigned char)*str];
	if (digit || *str == '0')
	{
		val *= 16;
		val += digit;
		// #todo is it feasible that the value would be greater?
		if (val > 0xFF)
		{
			lexer_error(self, "\\x argument out of range");
			val = 0xFF;
		}
		++str;
	}
	self->str = str;
	return val;
}

token next_char(lexer *self)
{
	token tok;
	const char *str = self->str;
	++str;
	if (*str == '\'')
	{
		lexer_error(self, "character literal cannot be empty");
		++str;
	}
	else if (*str == '\n')
	{
		lexer_error(self, "character literal cannot contain newline");
	}
	else if (*str == '\\')
	{
		++str;
		if (*str == 'x')
		{
			tok.tint = next_hex_escape(self);
		}
		else
		{
			tok.tint = escape_to_char[(unsigned char)(*str)];
			if (tok.tint == 0 && *str != '0')
			{
				lexer_error(self, "invalid character literal escape '\\%c'", *str);
			}
			++str;
		}
	}
	else
	{
		tok.tint = *str;
		++str;
	}
	if (*str != '\'')
	{
		lexer_error(self, "expected closing character quote, got '%'", *str);
	}
	else
	{
		++str;
	}
	self->str = str;
	tok.kind = TOK_INTEGER;
	tok.suffix = TOK_CHAR;
	return tok;
}

token next_str(lexer *self)
{
	const char *str = self->str;
	++str;
	char *buf = 0;
	while (*str && *str != '"')
	{
		char val = *str;
		if (val == '\n')
		{
			lexer_error(self, "string literal cannot contain newline");
			break;
		}
		else if (val == '\\')
		{
			++str;
			if (*str == 'x')
			{
				val = next_hex_escape(self);
			}
			else
			{
				val = escape_to_char[(unsigned char)*str];
				if (val == 0 && *str != '0')
				{
					lexer_error(self, "invalid string literal escape '\\%c'", *str);
				}
				++str;
			}
		}
		else
		{
			++str;
		}
		buf_push(buf, val);
	}
	if (*str)
	{
		++str;
	}
	else
	{
		lexer_error(self, "unexpected end of file within string literal");
	}
	buf_push(buf, 0);
	self->str = str;
	token tok;
	tok.kind = TOK_STRING;
	tok.tstr = onstr(self, buf, buf_len(buf) - 1);
	buf_free(buf);
	return tok;
}

bool next_token(lexer *self)
{
	token tok;
repeat: ;
	const char *start = self->str;
	switch (*self->str)
	{
		case 0:
		{
			tok.kind = TOK_EOF;
		} break;
		case '\n':
		{
			tok.kind = TOK_NEWLINE;
			online(self);
			++self->str;
			while (skip_space(&self->str) == '\n')
			{
				online(self);
				++self->str;
			}
		} break;
		case ' ': case '\r': case '\t': case '\v':
		{
			skip_space(&self->str);
			goto repeat;
		} break;
		case '\'':
		{
			tok = next_char(self);
		}
		case '"':
		{
			tok = next_str(self);
		} break;
		//case '.': { tok.kind = TOK_DOT;       ++self->str; } break;
		case ',': { tok.kind = TOK_COMMA;     ++self->str; } break;
		case '+': { tok.kind = TOK_PLUS;      ++self->str; } break;
		case '-': { tok.kind = TOK_MINUS;     ++self->str; } break;
		case '*': { tok.kind = TOK_MUL;       ++self->str; } break;
		case '[': { tok.kind = TOK_LBRACKET;  ++self->str; } break;
		case ']': { tok.kind = TOK_RBRACKET;  ++self->str; } break;
		case ':': { tok.kind = TOK_COLON;     ++self->str; } break;
		// #todo may end up using $$
		//case '$': { tok.kind = TOK_DOLLAR;    ++self->str; } break;
		case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
		{
			tok = next_int(self);
		} break;
		case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j':
		case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't':
		case 'u': case 'v': case 'w': case 'x': case 'y': case 'z':
		case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J':
		case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T':
		case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z':
		case '_': case '.':
		{
			while (isalnum(*self->str) || *self->str == '_' || *self->str == '.')
			{
				++self->str;
			}
			tok.tidentifier = onidentifier(self, start, self->str - start);
			// #hack
			tok.kind = self->tok.kind;
		} break;
		case ';':
		{
			while (*self->str && *self->str != '\n')
			{
				++self->str;
			}
			if (self->str == 0)
			{
				tok.kind = TOK_EOF;
			}
			else
			{
				++self->str;
				online(self);
				tok.kind = TOK_NEWLINE;
			}
		} break;
		default:
		{
			lexer_error(self, "invalid character '%c'", *self->str);
			++self->str;
			goto repeat;
		} break;
	}
	tok.range = (pos_range){ self->start_pos + (start - self->start_str), lexer_pos(self) };
	self->tok = tok;
	return self->tok.kind != TOK_EOF;
}
