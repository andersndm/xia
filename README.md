xia is toy 64-bit assembler written in c. It should support all user-level non-floating-point instructions. Only supports intel syntax. Example input files can be found in the `data` directory.  

Note: xia will only output object files, which must be manually linked into an executable or library.

# Build
Building has only been tested on Linux, but *should* compile without issue on macOS.
In the main directory simply run the `build.sh` script to compile.

# Running
```
usage: xia [flags, output_file] <input_file>
flags:
 -version                          print assembler version
 -code                             print the generated executable data
 -pinstr                           print the generated instructions
 -o <file>                         output file (default: out_<input_file>.o)
 ```
