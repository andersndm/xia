#ifndef XIA_ERRORS_H
#define XIA_ERRORS_H

#include "tokens.h"
#include "srcbase.h"

typedef enum xia_error_kind
{
	XIA_NOTES_START,

	// #note parser notes
	XIA_NOTE_PREVIOUS_DEFINITION,

	XIA_NOTES_END,
	XIA_WARNINGS_START = XIA_NOTES_END,

	// #note parser warnings
	XIA_WARNING_EXCEEDS_ALIGNMENT_CAP,
	XIA_WARNING_REGLOBAL,
	XIA_WARNING_VALUE_EXCEEDS_BYTE,

	XIA_WARNINGS_END,
	XIA_ERRORS_START = XIA_WARNINGS_END,

	XIA_ERROR_UNEXPECTED_TOKEN,

	XIA_ERROR_EXPECTED_IDENTIFIER,
	XIA_ERROR_EXPECTED_INTEGER,
	XIA_ERROR_EXPECTED_REGISTER,
	XIA_ERROR_EXPECTED_RBRACKET,
	XIA_ERROR_EXPECTED_PLUS_OR_RBRACKET,
	XIA_ERROR_EXPECTED_REGISTER_OR_INTEGER,
	XIA_ERROR_EXPECTED_SIZE_SPECIFIER,
	XIA_ERROR_EXPECTED_PLUS_OR_MINUS,
	XIA_ERROR_EXPECTED_REGISTER_OR_LBRACKET,
	XIA_ERROR_EXPECTED_OP,

	XIA_ERROR_LARGER_THAN_BYTE,
	XIA_ERROR_LARGER_THAN_2_BYTES,
	XIA_ERROR_LARGER_THAN_4_BYTES,
	XIA_ERROR_LARGER_THAN_8_BYTES,

	XIA_ERROR_INVALID_SCALAR,
	XIA_ERROR_INVALID_OFFSET_SIZE,

	XIA_ERROR_UNKNOWN_DIRECTIVE,
	XIA_ERROR_LABEL_REDEFINITON,
	XIA_ERROR_EXPECTED_SECTION,
	XIA_ERROR_EXPECTED_STATEMENT,

	// #note parser errors
	
} xia_error_kind;

typedef enum xia_error_type
{
	XIA_ERROR_TYPE_NONE,
	XIA_ERROR_TYPE_STRING,
} xia_error_type;

typedef struct xia_error
{
	xia_error_kind kind;
	xia_error_type type;
	printing_pos pp;
	// #todo switch to strings if no other data seems needed
	void *data;
} xia_error;

typedef struct xia_error_handler
{
	xia_error *error_buf;
	u32 num_errors;
	u32 num_warnings;
	bool warnings_are_errors;
	xia_error_kind *ignored_warngings;
} xia_error_handler;

void error_handler_init(xia_error_handler *self, bool warnings_are_errors, xia_error_kind *ignored_warnings);

void error_add(xia_error_handler *self, xia_error_kind kind, printing_pos pp);
void error_str_add(xia_error_handler *self, xia_error_kind kind, printing_pos pp, const char *fmt, ...);

void print_errors(xia_error_handler handler, source_base *srcbase);

#endif // !XIA_ERRORS_H
