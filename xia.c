#include "common/common.h"

#include "xia.h"
#include "errors.h"
#include "srcbase.h"
#include "lexer.h"
#include "parser.h"
#include "analyzer.h"
#include "x64/x64.h"

/* #todo
   
   add x8 instructions
   add x16 instructions
   add x32 instructions

   add more opcodes

   add flags, use bitwise system for flags

   use labels and relocations
   - all global labels, "label" could have a separate code section, with sub-labels occluded,
     i.e. "label.end", allow for '.' in labels or identifiers
	 
   - or add a global keyword

   output .o file, or executable
   
   add DWARF
   
   use qword, dword, word, byte for ptrs
   
   performance analysis
   
   add keywords
   
   semantic checking, unless the emitter should just handle it.
 */

#define MAX_PATH 512

#define XIA_VERSION_MAJOR "0"
#define XIA_VERSION_MINOR "1"
#define XIA_VERSION_PATCH "0"

void print_version(void)
{
	printf("xia: xi-assembler version %s.%s.%s\n",
		   XIA_VERSION_MAJOR, XIA_VERSION_MINOR, XIA_VERSION_PATCH);
}

bool flag_version;
bool flag_code;
bool flag_instructions;
//bool flag_parse_obj;

typedef enum flag_kind
{
	FLAG_BOOL,
	FLAG_STR,
	FLAG_ENUM
} flag_kind;

typedef struct flag_def
{
	flag_kind kind;
	const char *name;
	const char *help;
	const char *arg_name;
	const char **options;
	int num_options;
	union
	{
		int *i;
		bool *b;
		const char **s;
	} ptr;
} flag_def;

flag_def *flag_defs;

void path_normalize(char *path)
{
	char *ptr;
	for (ptr = path; *ptr; ++ptr)
	{
		if (*ptr == '\\')
		{
			*ptr = '/';
		}
	}
	if (ptr != path && ptr[-1] == '/')
	{
		ptr[-1] = 0;
	}
}

void path_copy(char path[MAX_PATH], const char *src)
{
	strncpy(path, src, MAX_PATH);
	path[MAX_PATH - 1] = 0;
	path_normalize(path);
}

void path_join(char path[MAX_PATH], const char *src)
{
	char *ptr = path + strlen(path);
	if (ptr != path && ptr[-1] == '/')
	{
		--ptr;
	}
	if (*src == '/')
	{
		++src;
	}
	snprintf(ptr, (unsigned long)(path - ptr) + MAX_PATH, "/%s", src);
}

char *path_file(char path[MAX_PATH])
{
	path_normalize(path);
	for (char *ptr = path + strlen(path); ptr != path; --ptr)
	{
		if (ptr[-1] == '/')
		{
			return ptr;
		}
	}
	return path;
}

char *path_ext(char path[MAX_PATH])
{
	for (char *ptr = path + strlen(path); ptr != path; --ptr)
	{
		if (ptr[-1] == '.')
		{
			return ptr;
		}
	}
	return path;
}

void add_flag(const char *name, bool *ptr, const char *help)
{
	buf_push(flag_defs, (flag_def){ .kind = FLAG_BOOL, .name = name, .help = help, .ptr.b = ptr });
}

void add_flag_str(const char *name, const char **ptr, const char *arg_name, const char *help)
{
	buf_push(flag_defs, (flag_def){ .kind = FLAG_STR, .name = name, .arg_name = arg_name, .help = help, .ptr.s = ptr });
}

void add_flag_enum(const char *name, int *ptr, const char *help, const char **options, int num_options)
{
	buf_push(flag_defs, (flag_def){ .kind = FLAG_ENUM, .name = name, .help = help, .ptr.i = ptr, .options = options,
			.num_options = num_options });
}

flag_def *get_flag_def(const char *name)
{
	for (usize i = 0; i < buf_len(flag_defs); ++i)
	{
		if (strcmp(flag_defs[i].name, name) == 0)
		{
			return &flag_defs[i];
		}
	}
	return 0;
}

void print_flags_usage(void)
{
	printf("flags:\n");
	for (usize i = 0; i < buf_len(flag_defs); ++i)
	{
		flag_def flag = flag_defs[i];
		char note[256] = {0};
		char format[256];
		switch (flag.kind)
		{
			case FLAG_STR:
			{
				snprintf(format, sizeof(format), "%s <%s>", flag.name, flag.arg_name ? flag.arg_name : "value");
				if (*flag.ptr.s)
				{
					snprintf(note, sizeof(note), "(default: %s)", *flag.ptr.s);
				}
			} break;
			case FLAG_ENUM:
			{
				char *end = format + sizeof(format);
				char *ptr = format;
				ptr += snprintf(ptr, (unsigned long)(end - ptr), "%s <", flag.name);
				for (int k = 0; k < flag.num_options; ++k)
				{
					ptr += snprintf(ptr, (unsigned long)(end - ptr), "%s%s", k == 0 ? "" : "|", flag.options[k]);
					if (k == *flag.ptr.i)
					{
						snprintf(note, sizeof(note), " (default: %s)", flag.options[k]);
					}
				}
				snprintf(ptr, (unsigned long)(end - ptr), ">");
			} break;
			case FLAG_BOOL:
			{
				snprintf(format, sizeof(format), "%s", flag.name);
			} break;
		}
		printf(" -%-32s %s%s\n", format, flag.help ? flag.help : "", note);
	}
}

const char *parse_flags(int *argc_ptr, const char ***argv_ptr)
{
	int argc = *argc_ptr;
	const char **argv = *argv_ptr;
	int i;
	for (i = 1; i < argc; ++i)
	{
		const char *arg = argv[i];
		const char *name = arg;
		if (*name == '-')
		{
			++name;
			if (*name == '-')
			{
				++name;
			}
			flag_def *flag = get_flag_def(name);
			if (!flag)
			{
				command_line_error("unknown flag: %s\n", arg);
			}
			switch (flag->kind)
			{
				case FLAG_BOOL:
				{
					*flag->ptr.b = true;
				} break;
				case FLAG_STR:
				{
					if (i + 1 < argc)
					{
						++i;
						*flag->ptr.s = argv[i];
					}
					else
					{
						command_line_error("no value argument after -%s\n", arg);
					}
				} break;
				case FLAG_ENUM:
				{
					const char *option;
					if (i + 1 < argc)
					{
						++i;
						option = argv[i];
					}
					else
					{
						command_line_error("no value after %s\n", arg);
						break;
					}
					bool found = false;
					for (int k = 0; k < flag->num_options; ++k)
					{
						if (strcmp(flag->options[k], option) == 0)
						{
							*flag->ptr.i = k;
							found = true;
							break;
						}
					}
					if (!found)
					{
						command_line_error("invalid value '%s' for %s\n", option, arg);
					}
				} break;
				default:
				{
					fatal_error("unhandled flag kind\n");
				} break;
			}
		}
		else
		{
			break;
		}
	}

	*argc_ptr = argc - i;
	*argv_ptr = argv + i;
	return path_file(strdup(argv[0]));
}

void test_onmsg(void *userdata, position pos, msg_kind msg, const char *str)
{
	printf("[%03d     ] ", pos);
	if (msg == MSG_ERROR)
	{
		PC_RED_BOLD("error: ");
	}
	else if (msg == MSG_WARNING)
	{
		PC_MAGENTA_BOLD("warning: ");
	}
	else
	{
		PC_WHITE_BOLD("note: ");
	}
	printf("%s\n", str);
}

void test_online(void *userdata, position pos)
{
	printf("[%03d     ] line\n", pos);
}

const char *test_onidentifier(void *userdata, position pos, const char *str, usize len)
{
	printf("[%03d     ] identifier: %.*s\n", pos, (s32)len, str);
	return 0;
}

void src_online(void *userdata, position pos)
{
	source *src = userdata;
	srcbase_add_line(src->srcbase, src, pos);
}

const char *src_onidentifier(void *userdata, position pos, const char *str, usize len)
{
	source *src = userdata;
	return srcbase_intern(src->srcbase, str, len);
}

// #todo move to semantic analysis section
void find_label_and_relocate(instruction *instrs, operand *oper, const char *label, u8 *offset)
{
	#if 0
	// #todo replace with equivalent
	for (u32 i = 0; i < buf_len(instrs); ++i)
	{
		instruction *instr = instrs + i;
		if (instr->kind == IK_LABEL)
		{
			if (instr->label.identifier == label)
			{
				u8 *pos = instr->code_offset;
				oper->kind = OPER_IMMEDIATE;
				oper->imm_op.id = (u32)((pos - offset) & 0xFFFFFFFF);
				oper->imm_op.kind = ID;
			}
		}
	}
	#endif
}

// #todo move to semantic analysis section
void replace_labels(instruction *instrs)
{
	#if 0
	// #todo replace with equivalent
	for (u32 i = 0; i < buf_len(instrs); ++i)
	{
		instruction *instr = instrs + i;
		if (instr->op1.kind == OPER_LABEL)
		{
			const char *label = instr->op1.label.identifier;
			find_label_and_relocate(instrs, &instr->op1, label, instr->code_offset + instr->code_len);
		}
		else if (instr->op2.kind == OPER_LABEL)
		{
			const char *label = instr->op2.label.identifier;
			find_label_and_relocate(instrs, &instr->op2, label, instr->code_offset + instr->code_len);
		}
	}
	#endif
}

s32 xia_main(int argc, const char **argv)
{
	const char *output_file = 0;

	add_flag("version", &flag_version, "print assembler version");
	add_flag("code", &flag_code, "print the generated executable data");
	add_flag("pinstr", &flag_instructions, "print the generated instructions");
	add_flag_str("o", &output_file, "file", "output file (default: out_<input_file>.o)");
	const char *program_name = parse_flags(&argc, &argv);
	if (flag_version)
	{
		print_version();
		return 0;
	}
	if (argc != 1)
	{
		printf("usage: %s [flags, output_file] <input_file>\n", program_name);
		print_flags_usage();
		return 1;
	}

	const char *input_file = argv[0];

	const char *input = read_file(input_file, 0);
	if (input == 0)
	{
		return 1;
	}
	source_base srcbase;
	memset(&srcbase, 0, sizeof(srcbase));
	srcbase_init(&srcbase);
	source *src1 = srcbase_new_str(&srcbase, input_file, input);


	xia_error_handler error_handler;
	error_handler_init(&error_handler, false, NULL);

	lexer lex;
	parser p;
	parser_init(&p, &srcbase, src1, &lex, &error_handler);

	next_token(&lex);
	parse_file(&p);

	compilation_unit unit = p.unit;

	if (error_handler.num_errors > 0)
	{
		print_errors(error_handler, &srcbase);
		if (error_handler.num_errors == 1)
		{
			fatal_error("1 error generated.\n");
		}
		else
		{
			fatal_error("%u errors generated.\n", error_handler.num_errors);
		}
	}
	else
	{
		u32 sum = 0;
		for (u32 i = 0; i < buf_len(unit.sections); ++i)
		{
			sum += buf_len(unit.sections[i].stmts);
		}
		if (sum == 0)
		{
			fatal_error("no valid statements parsed.\n");
		}
	}

	analyzer semantic_analyzer;
	analyzer_init(&semantic_analyzer, &srcbase);
	for (u32 i = 0; i < buf_len(unit.sections); ++i)
	{
		analyze_statements(&semantic_analyzer, unit.sections[i].stmts);
	}
	if (semantic_analyzer.errors > 0)
	{
		if (semantic_analyzer.errors == 1)
		{
			fatal_error("1 error generated.\n");
		}
		else
		{
			fatal_error("%u errors generated.\n", semantic_analyzer.errors);
		}
	}

	u64 sec_errors = 0;
	for (u32 i = 0; i < buf_len(unit.sections); ++i)
	{
		emit_section(unit.sections + i);
	}

	/* for (u32 i = 0; i < buf_len(unit.sections); ++i)
	{
		section *sec = unit.sections + i;
		for (label *lab = sec->labels; lab != buf_end(sec->labels); ++lab)
		{
			printf("label: %s @%lu\n", lab->name, lab->section_relative_offset);
		}
	} */

	for (u32 i = 0; i < buf_len(unit.sections); ++i)
	{
		section_replace_labels(unit.sections + i, unit.sections);
		sec_errors += (unit.sections + i)->errors;
	}

	if (sec_errors > 0)
	{
		if (sec_errors == 1)
		{
			fatal_error("1 error generated.\n");
		}
		else
		{
			fatal_error("%lu errors generated.\n", sec_errors);
		}
	}

	for (u32 i = 0; i < buf_len(unit.sections); ++i)
	{
		print_section(unit.sections + i);
	}

	#if 0
	if (flag_instructions)
	{
		// #todo add flags for what should be output
		for (u32 i = 0; i < buf_len(p.instrs); ++i)
		{
			instruction instr = p.instrs[i];
			// #todo continual need to pass instrs to the x64 code implies a restructuring is necessary
			// also the global emitter variables...
			print_instruction_bytes(p.instrs, instr, code, 4);
		}
	}

	if (flag_code)
	{
		print_code();
	}
	#endif

	char out_file[MAX_PATH];
	if (output_file)
	{
		path_copy(out_file, output_file);
	}
	else
	{
		char *file = path_file((char *)input_file);
		char *ext = path_ext(file);
		//snprintf(out_file, sizeof(out_file), "out_%.*s.bin", (int)((ext - 1) - file), file);
		snprintf(out_file, sizeof(out_file), "%.*s.o", (int)((ext - 1) - file), file);
	}

	#if 0
	intptr_t bytes_to_write = emit_ptr - code;
	printf("writing %ld bytes to \"%s\"\n", bytes_to_write, out_file);
	write_file(out_file, (const char *)code, bytes_to_write);
	#endif

	compilation_unit_output_obj(unit, out_file);

	return 0;
}
