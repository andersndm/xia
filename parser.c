#include "parser.h"

#include <stdarg.h>
#include <stdio.h>

#include "common/common.h"

#if 0
const char *test_onstr(void *userdata, printing_pos pp, const char *str, usize len)
{
	position pos = pp_start_pos(pp);
	printf("[%03d     ] str: \"%.*s\"\n", pos, (s32)len, str);
	return 0;
}
#endif

const char *parser_onstr(void *userdata, printing_pos pp, const char *str, usize len)
{
	parser *self = userdata;
	// #todo why intern the string? to remove duplicates?
	return srcbase_intern(self->srcbase, str, len);
}

internal token parser_next_token(parser *self);

const char *parser_onidentifier(void *userdata, printing_pos pp, const char *str, usize len)
{
	parser *self = userdata;
	const char *interned = srcbase_intern(self->srcbase, str, len);

	if (interned >= srcbase_directives_start(self->srcbase) &&
		interned < srcbase_directives_end(self->srcbase))
	{
		self->lex->tok.kind = TOK_DIRECTIVE;
	}
	else if (srcbase_is_register(self->srcbase, interned))
	{
		self->lex->tok.kind = TOK_REGISTER;
	}
	else if (interned >= srcbase_ops_start(self->srcbase) &&
			 interned < srcbase_ops_end(self->srcbase))
	{
		self->lex->tok.kind = TOK_OP;
	}
	else if (interned >= srcbase_keywords_start(self->srcbase) &&
			 interned < srcbase_keywords_end(self->srcbase))
	{
		self->lex->tok.kind = TOK_KEYWORD;
	}
	else
	{
		// #todo hacky, if there are spaces between the label and the ':' it won't work
		// #note this also removes the need for the colon token
		if (*self->lex->str == ':')
		{
			self->lex->tok.kind = TOK_LABEL;
			++self->lex->str;
		}
		else
		{
			lexer_start_from_pos(self->lex, pp_start_pos(pp));
			self->lex->tok.kind = TOK_IDENTIFIER;
		}
	}
	return interned;
}

void parser_online(void *userdata, position pos)
{
	parser *self = userdata;
	srcbase_add_line(self->srcbase, self->src, pos);
}

void parser_init(parser *self, source_base *srcbase, source *src, lexer *lex, xia_error_handler *error_handler)
{
	lexer_init(lex, 0, src->str);
	lex->userdata = self;
	lex->online = parser_online;
	lex->onidentifier = parser_onidentifier;
	lex->onstr = parser_onstr;

	self->srcbase = srcbase;
	self->src = src;
	self->lex = lex;
	self->error_handler = error_handler;
	self->unit.sections = NULL;
}

internal token parser_next_token(parser *self)
{
	if (!next_token(self->lex))
	{
		// #todo should add a newline token so this can still be valid
		// fatal_error("unexpected EOF");
	}
	return self->lex->tok;
}

void parser_eat_newline(parser *self)
{
	while (self->lex->tok.kind == TOK_NEWLINE)
	{
		parser_next_token(self);
	}
}

internal bool parser_expect_token(parser *self, token_kind kind)
{
	token tok = self->lex->tok;
	if (tok.kind != kind)
	{
		ASSERT(kind != TOK_IDENTIFIER);
		token expected;
		expected.kind = kind;
		error_str_add(self->error_handler, XIA_ERROR_UNEXPECTED_TOKEN, ppos_pos_range(tok.range.start, tok.range),
					  "expected '%s', found '%s'", token_repr(expected), token_repr(tok));
		skip_to_end_of_line(self->lex);
		return false;
	}
	else if (kind == TOK_NEWLINE)
	{
		parser_eat_newline(self);
	}
	else
	{
		parser_next_token(self);
	}
	return true;
}

internal bit_width get_width_from_keyword(keywords keyword)
{
	switch (keyword)
	{
		case KEYWORD_BYTE: { return B8; } break;
		case KEYWORD_WORD: { return B16; } break;
		case KEYWORD_DWORD: { return B32; } break;
		case KEYWORD_QWORD: { return B64; } break;
		case KEYWORD_NONE:
		case KEYWORD_REL:
		case NUM_KEYWORDS:
		default: { INVALID_CODE_PATH; } break;
	}
	return 0;
}

const char *parse_identifier(parser *self)
{
	token tok = self->lex->tok;
	if (tok.kind != TOK_IDENTIFIER)
	{
		error_add(self->error_handler, XIA_ERROR_EXPECTED_IDENTIFIER, ppos_range(tok.range));
		skip_to_end_of_line(self->lex);
		return NULL;
	}
	else
	{
		parser_next_token(self);
		return tok.tidentifier;
	}
}

internal void parse_operand_indexed(parser *self, operand *oper, registers base, u64 scale)
{
	token tok = self->lex->tok;
	if (scale != 1 && scale != 2 && scale != 4 && scale != 8)
	{
		error_add(self->error_handler, XIA_ERROR_INVALID_SCALAR, ppos_pos_range(tok.range.start, tok.range));
		skip_to_end_of_line(self->lex);
		oper->kind = OPER_NONE;
	}
	else
	{
		if (tok.kind != TOK_REGISTER)
		{
			error_add(self->error_handler, XIA_ERROR_EXPECTED_REGISTER, ppos_pos_range(tok.range.start, tok.range));
			skip_to_end_of_line(self->lex);
			oper->kind = OPER_NONE;
		}
		else
		{
			registers reg = srcbase_get_register(self->srcbase, tok);
			if (reg.reg == R_NONE)
			{
				error_add(self->error_handler, XIA_ERROR_EXPECTED_REGISTER, ppos_pos_range(tok.range.start, tok.range));
				skip_to_end_of_line(self->lex);
				oper->kind = OPER_NONE;
			}
			else
			{
				tok = parser_next_token(self);
				if (tok.kind == TOK_RBRACKET)
				{
					oper->kind = OPER_MEM_REGISTER_INDEXED;
					oper->mem_indexed.base = base;
					oper->mem_indexed.index = reg;
					oper->mem_indexed.scale = (log2_scale)(scale >> 3);
					switch (scale)
					{
						case 1: { oper->mem_indexed.scale = X1; } break;
						case 2: { oper->mem_indexed.scale = X2; } break;
						case 4: { oper->mem_indexed.scale = X4; } break;
						case 8: { oper->mem_indexed.scale = X8; } break;
						default: { INVALID_CODE_PATH; } break;
					}
					lexer_start_from_token(self->lex, tok);
				}
				else if (tok.kind == TOK_PLUS || tok.kind == TOK_MINUS)
				{
					bool negative = tok.kind == TOK_MINUS ? true : false;
					tok = parser_next_token(self);
					if (tok.kind == TOK_INTEGER)
					{
						oper->kind = OPER_MEM_REGISTER_INDEXED_OFFSET;
						oper->mem_indexed_offset.base = base;
						oper->mem_indexed_offset.index = reg;
						oper->mem_indexed_offset.scale = (log2_scale)(scale >> 3);
						switch (scale)
						{
							case 1: { oper->mem_indexed_offset.scale = X1; } break;
							case 2: { oper->mem_indexed_offset.scale = X2; } break;
							case 4: { oper->mem_indexed_offset.scale = X4; } break;
							case 8: { oper->mem_indexed_offset.scale = X8; } break;
							default: { INVALID_CODE_PATH; } break;
						}
						if (tok.suffix == TOK_S8 || tok.suffix == TOK_U8)
						{
							u8 val = tok.tint;
							if (negative)
							{
								u8 val = (u8)(0 - (s8)tok.tint);
							}
							oper->mem_indexed_offset.offset.kind = IB;
							oper->mem_indexed_offset.offset.ib = (0xFF & val);
							oper->mem_indexed_offset.offset.negative = negative;
							oper->mem_indexed_offset.offset.range = tok.range;
						}
						else if (tok.suffix <= TOK_S32 || tok.suffix <= TOK_U32)
						{
							u32 val = tok.tint;
							if (negative)
							{
								val = (u32)(0 - (s32)tok.tint);
							}
							oper->mem_indexed_offset.offset.kind = ID;
							oper->mem_indexed_offset.offset.id = (0xFFFFFFFF & val);
							oper->mem_indexed_offset.offset.negative = negative;
							oper->mem_indexed_offset.offset.range = tok.range;
						}
						else
						{
							error_add(self->error_handler, XIA_ERROR_INVALID_OFFSET_SIZE, ppos_pos_range(tok.range.start, tok.range));
							skip_to_end_of_line(self->lex);
							oper->kind = OPER_NONE;
						}
					}
					else
					{
						error_add(self->error_handler, XIA_ERROR_EXPECTED_INTEGER, ppos_pos_range(tok.range.start, tok.range));
						skip_to_end_of_line(self->lex);
						oper->kind = OPER_NONE;
					}
				}
				else
				{
					error_add(self->error_handler, XIA_ERROR_EXPECTED_PLUS_OR_RBRACKET, ppos_pos_range(tok.range.start, tok.range));
					skip_to_end_of_line(self->lex);
					oper->kind = OPER_NONE;
				}
			}
		}
	}
}

internal void parse_operand_mem_offset(parser *self, operand *oper, registers base, bool minus)
{
	token tok = self->lex->tok;
	if (minus)
	{
		if (tok.kind != TOK_INTEGER)
		{
			error_add(self->error_handler, XIA_ERROR_EXPECTED_INTEGER, ppos_pos_range(tok.range.start, tok.range));
			skip_to_end_of_line(self->lex);
			oper->kind = OPER_NONE;
			return;
		}
		else
		{
			if (tok.suffix == TOK_S8 || tok.suffix == TOK_U8)
			{
				oper->kind = OPER_MEM_REGISTER_OFFSET;
				oper->mem_offset.base = base;
				oper->mem_offset.offset.kind = IB;
				oper->mem_offset.offset.ib = (0xFF & -tok.tint);
				oper->mem_offset.offset.range = tok.range;
			}
			else if (tok.suffix == TOK_S32 || tok.suffix == TOK_U32)
			{
				oper->kind = OPER_MEM_REGISTER_OFFSET;
				oper->mem_offset.base = base;
				oper->mem_offset.offset.kind = ID;
				oper->mem_offset.offset.id = (0xFFFFFFFF & -tok.tint);
				oper->mem_offset.offset.range = tok.range;
			}
			else
			{
				error_add(self->error_handler, XIA_ERROR_INVALID_OFFSET_SIZE, ppos_pos_range(tok.range.start, tok.range));
				skip_to_end_of_line(self->lex);
				oper->kind = OPER_NONE;
			}
			oper->mem_offset.offset.negative = true;
		}
	}
	else
	{
		if (tok.kind != TOK_INTEGER)
		{
			if (tok.kind == TOK_REGISTER)
			{
				registers reg = srcbase_get_register(self->srcbase, tok);
				if (reg.reg == R_NONE)
				{
					error_add(self->error_handler, XIA_ERROR_EXPECTED_REGISTER_OR_INTEGER,
							  ppos_pos_range(tok.range.start, tok.range));
					skip_to_end_of_line(self->lex);
					oper->kind = OPER_NONE;
				}
				else
				{
					#if 0
					oper->kind = OPER_MEM_REGISTER_INDEXED;
					oper->mem_indexed.base = base;
					oper->mem_indexed.index = reg;
					oper->mem_indexed.scale = X1;
					#endif
					//parser_next_token(self);
					parse_operand_indexed(self, oper, base, 1);
				}
			}
			else
			{
					error_add(self->error_handler, XIA_ERROR_EXPECTED_REGISTER_OR_INTEGER,
							  ppos_pos_range(tok.range.start, tok.range));
					skip_to_end_of_line(self->lex);
				oper->kind = OPER_NONE;
			}
		}
		else
		{
			token next_tok = parser_next_token(self);
			if (next_tok.kind == TOK_MUL)
			{
				parser_next_token(self);
				parse_operand_indexed(self, oper, base, tok.tint);
			}
			else
			{
				if (tok.suffix == TOK_S8 || tok.suffix == TOK_U8)
				{
					oper->kind = OPER_MEM_REGISTER_OFFSET;
					oper->mem_offset.base = base;
					oper->mem_offset.offset.kind = IB;
					oper->mem_offset.offset.ib = (0xFF & tok.tint);
					oper->mem_offset.offset.negative = false;
					oper->mem_offset.offset.range = tok.range;
				}
				else if (tok.suffix == TOK_S16 || tok.suffix == TOK_U16)
				{
					oper->kind = OPER_MEM_REGISTER_OFFSET;
					oper->mem_offset.base = base;
					oper->mem_offset.offset.kind = IW;
					oper->mem_offset.offset.iw = (0xFFFF & tok.tint);
					oper->mem_offset.offset.negative = false;
					oper->mem_offset.offset.range = tok.range;
				}
				else if (tok.suffix == TOK_S32 || tok.suffix == TOK_U32)
				{
					oper->kind = OPER_MEM_REGISTER_OFFSET;
					oper->mem_offset.base = base;
					oper->mem_offset.offset.kind = ID;
					oper->mem_offset.offset.id = (0xFFFFFFFF & tok.tint);
					oper->mem_offset.offset.negative = false;
					oper->mem_offset.offset.range = tok.range;
				}
				else
				{
					error_add(self->error_handler, XIA_ERROR_INVALID_OFFSET_SIZE, ppos_pos_range(tok.range.start, tok.range));
					skip_to_end_of_line(self->lex);
					oper->kind = OPER_NONE;
				}
				lexer_start_from_token(self->lex, next_tok);
			}
		}
	}
	parser_next_token(self);
	if (self->lex->tok.kind != TOK_RBRACKET)
	{
		error_add(self->error_handler, XIA_ERROR_EXPECTED_RBRACKET, ppos_pos(tok.range.start));
		skip_to_end_of_line(self->lex);
		oper->kind = OPER_NONE;
	}
}

internal void parse_operand_mem(parser *self, operand *oper)
{
	token tok = self->lex->tok;
	if (tok.kind == TOK_REGISTER)
	{
		registers reg = srcbase_get_register(self->srcbase, tok);
		if (reg.reg == R_NONE)
		{
			error_add(self->error_handler, XIA_ERROR_EXPECTED_REGISTER, ppos_pos_range(tok.range.start, tok.range));
			skip_to_end_of_line(self->lex);
			oper->kind = OPER_NONE;
		}
		else
		{
			/*
			oper->kind = OPER_REGISTER;
			oper->reg = reg;
			*/
			parser_next_token(self);
			if (self->lex->tok.kind != TOK_RBRACKET)
			{
				if (self->lex->tok.kind == TOK_PLUS)
				{
					parser_next_token(self);
					parse_operand_mem_offset(self, oper, reg, false);
				}
				else if (self->lex->tok.kind == TOK_MINUS)
				{
					parser_next_token(self);
					parse_operand_mem_offset(self, oper, reg, true);
				}
				else
				{
					error_add(self->error_handler, XIA_ERROR_EXPECTED_RBRACKET, ppos_pos(tok.range.start));
					skip_to_end_of_line(self->lex);
					oper->kind = OPER_NONE;
				}
			}
			else
			{
				// #todo remove and check in the analyzer
				if (oper->width == BNONE)
				{
					error_add(self->error_handler, XIA_ERROR_EXPECTED_SIZE_SPECIFIER, ppos_pos(self->lex->tok.range.start));
					skip_to_end_of_line(self->lex);
				}
				oper->kind = OPER_MEM_REGISTER;
				oper->reg = reg;
				oper->range.end = self->lex->tok.range.end;
			}
		}
	}
	else
	{
		if (tok.kind == TOK_KEYWORD)
		{
			keywords keyword = srcbase_get_keyword(self->srcbase, tok);
			ASSERT(keyword == KEYWORD_REL);
			parser_next_token(self);
			#if 0
			// #todo use $ again if allowing to compile to binary
			// #todo proper parsing of $
			ASSERT(self->lex->tok.kind == TOK_DOLLAR);
			parser_next_token(self);
			#endif
			tok = self->lex->tok;
			if (tok.kind != TOK_PLUS && tok.kind != TOK_MINUS)
			{
				error_add(self->error_handler, XIA_ERROR_EXPECTED_PLUS_OR_MINUS, ppos_pos(tok.range.start));
				skip_to_end_of_line(self->lex);
				oper->kind = OPER_NONE;
			}
			else
			{
				bool minus = tok.kind == TOK_MINUS ? true : false;
				parser_next_token(self);
				tok = self->lex->tok;
				if (tok.kind != TOK_INTEGER)
				{
					error_add(self->error_handler, XIA_ERROR_EXPECTED_INTEGER, ppos_pos_range(tok.range.start, tok.range));
					skip_to_end_of_line(self->lex);
					oper->kind = OPER_NONE;
				}
				else
				{
					ASSERT(tok.suffix <= TOK_U32);
					oper->kind = OPER_RIP_DISPLACEMENT;
					if (minus)
					{
						oper->displacement = (u32)(0 - (s32)tok.tint);
					}
					else
					{
						oper->displacement = tok.tint;
					}
				}
			}
		}
		else if (tok.kind == TOK_IDENTIFIER)
		{
			oper->kind = OPER_MEM_LABEL;
			oper->label = tok.tidentifier;
		}
		else
		{
			// #todo lack of size keyword triggers this assertion
			ASSERT(tok.kind == TOK_INTEGER);
			ASSERT(tok.suffix <= TOK_U32);
			oper->kind = OPER_DISPLACEMENT;
			oper->displacement = tok.tint;
		}
		parser_next_token(self);
		if (self->lex->tok.kind != TOK_RBRACKET)
		{
			error_add(self->error_handler, XIA_ERROR_EXPECTED_RBRACKET, ppos_pos(tok.range.start));
			skip_to_end_of_line(self->lex);
			oper->kind = OPER_NONE;
		}
		oper->range.end = self->lex->tok.range.end;
	}
}

internal void parse_integer_operand(parser *self, operand *oper, token tok)
{
	ASSERT(tok.kind == TOK_INTEGER);
	switch (tok.suffix)
	{
		case TOK_CHAR:
		case TOK_S8:
		case TOK_U8:
		{
			oper->kind = OPER_IMMEDIATE;
			oper->imm_op.kind = IB;
			oper->imm_op.ib = (0xFF & tok.tint);
			oper->width = B8;
		} break;
		case TOK_S16:
		case TOK_U16:
		{
			oper->kind = OPER_IMMEDIATE;
			oper->imm_op.kind = IW;
			oper->imm_op.iw = tok.tint;
			oper->width = B16;
		} break;
		case TOK_S32:
		case TOK_U32:
		{
			oper->kind = OPER_IMMEDIATE;
			oper->imm_op.kind = ID;
			oper->imm_op.id = tok.tint;
			oper->width = B32;
		} break;
		case TOK_S64:
		case TOK_U64:
		{
			oper->kind = OPER_IMMEDIATE;
			oper->imm_op.kind = IQ;
			oper->imm_op.iq = tok.tint;
			oper->width = B64;
		} break;
		case TOK_NONE:
		case NUM_TOKEN_SUFFIXES:
		default:
		{
			INVALID_CODE_PATH;
		} break;
	}
	oper->imm_op.negative = false;
	oper->imm_op.range = tok.range;
	oper->range = tok.range;
}

internal void parse_operand(parser *self, operand *oper)
{
	token tok = self->lex->tok;
	oper->width = BNONE;
	oper->range.start = tok.range.start;
	if (tok.kind == TOK_NEWLINE)
	{
		return;
	}
	else if (tok.kind == TOK_KEYWORD)
	{
		keywords keyword = srcbase_get_keyword(self->srcbase, tok);
		oper->width = get_width_from_keyword(keyword);
		parser_next_token(self);
		tok = self->lex->tok;
	}
	if (tok.kind == TOK_REGISTER)
	{
		registers reg = srcbase_get_register(self->srcbase, tok);
		if (reg.reg == R_NONE)
		{
			error_add(self->error_handler, XIA_ERROR_EXPECTED_REGISTER, ppos_pos_range(tok.range.start, tok.range));
			skip_to_end_of_line(self->lex);
			oper->kind = OPER_NONE;
		}
		else
		{
			oper->kind = OPER_REGISTER;
			oper->reg = reg;
			oper->width = reg.width;
			oper->reg.range = tok.range;
			oper->range.end = tok.range.end;
		}
	}
	else
	{
		if (tok.kind == TOK_LBRACKET)
		{
			if (oper->width == BNONE)
			{
				error_add(self->error_handler, XIA_ERROR_EXPECTED_SIZE_SPECIFIER, ppos_pos(tok.range.start));
				skip_to_end_of_line(self->lex);
			}
			parser_next_token(self);
			parse_operand_mem(self, oper);
		}
		else if (tok.kind == TOK_INTEGER)
		{
			parse_integer_operand(self, oper, tok);
			oper->range.end = tok.range.end;
		}
		else if (tok.kind == TOK_IDENTIFIER)
		{
			oper->kind = OPER_LABEL;
			oper->label = tok.tidentifier;
		}
		else
		{
			error_add(self->error_handler, XIA_ERROR_EXPECTED_REGISTER_OR_LBRACKET, ppos_pos(tok.range.start));
			skip_to_end_of_line(self->lex);
		}
	}
}

instruction parse_instruction(parser *self)
{
	u64 num_errors = self->error_handler->num_errors;
	token tok = self->lex->tok;
	ASSERT(tok.kind == TOK_OP);
	instruction instr;
	instr.code_offset = 0;
	instr.code_len = 0;
	instr.op1.kind = OPER_NONE;
	instr.op2.kind = OPER_NONE;
	instr.op3.kind = OPER_NONE;
	opcode op = srcbase_get_opcode(self->srcbase, tok);
	if (op == OP_NONE)
	{
		error_add(self->error_handler, XIA_ERROR_EXPECTED_OP, ppos_pos_range(tok.range.start, tok.range));
		skip_to_end_of_line(self->lex);
	}
	else
	{
		instr.op_range = tok.range;
		instr.op_range.end--;
	}

	bool cc_op = false;

	if (op >= FIRST_JCC && op <= LAST_JCC)
	{
		instr.op1.kind = OPER_CC;
		instr.op1.cc = op_to_cc[op];
		instr.op1.range.start = instr.op_range.start + 1;
		instr.op1.range.end = instr.op_range.end;
		ASSERT(instr.op1.cc != 0 || op == JO);
		instr.op = JMP;
		parser_next_token(self);
		cc_op = true;
	}
	else if (op >= FIRST_CMOVCC && op <= LAST_CMOVCC)
	{
		instr.op1.kind = OPER_CC;
		instr.op1.cc = op_to_cc[op];
		instr.op1.range.start = instr.op_range.start + 1;
		instr.op1.range.end = instr.op_range.end;
		ASSERT(instr.op1.cc != 0 || op == CMOVO);
		instr.op = MOV;
		parser_next_token(self);
		cc_op = true;
	}
	else if (op >= FIRST_SETCC && op <= LAST_SETCC)
	{
		instr.op1.kind = OPER_CC;
		instr.op1.cc = op_to_cc[op];
		instr.op1.range.start = instr.op_range.start + 1;
		instr.op1.range.end = instr.op_range.end;
		ASSERT(instr.op1.cc != 0 || op == SETO);
		instr.op = SET;
		parser_next_token(self);
		cc_op = true;
	}

	if (cc_op)
	{
		parse_operand(self, &instr.op2);
		if (instr.op2.kind != OPER_NONE)
		{
			token next_tok = parser_next_token(self);
			if (next_tok.kind == TOK_COMMA)
			{
				next_tok = parser_next_token(self);
				if (next_tok.kind != TOK_EOF)
				{
					parse_operand(self, &instr.op3);
				}
			}
		}
	}
	else
	{
		instr.op = (opcode)(op);
		parser_next_token(self);
		parse_operand(self, &instr.op1);
		if (instr.op1.kind != OPER_NONE)
		{
			token next_tok = parser_next_token(self);
			if (next_tok.kind == TOK_COMMA)
			{
				parser_next_token(self);
				parse_operand(self, &instr.op2);
				if (instr.op2.kind != OPER_NONE)
				{
					next_tok = parser_next_token(self);
					if (next_tok.kind == TOK_COMMA)
					{
						parser_next_token(self);
						parse_operand(self, &instr.op3);
					}
				}
			}
		}
	}
	parser_expect_token(self, TOK_NEWLINE);
	
	return instr;
}

// #todo directives not keywords
bool parse_keyword(parser *self)
{
	token tok = self->lex->tok;
	//parser_log(self, ppos_pos(tok.range.start), "keyword \"%s\"", tok.tidentifier);
	keywords keyword = srcbase_get_keyword(self->srcbase, tok);
	ASSERT(keyword != KEYWORD_NONE);
	parser_next_token(self);
	tok = self->lex->tok;

	if (!next_token(self->lex))
	{
		return false;
	}
	parser_next_token(self);
	return true;
}

// #todo add parsing of section flags and types
directives parse_directive(parser *self, token dir_tok)
{
	directives dir;
	dir.kind = srcbase_get_directive(self->srcbase, dir_tok);
	dir.pos = dir_tok.range.start;
	token tok = self->lex->tok;
	switch (dir.kind)
	{
		case DIR_SECTION:
		{
			dir.section.name = parse_identifier(self);
			dir.section.name_range = tok.range;
			dir.section.type = SHT_NULL;
			dir.section.flags = 0;
			if (dir.section.name == srcbase_intern(self->srcbase, ".text", 5))
			{
				dir.section.type = SHT_PROGBITS;
				dir.section.flags = SHF_ALLOC | SHF_EXECINSTR;
			}
			else if (dir.section.name == srcbase_intern(self->srcbase, ".data", 5))
			{
				dir.section.type = SHT_PROGBITS;
				dir.section.flags = SHF_ALLOC | SHF_WRITE;
			}
			else if (dir.section.name == srcbase_intern(self->srcbase, ".rodata", 7))
			{
				dir.section.type = SHT_PROGBITS;
				dir.section.flags = SHF_ALLOC;
			}
			else if (dir.section.name == srcbase_intern(self->srcbase, ".bss", 4))
			{
				dir.section.type = SHT_NOBITS;
				dir.section.flags = SHF_ALLOC | SHF_WRITE;
			}
		} break;
		case DIR_GLOBAL:
		{
			dir.global.name = parse_identifier(self);
			dir.global.name_range = tok.range;
		} break;
		case DIR_ALIGN:
		{
			dir.align.alignment = 0;
			// #todo for different sections a different default should be used
			dir.align.fill_byte = 0;
			dir.align.bytes_written = 0;
			if (parser_expect_token(self, TOK_INTEGER))
			{
				dir.align.alignment = tok.tint;
				if (tok.tint > MAX_ALIGNMENT) // #note in case integer > 0xFF
				{
					dir.align.alignment = MAX_ALIGNMENT;
					error_str_add(self->error_handler, XIA_WARNING_EXCEEDS_ALIGNMENT_CAP, ppos_range(tok.range), "%u", MAX_ALIGNMENT);
				}
				dir.align.alignment_range = tok.range;
				tok = self->lex->tok;
				if (tok.kind == TOK_COMMA)
				{
					parser_next_token(self);
					tok = self->lex->tok;
					if (parser_expect_token(self, TOK_INTEGER))
					{
						dir.align.fill_byte = tok.tint;
						if (tok.tint > 0xFF)
						{
							dir.align.fill_byte = 0xFF;
							error_add(self->error_handler, XIA_WARNING_VALUE_EXCEEDS_BYTE, ppos_range(tok.range));
						}
						dir.align.alignment_range = tok.range;
					}
				}
			}
		} break;
		case DIR_ASCII:
		{
			// #todo replace with parser expect token
			if (parser_expect_token(self, TOK_STRING))
			{
				dir.str = tok.tstr;
			}
		} break;
		// #todo should just specify the number of bytes? or force a combination
		case DIR_I8:
		{
			if (parser_expect_token(self, TOK_INTEGER))
			{
				if (tok.suffix > TOK_U8)
				{
					error_add(self->error_handler, XIA_ERROR_LARGER_THAN_BYTE, ppos_pos_range(dir_tok.range.start, tok.range));
					skip_to_end_of_line(self->lex);
				}
				else
				{
					dir.i8 = (u8)(tok.tint & 0xFF);
				}
			}
		} break;
		case DIR_I16:
		{
			if (parser_expect_token(self, TOK_INTEGER))
			{
				if (tok.suffix > TOK_U16)
				{
					error_add(self->error_handler, XIA_ERROR_LARGER_THAN_2_BYTES, ppos_pos_range(dir_tok.range.start, tok.range));
					skip_to_end_of_line(self->lex);
				}
				else
				{
					dir.i16 = (u16)(tok.tint & 0xFFFF);
				}
			}
		} break;
		case DIR_I32:
		{
			if (parser_expect_token(self, TOK_INTEGER))
			{
				if (tok.suffix > TOK_U32)
				{
					error_add(self->error_handler, XIA_ERROR_LARGER_THAN_4_BYTES, ppos_pos_range(dir_tok.range.start, tok.range));
					skip_to_end_of_line(self->lex);
				}
				else
				{
					dir.i32 = (u32)(tok.tint & 0xFFFFFFFF);
				}
			}
		} break;
		case DIR_I64:
		{
			if (parser_expect_token(self, TOK_INTEGER))
			{
				dir.i64 = tok.tint;
			}
		} break;
		case NUM_DIRECTIVES:
		{
			INVALID_CODE_PATH;
		} break;
		case DIRECTIVE_NONE:
		{
			error_str_add(self->error_handler, XIA_ERROR_UNKNOWN_DIRECTIVE, ppos_range(dir_tok.range),
						  "'%s'", dir_tok.tidentifier);
			skip_to_end_of_line(self->lex);
		} break;
	}
	parser_expect_token(self, TOK_NEWLINE);
	return dir;
}

section preprocess_section(parser *self, directives dir)
{
	/*
		Only process labels and whether or not they are global

		Except: process directives that would increase code size (e.g. align), but only to know their increase

		Exit once a new section is reached, noting the position range

		When actually processing the the sections instructions ignore the global specifier

		#todo there may be more efficient ways of handling this, it will have to be tested
	*/
	token tok = self->lex->tok;
	token prev_tok = { 0 };

	ASSERT(dir.kind == DIR_SECTION);

	section result;
	result.type = SHT_NULL;
	result.start = dir.pos;
	result.name = dir.section.name;
	result.name_range = dir.section.name_range;
	result.flags = dir.section.flags;
	result.labels = NULL;
	result.max_code_len = 0;
	result.code_len = 0;
	result.code = NULL;
	result.range.start = tok.range.start;
	result.stmts = NULL;
	result.errors = 0;
	result.srcbase = self->srcbase;
	result.relocations = NULL;
	result.max_align = 0;
	while (tok.kind != TOK_EOF)
	{
		WRNG_IGNORE_PUSH(WRNG_SWITCH_ENUM)
		switch (tok.kind)
		{
			case TOK_DIRECTIVE:
			{
				directive_kind kind = srcbase_get_directive(self->srcbase, tok);
				if (kind == DIR_SECTION)
				{
					goto pp_section_end;
				}
				else if (kind == DIR_ALIGN)
				{
					parser_next_token(self);
					directives pdir = parse_directive(self, tok);
					if (pdir.align.alignment > result.max_align)
					{
						result.max_align = pdir.align.alignment;
					}
					result.max_code_len += pdir.align.alignment;
				}
				else if (kind == DIR_GLOBAL)
				{
					parser_next_token(self);
					directives gdir = parse_directive(self, tok);
					if (gdir.global.name == NULL) { break; }
					label *gl = section_get_label(&result, gdir.global.name);
					if (gl == NULL)
					{
						label l;
						l.name = gdir.global.name;
						l.defined = false;
						l.global = true;
						l.range = (pos_range){ 0, 0 };
						l.section_relative_offset = 0;
						l.symbol_index = 0;
						buf_push(result.labels, l);
					}
					else
					{
						if (gl->global)
						{
							// #todo should add note of previous global
							error_str_add(self->error_handler, XIA_WARNING_REGLOBAL, ppos_pos_range(gdir.pos, gdir.global.name_range),
										  "'%s'", gdir.global.name);
						}
						gl->global = true;
					}
				}
				else if (kind >= FIRST_DATA_DIR && kind <= LAST_DATA_DIR)
				{
					parser_next_token(self);
					directives data_dir = parse_directive(self, tok);
					if (data_dir.kind == DIR_ASCII)
					{
						result.max_code_len += strlen(data_dir.str) + 1;
					}
					else if (data_dir.kind == DIR_I8)
					{
						++result.max_code_len;
					}
					else if (data_dir.kind == DIR_I16)
					{
						u8 byte_size = 2;
						result.max_code_len += byte_size;
						if (result.max_align < byte_size)
						{
							result.max_align = byte_size;
						}
					}
					else if (data_dir.kind == DIR_I32)
					{
						u8 byte_size = 4;
						result.max_code_len += byte_size;
						if (result.max_align < byte_size)
						{
							result.max_align = byte_size;
						}
					}
					else
					{
						ASSERT(data_dir.kind == DIR_I64);
						u8 byte_size = 8;
						result.max_code_len += byte_size;
						if (result.max_align < byte_size)
						{
							result.max_align = byte_size;
						}
					}
				}
				else
				{
					error_str_add(self->error_handler, XIA_ERROR_UNKNOWN_DIRECTIVE, ppos_range(tok.range),
								"'%s'", tok.tidentifier);
					skip_to_end_of_line(self->lex);
					parser_eat_newline(self);
				}
			} break;
			case TOK_LABEL:
			{
				label *pl = section_get_label(&result, tok.tidentifier);
				if (pl != NULL && pl->defined)
				{
					error_add(self->error_handler, XIA_ERROR_LABEL_REDEFINITON, ppos_range(tok.range));
					skip_to_end_of_line(self->lex);
					error_add(self->error_handler, XIA_NOTE_PREVIOUS_DEFINITION, ppos_range(pl->range));
					skip_to_end_of_line(self->lex);
					parser_eat_newline(self);
				}
				if (pl != NULL && !pl->defined)
				{
					pl->defined = true;
					ASSERT(pl->global);
					pl->range = tok.range;
				}
				else
				{
					label l;
					l.name = tok.tidentifier;
					l.defined = true;
					l.global = false;
					l.range = tok.range;
					l.section_relative_offset = 0;
					l.symbol_index = 0;
					buf_push(result.labels, l);
				}
				parser_next_token(self);
				parser_expect_token(self, TOK_NEWLINE);
			} break;
			case TOK_OP:
			{
				skip_to_end_of_line(self->lex);
				result.max_code_len += 15; // #note max allowed x86-64 instruction length
				parser_eat_newline(self);
			} break;
			case TOK_NEWLINE:
			{
				parser_eat_newline(self);
			} break;
			default:
			{
				#if 0
				INVALID_CODE_PATH
				#else
				// #todo
				skip_to_end_of_line(self->lex);
				parser_eat_newline(self);
				#endif
			} break;
		}
		WRNG_IGNORE_POP
		prev_tok = tok;
		tok = self->lex->tok;
	}
pp_section_end:
	result.range.end = prev_tok.range.end;
	return result;
}

void preprocess_compilation_unit(parser *self)
{
	while (self->lex->tok.kind != TOK_EOF)
	{
		token tok = self->lex->tok;
		if (parser_expect_token(self, TOK_DIRECTIVE))
		{
			directives dir = parse_directive(self, tok);
			if (dir.kind != DIR_SECTION)
			{
				error_add(self->error_handler, XIA_ERROR_EXPECTED_SECTION, ppos_pos(dir.pos));
				skip_to_end_of_line(self->lex);
			}
			else
			{
				buf_push(self->unit.sections, preprocess_section(self, dir));
			}
		}
		else
		{
			parser_eat_newline(self);
		}
	}
}

void parse_section(parser *self, section *sec)
{
	source *src = srcbase_src_at_pos(self->srcbase, sec->range.start);
	const char *str = src->str + sec->range.start - src->start;
	lexer_init(self->lex, sec->range.start, str);
	parser_next_token(self);
	while (self->lex->tok.range.start < sec->range.end)
	{
		token tok = self->lex->tok;
		u64 num_errors = self->error_handler->num_errors;
		WRNG_IGNORE_PUSH(WRNG_SWITCH_ENUM)
		switch (tok.kind)
		{
			case TOK_DIRECTIVE:
			{
				directive_kind kind = srcbase_get_directive(self->srcbase, tok);
				if (kind == DIR_SECTION)
				{
					INVALID_CODE_PATH;
				}
				else if (kind == DIR_ALIGN)
				{
					parser_next_token(self);
					statement stmt;
					stmt.kind = STMT_DIRECTIVE;
					stmt.dir = parse_directive(self, tok);
					if (num_errors == self->error_handler->num_errors)
					{
						buf_push(sec->stmts, stmt);
					}
				}
				else if (kind == DIR_GLOBAL)
				{
					skip_to_end_of_line(self->lex);
					parser_eat_newline(self);
				}
				else if (kind >= FIRST_DATA_DIR && kind <= LAST_DATA_DIR)
				{
					if (sec->type == SHT_NULL)
					{
						sec->type = SHT_PROGBITS;
						sec->flags = SHF_ALLOC;
					}
					ASSERT((sec->type == SHT_PROGBITS || sec->type == SHT_NOBITS) && (sec->flags & SHF_EXECINSTR) == 0); // #todo replace asserts with parser errors
					statement stmt;
					stmt.kind = STMT_DIRECTIVE;
					parser_next_token(self);
					stmt.dir = parse_directive(self, tok);
					if (self->error_handler->num_errors == num_errors)
					{
						buf_push(sec->stmts, stmt);
					}
				}
				else
				{
					error_str_add(self->error_handler, XIA_ERROR_UNKNOWN_DIRECTIVE, ppos_range(tok.range), "'%s'", tok.tidentifier);
					skip_to_end_of_line(self->lex);
					parser_eat_newline(self);
				}
			} break;
			case TOK_LABEL:
			{
				statement stmt;
				stmt.kind = STMT_LABEL;
				stmt.lab = *section_get_label(sec, tok.tidentifier);
				if (self->error_handler->num_errors == num_errors)
				{
					buf_push(sec->stmts, stmt);
				}
				parser_next_token(self);
				parser_expect_token(self, TOK_NEWLINE);
			} break;
			case TOK_OP:
			{
				if (sec->type == SHT_NULL)
				{
					sec->type = SHT_PROGBITS;
					sec->flags = SHF_ALLOC | SHF_EXECINSTR;
				}
				ASSERT(sec->type == SHT_PROGBITS && sec->flags & SHF_EXECINSTR); // #todo replace asserts with parser errors
				statement stmt;
				stmt.kind = STMT_INSTRUCTION;
				stmt.instr = parse_instruction(self);
				if (self->error_handler->num_errors == num_errors)
				{
					buf_push(sec->stmts, stmt);
				}
			} break;
			default:
			{
				error_add(self->error_handler, XIA_ERROR_EXPECTED_STATEMENT, ppos_pos(tok.range.start));
				skip_to_end_of_line(self->lex);
				parser_eat_newline(self);
			} break;
		}
		WRNG_IGNORE_POP
	}
}

void parse_file(parser *self)
{
	preprocess_compilation_unit(self);
	for (u32 i = 0; i < buf_len(self->unit.sections); ++i)
	{
		parse_section(self, self->unit.sections + i);
	}
}
