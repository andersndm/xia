#include <string.h>

#include "srcbase.h"

#include "common/macros.h"
#include "common/memory.h"
#include "common/buf.h"

const usize SRCBASE_BLOCK_SIZE = KILOBYTES(4);

const char *srcbase_intern(source_base *self, const char *str, usize len)
{
	return str_intern(&self->identifiers, str, len);
}

void srcbase_init(source_base *self)
{
	for (u32 i = 0; i < NUM_REGISTERS; ++i)
	{
		const char *reg = register_x64_names[i];
		ASSERT(reg);
		self->regs64[i] = srcbase_intern(self, reg, strlen(reg));
	}
	for (u32 i = 0; i < NUM_REGISTERS; ++i)
	{
		const char *reg = register_x32_names[i];
		ASSERT(reg);
		self->regs32[i] = srcbase_intern(self, reg, strlen(reg));
	}
	for (u32 i = 0; i < NUM_REGISTERS; ++i)
	{
		const char *reg = register_x16_names[i];
		ASSERT(reg);
		self->regs16[i] = srcbase_intern(self, reg, strlen(reg));
	}
	for (u32 i = 0; i < NUM_REGISTERS; ++i)
	{
		const char *reg = register_x8_names[i];
		ASSERT(reg);
		self->regs8[i] = srcbase_intern(self, reg, strlen(reg));
	}

	for (u32 i = 0; i < NUM_OPCODES; ++i)
	{
		const char *op = op_names[i];
		ASSERT(op);
		self->ops[i] = srcbase_intern(self, op, strlen(op));
	}

	for (u32 i = 0; i < NUM_KEYWORDS; ++i)
	{
		const char *keyword = keyword_names[i];
		ASSERT(keyword);
		self->keywords[i] = srcbase_intern(self, keyword, strlen(keyword));
	}

	for (u32 i = 0; i < NUM_DIRECTIVES; ++i)
	{
		const char *directive = directive_names[i];
		ASSERT(directive);
		self->directives[i] = srcbase_intern(self, directive, strlen(directive));
	}
}

const char *srcbase_regs64_start(source_base *self)
{
	return self->regs64[0];
}

const char *srcbase_regs64_end(source_base *self)
{
	return self->regs64[NUM_REGISTERS - 1] + strlen(self->regs64[NUM_REGISTERS - 1]);
}

const char *srcbase_regs32_start(source_base *self)
{
	return self->regs32[0];
}

const char *srcbase_regs32_end(source_base *self)
{
	return self->regs32[NUM_REGISTERS - 1] + strlen(self->regs32[NUM_REGISTERS - 1]);
}

const char *srcbase_regs16_start(source_base *self)
{
	return self->regs16[0];
}

const char *srcbase_regs16_end(source_base *self)
{
	return self->regs16[NUM_REGISTERS - 1] + strlen(self->regs16[NUM_REGISTERS - 1]);
}

const char *srcbase_regs8_start(source_base *self)
{
	return self->regs8[0];
}

const char *srcbase_regs8_end(source_base *self)
{
	return self->regs8[NUM_REGISTERS - 1] + strlen(self->regs8[NUM_REGISTERS - 1]);
}

const char *srcbase_ops_start(source_base *self)
{
	return self->ops[0];
}

const char *srcbase_ops_end(source_base *self)
{
	return self->ops[NUM_OPCODES - 1] + strlen(self->ops[NUM_OPCODES - 1]);
}

const char *srcbase_keywords_start(source_base *self)
{
	return self->keywords[0];
}

const char *srcbase_keywords_end(source_base *self)
{
	return self->keywords[NUM_KEYWORDS - 1] + strlen(self->keywords[NUM_KEYWORDS - 1]);
}

const char *srcbase_directives_start(source_base *self)
{
	return self->directives[0];
}

const char *srcbase_directives_end(source_base *self)
{
	return self->directives[NUM_DIRECTIVES - 1] + strlen(self->directives[NUM_DIRECTIVES - 1]);
}

bool srcbase_is_register(source_base *self, const char *str)
{
	if (
		(str >= srcbase_regs64_start(self) && str < srcbase_regs64_end(self)) ||
		(str >= srcbase_regs32_start(self) && str < srcbase_regs32_end(self)) ||
		(str >= srcbase_regs16_start(self) && str < srcbase_regs16_end(self)) ||
		(str >= srcbase_regs8_start(self)  && str < srcbase_regs8_end(self)))
	{
		return true;
	}
	return false;
}

registers srcbase_get_register(source_base *self, token tok)
{
	ASSERT(tok.kind == TOK_REGISTER);
	bit_width width = B64;
	for (register_x64 reg = RAX; reg < NUM_REGISTERS; ++reg)
	{
		if (tok.tidentifier == self->regs64[reg])
		{
			return (registers){ reg, width, tok.range };
		}
	}
	width = B32;
	for (register_x64 reg = RAX; reg < NUM_REGISTERS; ++reg)
	{
		if (tok.tidentifier == self->regs32[reg])
		{
			return (registers){ reg, width, tok.range };
		}
	}
	width = B16;
	for (register_x64 reg = RAX; reg < NUM_REGISTERS; ++reg)
	{
		if (tok.tidentifier == self->regs16[reg])
		{
			return (registers){ reg, width, tok.range };
		}
	}
	width = B8;
	for (register_x64 reg = RAX; reg < NUM_REGISTERS; ++reg)
	{
		if (tok.tidentifier == self->regs8[reg])
		{
			return (registers){ reg, width, tok.range };
		}
	}
	return (registers){ R_NONE, B64, tok.range };
}

opcode srcbase_get_opcode(source_base *self, token tok)
{
	ASSERT(tok.kind == TOK_OP);
	for (opcode op = (opcode)0; op < NUM_OPCODES; ++op)
	{
		if (tok.tidentifier == self->ops[op])
		{
			return op;
		}
	}
	return OP_NONE;
}

keywords srcbase_get_keyword(source_base *self, token tok)
{
	ASSERT(tok.kind == TOK_KEYWORD);
	for (keywords keyword = (keywords)0; keyword < NUM_KEYWORDS; ++keyword)
	{
		if (tok.tidentifier == self->keywords[keyword])
		{
			return keyword;
		}
	}
	return KEYWORD_NONE;
}

directive_kind srcbase_get_directive(source_base *self, token tok)
{
	ASSERT(tok.kind == TOK_DIRECTIVE);
	for (directive_kind directive = (directive_kind)0; directive < NUM_DIRECTIVES; ++directive)
	{
		if (tok.tidentifier == self->directives[directive])
		{
			return directive;
		}
	}
	return DIRECTIVE_NONE;
}

//WRNG_IGNORE_PUSH(WRNG_UNUSED_PARAMETER)
void srcbase_add_line(source_base *self, source *src, position pos)
{
	u32 offset = (pos - src->start);
	u32 len = 0;
	if (src->lines != 0)
	{
		len = buf_len(src->lines);
	}
	if (len == 0 || src->lines[len - 1] < offset)
	{
		buf_push(src->lines, offset);

		#if 0
		printf("line: %lu - pos: %u\n", buf_len(src->lines), pos);
		const char *start = src->str + offset;
		const char *end = start;
		while (*end && *end != '\n')
		{
			++end;
		}
		printf("%d\n", (s32)(end - start));
		printf("%.*s\n", (s32)(end - start), start);
		#endif
	}
}
//WRNG_IGNORE_POP

source *srcbase_new_str(source_base *self, const char *name, const char *str)
{
	u32 str_len = strlen(str);
	u32 name_len = strlen(name);
	source *src = xmalloc(sizeof(source));
	src->srcbase = self;
	src->str = xmalloc(str_len + 1);
	strcpy((char *)src->str, str);
	src->len = str_len;
	src->lines = 0;
	src->start = self->end;
	src->name = xmalloc(name_len + 1);
	// #todo seperate path and name
	src->path = src->name;
	strcpy((char *)src->name, name);
	buf_push(self->srcs, src);
	self->end += str_len;
	srcbase_add_line(self, src, src->start);
	return src;
}

source *srcbase_src_at_pos(source_base *self, position pos)
{
	// #todo binary search
	for (usize i = 0; i < buf_len(self->srcs); ++i)
	{
		source *src = self->srcs[i];
		u32 end = src->start + src->len;
		if (pos < end)
		{
			return src;
		}
	}
	return NULL;
}

position_info srcbase_info_at_pos(source_base *self, position pos)
{
	source *src = srcbase_src_at_pos(self, pos);
	if (!src)
	{
		return (position_info){ 0 };
	}
	position offset = pos - src->start;
	usize line = 0;
	for (; line < buf_len(src->lines); ++line)
	{
		if (offset < src->lines[line])
		{
			break;
		}
	}
	usize col = line == 0 ? offset : offset - src->lines[line - 1];
	return (position_info){ src, line, col };
}

printing_pos ppos_pos(position pos)
{
	return (printing_pos){ .kind = PP_POS, .pp_pos.pos = pos };
}

printing_pos ppos_range(pos_range range)
{
	return (printing_pos){ .kind = PP_RANGE, .pp_range.range = range };
}

printing_pos ppos_pos_range(position pos, pos_range range)
{
	return (printing_pos){ .kind = PP_POS_RANGE, .pp_pos_range.pos = pos, .pp_pos_range.range = range };
}

printing_pos ppos_range2(pos_range r1, pos_range r2)
{
	return (printing_pos){ .kind = PP_RANGE2, .pp_range2.r1 = r1, .pp_range2.r2 = r2 };
}

printing_pos ppos_range3(pos_range r1, pos_range r2, pos_range r3)
{
	return (printing_pos){ .kind = PP_RANGE3, .pp_range3.r1 = r1, .pp_range3.r2 = r2, .pp_range3.r3 = r3 };
}


position pp_start_pos(printing_pos pp)
{
	position pos = 0;
	switch (pp.kind)
	{
		case PP_POS:
		{
			pos = pp.pp_pos.pos;
		} break;
		case PP_RANGE:
		{
			pos = pp.pp_range.range.start;
		} break;
		case PP_POS_RANGE:
		{
			if (pp.pp_pos_range.pos < pp.pp_pos_range.range.start)
			{
				pos = pp.pp_pos_range.pos;
			}
			else
			{
				pos = pp.pp_pos_range.range.start;
			}
		} break;
		case PP_RANGE2:
		{
			if (pp.pp_range2.r1.start < pp.pp_range2.r2.start)
			{
				pos = pp.pp_range2.r1.start;
			}
			else
			{
				pos = pp.pp_range2.r2.start;
			}
		} break;
		case PP_RANGE3:
		{
			if (pp.pp_range3.r1.start < pp.pp_range3.r2.start)
			{
				pos = pp.pp_range3.r1.start;
			}
			else
			{
				pos = pp.pp_range3.r2.start;
			}
			if (pp.pp_range3.r3.start < pos)
			{
				pos = pp.pp_range3.r3.start;
			}
		} break;
	}
	return pos;
}

position pp_end_pos(printing_pos pp)
{
	position pos = 0;
	switch (pp.kind)
	{
		case PP_POS:
		{
			pos = pp.pp_pos.pos;
		} break;
		case PP_RANGE:
		{
			pos = pp.pp_range.range.end;
		} break;
		case PP_POS_RANGE:
		{
			if (pp.pp_pos_range.pos > pp.pp_pos_range.range.end)
			{
				pos = pp.pp_pos_range.pos;
			}
			else
			{
				pos = pp.pp_pos_range.range.end;
			}
		} break;
		case PP_RANGE2:
		{
			if (pp.pp_range2.r1.end > pp.pp_range2.r2.end)
			{
				pos = pp.pp_range2.r1.end;
			}
			else
			{
				pos = pp.pp_range2.r2.end;
			}
		} break;
		case PP_RANGE3:
		{
			if (pp.pp_range3.r1.end > pp.pp_range3.r2.end)
			{
				pos = pp.pp_range3.r1.end;
			}
			else
			{
				pos = pp.pp_range3.r2.end;
			}
			if (pp.pp_range3.r3.end > pos)
			{
				pos = pp.pp_range3.r3.end;
			}
		} break;
	}
	return pos;
}

void print_pos(source_base *srcbase, printing_pos pp)
{
	position start_pos = pp_start_pos(pp);
	position_info pf = srcbase_info_at_pos(srcbase, start_pos);
	usize n = snprintf(0, 0, "%lu", pf.line);
	if (n > 31) { n = 31; }
	char buf[32];
	for (u32 i = 0; i < n; ++i)
	{
		buf[i] = ' ';
	}
	buf[n] = 0;
	printf("%s", buf);
	PC_BLUE_BOLD(" -> ");
	printf("%s:%zu:%zu\n", pf.src->name, pf.line, pf.col);
	PC_BLUE_BOLD("%s |\n", buf);
	u32 line_start = pf.src->lines[pf.line - 1];
	const char *start = pf.src->str + pf.src->lines[pf.line - 1];
	while (*start)
	{
		if (*start == ' ' || *start == '\r' || *start == '\t' || *start == '\v')
		{
			++start;
			++line_start;
		}
		else
		{
			break;
		}
	}
	ASSERT(*start);
	const char *end = start;
	while (*end && *end != '\n')
	{
		++end;
	}
	PC_BLUE_BOLD("%zu | ", pf.line);
	printf("%.*s\n", (s32)(end - start), start); //pf.src->str + pf.src->lines[pf.line]);
	PC_BLUE_BOLD("%s | ", buf);
	position end_pos = pp_end_pos(pp); // #note one past end
	switch (pp.kind)
	{
		case PP_POS:
		{
			for (u32 i = line_start; i < end_pos; ++i)
			{
				printf(" ");
			}
			PC_YELLOW_BOLD("^");
		} break;
		case PP_RANGE:
		{
			for (u32 i = line_start; i < end_pos; ++i)
			{
				if (i >= pp.pp_range.range.start)
				{
					PC_YELLOW_BOLD("~");
				}
				else
				{
					printf(" ");
				}
			}
		} break;
		case PP_POS_RANGE:
		{
			for (u32 i = line_start; i < end_pos; ++i)
			{
				if (i == pp.pp_pos_range.pos)
				{
					PC_YELLOW_BOLD("^");
				}
				else if (i >= pp.pp_pos_range.range.start && i <= pp.pp_pos_range.range.end)
				{
					PC_YELLOW_BOLD("~");
				}
				else
				{
					printf(" ");
				}
			}
		} break;
		case PP_RANGE2:
		{
			for (u32 i = line_start; i < end_pos; ++i)
			{
				if ((i >= pp.pp_range2.r1.start && i <= pp.pp_range2.r1.end) ||
					(i >= pp.pp_range2.r2.start && i <= pp.pp_range2.r2.end))
				{
					PC_YELLOW_BOLD("~");
				}
				else
				{
					printf(" ");
				}
			}
		} break;
		case PP_RANGE3:
		{
			for (u32 i = line_start; i < end_pos; ++i)
			{
				if ((i >= pp.pp_range3.r1.start && i <= pp.pp_range3.r1.end) ||
					(i >= pp.pp_range3.r2.start && i <= pp.pp_range3.r2.end) ||
					(i >= pp.pp_range3.r3.start && i <= pp.pp_range3.r3.end))
				{
					PC_YELLOW_BOLD("~");
				}
				else
				{
					printf(" ");
				}
			}
		} break;
	}
	printf("\n");
}
