#ifndef IA_TOKENS_H
#define IA_TOKENS_H

#include "common/types.h"

typedef enum token_kind
{
	TOK_EOF,
	TOK_UNKNOWN,
	TOK_NEWLINE,

	TOK_INTEGER,
	TOK_IDENTIFIER,
	TOK_KEYWORD,
	TOK_STRING,
	TOK_DIRECTIVE,
	TOK_REGISTER,
	TOK_OP,
	TOK_LABEL,

	TOK_PLUS,
	TOK_MINUS,
	TOK_MUL,

	TOK_COLON,
	TOK_DOLLAR,

	TOK_LBRACKET,
	TOK_RBRACKET,

	TOK_COMMA,
	// #todo should tok_dot be removed, now implicit in directives
	TOK_DOT,

	NUM_TOKEN_KINDS
	
} token_kind;

typedef enum token_suffix
{
	TOK_NONE,
	TOK_CHAR,
	TOK_S8,
	TOK_U8,
	TOK_S16,
	TOK_U16,
	TOK_S32,
	TOK_U32,
	TOK_S64,
	TOK_U64,
	NUM_TOKEN_SUFFIXES
} token_suffix;

typedef u32 position;

typedef struct pos_range
{
	position start;
	position end;
} pos_range;

// #todo better name than printing_pos!
typedef enum printing_pos_kind
{
	PP_POS,
	PP_RANGE,
	PP_POS_RANGE,
	PP_RANGE2,
	PP_RANGE3
} printing_pos_kind;

typedef struct printing_pos
{
	printing_pos_kind kind;
	union
	{
		struct
		{
			position pos;
		} pp_pos;
		struct
		{
			pos_range range;
		} pp_range;
		struct
		{
			position pos;
			pos_range range;
		} pp_pos_range;
		struct
		{
			pos_range r1;
			pos_range r2;
		} pp_range2;
		struct
		{
			pos_range r1;
			pos_range r2;
			pos_range r3;
		} pp_range3;
	};
	
} printing_pos;

typedef struct token
{
	token_kind kind;
	token_suffix suffix;
	pos_range range;
	union
	{
		const char *tidentifier;
		const char *tstr;
		u64 tint;
	};
} token;

const char *token_kind_name(token_kind kind);
const char *token_repr(token tok);

extern char const *token_kind_names[NUM_TOKEN_KINDS];
extern char const *suffix_names[NUM_TOKEN_SUFFIXES];
#endif // !IA_TOKENS_H
