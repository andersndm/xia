#ifndef XIA_SRCBASE_H
#define XIA_SRCBASE_H

#include "tokens.h"
#include "keywords.h"
#include "directives.h"
#include "common/strings.h"
#include "common/types.h"

#include "x64/registers.h"
#include "x64/opcodes.h"

typedef enum msg_kind
{
	MSG_INFO,
	MSG_WARNING,
	MSG_ERROR
} msg_kind;

typedef void (*online_fn)(void *, position);
typedef const char *(*onstr_fn)(void *, printing_pos, const char *, usize);
typedef const char *(*onidentifier_fn)(void *, printing_pos, const char *, usize);
typedef void (*onmsg_fn)(void *, printing_pos, msg_kind, const char *);

typedef struct source_base source_base;

typedef struct source
{
	source_base *srcbase;
	const char *str;
	usize len;
	position start;
	const char *name;
	const char *path;
	u32 *lines;
} source;

struct source_base
{
	source **srcs;
	position end;
	strmap identifiers;

	char const *regs64[NUM_REGISTERS];
	char const *regs32[NUM_REGISTERS];
	char const *regs16[NUM_REGISTERS];
	char const *regs8[NUM_REGISTERS];
	char const *ops[NUM_OPCODES];
	char const *keywords[NUM_KEYWORDS];
	char const *directives[NUM_DIRECTIVES];
};

typedef struct position_info
{
	source *src;
	usize line;
	usize col;
} position_info;

void srcbase_init(source_base *self);

bool srcbase_is_register(source_base *self, const char *str);

const char *srcbase_ops_start(source_base *self);
const char *srcbase_ops_end(source_base *self);
const char *srcbase_keywords_start(source_base *self);
const char *srcbase_keywords_end(source_base *self);
const char *srcbase_directives_start(source_base *self);
const char *srcbase_directives_end(source_base *self);

registers srcbase_get_register(source_base *self, token tok);
opcode srcbase_get_opcode(source_base *self, token tok);
keywords srcbase_get_keyword(source_base *self, token tok);
directive_kind srcbase_get_directive(source_base *self, token tok);

source *srcbase_new_str(source_base *self, const char *name, const char *str);
const char *srcbase_intern(source_base *self, const char *str, usize len);
void srcbase_add_line(source_base *self, source *src, position pos);
source *srcbase_src_at_pos(source_base *self, position pos);
position_info srcbase_info_at_pos(source_base *self, position pos);

printing_pos ppos_pos(position pos);
printing_pos ppos_range(pos_range range);
printing_pos ppos_pos_range(position pos, pos_range range);
printing_pos ppos_range2(pos_range r1, pos_range r2);
printing_pos ppos_range3(pos_range r1, pos_range r2, pos_range r3);
position pp_start_pos(printing_pos pp);
position pp_end_pos(printing_pos pp);
void print_pos(source_base *srcbase, printing_pos pp);

#endif // !XIA_SRCBASE_H
