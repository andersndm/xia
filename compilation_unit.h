#ifndef XIA_COMPILATION_UNIT_H
#define XIA_COMPILATION_UNIT_H

#include "common/types.h"
#include "elf64/header_info.h"

#include "tokens.h"
#include "directives.h"
#include "x64/instruction.h"
#include "srcbase.h"

typedef struct label
{
	const char *name;
	u64 section_relative_offset;
	bool defined;
	bool global;
	pos_range range;
	u32 symbol_index;
} label;

// #todo better name for line, command? input? ...
typedef enum statement_kind
{
	STMT_NONE,
	STMT_LABEL,
	STMT_INSTRUCTION,
	STMT_DIRECTIVE
} stmt_kind;

typedef struct statement
{
	stmt_kind kind;
	union
	{
		// #todo the label can just be a string, since they are being saved in the section
		label lab;
		directives dir;
		instruction instr;
	};
} statement;

typedef enum relocation_kind
{
	RELOCATION_NONE,
	RELOCATION_CALL,
	RELOCATION_DATA
} relocation_kind;

typedef struct relocation
{
	relocation_kind kind;
	union
	{
		struct
		{
			label *lab;
			u64 offset;
		} call;
		struct
		{
			label *lab;
			u64 offset;
		} data;
	};
} relocation;

typedef struct section
{
	section_type type;
	u64 flags;

	position start;
	pos_range name_range;
	const char *name;
	directives dir;
	label *labels;
	u64 code_len;
	u64 max_code_len;
	u8 *code;
	pos_range range;
	statement *stmts;
	source_base *srcbase;
	u64 errors;
	u8 max_align;

	relocation *relocations;
} section;

typedef struct compilation_unit
{
	// #todo add file name, and other data
	section *sections;
} compilation_unit;

label *section_get_label(section *self, const char *name);
void section_replace_labels(section *sec, section *sections);
void compilation_unit_output_obj(compilation_unit unit, const char *out_path);

#endif // !XIA_COMPILATION_UNIT_H
